<?php
namespace core\Request;

class CliRequest
    extends AbstractRequest
{
    
    public function __construct()
    {
        /* Cli.php METHOD /bundle/controller/action/param1/param2 arg1=val1 arg2=val2 */
        $this->mode = 'cli';
        
        $this->script = \laabs\basename(reset($_SERVER['argv']));

        $this->getAuthentication();
        
        $this->method = next($_SERVER['argv']);
        
        $cmd = next($_SERVER['argv']);
        if ($cmd && $cmd[0] == LAABS_URI_SEPARATOR) {
            $cmd = substr($cmd, 1);
        }

        $this->uri = $cmd;

        $this->queryType = "arg";
        
        $this->contentType = "url";
        
        while ($arg = next($_SERVER['argv'])) {
            if (preg_match("#^(?<type>\w+):\\/\\/(?<body>.*)$#", $arg, $matches)) {
                switch ($matches['type']) {
                    case 'data':
                        $this->body = $matches['body'];
                        break;

                    case 'file':
                    case 'href':
                        $this->body = file_get_contents($matches['body']);
                        break;

                    case 'url':
                        $this->contentType = 'url';
                        $this->body = $matches['body'];
                        break;
                }
            } elseif ($arg[0] == '-') {
                $arg = substr($arg, 1);
                $sep = strpos($arg, ":");
                $name = substr($arg, 0, $sep);
                $value = substr($arg, $sep+1);
                switch($name) {
                    case 'token':
                        $this->parseTokens($value);
                        break;

                }

            } else {
                $argname = strtok($arg, LAABS_CLI_ARG_OPERATOR);
                $argvalue = strtok(LAABS_CLI_ARG_OPERATOR);
                if (!isset($this->query[$argname])) {
                    $this->query[$argname] = $argvalue;
                } else {
                    $this->query[$argname] = array($this->query[$argname], $argValue);
                }
            }
        }

    }

    protected function parseTokens($tokenString)
    {
        foreach (explode(';', $tokenString) as $tokenPair) {
            list($name, $value) = explode('=', trim($tokenPair));

            $key = \laabs::getCryptKey();

            $binToken = base64_decode($value);
            $jsonToken = \laabs::decrypt($binToken, $key);
            $token = \json_decode(trim($jsonToken));

            $GLOBALS['TOKEN'][$name] = $token;
        }
    }
}