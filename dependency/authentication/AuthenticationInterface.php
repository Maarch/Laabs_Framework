<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authentication.
 *
 * Dependency authentication is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authentication is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authentication.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authentication;
/**
 * Interface for authentication of a user
 *
 * @package Dependency\Authentication
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 */
Interface AuthenticationInterface
{
    /**
     * Authenticate a user with a userName and a password.
     * Check user existence, verify password, user is enabled and not locked
     * Load the authenticated user object in session/dependency/authentication/credential 
     * @param string $userName The user name
     * @param string $password The user password
     * 
     * @return string A connection token 
     */
    public function logIn($userName, $password);

    /**
     * End the session
     * @todo unstore the token with date and validity
     * 
     * @return bool 
     */
    public function logOut();

    /**
     * Get the authentication credential
     * 
     * @return object 
     */
    public function credential();

}