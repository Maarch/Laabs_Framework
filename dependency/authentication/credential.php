<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authentication.
 *
 * Dependency authentication is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authentication is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authentication.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authentication;

/**
 * Class for the authenticated user and authentication info
 */
class credential
{
    use \core\ReadonlyTrait;

    public $user;

    public $app;

    public $timestamp;

    public function __construct($user)
    {
        $this->user = $user;

        $this->app = \laabs::getApp();

        $this->timestamp = time();
    }

}