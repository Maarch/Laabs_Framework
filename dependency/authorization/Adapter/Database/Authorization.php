<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authorization.
 *
 * Dependency authorization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authorization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authorization.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authorization\Adapter\Database;
/**
 * Class for authentication of a user
 *
 * @package Dependency\Authentication
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 */
class Authorization
    implements \dependency\authorization\AuthorizationInterface
{
    
    /* Properties */
    protected $ds;

    protected $group = array(
        'schema' => 'authorization',
        'table'  => 'group',
        'groupid' => 'groupId',
        );

    protected $groupMember = array(
        'schema' => 'authorization',
        'table'  => 'groupMember',
        'userid' => 'userId',
        'groupid' => 'groupId',
        );

    protected $privilege = array(
        'schema' => 'authorization',
        'table'  => 'privilege',
        'groupid' => 'groupId',
        'route' => 'route',
        );

    protected $accessRule = array(
        'schema' => 'authorization',
        'table'  => 'accessRule',
        'groupid' => 'groupId',
        'classname' => 'className',
        'context' => 'context',
        );



    
    /* Methods */
    /**
     * Constructor
     * @param \dependency\datasource\DatasourceInterface $ds      The Data Access Service
     * @param array                                      $options The authentication options
     *
     * @return void
     */
    public function __construct(\dependency\datasource\DatasourceInterface $ds, array $options=null)
    {
        $this->ds = $ds;
        
        if ($options) {
            foreach ($options as $name => $value) {
                if (property_exists(__CLASS__, $name)) {
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * Check user identifier is given
     * @return bool
     */
    public function isUserAuthenticated()
    {
        return (bool) \laabs::getToken("AUTH");
    }
    
    /**
     * Get a user list of privileges
     *
     * @return array The list of authorized actions controllers
     */
    public function getUserPrivileges() 
    {
        if (!(bool) \laabs::getToken("AUTH")) {
            throw new \Exception("Authorization check failure: no user identification given");
        }

        //if (!isset($_SESSION['dependency']['Authorization']['privileges'])) {

            /* Check user existence */
            $userGroups = $this->getUserGroups();
            $userPrivileges = array();
            foreach ($userGroups as $userGroup) {
                $userPrivileges = array_merge($userPrivileges, $this->getGroupPrivileges($userGroup));
            }

            $_SESSION['dependency']['authorization']['privileges'] = array_unique($userPrivileges);
        //}

        return $userPrivileges;
    }

    /**
     * Get a user list of access rules
     * @param string $className The class name for objects
     *
     * @return array The list of rules
     */
    public function getUserAccessRule($className) 
    {
        if (!(bool) \laabs::getToken("AUTH")) {
            throw new \Exception("Authorization check failure: no user identification given");
        }

        $userGroups = $this->getUserGroups();
        $userAccessControlRules = array();
        foreach ($userGroups as $userGroupId) {
            $userAccessControlRules = array_merge($userAccessControlRules, $this->getGroupRules($userGroupId, $className));
        }

        if (count($userAccessControlRules) > 0) {
            return "(" . \laabs\implode(") OR (", $userAccessControlRules) . ")";
        }

        return "false";
        
    }


    /**
     * Checks if the user has access to the action
     * @param string $routeName The requested route to check privilege for
     *
     * @return bool
     */
    public function hasUserPrivilege($routeName) 
    {
        if (!(bool) \laabs::getToken("AUTH")) {
            throw new \Exception("Authorization check failure: no user identification given");
        }
 
        $userPrivileges = $this->getUserPrivileges();

        $bundle = strtok($routeName, LAABS_URI_SEPARATOR);
        $interface = strtok(LAABS_URI_SEPARATOR);
        $method = strtok(LAABS_URI_SEPARATOR);

        foreach ($userPrivileges as $privilege) {
            /*if (fnmatch($privilege, '')
                || fnmatch($privilege, $bundle)
                || fnmatch($privilege, $bundle . LAABS_URI_SEPARATOR . $interface)
                || fnmatch($privilege, $routeName)
            ) {*/
            if (fnmatch($privilege, $routeName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the list of the user acess control groups
     *
     * @return array 
     */
    public function getUserGroups() 
    {
        $queryString = 'SELECT * FROM "' . $this->groupMember['schema'] . '"."' . $this->groupMember['table'] . '"'
            . ' WHERE "' . $this->groupMember['userid'] . '" = ?';

        $stmt = $this->ds->prepare($queryString);

        $stmt->execute(array(\laabs::getToken("AUTH")->user));
        
        $groupIds = array();

        foreach ($stmt->fetchAll() as $groupMember) {
            $groupIds[] = $groupMember->{$this->groupMember['groupid']};
        }

        return $groupIds;
    }

    /**
     * Get a given group privileges
     * @param string $groupId The group id
     *
     * @return array
     */
    public function getGroupPrivileges($groupId) 
    {
        $queryString = 'SELECT * FROM "' . $this->privilege['schema'] . '"."' . $this->privilege['table'] . '"'
            . ' WHERE "' . $this->privilege['groupid'] . '" = ?';

        $stmt = $this->ds->prepare($queryString);

        $stmt->execute(array($groupId));

        $privileges = $stmt->fetchAll();
        $groupPrivileges = array();
        foreach ($privileges as $privilege) {
            $groupPrivileges[] = $privilege->{$this->privilege['route']};
        }

        return $groupPrivileges;
    }

    /**
     * Get a given group list of rules
     * @param string $groupId   The group id
     * @param string $className The name of an object class to check access to 
     *
     * @return array
     */
    public function getGroupRules($groupId, $className) 
    {
        $queryString = 'SELECT * FROM "' . $this->accessRule['schema'] . '"."' . $this->accessRule['table'] . '"'
            . ' WHERE "' . $this->accessRule['groupid'] . '" = ?';

        $stmt = $this->ds->prepare($queryString);

        $stmt->execute(array($groupId));

        $bundle = strtok($className, LAABS_URI_SEPARATOR);
        $type = strtok(LAABS_URI_SEPARATOR);

        $groupRules = array();
        foreach ($stmt->fetchAll() as $rule) {
            $ruleClassName = $rule->{$this->accessRule['classname']};

            if (fnmatch($ruleClassName, '')
                || fnmatch($ruleClassName, $bundle)
                || fnmatch($ruleClassName, $className)
            ) {
                $groupRule = $rule->{$this->accessRule['context']};
                if (!$groupRule) {
                    $groupRule = "true";
                }
                    $groupRules[] = $groupRule;
            }
            
        }
        
        return $groupRules;
    }

    
}