<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency datasource.
 *
 * Dependency datasource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency datasource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency datasource.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\datasource\Adapter\Database;

class Statement
    implements \dependency\datasource\StatementInterface,
               \dependency\datasource\ResultSetInterface,
               \IteratorAggregate
{
    /* Constants */
    
    /* Properties */    
    protected $pdoStatement;
    
    /* Methods */
    /**
     * Constructor
     * @param \PDOStatement $pdoStatement The PDO Statement object
     */
    public function __construct(\PDOStatement $pdoStatement)
    {
        $this->pdoStatement = $pdoStatement;
    }
    
    /* Statement methods */
    /**
     * Bind a parameter
     * @param string  $name
     * @param mixed   &$variable
     * @param string  $type
     * @param integer $length
     * @param array   $driver_options
     * @param string  $ref
     * 
     * @return bool
     */
    public function bindParam($name, &$variable, $type=\PDO::PARAM_STR, $length=null, $driver_options=array(), $ref=false)
    {
        $Param = new Param($name, $variable, $type, $length, $ref);
        $this->params[$name] = $Param;
        
        return $this->pdoStatement->bindParam(':' . $name, $variable, $type, $length);
    }
    
    public function bindValue($name, $value, $type=\PDO::PARAM_STR)
    {
        $this->params[$name] = $value;
        
        return $this->pdoStatement->bindValue(':' . $name, $value, $type);
    }
    
    public function getError()
    {
        $errInfo = $this->pdoStatement->errorInfo();
        
        return new \core\Error($errInfo[2], null, $errInfo[0], null, null, $errInfo);
    }
    
    public function execute($inputParameters=null)
    {
        if ($inputParameters && \laabs\is_assoc($inputParameters)) {
            foreach ($inputParameters as $name => $value) {
                $inputParameters[":" . $name] = str_replace("*", "%", $value);
                unset($inputParameters[$name]);
            }
        }
        
        return $this->pdoStatement->execute($inputParameters);
    }
    
    public function getParams()
    {
        return $this->params;
    }
    
    public function debugDumpParams()
    {
        return $this->pdoStatement->debugDumpParams();
    }

        
    /* ResultSet methods */
    public function fetch($class="\stdClass", array $ctor_args=array())
    {
        $object = $this->pdoStatement->fetchObject($class, $ctor_args);

        if ($object) {
            foreach ($object as $name => $value) {
                if (gettype($value) == 'resource') {
                    $object->$name = stream_get_contents($value);
                }
            }
        }
        
        return $object;
    }
    
    public function fetchAll($class="\stdClass", array $ctor_args=array())
    {
        $resultSet = array();
        // Fix abnormal behavious of PDOStatement::fetchAll
        // Only ONE pointer used for LOBs so resources will all point to the last LOB retrieved
        while ($object = $this->pdoStatement->fetchObject($class, $ctor_args)) {
            foreach ($object as $name => $value) {
                if (gettype($value) == 'resource') {
                    $object->$name = stream_get_contents($value);
                }
            }
            $resultSet[] = $object;
        }

        return $resultSet;
    }
    
    public function fetchItem($cursor_offset=0, $class="\stdClass")
    {
        $this->setFetchMode(\PDO::FETCH_CLASS, $class);
        
        return $this->pdoStatement->fetch(\PDO::FETCH_CLASS, \PDO::FETCH_ORI_ABS, $cursor_offset);
    }
    
    public function fetchColumn($offset=0)
    {
        return $this->pdoStatement->fetchColumn($offset);
    }
    
    public function rowCount()
    {
        return $this->pdoStatement->rowCount();
    }
    
    public function getQueryString()
    {
        return $this->pdoStatement->queryString;
    }
    
    public function getIterator()
    {
        $DataSet = $this->fetchAll();
        
        return new ArrayIterator($DataSet);
    }
}