<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiException.php';

require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiInfo.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiFile.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiFormat.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiSoftware.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiStatus.php';

require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiMedia.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiVideo.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiAudio.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiText.php';

require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiMetadata.php';

require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiMediaInfoTrait.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiDroidTrait.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiJhoveTrait.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiFddTrait.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DfiTikaTrait.php';

define('DFI_USE_DROID', 1);
define('DFI_USE_JHOVE', 2);
define('DFI_USE_MEDIAINFO', 4);
define('DFI_USE_TIKA', 8);

$rootdir = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..');
define('DFI_ROOT_DIR', $rootdir);
define('DFI_DATA_DIR', $rootdir . DIRECTORY_SEPARATOR . 'data');
/**
 * Main class for file format identification
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
class Dfi
{
    use DfiMediaInfoTrait, DfiDroidTrait, DfiJhoveTrait, DfiFddTrait, DfiTikaTrait;

    /**
     * The conf
     * @var array
     */
    public $conf;

    /**
     * The fileinfo utility
     * @var object
     */
    public $finfo;

    /**
     * The formats document
     * @var object
     */
    public $formats;

    /**
     * The current info
     * @var array
     */
    protected $info;


    /**
     * Constructor
     * @param string $conffile The name of a configuration file
     */
    public function __construct($conffile=false)
    {
        if (!$conffile) {
            $conffile = DFI_ROOT_DIR . DIRECTORY_SEPARATOR . 'conf' . DIRECTORY_SEPARATOR . 'default.ini';
        }
        $this->conf = parse_ini_file($conffile);

        $this->finfo = new finfo();

        $this->formats = simplexml_load_file(DFI_ROOT_DIR . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "formats.xml");
        $this->formats->registerXPathNamespace("dfi", "maarch.org:dfi:formats");       

        if (isset($this->conf['enable_droid'])) {
            $this->constructDroid();
        }

        if (isset($this->conf['enable_mediainfo'])) {
            $this->constructMediaInfo();
        }

        if (isset($this->conf['enable_jhove'])) {
            $this->constructJhove();
        }

        if (isset($this->conf['enable_tika'])) {
            $this->constructTika();
        }

        if (isset($this->conf['enable_fdd'])) {
            $this->constructFdd();
        }

    }

    /**
     * Get the info on a file with the filename
     * @param string  $filename  The filename of the resource
     * @param integer $infotypes A bitmask of utilities to use
     * 
     * @return object The info
     */
    public function getInfo($filename, $infotypes=1)
    {       
        $info = new DfiInfo();

        $info->size = filesize($filename);

        $info->file = new DfiFile();
        $info->file->extension = pathinfo($filename, PATHINFO_EXTENSION);
        $info->file->name = basename($filename);
        $info->file->dirname = dirname($filename);
       
        // Mime type
        $info->format->mimetype = $this->finfo->file($filename, FILEINFO_MIME_TYPE);     

        $this->gatherInfo($filename, $info, $infotypes);

        return $info;
    }

    /**
     *  Try to match a droid format with the contents and resource info
     * @param string  $contents  The contents of the resource
     * @param integer $infotypes A bitmask of utilities to use
     * 
     * @return array The possible formats
     */
    public function getInfoBuffer($contents, $infotypes=1)
    {
        $info = new DfiInfo();
        
        $info->size = strlen($contents);
        
        // Mime type
        $finfo = new finfo();
        if ($info->size > 65535) {
            $info->format->mimetype = $this->finfo->buffer(substr($contents, 0, 65535), FILEINFO_MIME_TYPE);
        } else {
            $info->format->mimetype = $this->finfo->buffer($contents, FILEINFO_MIME_TYPE);
        }

        $filename = tempnam(sys_get_temp_dir(), 'dfi');
        file_put_contents($filename, $contents);
        
        $this->gatherInfo($filename, $info, $infotypes);

        return $info;
    }

    protected function gatherInfo($filename, $info, $infotypes)
    {
        // Droid enabled
        if ($infotypes & DFI_USE_DROID) {
            $this->runDroid($filename, $info);
        }

        // Mediainfo enabled
        if ($infotypes & DFI_USE_MEDIAINFO) {
            $this->runMediaInfo($filename, $info);
        }

        // jhove enabled
        if ($infotypes & DFI_USE_JHOVE) {
            $this->runJhove($filename, $info);
        }

        // jhove enabled
        if ($infotypes & DFI_USE_TIKA) {
            $this->runTika($filename, $info);
        }

        $this->getFdd($info);
    }

    /**
     * Search a format
     * @param array $args An associative array of attributes names and values to search
     * 
     * @return array
     */
    public function searchFormats(array $args=array())
    {
        $predicates = array();
        foreach ($args as $name => $value) {
            if (is_null($value)) {
                $predicates[] = "not(@" . $name . ")"; 
            } elseif (empty($value)) {
                continue;
            } elseif (is_array($value)) {
                $valuePredicates = array();
                foreach ($value as $item) {
                    if (empty($item)) {
                        continue;
                    } else if ($item[0] == "*" || $item[strlen($item)-1] == "*") {
                        $valuePredicates[] = "contains(@" . $name . ", '" . str_replace("*", "", $item) . "')";
                    } else {
                        $valuePredicates[] = "@" . $name . "='" . $value . "'";
                    }
                }
                $predicates[] = implode(' or ', $valuePredicates);
            } elseif (is_scalar($value) && ($value[0] == "*" || $value[strlen($value)-1] == "*")) {
                $predicates[] = "contains(@" . $name . ", '" . str_replace("*", "", $value) . "')";
            } else {
                $predicates[] = "@" . $name . "='" . $value . "'";
            }
        }

        return $this->formats->xpath("//dfi:format[" . implode(' and ', $predicates) . "]");
    }


}