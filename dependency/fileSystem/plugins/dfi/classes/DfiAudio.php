<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The Dfi video part info
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
class DfiAudio
    extends DfiMedia
{

    /**
     * The language
     * @var string
     */
    public $language;
    
    /**
     * The number of channels
     * @var integer
     */
    public $channels;

    /**
     * The sample rate in KHz
     * @var integer
     */
    public $samplerate;

    /**
     * The compression mode, lossy, lossless
     * @var string
     */
    public $compressionMode;

}