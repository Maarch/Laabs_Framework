<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Trait for Droid
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
trait DfiDroidTrait
{
    /**
     * The Droid utility
     * @var object
     */
    public $droid;

    /**
     * Constructor
     */
    protected function constructDroid()
    {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'Droid/Main.php';

        if (isset($this->conf['droid.signature_file'])) {
            $droidSignatureFile = $conf['droid.signature_file'];
        } else {
            $droidSignatureFile = false;
        }

        if (isset($this->conf['droid.container_signature_file'])) {
            $droidContainerSignatureFile = $conf['droid.container_signature_file'];
        } else {
            $droidContainerSignatureFile = false;
        }

        if (isset($this->conf['compression.extract_cmd'])) {
            $uncompressCmdFormat = $conf['compression.extract_cmd'];
        } else {
            $uncompressCmdFormat = false;
        }

        $this->droid = new Droid($droidSignatureFile, $droidContainerSignatureFile, $uncompressCmdFormat);
    }

    protected function runDroid($filename, $info)
    {
        $format = $this->droid->match($filename);

        if ($format) {
            $info->format->puid = $format->puid;

            $info->format->name = $format->name;
            $info->format->version = $format->version;

            if ($format->containerType) {
                switch ($format->containerType) {
                    case 'ZIP':
                        if ($info->format->mimetype == 'application/zip') {
                            $info->format->mimetype = reset($format->mimetypes);
                        }
                        // Continue

                    case 'OLE2':
                        
                }
            }

        }
    }

}