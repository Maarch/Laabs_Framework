<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Trait for Library Of Congress Format Description Documents
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
trait DfiFddTrait
{
    /**
     * The Fdd utility
     * @var object
     */
    public $fdd;

    /**
     * Constructor
     */
    protected function constructFdd()
    {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'Fdd/Main.php';

        if (isset($this->conf['fdd.directory'])) {
            $fddDirectory = $conf['fdd.directory'];
        } else {
            $fddDirectory = false;
        }

        $this->fdd = new Fdd($fddDirectory);
    }

    protected function getFdd($info)
    {
        $fdds = array();

        $asserts = array();

        // Get fdd from puid
        if (isset($info->format->puid)) {
            $asserts['puid'] = $info->format->puid;
        } elseif (isset($info->format->name)) {
            $asserts['name'] = $info->format->name;
            
            if (isset($info->format->version)) {
                $asserts['version'] = $info->format->version;
            } else {
                $asserts['version'] = null;
            }
        } else {
            return;
        }

        $formats = $this->searchFormats($asserts);
        
        if (count($formats) > 1 || count($formats) == 0) {
            return;
        }
        
        $format = reset($formats);
        
        if (!isset($info->format->puid) && isset($format->attributes()->puid)) {
            $info->format->puid = (string) $format->attributes()->puid;
        }

        if (!isset($info->format->name)) {
            $info->format->name = (string) $format->attributes()->name;
        }

        if (!isset($info->format->version) && isset($format->attributes()->version)) {
            $info->format->version = (string) $format->attributes()->version;
        }

        if (!isset($info->format->version) && isset($format->attributes()->version)) {
            $info->format->version = (string) $format->attributes()->version;
        }

        if (!isset($format->attributes()->fddid)) {
            return;
        }

        $fddid = (string) $format->attributes()->fddid;
        $info->format->fddid = $fddid;

        $fdd = simplexml_load_string($this->fdd->get($fddid));
        $fdd->registerXPathNamespace("fdd", "http://www.digitalpreservation.gov/formats/schemas/fdd/v1");

        $genres = $fdd->xpath('.//fdd:gdfrGenre');
        if (count($genres)) {
            $info->format->category = (string) $genres[0];
        }

    }


    protected function applyPrecedence($fdds)
    {
        foreach ($fdds as $fddid => $fdd) {
            $fdd->registerXPathNamespace("fdd", "http://www.digitalpreservation.gov/formats/schemas/fdd/v1");
            
            $subtypeRelatedIds = $fdd->xpath('.//fdd:relationship[./fdd:typeOfRelationship/text()="Subtype of"]/fdd:relatedTo/fdd:id');
            
            foreach ($subtypeRelatedIds as $subtypeRelatedId) {
                if (isset($fdds[(string) $subtypeRelatedId])) {
                    unset($fdds[(string) $subtypeRelatedId]);
                }
            }
        }

        return $fdds;
    }
        
}