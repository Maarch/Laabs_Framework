<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The Dfi file info
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
class DfiFormat
{
    /**
     * The category : text, image, video, audio, geospatial, dataset
     * @var string
     */
    public $category;

    /**
     * The name
     * @var string
     */
    public $name;

    /**
     * The version
     * @var string
     */
    public $version;

    /**
     * The profile
     * @var array
     */
    public $profile = array();

    /**
     * The mime type
     * @var string
     */
    public $mimetype;

    /**
     * The PRONOM unique identifier (UK National Archives)
     * @var string
     */
    public $puid;

    /**
     * The Format Description Document Identifier (Library of Congress)
     * @var string
     */
    public $fddid;

    /**
     * The info
     * @var string
     */
    public $info;

}