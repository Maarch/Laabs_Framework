<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The Dfi info
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
class DfiInfo
{
    /**
     * The format
     * @var DfiFormat
     */
    public $format;

    /**
     * The software that has generated the content
     * @var DfiSoftware
     */
    public $software;

    /**
     * The size
     * @var integer
     */
    public $size;

    /**
     * The creation date
     * @var string
     */
    public $created;

    /**
     * The status code
     * @var DfiStatus
     */
    public $status;

    /**
     * The file if source is a file
     * @var DfiFile
     */
    public $file;

    /**
     * The document metadata
     * @var array
     */
    public $metadata;

    /**
     * The contents (for specific media OR multimedia / multipart)
     * @var array
     */
    public $contents;  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->format = new DfiFormat();
    }
}