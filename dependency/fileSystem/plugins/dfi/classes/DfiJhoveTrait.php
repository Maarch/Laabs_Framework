<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Trait for Jhove
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
trait DfiJhoveTrait
{
    /**
     * The Jhove utility
     * @var object
     */
    public $jhove;

    /**
     * Constructor
     */
    protected function constructJhove()
    {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'Jhove/Main.php';

        if (isset($this->conf['jhove.classpath'])) {
            $jhoveClasspath = $this->conf['jhove.classpath'];
        } else {
            $jhoveClasspath = false;
        }

        if (isset($this->conf['jhove.conffile'])) {
            $jhoveConffile = $this->conf['jhove.conffile'];
        } else {
            $jhoveConffile = false;
        }

        $this->jhove = new Jhove($jhoveClasspath, $jhoveConffile);
    }

    protected function runJhove($filename, $info)
    {
        $info->status = new DfiStatus();

        $jhoveXml = $this->jhove->getInfo($filename);

        $jhoveObj = simplexml_load_string($jhoveXml);

        $this->info['jhove'] = $jhoveObj;
        //var_dump($jhoveObj);
        $info->status->message = (string) $jhoveObj->repInfo->status;

        $info->status->module = str_replace('-hul', '', (string) $jhoveObj->repInfo->reportingModule);
        switch($info->status->message) {
            case 'Well-Formed and valid':
                $info->status->code = 0;
                break;

            case 'Well-Formed':
            case 'Well-Formed, but not valid':
                $info->status->code = 1;
                $info->status->info = utf8_decode((string) $jhoveObj->repInfo->messages->message);
                break;

            default:
                $info->status->code = 2;
                $info->status->info = (string) $jhoveObj->repInfo->messages->message;
        }

        if (!isset($info->format->name)) {
            $info->format->name = (string) $jhoveObj->repInfo->format;
        }

        if (!isset($info->format->version)) {
            $info->format->version = (string) $jhoveObj->repInfo->version;
        }

        if (!isset($info->format->mimetype) && isset($jhoveObj->repInfo->mimeType)) {
            $info->format->mimetype = (string) $jhoveObj->repInfo->mimeType;
        } 

        if (isset($jhoveObj->repInfo->profiles)) {
            foreach ($jhoveObj->repInfo->profiles[0]->profile as $jhoveProfile) {
                $info->format->profile[] = (string) $jhoveProfile;
            }
        }
        
        if (isset($jhoveObj->repInfo->properties)) {
            $this->extractMetadata($jhoveObj->repInfo->properties, $info);
        }
    }

    protected function extractMetadata($properties, $info)
    {
        foreach ($properties->property as $property) {
            switch ($property->name) {
                case "PDFMetadata":
                    $this->extractPdfMetadata($property, $info);
                    break;

                case "XMLMetadata":
                    $this->extractXmlMetadata($property, $info);
                    break;
            }
        } 
    }

    protected function extractPdfMetadata($PDFMetadata, $info)
    {
        $PDFMetadata->registerXPathNamespace("j", "http://hul.harvard.edu/ois/xml/ns/jhove");

        $author = $PDFMetadata->xpath('.//j:property[j:name="Author"]/j:values/j:value');
        if (count($author)) {
            $info->metadata[] = new DfiMetadata("author", 'string', 1, (string) $author[0]);
        }

        $created = $PDFMetadata->xpath('.//j:property[j:name="CreationDate"]/j:values/j:value');
        if (count($created)) {
            $info->metadata[] = new DfiMetadata("creationDate", 'date', 1, (string) $created[0]);
        }

        $creator = $PDFMetadata->xpath('.//j:property[j:name="Creator"]/j:values/j:value');
        if (count($creator)) {
            if (!isset($info->software)) {
                $info->software = new DfiSoftware();
            } 
            $info->software->library = (string) $creator[0];
        }

        $keywords = $PDFMetadata->xpath('.//j:property[j:name="Keywords"]/j:values/j:value');
        if (count($keywords)) {
            $value = array();
            foreach ($keywords as $keyword) {
                $value[] = (string) $keyword;
            }
            $info->metadata[] = new DfiMetadata("keywords", 'string', "*", $value);
        }

        $modified = $PDFMetadata->xpath('.//j:property[j:name="ModifyDate"]/j:values/j:value');
        if (count($modified)) {
            $info->metadata[] = new DfiMetadata("creationDate", 'date', 1, (string) $modified[0]);
        }

        $producer = $PDFMetadata->xpath('.//j:property[j:name="Producer"]/j:values/j:value');
        if (count($producer)) {
            if (!isset($info->software)) {
                $info->software = new DfiSoftware();
            } 
            $info->software->name = (string) $producer[0];
        }

        $subject = $PDFMetadata->xpath('.//j:property[j:name="Subject"]/j:values/j:value');
        if (count($subject)) {
            $info->metadata[] = new DfiMetadata("subject", 'string', 1, (string) $subject[0]);
        }

        $title = $PDFMetadata->xpath('.//j:property[j:name="Title"]/j:values/j:value');
        if (count($title)) {
            $info->metadata[] = new DfiMetadata("title", 'string', 1, (string) $title[0]);
        }
    }

    protected function extractXmlMetadata($XMLMetadata, $info)
    {
        $XMLMetadata->registerXPathNamespace("j", "http://hul.harvard.edu/ois/xml/ns/jhove");

        if (!isset($info->contents)) {
            $info->contents = array();
            $text = new DfiText();
            $info->contents[] = $text;
        } else {
            foreach ($info->contents as $content) {
                if ($content instanceof DfiText) {
                    $text = $content;
                    break;
                }
            }
            $text = new DfiText();
            $info->contents[] = $text;
        }

        $encoding = $XMLMetadata->xpath('.//j:property[j:name="Encoding"]/j:values/j:value');
        if (count($encoding)) {
            $text->charset = (string) $encoding[0];
        }

        $schemas = $XMLMetadata->xpath('.//j:property[j:name="Schema"]/j:values/j:value');
        if (count($schemas)) {
            $value = array();
            foreach ($schemas as $schema) {
                $value[] = (string) $schema;
            }
            $info->metadata[] = new DfiMetadata("schema", 'string', "*", $value);
        }
    }

    protected function parseProperties($jhoveProperties) 
    {
        $jhoveProperties->registerXPathNamespace("jhove", "http://hul.harvard.edu/ois/xml/ns/jhove");
        $properties = array();
        foreach ($jhoveProperties->xpath("./jhove:property") as $property) {
    
            $properties[] = $this->parseProperty($property);

        }

        return $properties;
    }

    protected function parseProperty($property) 
    {
        $name = (string) $property->name;
        $type = (string) $property->values->attributes()->type;
        $arity = "n";
        switch ($property->values->attributes()->arity) {
            case 'Scalar':
                $values = (string) $property->values->value;
                $arity = "1";
                break;

            case 'List': // Java list, allows duplicates and null
            case 'Set' : // Java set, no duplicates
                switch($type) {
                    case 'Property':
                        $values = $this->parseProperties($property->values);
                        $type = 'metadata';
                        break;

                    default:
                        $values = array();
                        foreach ($property->values->value as $value) {
                            $values[] = (string) $value;
                        }
                }
                break;

            case 'Map' : // Java map, unique names
                switch($type) {
                    case 'Property':
                        $values = new StdClass();
                        foreach ($this->parseProperties($property->values) as $item) {
                            $values->{$item->name} = $item;
                        }
                        $type = 'object';

                        break;

                    default:
                        $values = array();
                        foreach ($property->values->value as $value) {
                            $values[] = (string) $value;
                        }
                }
                break;
            case 'Array':
                $values = array();
                foreach ($property->values->value as $value) {
                    $values[] = (string) $value;
                }
                break;
        }

        return new DfiMetadata($name, strtolower($type), $arity, $values);
    }
}