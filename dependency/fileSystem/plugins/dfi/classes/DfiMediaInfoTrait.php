<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Dfi trait for mediainfo
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
trait DfiMediaInfoTrait
{
    /**
     * The MediaInfo utility
     * @var object
     */
    public $mediainfo;

    protected function constructMediaInfo()
    {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'MediaInfo/Main.php';

        if (isset($this->conf['mediainfo.cmd'])) {
            $mediaInfoCmd = $this->conf['mediainfo.cmd'];
        } else {
            $mediaInfoCmd = false;
        }

        $this->mediainfo = new MediaInfo($mediaInfoCmd);
    }

    /**
     * Get the info abot a file
     * @param string  $filename
     * @param DfiInfo $info
     */
    public function runMediaInfo($filename, $info)
    {
        $mediaInfoXml = $this->mediainfo->mediainfo($filename);
        $mediaInfoObj = simplexml_load_string($mediaInfoXml);

        $this->info['mediainfo'] = $mediaInfoObj;
        var_dump($mediaInfoObj);
        foreach ($mediaInfoObj->File->track as $track) {
            switch ((string) $track->attributes()->type) {
                case 'General':
                    $this->getGeneralInfo($track, $info);
                    break;

                case 'Video':
                    $info->contents[] = $this->getVideoInfo($track);
                    break;

                case 'Audio':
                    $info->contents[] = $this->getAudioInfo($track);
                    break;

                case 'Text':
                    $info->contents[] = $this->getTextInfo($track);
                    break;
            }
        }
    }

    protected function getGeneralInfo($track, $info)
    {
        if (!isset($info->format->name) && isset($track->Format)) {
            $info->format->name = (string) $track->Format;
        }

        if (!isset($info->format->version) && isset($track->Format_version)) {
            $info->format->version = (string) $track->Format_version;
        }

        if (isset($track->Format_profile)) {
            $info->format->profile[] = (string) $track->Format_profile;
        }

        if (isset($track->Writing_application)) {
            if (!isset($info->software)) {
                $info->software = new DfiSoftware();
            }
            $info->software->name = (string) $track->Writing_application;
        }

        if (isset($track->Writing_library)) {
            if (!isset($info->software)) {
                $info->software = new DfiSoftware();
            }
            $info->software->library = (string) $track->Writing_library;
        }

        if (!isset($info->language) && isset($track->Language)) {
            $info->language = (string) $track->Language;
        }

        if (isset($track->Encoded_date)) {
            $info->created = (string) $track->Encoded_date;
        }

        foreach ($track as $name => $value) {
            switch ($name) {
                case 'Title':
                    $metadata = new DfiMetadata('Title', 'string', 1, (string) $value);
                    $info->metadata[] = $metadata;
                    break;
            }
            
        }
    }

    protected function getTrackInfo($track, $part)
    {
        if (isset($track->Duration)) {
            $timeparts = explode(' ', (string) $track->Duration);
            foreach ($timeparts as $timepart) {
                preg_match('#(\d+)(\w+)#', $timepart, $match);
                switch (strtolower($match[2])) {
                    case 'ms':
                        $durationParts['S'] .= "." . $match[1];
                        break;

                    case 'mn':
                        $durationParts['M'] = $match[1];
                        break;

                    default:
                        $durationParts[strtoupper($match[2])] = $match[1];
                }
            }
            $part->duration = "PT";
            foreach ($durationParts as $unit => $value) {
                $part->duration .= $value.$unit;
            }
        }

        if (isset($track->Stream_size)) {
            $part->size = (float) strtok($track->Stream_size, ' ');
            $unit = strtok(' ');
            switch ($unit) {
                case 'MiB':
                    $part->size = (int) ($part->size * 1048576);
                    break;

                case 'KiB':
                    $part->size = (int) ($part->size * 1024);
                    break;

                case "B":
                default:
                    $part->size = (int) $part->size;
            }
        }

        if (isset($track->Codec_ID)) {
            $part->codec = (string) $track->Codec_ID;
        }

        if (isset($track->Bit_rate)) {
            $part->bitrate = intval(str_replace(' ', '', (string) $track->Bit_rate));
        }
    }

    protected function getVideoInfo($track)
    {
        $video = new DfiVideo();
        $video->format->category = 'video';

        $this->getGeneralInfo($track, $video);
        $this->getTrackInfo($track, $video);

        if (isset($track->Width)) {
            $video->width = intval(str_replace(' ', '', (string) $track->Width));
        }

        if (isset($track->Height)) {
            $video->height = intval(str_replace(' ', '', (string) $track->Height));
        }

        if (isset($track->Nominal_bit_rate)) {
            $video->bitrate = intval(str_replace(' ', '', (string) $track->Nominal_bit_rate));
        }

        if (isset($track->Frame_rate)) {
            $video->framerate = floatval((string) $track->Frame_rate);
        }

        return $video;
    }

    protected function getAudioInfo($track)
    {
        $audio = new DfiAudio();
        $audio->format->category = 'audio';

        $this->getGeneralInfo($track, $audio);
        $this->getTrackInfo($track, $audio);

        if (isset($track->Channel_s_)) {
            $audio->channels = (int) $track->Channel_s_;
        }

        if (isset($track->Sampling_rate)) {
            $audio->samplerate = floatval((string) $track->Sampling_rate);
        }

        if (isset($track->Compression_mode)) {
            $audio->compressionMode = strtolower((string) $track->Compression_mode);
        }

        return $audio;
    }

    protected function getTextInfo($track)
    {
        $text = new DfiText();
        $text->format->category = 'text';

        $this->getGeneralInfo($track, $text);
        $this->getTrackInfo($track, $text);

        return $text;
    }
    

}