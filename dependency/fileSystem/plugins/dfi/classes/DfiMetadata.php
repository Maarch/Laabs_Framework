
<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Class for Dfi metadata
 *
 * @package Dfi
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class DfiMetadata
{

    public $name;

    public $type;

    public $arity;

    public $value;

    /**
     * Construct environment
     * @param string $name  The name
     * @param string $type  The type
     * @param string $arity The arity
     * @param mixed  $value The value 
     *
     * @return void
     * @author 
     */
    public function __construct($name, $type, $arity, $value)
    {
        $this->name = $name;

        $this->type = $type;

        $this->arity = $arity;

        $this->value = $value;
    }

} // END class jhove 
