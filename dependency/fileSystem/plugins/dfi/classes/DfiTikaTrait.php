<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Trait for Tika
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
trait DfiTikaTrait
{
    /**
     * The tika utility
     * @var object
     */
    public $tika;

    /**
     * Constructor
     */
    protected function constructTika()
    {
        require_once __DIR__ . DIRECTORY_SEPARATOR . 'Tika/Main.php';

        if (isset($this->conf['tika.jarfile'])) {
            $tikaJarfile = $this->conf['tika.jarfile'];
        } else {
            $tikaJarfile = false;
        }

        $this->tika = new Tika($tikaJarfile);
    }

    /**
     * Gte tika information
     * @param string $filename
     * 
     * @return string
     */
    public function runTika($filename)
    {
        $tikaXml = $this->tika->getInfo($filename);

        $tikaObj = simplexml_load_string($tikaXml);

        $this->info['tika'] = $tikaObj;

        var_dump($tikaObj);
    }
}