<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Class for Library Of Congress Format Description Documents
 *
 * @package Dfi
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class Fdd
{

    protected $directory;

    /**
     * Construct environment
     * @param string $directory The Fdd directory
     *
     * @return void
     * @author 
     */
    public function __construct($directory=false)
    {
        if (!$directory) {
            $this->directory = DFI_DATA_DIR . DIRECTORY_SEPARATOR . 'fddXML';
        } else {
            $this->directory = $directory;
        }
    }

    /**
     * Get format validation
     * @param string $fddid The identifier
     * 
     * @return object
     */
    public function get($fddid)
    {
        $fddfile = $this->directory . DIRECTORY_SEPARATOR . $fddid . ".xml";

        if (!is_file($fddfile)) {
            return;
        }
        
        return file_get_contents($fddfile);
    }

    /**
     * Get messages
     * 
     * @return array The validation errors
     */
    public function version()
    {
        $readmefile = $this->directory . DIRECTORY_SEPARATOR . "fddXMLreadme.txt";
        $lines = file($readmefile);

        foreach ($lines as $line) {
            if (substr($line, 0, 7) == "Version") {
                return $line;
            }
        }

    }


} // END class
