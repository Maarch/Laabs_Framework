<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Class for jhove JSTOR/Harvard Object Validation Environment
 *
 * @package Dfi
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class Jhove
{

    protected $executable;

    protected $conffile;

    public $errors;

    /**
     * Construct environment
     * @param string $classpath The Jhove class directory
     * @param string $conffile  The configuration file 
     *
     * @return void
     * @author 
     */
    public function __construct($classpath, $conffile)
    {
        $this->executable = 'java -cp "' . $classpath . '" Jhove';

        $this->conffile = $conffile;
    }

    /**
     * Get format validation
     * @param mixed  $filename A file, array of filenames or directory
     * @param string $module   The module to use
     * 
     * @return boolean
     */
    public function getInfo($filename, $module=false)
    {
        $tokens = array();
        $tokens[] = $this->executable;
        $tokens[] = '-h xml';
        if ($module) {
            $tokens[] = "-m ".$module."-hul";
        }
        $filenames = (array) $filename;
        foreach ($filenames as $filename) {
            $tokens[] = '"' . $filename . '"';
        }

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;

        exec($command, $output, $return);
        //var_dump($command);
        //var_dump($output);
        //var_dump($return);

        if ($return !== 0) {
            throw new Exception("Failed to validate file format: execution error." . implode(' ', $output));
        }

        $this->errors = array();
        
        while (($line=reset($output)) && (substr($line, 0, 5) !== '<?xml')) {
            array_shift($output);
        }

        $xml = implode('', $output);
        
        return $xml;
    }

    /**
     * Get version
     * 
     * @return array The validation errors
     */
    public function version()
    {
        $tokens = array();
        $tokens[] = $this->executable;
        $tokens[] = '--version';

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;

        exec($command, $output, $return);

        return $output[0];
    }

    /**
     * Get messages
     * 
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the usable module for a given puid
     * @param string $puid The pronom format identifier
     * 
     * @return string $the name of the module
     */
    public function getModule($puid)
    {
        
        foreach ($this->modules as $name => $formats) {
            if (in_array($puid, $formats)) {
                return $name;
            }
        }
    }

} // END class jhove 
