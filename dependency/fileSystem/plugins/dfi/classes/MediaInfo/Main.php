<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The MediaInfo tool
 * 
 * @author Cyril Vazquez Maarch <cyril.vazquez@maarch.org>
 */
class MediaInfo
{

    /**
     * The command container extraction command
     * @var string
     */
    protected $mediainfoCmd;

    /**
     * Constructor
     * @param string $mediainfoCmd The mediainfo command
     */
    public function __construct($mediainfoCmd=false)
    {
        // The command to extract zip container
        if (!$mediainfoCmd) {
            switch (DIRECTORY_SEPARATOR) {
                // Windows installation
                case '\\':
                    $mediainfoCmd = '"C:\Program Files\MediaInfo\mediainfo"';
                    break;

                case "/":
                default:
                    $mediainfoCmd = "mediainfo";
            }
        }
        $this->mediainfoCmd = $mediainfoCmd;
    }

    /**
     * Get info from directory or file
     * @param mixed $path The path to the directory to check or a filename or a file list
     * 
     * @return array
     */
    public function mediainfo($path)
    {
        $command = '"' . $this->mediainfoCmd . '" --Output=XML "' . $path . '"';

        $output = array();
        $return = null;

        exec($command, $output, $return);

        if ($return !== 0) {
            throw  new DfiException("Failed to retrieve media information: execution error. $command " . implode(' ', $output));
        }

        $xml = implode('', $output);

        return $xml;
    }

    /**
     * Get the version
     * 
     * @return string
     */
    public function version()
    {
        $command = '"' . $this->mediainfoCmd . '" --version';

        $output = array();
        $return = null;

        exec($command, $output, $return);

        return $output[1];
    }


}