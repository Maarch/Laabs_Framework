<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of DFI.
 *
 * DFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DFI. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Class for Apache TIKA
 *
 * @package Dfi
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class Tika
{

    protected $executable;

    protected $conffile;

    public $errors;

    /**
     * Construct environment
     * @param string $jarfile The Tika jar file
     *
     * @return void
     * @author 
     */
    public function __construct($jarfile)
    {
        $this->executable = 'java -jar "' . $jarfile . '"';
    }

    /**
     * Get format
     * @param string $filename 
     * 
     * @return string
     */
    public function getInfo($filename)
    {
        $command = $this->executable . " " . $filename;

        $output = array();
        $return = null;

        exec($command, $output, $return);

        if ($return !== 0) {
            throw new Exception("Failed to validate file format: execution error." . implode(' ', $output));
        }

        $this->errors = array();
        
        while (($line=reset($output)) && (substr($line, 0, 5) !== '<?xml')) {
            array_shift($output);
        }

        $xml = implode('', $output);
        
        return $xml;
    }

    /**
     * Get format
     * @param string $filename 
     * 
     * @return string
     */
    public function getText($filename)
    {
        $command = $this->executable . " --text " . $filename;

        $output = array();
        $return = null;

        exec($command, $output, $return);
        var_dump($command);
        var_dump($output);
        var_dump($return);
        exit;
        if ($return !== 0) {
            throw new Exception("Failed to validate file format: execution error." . implode(' ', $output));
        }

        $this->errors = array();
        
        while (($line=reset($output)) && (substr($line, 0, 5) !== '<?xml')) {
            array_shift($output);
        }

        $xml = implode('', $output);
        
        return $xml;
    }

    /**
     * Get version
     * 
     * @return array The validation errors
     */
    public function version()
    {
        $tokens = array();
        $tokens[] = $this->executable;
        $tokens[] = '--version';

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;

        exec($command, $output, $return);

        return $output[0];
    }

    /**
     * Get messages
     * 
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the usable module for a given puid
     * @param string $puid The pronom format identifier
     * 
     * @return string $the name of the module
     */
    public function getModule($puid)
    {
        
        foreach ($this->modules as $name => $formats) {
            if (in_array($puid, $formats)) {
                return $name;
            }
        }
    }

} // END class jhove 
