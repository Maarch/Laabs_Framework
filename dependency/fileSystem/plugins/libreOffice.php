<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency fileSystem.
 *
 * Dependency fileSystem is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency fileSystem is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency fileSystem.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace dependency\fileSystem\plugins;

/**
 * Class for libreOffice functions
 *
 * @package Dependency/fileSystem
 */
class libreOffice implements \dependency\fileSystem\conversionInterface
{
    protected $libreOfficeExecutable;

    public function __construct($libreOfficeExecutable)
    {
        $this->libreOfficeExecutable = $libreOfficeExecutable;
    }

    public function convert($srcfile, $tgtfmt, $tgtdir = false, $srcfmt = null)
    {
        $tokens = array('"'.$this->libreOfficeExecutable.'"');
        $tokens[] = "--headless";
        $tokens[] = "--convert-to";
        $tokens[] = $tgtfmt;
        $tokens[] = '"'.$srcfile.'"';
        if (!$tgtdir) {
            $tgtdir = dirname($srcfile);
        }
        $tokens[] = '--outdir "'.$tgtdir.'"';
        if ($srcfmt) {
            $tokens[] = $srcfmt;
        }

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;
        $this->errors = array();

        exec($command, $output, $return);

        if ($return === 0) {
            $ext = strtok($tgtfmt, ":");

            return $tgtdir . DIRECTORY_SEPARATOR . pathinfo($srcfile, PATHINFO_FILENAME) . "." . $ext;
        } else {
            throw new \dependency\fileSystem\Exception("error during conversion", $return, null, $output);
        }
    }

    public function getSoftwareName()
    {
        $tokens = array('"'.$this->libreOfficeExecutable.'"');
        $tokens[] = "--version";

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;
        $this->errors = array();

        exec($command, $output, $return);

        if ($return === 0) {
            try {
                $result = explode(' ',$output[0])[0];
            } catch (Exception $ex) {
                throw new \dependency\fileSystem\Exception("error during the recuperation of the software name", $return, null, $output);
            }
            return $result;
        } else {
            throw new \dependency\fileSystem\Exception("error during the recuperation of the software name", $return, null, $output);
        }
    }

    public function getSoftwareVersion()
    {
        $tokens = array('"'.$this->libreOfficeExecutable.'"');
        $tokens[] = "--version";

        $command = implode(' ', $tokens);

        $output = array();
        $return = null;
        $this->errors = array();

        exec($command, $output, $return);

        if ($return === 0) {
            try {
                $result = explode(' ', $output[0])[1];
            } catch (Exception $ex) {
                throw new \dependency\fileSystem\Exception("error during the recuperation of the software name", $return, null, $output);
            }
            return $result;
        } else {
            throw new \dependency\fileSystem\Exception("error during the recuperation of the software version", $return, null, $output);
        }
    }
}
