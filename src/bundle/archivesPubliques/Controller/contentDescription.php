<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Controller;

/**
 * Classe de description d'archive
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class contentDescription implements \bundle\recordsManagement\Controller\archiveDescriptionInterface
{

    protected $sdoFactory;

    /**
     * Constructeur de contentDescription
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Créer un nouveau contenu de description
     *
     * @param object $contentDescription L'objet de description d'archive
     *
     * @return archivesPubliques/contentDescription
     */
    public function create($contentDescription)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->create($contentDescription, "archivesPubliques/contentDescription");
            if ($contentDescription->custodialHistory) {
                $this->sdoFactory->createCollection($contentDescription->custodialHistory, "archivesPubliques/custodialHistory");
            }
            if ($contentDescription->keyword) {
                $this->sdoFactory->createCollection($contentDescription->keyword, "archivesPubliques/keyword");
            }
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw \laabs::newException("archivesPubliques/contentDescriptionException", "Content description not created");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return $contentDescription;
    }

    /**
     * Lire un contenu de description
     *
     * @param string $contentDescriptionId L'identifiant de l'archive
     *
     * @return archivesPubliques/contentDescription
     */
    public function read($contentDescriptionId)
    {
        $contentDescription = $this->sdoFactory->read("archivesPubliques/contentDescription", $contentDescriptionId);
        $contentDescription->custodialHistory = $this->sdoFactory->readChildren("archivesPubliques/custodialHistory", $contentDescription);
        $contentDescription->keyword = $this->sdoFactory->readChildren("archivesPubliques/keyword", $contentDescription);

        return $contentDescription;
    }

    /**
     * Mettre un jour un contenu de description
     *
     * @param type $contentDescription L'object de contenu de description
     *
     * @return archivesPubliques/contentDescription
     */
    public function update($contentDescription)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->update($contentDescription, "archivesPubliques/contentDescription");

            foreach ($contentDescription->custodialHistory as $key => $custodialHistory) {
                $this->sdoFactory->update($custodialHistory, "archivesPubliques/custodialHistory");
            }

            foreach ($contentDescription->keyword as $key => $keyword) {
                $this->sdoFactory->update($contentDescription->keyword, "archivesPubliques/keyword");
            }
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw \laabs::newException("archivesPubliques/contentDescriptionException", "Content description not updated");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return $contentDescription;
    }

    /**
     * Supprimer un contenu de description
     *
     * @param type $contentDescriptionId L'identifiant de l'archive
     *
     * @return boolean
     */
    public function delete($contentDescriptionId)
    {
        $contentDescription = $this->read($contentDescriptionId);

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->deleteChildren("archivesPubliques/custodialHistory", $contentDescription);
            $this->sdoFactory->deleteChildren("archivesPubliques/keyword", $contentDescription);
            $this->sdoFactory->delete($contentDescription, "archivesPubliques/contentDescription");
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw \laabs::newException("archivesPubliques/contentDescriptionException", "Content description not deleted");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }


    /**
     * Recherche d'un contenu d'archive
     * @param string    $contentDescriptionId
     * @param string    $archiveName
     * @param string    $description
     * @param timestamp $oldestDate
     * @param timestamp $latestDate
     * @param array     $keyword
     * @param string    $status
     * @param string    $finalDisposition
     * @param string    $archivalAgreementReference
     * @param string    $archivalProfileReference
     * @param bool      $isCommunicable
     * @param bool      $isExpired
     * @param string    $depositorOrgRegNumber
     * @param string    $originatorOrgRegNumber
     * @param bool      $isConverted
     * @param bool      $hasParent
     *
     * @return Array Array of archive description
     */
    public function find(
        $contentDescriptionId = null, 
        $archiveName = null, 
        $description = null, 
        $oldestDate = null, 
        $latestDate = null, 
        $keyword = array(), 
        $status = null, 
        $finalDisposition = null, 
        $archivalAgreementReference = null, 
        $archivalProfileReference = null, 
        $isCommunicable = null, 
        $isExpired = null, 
        $depositorOrgRegNumber = null, 
        $originatorOrgRegNumber = null,
        $isConverted = null,
        $hasParent = null)
    {
        $queryContentDescription = array();
        $queryParams = array();

        $queryKeyword = array();

        $currentDate = \laabs::newDate();
        $currentDateString = $currentDate->format('Y-m-d');

        $orgIds = \laabs::callService('organization/userPosition/readOrgids');
        $descendentOrgUnitIds = \laabs::callService('organization/userPosition/readDescendantorgRegNumbers');

        $queryContentDescription['originatorOwnerOrgId'] = "(originatorOwnerOrgId=['". implode("', '", $orgIds) ."'] OR originatorOwnerOrgId=null)";
        $queryContentDescription['accessRule'] = "(originatorOrgRegNumber = ['". implode("', '", $descendentOrgUnitIds) ."'] OR accessRuleComDate <= '$currentDate' OR accessRuleComDate = NULL)";
        $queryContentDescription['disposable'] = "status != 'disposed'";

        // content description
        if ($contentDescriptionId) {
            $queryContentDescription['contentDescriptionId'] = "(contentDescriptionId = '*$contentDescriptionId*' OR archiverArchiveId = '*$contentDescriptionId*' OR depositorArchiveId = '*$contentDescriptionId*')";
        }
        if ($archiveName) {
            $queryContentDescription['archiveName'] = "archiveName = '*$archiveName*'";
        }
        if ($description) {
            $queryContentDescription['description'] = "description = '*$description*'";
        }
        if ($oldestDate && $latestDate) {
            $queryParams['oldestDate'] = $oldestDate;
            $queryParams['latestDate'] = $latestDate;
            $queryContentDescription['date'] = "latestDate >= :oldestDate AND oldestDate <= :latestDate";

        } elseif ($oldestDate) {
            $queryParams['oldestDate'] = $oldestDate;
            $queryContentDescription['date'] = "latestDate >= :oldestDate AND oldestDate <= :oldestDate";

        } elseif ($latestDate) {
            $queryParams['latestDate'] = $latestDate;
            $queryContentDescription['date'] = "latestDate >= :latestDate AND oldestDate <= :latestDate";
        }
        if ($status) {
            $queryParams['status'] = $status;
            $queryContentDescription['status'] = "status = :status";
        }
        if ($finalDisposition) {
            $queryParams['finalDisposition'] = $finalDisposition;
            $queryContentDescription['finalDisposition'] = "finalDisposition = :finalDisposition";
        }
        if ($archivalProfileReference) {
            $queryParams['archivalProfileReference'] = $archivalProfileReference;
            $queryContentDescription['archivalProfileReference'] = "archivalProfileReference = :archivalProfileReference";
        }
        if ($isCommunicable === true) {
            $queryParams['accessRuleComDate'] = $currentDateString;
            $queryContentDescription['accessRuleComDate'] = "(accessRuleComDate <= :accessRuleComDate OR accessRuleComDate = NULL)";
        } 
        if ($isCommunicable === false) {
            $queryParams['accessRuleComDate'] = $currentDateString;
            $queryContentDescription['accessRuleComDate'] = "(accessRuleComDate >= :accessRuleComDate AND accessRuleComDate != NULL)";
        }
        if ($isExpired === true) {
            $queryParams['disposalDate'] = $currentDateString;
            $queryContentDescription['disposalDate'] = "disposalDate <= :disposalDate";
        }
        if ($isExpired === false) {
            $queryParams['disposalDate'] = $currentDateString;
            $queryContentDescription['disposalDate'] = "disposalDate >= :disposalDate";
        }
        if ($archivalAgreementReference) {
            $queryParams['archivalAgreementReference'] = $archivalAgreementReference;
            $queryContentDescription['archivalAgreementReference'] = "archivalAgreementReference = :archivalAgreementReference";
        }
        if ($originatorOrgRegNumber) {
            $queryParams['originatorOrgRegNumber'] = $originatorOrgRegNumber;
            $queryContentDescription['originatorOrgRegNumber'] = "originatorOrgRegNumber = :originatorOrgRegNumber";
        }
        if ($depositorOrgRegNumber) {
            $queryParams['depositorOrgRegNumber'] = $depositorOrgRegNumber;
            $queryContentDescription['depositorOrgRegNumber'] = "depositorOrgRegNumber = :depositorOrgRegNumber";
        }
        if ($isConverted === true) {
            $queryContentDescription['isConverted'] = "(documentCount>0 and documentCopies>0)";
        }
        if ($isConverted === false) {
            $queryContentDescription['isConverted'] = "(documentCount=0 or documentCopies=0)";
        }

        if ($hasParent === true) {
            $queryContentDescription['hasParent'] = "parentArchiveId!=null";
        }
        if ($hasParent === false) {
            $queryContentDescription['hasParent'] = "parentArchiveId=null";
        }

        // keyword
        if (count($keyword)) {
            foreach ($keyword as $key) {
                $queryKeyword[] = "( content = '*$key*' OR reference = '*$key*')";
            }
            $queryStringKeyword = implode(' OR ', $queryKeyword);

            $queryContentDescription['keyword'] = "contentDescriptionId=[READ archivesPubliques/keyword [contentDescriptionId] ($queryStringKeyword)]";
        }

        $queryString = \laabs\implode(' AND ', $queryContentDescription);

        $contentDescriptions = $this->sdoFactory->find("archivesPubliques/archiveDescription", $queryString, $queryParams, false, 0, 100);

        return $contentDescriptions;
    }

    /**
     * Communicabilité de la description
     * @param string $contentDescriptionId The content description identifier
     *
     * @return bool
     */
    public function isVisibleDescription($contentDescriptionId)
    {
        $contentDescription = $this->sdoFactory->find("archivesPubliques/contentDescription", "contentDescriptionId='$contentDescriptionId");
        $date = new \core\Type\DateTime(\laabs::newDate());
        if (!isset($contentDescription->accessRuleComDate)) {
            return true;
        } else {
            $comDate = new \core\Type\DateTime($contentDescription->accessRuleComDate);
            if (!$date->diff($comDate)) {
                return true;
            }
        }

        if ($currentOrganization = \laabs::getToken("ORGANIZATION")) {
            $orgRole = (array) $currentOrganization->orgRoleCodes;
            if (in_array("archiver", $orgRole) == false && in_array("depositor", $orgRole) == false) {
                if (in_array("originator", $orgRole)) {
                    if ($archives->originatorOrgRegNumber == $currentOrganization->registrationNumber) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        }

        return false;
    }
}
