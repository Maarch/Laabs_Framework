<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Controller;

/**
 * Classe de description d'archive
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class document implements \bundle\recordsManagement\Controller\archiveDescriptionInterface
{

    protected $sdoFactory;

    /**
     * Constructeur de document
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Créer un nouveau document
     *
     * @param type $document Le document
     *
     * @return archivesPubliques/document
     */
    public function create($document)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->create($document, "archivesPubliques/document");

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "archivesPubliques/documentCreateException";
                $auditEntry->objectClass = "archivesPubliques/document";
                $auditEntry->objectId = $document->docId;
                $auditEntry->message = $exception;
                $auditEntry->action = 'archivesPubliques/document/create';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }

            throw \laabs::newException("archivesPubliques/sdoException");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        if (\laabs::hasBundle('audit')) {
            $auditEntry = \laabs::newInstance('audit/entry');
            $auditEntry->entryType = "archivesPubliques/documentCreate";
            $auditEntry->objectClass = "archivesPubliques/document";
            $auditEntry->objectId = $document->docId;
            $auditEntry->message = "archivesPubliques created";
            $auditEntry->action = 'archivesPubliques/document/create';

            \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }

        return $document;
    }

    /**
     * Lire un document
     *
     * @param string $docId L'identifiant de l'archive
     *
     * @return archivesPubliques/document
     */
    public function read($docId)
    {
        $document = $this->sdoFactory->read("archivesPubliques/document", array("docId" => $docId));

        return $document;
    }

    /**
     * Mettre un jour un document
     *
     * @param type $document L'object de document
     *
     * @return archivesPubliques/document
     */
    public function update($document)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->update($document, "archivesPubliques/document");

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "archivesPubliques/documentUpdateException";
                $auditEntry->objectClass = "archivesPubliques/document";
                $auditEntry->objectId = $document->docId;
                $auditEntry->message = $exception;
                $auditEntry->action = 'archivesPubliques/document/updated';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }

            throw \laabs::newException("archivesPubliques/sdoException");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        if (\laabs::hasBundle('audit')) {
            $auditEntry = \laabs::newInstance('audit/entry');
            $auditEntry->entryType = "archivesPubliques/documentUpdate";
            $auditEntry->objectClass = "archivesPubliques/document";
            $auditEntry->objectId = $document->docId;
            $auditEntry->message = "archivesPubliques updated";
            $auditEntry->action = 'archivesPubliques/document/update';

            \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }

        return $document;
    }

    /**
     * Supprimer un document
     *
     * @param type $docId L'identifiant de l'archive
     *
     * @return boolean
     */
    public function delete($docId)
    {
        $document = $this->read($docId);

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->deleteChildren("archivesPubliques/custodialHistory", $document);
            $this->sdoFactory->deleteChildren("archivesPubliques/keyword", $document);
            $this->sdoFactory->delete($document, "archivesPubliques/document");
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "archivesPubliques/documentDeleteException";
                $auditEntry->objectClass = "archivesPubliques/document";
                $auditEntry->objectId = $docId;
                $auditEntry->message = $exception;
                $auditEntry->action = 'archivesPubliques/document/delete';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }

            throw \laabs::newException("archivesPubliques/sdoException");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        if (\laabs::hasBundle('audit')) {
            $auditEntry = \laabs::newInstance('audit/entry');
            $auditEntry->entryType = "archivesPubliques/documentDelete";
            $auditEntry->objectClass = "archivesPubliques/document";
            $auditEntry->objectId = $document->docId;
            $auditEntry->message = "archivesPubliques deleted";
            $auditEntry->action = 'archivesPubliques/document/delete';

            \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }

        return true;
    }

    /**
     * Recherche d'un contenu d'archive
     *
     * @return bool
     */
    public function find($description = null, $descriptionLevel = null, $filePlanPosition = null, $latestDate = null, $oldestDate = null, $otherDescriptiveData = null, $repositoryOrgRegNumber = null, $archiveName = null, $content = null, $reference = null, $type = null)
    {
        $queryDocument = array();
        $queryKeyword = array();

        //$queryDocument[] = $this->auth->getUserAccessRule('archivesPubliques/document');

        // content description
        if ($description) {
            $queryDocument['description'] = "description = '*$description*'";
        }
        if ($descriptionLevel) {
            $queryDocument['descriptionLevel'] = "descriptionLevel = '*$descriptionLevel*'";
        }
        if ($filePlanPosition) {
            $queryDocument['filePlanPosition'] = "filePlanPosition = '*$filePlanPosition*'";
        }
        if ($latestDate) {
            $queryDocument['latestDate'] = "latestDate >= '$latestDate'";
        }
        if ($oldestDate) {
            $queryDocument['oldestDate'] = "oldestDate <= '$oldestDate'";
        }
        if ($otherDescriptiveData) {
            $queryDocument['otherDescriptiveData'] = "otherDescriptiveData = '*$otherDescriptiveData*'";
        }
        if ($repositoryOrgRegNumber) {
            $queryDocument['repositoryOrgRegNumber'] = "repositoryOrgRegNumber = '*$repositoryOrgRegNumber*'";
        }
        if ($archiveName) {
            $queryDocument['archiveName'] = "archiveName = '*$archiveName*'";
        }

        // keyword
        if ($content) {
            $queryKeyword['content'] = "content = '*$content*'";
        }
        if ($reference) {
            $queryKeyword['reference'] = "reference = '*$reference*'";
        }
        if ($type) {
            $queryKeyword['type'] = "type = '*$type*'";
        }

        $queryStringKeyword = "";
        if (count($queryKeyword) != 0) {
            $queryStringKeyword = implode(' AND ', $queryKeyword)."AND ";
        }

        $queryString = implode(' AND ', $queryDocument)." AND docId=[READ archivesPubliques/keyword [docId] (".$queryStringKeyword." true=true)]";
        $document = $this->sdoFactory->find("archivesPubliques/document", $queryString, array(), false, 0, 100);

        return $document;
    }

    /**
     *  Communicabilité de la description
     * @param string $docId The document identifier
     *
     * @return bool
     */
    public function isVisibleDescription($docId)
    {
        $document = $this->sdoFactory->find("archivesPubliques/document", "docId='$docId");
        $date = new \core\Type\DateTime(\laabs::newDate());
        if (!isset($document->accessRuleComDate)) {
            return true;
        } else {
            $comDate = new \core\Type\DateTime($document->accessRuleComDate);
            if (!$date->diff($comDate)) {
                return true;
            }
        }

        if ($currentOrganization = \laabs::getToken("ORGANIZATION")) {
            $orgRole = (array) $currentOrganization->orgRoleCodes;
            if (in_array("archiver", $orgRole) == false && in_array("depositor", $orgRole) == false) {
                if (in_array("originator", $orgRole)) {
                    if ($archives->originatorOrgRegNumber == $currentOrganization->registrationNumber) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        }

        return false;
    }
}
