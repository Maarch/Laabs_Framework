<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle digitalResource.
 *
 * Bundle digitalResource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle digitalResource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Controller;

/**
 * Class for thesaurus W
 */
class thesaurus
{

    protected $domDocument;
    protected $domXPath;
    protected $format;

    /**
     * Constructor
     */
    public function __construct()
    {
        $thesaurusFile = 
            LAABS_BUNDLE . DIRECTORY_SEPARATOR
            . 'archivesPubliques' . DIRECTORY_SEPARATOR
            . LAABS_RESOURCE . DIRECTORY_SEPARATOR
            . 'thesaurus' . DIRECTORY_SEPARATOR
            . 'thesaurusW.xml';

        $this->thesaurusFile = $thesaurusFile;
        $this->domDocument = new \DOMDocument();
        $this->domDocument->load($thesaurusFile);
        $this->domXPath = \laabs::newService("dependency/xml/XPath", $this->domDocument);

        $this->domXPath->registerNamespace('skos', 'skos');
        $this->domXPath->registerNamespace('dc', 'dc');
        $this->domXPath->registerNamespace('dct', 'dct');
        $this->domXPath->registerNamespace('rdf', 'rdf');
        $this->domXPath->registerNamespace('rdfs', 'rdfs');
        $this->domXPath->registerNamespace('foaf', 'foaf');
    }

    /**
     * Get all the formats
     * @return digitalResource/format[]
     */
    public function index()
    {
        $topConcepts = $this->domXPath->query('/rdf:RDF/skos:ConceptScheme/skos:hasTopConcept');
        foreach ($topConcepts as $topConcepts) {

        }

        $concepts = $this->domXPath->query('/rdf:RDF/skos:Concept');
        foreach ($concepts as $concept) {
            // Get Position
            $about = $concept->getAttributeNS('rdf', 'about');
            $conceptId = \laabs\basename($about);

            // Get Name
            $label = $concept->getElementsByTagNameNS('skos', 'prefLabel')->item(0)->nodeValue;
            $label = str_replace(array('\r\n', '\n', '\r', '\t'), ' ', $label);
            
            if ($broader = $concept->getElementsByTagNameNS('skos', 'broader')->item(0)) {
                $resource = $broader->getAttributeNS('rdf', 'resource');
                $broaderId = \laabs\basename($resource);
            }

        }
    }

    /**
     * Parse a concept element from Xml into a archivesPubliques/concept object
     * @param \DOMElement $conceptElement
     * 
     * @return archivesPubliques/concept
     */
    protected function parseConceptElement($conceptElement)
    {
        $concept = \laabs::newInstance("archivesPubliques/concept");

        // Extract identifier
        $about = $conceptElement->getAttributeNS('rdf', 'about');
        $concept->conceptId = \laabs\basename($about);

        // Extract pref label
        $prefLabel = $conceptElement->getElementsByTagNameNS('skos', 'prefLabel')->item(0);
        $concept->label = $prefLabel->nodeValue;
        
        // Extract alternative labels
        $altLabels = $conceptElement->getElementsByTagNameNS('skos', 'altLabel');
        foreach ($altLabels as $altLabel) {
            $concept->altLabels[] = $altLabel->nodeValue;
        }

        // Extract broader concept
        if ($broader = $conceptElement->getElementsByTagNameNS('skos', 'broader')->item(0)) {
            $resource = $broader->getAttributeNS('rdf', 'resource');
            $concept->broaderConceptId = \laabs\basename($resource);
        }

        // Extract narrower concepts
        $narrowers = $conceptElement->getElementsByTagNameNS('skos', 'narrower');
        foreach ($narrowers as $narrower) {
            $resource = $narrower->getAttributeNS('rdf', 'resource');
            $concept->narrowerConceptIds[] = \laabs\basename($resource);
        }

        // Extract related concepts
        // From resource name
        // From inline concept definition (child)
        $relateds = $conceptElement->getElementsByTagNameNS('skos', 'related');
        foreach ($relateds as $related) {
            if ($related->hasAttributeNS('rdf', 'resource')) {
                $resource = $related->getAttributeNS('rdf', 'resource');
                $concept->relatedConceptIds[] = \laabs\basename($resource);
            } else {
                $relatedConcept = $related->getElementsByTagNameNS('skos', 'Concept')->item(0);
                $resource = $relatedConcept->getAttributeNS('rdf', 'about');
                $concept->relatedConceptIds[] = \laabs\basename($about);
            }
        }
        // Extract related concept from inline concept definition (parent)
        if ($conceptElement->parentNode->nodeName == "skos:related") {
            $relatedConcept = $conceptElement->parentNode->parentNode;
            $about = $relatedConcept->getAttributeNS('rdf', 'about');

            $concept->relatedConceptIds[] = \laabs\basename($about);
        }
       
        return $concept;
    }

    /**
     * Search a concept
     * @param string $query
     *
     * @return array The list of concepts found
     */
    public function find($query = false)
    {
        $concepts = array();

        $queryPredicats[] = "contains(lower-case(skos:prefLabel), lower-case('$query'))";
        $queryPredicats[] = "contains(lower-case(skos:altLabel), lower-case('$query'))";
        
        $queryString = "//skos:Concept[" . implode(" or ", $queryPredicats) . "]";

        $conceptElements = $this->domXPath->query($queryString);

        foreach ($conceptElements as $conceptElement) {
            $concepts[] = $this->parseConceptElement($conceptElement);
        }

        return $concepts;
    }
}