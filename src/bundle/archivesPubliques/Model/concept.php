<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle digitalResource.
 *
 * Bundle digitalResource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle digitalResource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\archivesPubliques\Model;
/**
 * Class model that represents a thesaurus skos concept
 *
 * @package ArchivesPubliques
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @pkey [conceptId]
 * 
 */
class concept
{
    /**
     * The skos concept "about" identifier
     *
     * @var string
     */
    public $conceptId;

    /**
     * The concept label
     *
     * @var string
     */
    public $label;

    /**
     * The alternative labels
     *
     * @var array
     */
    public $altLabels = array();

    /**
     * The broader concept Id
     *
     * @var string
     */
    public $broaderConceptId;

    /**
     * The narrower concept Ids
     *
     * @var array
     */
    public $narrowerConceptIds = array();

    /**
     * The related concept Ids
     *
     * @var array
     */
    public $relatedConceptIds = array();

} 
