<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Model;

/**
 * Classe du modele qui représente un contenu de description
 *
 * @package ArchivesPubliques
 * @author  Alexis RAGOT (Maarch) <alexis.ragot@maarch.org>
 *
 * @pkey [contentDescriptionId]
 */
class contentDescription
{
    /**
     * Identifiant de l'archive
     *
     * @var id
     * @notempty
     */
    public $contentDescriptionId;
    
    /**
     * Description
     *
     * @var string
     */
    public $description;
    
    /**
     * Niveau de description
     *
     * @var string
     */
    public $descriptionLevel;
    
    /**
     * Nom de l'archive
     *
     * @var string
     */
    public $archiveName;
    
    /**
     * The originator archive identifier
     *
     * @var string
     */
    public $originatorArchiveId;

    /**
     * The archiver archive identifier
     *
     * @var string
     */
    public $archiverArchiveId;

    /**
     * The depositor archive identifier
     *
     * @var string
     */
    public $depositorArchiveId;
    
    /**
     * Positions du plan de classement
     *
     * @var tokenlist
     */
    public $filePlanPosition;
            
    /**
     * La langue
     *
     * @var tokenlist
     */
    public $language;
    
    /**
     * La dernière date
     *
     * @var date
     */
    public $latestDate;
            
    /**
     * La plus ancienne date
     *
     * @var date
     */
    public $oldestDate;
            
    /**
     * Les autres données de descriptions
     *
     * @var string
     */
    public $otherDescriptiveData;
            
    /**
     * Le code de règle d'accès
     *
     * @var string
     */
    public $accessRuleCode;
    
    /**
     * Le durée de la règle d'accès
     *
     * @var duration
     */
    public $accessRuleDuration;
    
     /**
     * Le date de départ de la règle d'accès
     *
     * @var date
     */
    public $accessRuleStartDate;
    
    /**
     * Le date de communicabilité
     *
     * @var date
     */
    public $accessRuleComDate;
            
    /**
     * Le code de règle d'accès
     *
     * @var xml
     */
    public $sedaXml;
    
    /**
     * Numéro d'organisation du site de stockage
     *
     * @var string
     */
    public $repositoryOrgRegNumber;
    
    /**
     * Information d'histoire
     *
     * @var archivesPubliques/custodialHistory[]
     */
    public $custodialHistory;
    
    /**
     * Mot cle
     *
     * @var archivesPubliques/keyword[]
     */
    public $keyword;
}
