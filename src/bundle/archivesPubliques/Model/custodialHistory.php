<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Model;

/**
 * Classe du modele qui représente une information d'histoire
 *
 * @package ArchivesPubliques
 * @author  Alexis RAGOT (Maarch) <alexis.ragot@maarch.org>
 * 
 * @fkey [contentDescriptionId] archivesPubliques/contentDescription [contentDescriptionId]
 */
class custodialHistory
{
    /**
     * Identifiant de l'archive
     *
     * @var id
     * @notempty
     */
    public $contentDescriptionId;
    
    /**
     * Quand
     *
     * @var date
     */
    public $when;
            
    /**
     * item
     *
     * @var string
     * @notempty
     */
    public $item;
}
