<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Model;

/**
 * Description of document
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 * 
 * @pkey [docId]
 */
class document
{
    /**
     * The identifier of document
     *
     * @var string
     * @notempty
     */
    public $docId;
    
    /**
     * The description of document
     *
     * @var string
     */
    public $description;
    
    /**
     * Language of the document
     *
     * @var string
     */
    public $language;

    /**
     * Purpose of the document
     *
     * @var string
     */
    public $purpose;
    
    /**
     * Date of creation of the document
     *
     * @var timestamp
     */
    public $creation;
    
    /**
     * Date of issue of the document
     *
     * @var timestamp
     */
    public $issue;

    /**
     * Date of receipt of the document
     *
     * @var timestamp
     */
    public $receipt;

    /**
     * Date of response of the document
     *
     * @var timestamp
     */
    public $response;
    
    /**
     * Date of submission of the document
     *
     * @var timestamp
     */
    public $submission;
    
    /**
     * The archiver document identifier
     *
     * @var string
     */
    public $archiverDocId;
    
    /**
     * The depositor document identifier
     *
     * @var string
     */
    public $depositorDocId;
    
    /**
     * The originator document identifier
     *
     * @var string
     */
    public $originatorDocId;
}
