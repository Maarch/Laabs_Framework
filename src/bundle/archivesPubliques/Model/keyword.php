<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\archivesPubliques\Model;

/**
 * Classe du modele qui représente un mot cle
 *
 * @package ArchivesPubliques
 * @author  Alexis RAGOT (Maarch) <alexis.ragot@maarch.org>
 * 
 * @fkey [contentDescriptionId] archivesPubliques/contentDescription [contentDescriptionId]
 */
class keyword
{
    /**
     * Identifiant de l'archive
     *
     * @var id
     * @notempty
     */
    public $contentDescriptionId;
    
    /**
     * Contenu du mot cle
     *
     * @var string
     * @notempty
     */
    public $content;
    
    /**
     * Reference du mot cle
     *
     * @var string
     */
    public $reference;
    
    /**
     * Type du mot cle
     *
     * @var string
     */
    public $type;
    
    /**
     * Le code de règle d'accès
     *
     * @var string
     */
    public $accessRuleCode;
    
    /**
     * Le durée de la règle d'accès
     *
     * @var duration
     */
    public $accessRuleDuration;
    
     /**
     * Le date de départ de la règle d'accès
     *
     * @var date
     */
    public $accessRuleStartDate;
    
    /**
     * Le date de communicabilité
     *
     * @var date
     */
    public $accessRuleComDate;
}
