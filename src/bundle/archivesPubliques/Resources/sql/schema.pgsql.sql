DROP SCHEMA IF EXISTS "archivesPubliques" CASCADE;

CREATE SCHEMA "archivesPubliques"
  AUTHORIZATION postgres;

CREATE TABLE "archivesPubliques"."contentDescription"
(
  "contentDescriptionId" text NOT NULL,
  "description" text,
  "descriptionLevel" text,
  "archiveName" text,
  "originatorArchiveId" text,
  "archiverArchiveId" text,
  "depositorArchiveId" text,
  "filePlanPosition" text,
  "language" text DEFAULT 'fra'::text,
  "latestDate" date,
  "oldestDate" date,
  "otherDescriptiveData" text,
  "accessRuleCode" text,
  "accessRuleDuration" text,
  "accessRuleStartDate" date,
  "accessRuleComDate" date,
  "sedaXml" Xml,
  "repositoryOrgRegNumber" text,
  PRIMARY KEY ("contentDescriptionId")
);
 
CREATE TABLE "archivesPubliques"."custodialHistory"
(
  "contentDescriptionId" text NOT NULL, 
  "when" date,
  "item" text NOT NULL,
  FOREIGN KEY ("contentDescriptionId")
      REFERENCES "archivesPubliques"."contentDescription" ("contentDescriptionId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
 
CREATE TABLE "archivesPubliques"."keyword"
(
  "contentDescriptionId" text NOT NULL,
  "content" text NOT NULL,
  "reference" text,
  "type" text,
  "accessRuleCode" text,
  "accessRuleDuration" text,
  "accessRuleStartDate" date,
  "accessRuleComDate" date,
  FOREIGN KEY ("contentDescriptionId")
      REFERENCES "archivesPubliques"."contentDescription" ("contentDescriptionId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "archivesPubliques"."document"
(
  "docId" text NOT NULL,
  "description" text,
  "language" text,
  "purpose" text,
  "creation" timestamp,
  "issue" timestamp,
  "receipt" timestamp,
  "response" timestamp,
  "submission" timestamp,
  "archiverDocId" text,
  "depositorDocId" text,
  "originatorDocId" text,
  PRIMARY KEY ("docId")
);