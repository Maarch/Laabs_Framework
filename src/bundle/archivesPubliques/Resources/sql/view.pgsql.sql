DROP VIEW "archivesPubliques"."archiveDescription";

CREATE VIEW "archivesPubliques"."archiveDescription" AS
SELECT "contentDescription"."contentDescriptionId",
    "contentDescription"."description",
    "contentDescription"."descriptionLevel",
    "contentDescription"."originatorArchiveId",
    "contentDescription"."archiverArchiveId",
    "contentDescription"."depositorArchiveId",
    "contentDescription"."filePlanPosition",
    "contentDescription"."language",
    "contentDescription"."latestDate",
    "contentDescription"."oldestDate",
    "contentDescription"."otherDescriptiveData",
    "contentDescription"."accessRuleCode",
    "contentDescription"."accessRuleDuration",
    "contentDescription"."accessRuleStartDate",
    "contentDescription"."accessRuleComDate",
    "contentDescription"."sedaXml",
    "contentDescription"."repositoryOrgRegNumber",
    "archive"."archiveId",
    "archive"."archiveName",
    "archive"."originatorOrgRegNumber",
    "archive"."originatorOwnerOrgId",
    "archive"."depositorOrgRegNumber",
    "archive"."archiverOrgRegNumber",
    "archive"."status",
    "archive"."archivalProfileReference",
    "archive"."archivalAgreementReference",
    "archive"."finalDisposition",
    "archive"."disposalDate",
    "archive"."descriptionId",
	"archive"."parentArchiveId",
    "document"."count" as "documentCount",
    "document"."copies" as "documentCopies"
   FROM "archivesPubliques"."contentDescription"
     JOIN "recordsManagement"."archive" ON "archive"."descriptionClass" = 'archivesPubliques/contentDescription' AND "archive"."descriptionId" = "contentDescription"."contentDescriptionId"
     LEFT JOIN ( SELECT "archiveId", count(1) as "count", sum(case when "copy"=true then 1 else 0 end)  as "copies" FROM "documentManagement"."document" WHERE "type" = 'CDO' GROUP BY "archiveId") "document" ON "archive"."archiveId" = "document"."archiveId";

