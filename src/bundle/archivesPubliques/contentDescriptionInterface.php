<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle archivesPubliques.
 *
 * Bundle archivesPubliques is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle archivesPubliques is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle archivesPubliques.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\archivesPubliques;
/**
 * Interface pour le contenu de description
 * 
 * @package ArchivesPubliques
 * @author  Alexis RAGOT Maarch <alexis.ragot@maarch.org>
 */ 
interface contentDescriptionInterface
{
    /**
     * Recherche d'un contenu d'archive
     * @param string    $contentDescriptionId
     * @param string    $archiveName
     * @param string    $description
     * @param timestamp $oldestDate
     * @param timestamp $latestDate
     * @param array     $keyword
     * @param string    $status
     * @param string    $finalDisposition
     * @param string    $archivalAgreementReference
     * @param string    $archivalProfileReference
     * @param bool      $isCommunicable
     * @param bool      $isExpired
     * @param string    $depositorOrgRegNumber
     * @param string    $originatorOrgRegNumber
     * @param bool      $isConverted
     * @param bool      $hasParent
     *
     * @action archivesPubliques/contentDescription/find
     */
    public function createSearch(
        $contentDescriptionId = null,
        $archiveName = null,
        $description = null,
        $oldestDate = null,
        $latestDate = null,
        $keyword = null,
        $status = null,
        $finalDisposition = null,
        $archivalAgreementReference = null,
        $archivalProfileReference = null,
        $isCommunicable = null,
        $isExpired = null,
        $depositorOrgRegNumber = null,
        $originatorOrgRegNumber = null,
        $isConverted = null,
        $hasParent = null
    );
    

    /**
     * Validate data
     * @param archivesPubliques/contentDescription $contentDescription
     * 
     * @return  archivesPubliques/contentDescription/validationData
     */
    public function readValidation($contentDescription);
    
    /**
     * Enregistrer un contenu de description
     * @param archivesPubliques/contentDescription $contentDescription Le contenu de description
     * 
     * @return  archivesPubliques/contentDescription/create
     */
    public function create($contentDescription);
    
    /**
     * Lire un contenu de description
     * @action archivesPubliques/contentDescription/read
     */
    public function read_contentDescriptionId_();
    
    /**
     * Modifier un contenu de description
     * @param archivesPubliques/contentDescription $contentDescription Le contenu de description
     * 
     * @return  archivesPubliques/contentDescription/update
     */
    public function update($contentDescription);

    /**
     * Supprimer un contenu de description
     * 
     * @return  archivesPubliques/contentDescription/delete
     */
    public function delete_contentDescriptionId_();
}