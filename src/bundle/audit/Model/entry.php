<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit\Model;
/**
 * audit entry definition
 *
 * @package audit
 *
 */
class entry
    extends entryInfo
{

    /**
     * @var resource
     */
    public $dataContext;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->dataContext = \laabs::createMemoryStream("");
    }

    public function setDataContext($dataContext=null)
    {
        switch (gettype($dataContext)) {
            case 'resource':
                $this->dataContext = $dataContext;
                break;

            case 'object':
            case 'array':
            default:
                $this->dataContext = \laabs::createMemoryStream(serialize($dataContext));
        }

    }

    public function mergeMessage()
    {
        if ($this->variables) {
            $this->variables = json_decode($this->variables);

            $this->message = vsprintf($this->message, $this->variables);
        }
    }

}
