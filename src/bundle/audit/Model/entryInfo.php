<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit\Model;
/**
 * audit entry definition
 *
 * @package Audit
 *
 * @pkey [entryId]
 * @substitution audit/entry
 */
class entryInfo
{
    /**
     * @var id
     */
    public $entryId;

    /**
     * @var timestamp
     */
    public $entryDate;

    /**
     * @var string
     */
    public $entryType;

    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $variables;

    /**
     * @var string
     */
    public $objectClass;

    /**
     * @var id
     */
    public $objectId;

    /**
     * @var audit/entryRelationship[]
     */
    public $entryRelationship;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entryDate = \laabs::newTimestamp();
    }

    /**
     * Add a relationship
     * @param string $toEntryId The associated entry id
     */
    public function addRelationship($toEntryId)
    {
        $entryRelationship = \laabs::newInstance('audit/entryRelationship');
        $entryRelationship->fromEntryId = $this->entryId;
        $entryRelationship->toEntryId = $toEntryId;

        $this->entryRelationship[] = $entryRelationship;
    }
}
