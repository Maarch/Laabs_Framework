-- Schema: audit

DROP SCHEMA IF EXISTS audit CASCADE;
CREATE SCHEMA "audit";

-- Table: audit.event

-- DROP TABLE audit.event;

CREATE TABLE "audit"."event"
(
  "eventId" text NOT NULL,
  "eventDate" timestamp NOT NULL,
  "accountId" text NOT NULL,
  "path" text,
  "variables" json,
  "input" json,
  "output" text,
  "status" boolean default true,
  "info" text,
  PRIMARY KEY ("eventId")
)
WITH (
  OIDS=FALSE
);

-- DROP TABLE audit.entryType;

CREATE TABLE "audit"."entryType"
(
  "code" text NOT NULL,
  "format" text NOT NULL,
  PRIMARY KEY ("code")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "audit"."entryType"
  OWNER TO postgres;

-- Table: audit.entry

-- DROP TABLE audit.entry;

CREATE TABLE "audit"."entry"
(
  "entryId" text NOT NULL,
  "entryDate" timestamp,
  "entryType" text,
  "userId" text,
  "message" text,
  "variables" text,
  "objectClass" text,
  "objectId" text,
  "dataContext" bytea,
  PRIMARY KEY ("entryId")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "audit"."entry"
  OWNER TO postgres;


-- Table: audit."entryRelationship"

-- DROP TABLE audit."entryRelationship";

CREATE TABLE "audit"."entryRelationship"
(
  "formEntryId" text,
  "toEntryId" text,
  CONSTRAINT "fromEntryId_entryId_fkey" FOREIGN KEY ("formEntryId")
      REFERENCES "audit"."entry" ("entryId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "toEntryId_EntryId_fkey" FOREIGN KEY ("toEntryId")
      REFERENCES "audit"."entry" ("entryId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "audit"."entryRelationship"
  OWNER TO postgres;

