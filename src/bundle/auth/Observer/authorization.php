<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle auth.
 *
 * Bundle auth is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle auth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle auth.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\auth\Observer;
/**
 * Service for authorization check observer
 *
 * @package Auth
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 */
class authorization
{

    public $ignoreRoutes;

    /**
     * Constructor
     * @param array $ignoreRoutes
     */
    public function __construct(array $ignoreRoutes=array())
    {
        $this->ignoreRoutes = $ignoreRoutes;
    }

    /**
     * Check user privilege against requested route
     * @param \core\Reflection\Route &$serviceRoute The reflection of requested route
     * @param array                  &$args         The arguments
     * 
     * @return boolean
     * 
     * @subject _LAABS_SERVICE_ROUTE
     */
    public function checkPrivilege(&$serviceRoute, array &$args=null)
    {
        foreach ($this->ignoreRoutes as $pattern) {
            if (fnmatch($pattern, $serviceRoute->domain . LAABS_URI_SEPARATOR . $serviceRoute->interface)) {
                return true;
            }
        }

        $userHasPrivilege = \laabs::callService('auth/userAccount');

        if (!$this->auth->hasUserPrivilege($routename)) {
            $authRoute = \laabs::route('READ', 'authorization/noPrivilege');
            $requestRoute->reroute($authRoute);
            for ($i=0, $l=count($args); $i<$l; $i++) {
                unset($args[$i]);
            }

            return false;
        }

    }

}