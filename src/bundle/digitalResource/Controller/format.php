<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle digitalResource.
 *
 * Bundle digitalResource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle digitalResource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\digitalResource\Controller;

/**
 * Class for digitalResource format ref
 */
class format
{

    protected $domDocument;
    protected $domXPath;
    protected $format;

    /**
     * Constructor
     * @param string $droidSignatureFile The path to a droid signature file
     */
    public function __construct($droidSignatureFile = null)
    {
        if (is_null($droidSignatureFile)) {
            $droidSignatureFiles = glob(
                ".." . DIRECTORY_SEPARATOR // src->root
                . LAABS_DEPENDENCY . DIRECTORY_SEPARATOR
                . 'fileSystem' . DIRECTORY_SEPARATOR
                . 'plugins' . DIRECTORY_SEPARATOR
                . 'dfi' . DIRECTORY_SEPARATOR
                . 'data' . DIRECTORY_SEPARATOR
                . 'droid' . DIRECTORY_SEPARATOR
                . 'DROID_SignatureFile_V*.xml'
            );
            rsort($droidSignatureFiles);
            $droidSignatureFile = $droidSignatureFiles[0];
        }
        $this->droidSignatureFile = $droidSignatureFile;
        $this->domDocument = new \DOMDocument();
        $this->domDocument->load($droidSignatureFile);
        $this->domXPath = \laabs::newService("dependency/xml/XPath", $this->domDocument);
        $this->domXPath->registerNamespace('psf', 'http://www.nationalarchives.gov.uk/pronom/SignatureFile');
    }

    /**
     * Get all the formats
     * @return digitalResource/format[]
     */
    public function index()
    {
        $formats = array();
        $formatElements = $this->domXPath->query("/psf:FFSignatureFile/psf:FileFormatCollection/psf:FileFormat");

        foreach ($formatElements as $formatElement) {
            $formats[] = $this->parseFormatElement($formatElement);
        }

        return $formats;
    }

    /**
     * Parse a format element from Xml into a digitalResource/format object
     * @param \DOMElement $formatElement
     *
     * @return digitalResource/format
     */
    protected function parseFormatElement($formatElement)
    {
        $format = \laabs::newInstance("digitalResource/format");
        $format->puid = $formatElement->getAttribute('PUID');
        $format->name = $formatElement->getAttribute('Name');

        if ($formatElement->hasAttribute('Version')) {
            $format->version = $formatElement->getAttribute('Version');
        }

        if ($formatElement->hasAttribute('MIMEType')) {
            $format->mimetypes = \laabs\explode(', ', $formatElement->getAttribute('MIMEType'));
        }

        $mediatypes = array('application', 'message', 'audio', 'video', 'text', 'multipart', 'model', 'image');
        foreach ((array) $format->mimetypes as $mimetype) {
            if (strpos($mimetype, "/") && (in_array($mediatype = strtok($mimetype, "/"), $mediatypes))) {
                $format->mediatype = $mediatype;
                break;
            }
        }

        foreach ($formatElement->getElementsByTagName('Extension') as $extensionElement) {
            $format->extensions[] = $extensionElement->nodeValue;
        }

        return $format;
    }

    /**
     * Get a format by id
     * @param string $puid The puid of format to retrieve
     *
     * @return digitalResource/format
     */
    public function get($puid)
    {
        $formatElements = $this->domXPath->query("/psf:FFSignatureFile/psf:FileFormatCollection/psf:FileFormat[@PUID='$puid']");

        if ($formatElements->length == 1) {
            return $this->parseFormatElement($formatElements->item(0));
        }
    }

    /**
     * Get all formats matching mimetype
     * @param string $mimetype The mimetype of formats to retrieve
     *
     * @return digitalResource/format[]
     */
    public function mimetype($mimetype)
    {
        $formats = array();
        $formatElements = $this->domXPath->query("/psf:FFSignatureFile/psf:FileFormatCollection/psf:FileFormat[contains(@MIMEType, '$mimetype')]");
        foreach ($formatElements as $formatElement) {
            $formats[] = $this->parseFormatElement($formatElement);
        }

        return $formats;
    }

    /**
     * Get all formats matching extension
     * @param string $extension The extension of formats to retrieve
     *
     * @return digitalResource/format[]
     */
    public function extension($extension)
    {
        $formats = array();
        $formatElements = $this->domXPath->query("/psf:FFSignatureFile/psf:FileFormatCollection/psf:FileFormat[./psf:Extension/text() = '$extension']");

        foreach ($formatElements as $formatElement) {
            $formats[] = $this->parseFormatElement($formatElement);
        }

        return $formats;
    }

    /**
     * Get the list of type
     * @param string $query
     *
     * @return array The list of type found
     */
    public function find($query = false)
    {
        $formats = array();

        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $queryPredicats = array();
        foreach ($queryTokens as $queryToken) {
            $queryPredicats[] = "contains(lower-case(@Name), lower-case('$queryToken'))";
            $queryPredicats[] = "contains(lower-case(@MIMEType), lower-case('$queryToken'))";
            $queryPredicats[] = "./Extension/text() = '$queryToken'";
            $queryPredicats[] = "contains(lower-case(@PUID), lower-case('$queryToken'))";
        }
        $queryString = "/psf:FFSignatureFile/psf:FileFormatCollection/psf:FileFormat[".implode(" or ", $queryPredicats)."]";

        $formatElements = $this->domXPath->query($queryString);

        foreach ($formatElements as $formatElement) {
            $formats[] = $this->parseFormatElement($formatElement);
        }

        return $formats;
    }

    /**
     * Get the signature file info
     * @return string
     */
    public function formatDescription()
    {
        $description = array();
        $elements = $this->domDocument->getElementsByTagName("FFSignatureFile");
        foreach ($elements as $element) {
            $description[0] = $element->getAttribute("Version");
            $description[1] = explode("T", $element->getAttribute("DateCreated"))[0];
        }

        return $description;
    }

    /**
     * Get all information about the content file (puid, md5/sha256/sha512 hash)
     * @param string $contents  The content file
     * @param string $extension The extension of file
     *
     * @return digitalResource/fileInformation The file information object
     */
    public function getFileInformation($contents, $extension = null)
    {
        $contents = base64_decode($contents);
        $dfi = \laabs::newService('dependency/fileSystem/plugins/dfi');

        $dfiInfo = $dfi->getInfoBuffer($contents, DFI_USE_DROID);

        $fileInformation = \laabs::newMessage("digitalResource/fileInformation");

        $fileInformation->hashMD5 = hash("MD5", $contents);
        $fileInformation->hashSHA256 = hash("SHA256", $contents);
        $fileInformation->hashSHA512 = hash("SHA512", $contents);
        $fileInformation->mimetype = $dfiInfo->format->mimetype;
        $fileInformation->puid = $dfiInfo->format->puid;

        return $fileInformation;
    }
}
