<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle digitalResource.
 *
 * Bundle digitalResource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle digitalResource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\digitalResource\Controller;

/**
 * Class of adminRepository
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class repository
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The sdo factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Allow to display all repositories
     *
     * @return bool
     */
    public function index()
    {
        return $this->sdoFactory->find("digitalResource/repository");
    }

    /**
     * Edit a repository
     * @param string $repositoryId
     *
     * @return object Repository object
     */
    public function edit($repositoryId = null)
    {
        if ($repositoryId) {
            $repository = $this->getById($repositoryId);
        } else {
            $repository = \laabs::newInstance("digitalResource/repository");
        }

        return $repository;
    }

    /**
     * create a repository
     * @param digitalResource/repository $repository The repository object
     *
     * @return boolean
     */
    public function create($repository)
    {
        if ($this->sdoFactory->exists("digitalResource/repository", array('repositoryUri' => $repository->repositoryUri))) {
            throw \laabs::newException("digitalResource/repositoryException", "Repository URI already exist.");
        }

        $repository->repositoryId = \laabs::newId();

        try {
            \laabs::validate($repository, 'digitalResource/repository');
        } catch (\Exception $e) {
            throw \laabs::newException("digitalResource/repositoryException", "Invalid repository information.");
        }

        if (count($repository->repositoryParam)) {
            foreach ($repository->repositoryParam as $repositoryParam) {
                $repositoryParam->repositoryId = $repository->repositoryId;
            }
        }

        // Try to instanciate service to validate repo
        try {
            $this->getService($repository);
        } catch (\Exception $e) {
            throw \laabs::newException("digitalResource/repositoryException", "Service not found.");
        }

        try {
            $this->sdoFactory->beginTransaction();
            $this->sdoFactory->create($repository, 'digitalResource/repository');

            if (count($repository->repositoryParam)) {
                $this->sdoFactory->createCollection($repository->repositoryParam, "digitalResource/repositoryParam");
            }
        } catch (\Exception $e) {
            $this->sdoFactory->rollback();
            throw \laabs::newException("digitalResource/sdoException");
        }
        $this->sdoFactory->commit();

        return true;
    }

    /**
     * update a repository
     * @param digitalResource/repository $repository The repository object
     *
     * @return boolean
     */
    public function update($repository)
    {
        if (count($this->sdoFactory->find("digitalResource/repository", "repositoryUri = '$repository->repositoryUri'")) > 1) {
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "digitalResource/repositoryUpdateExceptionURI";
                $auditEntry->objectClass = "digitalResource/repository";
                $auditEntry->objectId = $repository->repositoryId;
                $auditEntry->message = "Repository URI already exist.";
                $auditEntry->action = 'digitalRessource/repository/updateException';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            throw \laabs::newException("digitalResource/repositoryException", "Repository URI already exist.");
        }

        if (count($repository->repositoryParam)) {
            foreach ($repository->repositoryParam as $repositoryParam) {
                $repositoryParam->repositoryId = $repository->repositoryId;
            }
        }


        // Try to instanciate service to validate repo
        try {
            $this->getService($repository);
        } catch (\Exception $e) {
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "digitalResource/repositoryUpdateExceptionRepo";
                $auditEntry->objectClass = "digitalResource/repository";
                $auditEntry->objectId = $repository->repositoryId;
                $auditEntry->message = "Service not found";
                $auditEntry->action = 'digitalRessource/repository/updateException';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            throw \laabs::newException("digitalResource/repositoryException", "Service not found.");
        }

        try {
            $this->sdoFactory->beginTransaction();
            $this->sdoFactory->update($repository, 'digitalResource/repository');
            $this->sdoFactory->deleteChildren("digitalResource/repositoryParam", $repository, 'digitalResource/repository');

            if (count($repository->repositoryParam)) {
                $this->sdoFactory->createCollection($repository->repositoryParam, "digitalResource/repositoryParam");
            }
        } catch (\core\Route\Exception $e) {
            $this->sdoFactory->rollback();

            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "digitalResource/repositoryUpdateExceptionCollection";
                $auditEntry->objectClass = "digitalResource/repository";
                $auditEntry->objectId = $repository->repositoryId;
                $auditEntry->message = "Collection not created";
                $auditEntry->action = 'digitalRessource/repository/updateException';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            throw \laabs::newException("digitalResource/repositoryException", "Collection not created.");
        }
        $this->sdoFactory->commit();

        if (\laabs::hasBundle('audit')) {
            $auditEntry = \laabs::newInstance('audit/entry');
            $auditEntry->entryType = "digitalResource/repositoryModification";
            $auditEntry->objectClass = "digitalResource/repository";
            $auditEntry->objectId = $repository->repositoryId;
            $auditEntry->message = "Repositry updated";
            $auditEntry->action = 'digitalResource/repository/update';

            \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
        }

        return true;
    }

    /**
     * Get by id
     * @param string $repositoryId
     *
     * @return object
     */
    public function getById($repositoryId)
    {
        $repository = $this->sdoFactory->read("digitalResource/repository", $repositoryId);
        $repository->repositoryParam = $this->sdoFactory->readChildren("digitalResource/repositoryParam", $repository);

        return $repository;
    }

    /**
     * Open repository service for given mode
     * @param string $repositoryId
     *
     * @return object The repository with its service
     */
    public function openRepository($repositoryId)
    {
        $repository = $this->sdoFactory->read("digitalResource/repository", $repositoryId);
        $repository->repositoryParam = $this->sdoFactory->readChildren("digitalResource/repositoryParam", $repository);

        $repositoryService = $this->getService($repository);
        $repository->setService($repositoryService);

        return $repository;
    }

    /**
     * Store a resource on repository (after opening it)
     * @param digitalResource/repository      $repository
     * @param digitalResource/digitalResource $resource
     * @param string                          $collection The name of a colection/bucket/directory to store resources in
     *
     * @return digitalResource/address
     */
    public function storeResource($repository, $resource, $collection = null)
    {
        $repositoryService = $repository->getService();

        $contents = $resource->getContents();

        if ($metadata = $resource->getMetadata()) {
            $uri = $repositoryService->create($contents, $metadata, $collection);
        } else {
            $uri = $repositoryService->create($contents, null, $collection);
        }

        if (!$uri) {
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "digitalResource/repositoryStoreResourceException";
                $auditEntry->objectClass = "digitalResource/repository";
                $auditEntry->objectId = $repository->repositoryId;
                $auditEntry->message = "No address return for storage of resource in repository $repository->repositoryId";
                $auditEntry->action = 'digitalRessource/repository/storeResourceException';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            throw \laabs::newException("digitalResource/repositoryException", "No address return for storage of resource in repository".$repository->repositoryId);
        }

        $address = \laabs::newInstance("digitalResource/address");
        $address->resId = $resource->resId;
        $address->repositoryId = $repository->repositoryId;
        $address->path = $uri;
        $address->created = \laabs::newTimestamp();

        // Store repository and service on address for rollback purpose
        $address->repository = $repository;

        $this->sdoFactory->create($address);

        return $address;
    }

    /**
     * Rollback storage transaction
     * @param digitalResource/address $address
     */
    public function rollbackStorage($address)
    {
        $repositoryService = $address->repository->getService();
        $repositoryService->delete($address->path);
    }

    /**
     * Store a resource contents on repo (after opening it)
     * @param digitalResource/repository      $repository
     * @param digitalResource/digitalResource $address
     *
     * @return string
     */
    public function retrieveContents($repository, $address)
    {
        try {
            $repositoryService = $repository->getService();
            $contents = $repositoryService->read($address->path);
        } catch (\Exception $e) {
            if (\laabs::hasBundle('audit')) {
                $auditEntry = \laabs::newInstance('audit/entry');
                $auditEntry->entryType = "digitalResource/repositoryStoreResourceException";
                $auditEntry->objectClass = "digitalResource/repository";
                $auditEntry->objectId = $repository->repositoryId;
                $auditEntry->message = "Resource not available.";
                $auditEntry->action = 'digitalRessource/repository/retrieveContentsException';

                \laabs::notify(\bundle\audit\AUDIT_ENTRY, $auditEntry);
            }
            throw \laabs::newException("digitalResource/repositoryException", "Resource not available.");
        }

        return $contents;
    }

    /**
     * Get repo service
     * @param digitalResource/repository $repository The repository object
     *
     * @return object The repository service object (implements dependency/repository/repositoryInterface)
     */
    protected function getService($repository)
    {
        $repositoryDependency = \laabs::dependency('repository');

        $repositoryServiceName = LAABS_ADAPTER.LAABS_URI_SEPARATOR.$repository->repositoryType.LAABS_URI_SEPARATOR."Repository";

        $repositoryServiceArgs['name'] = $repository->repositoryUri;

        if (count($repository->repositoryParam)) {
            $repositoryServiceArgs['options'] = $repository->repositoryParam;
        }

        return $repositoryDependency->callService($repositoryServiceName, $repositoryServiceArgs);
    }
}
