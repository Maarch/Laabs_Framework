<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications fonctionnelles](#spécifications-fonctionnelles)
    - [Modèle statique](#modèle-statique)
        - [Classe digitalResource](#classe-digitalresource)
        - [Classe format](#classe-format)
        - [Classe address](#classe-address)
        - [Classe repository](#classe-repository)
        - [Classe repositoryParam](#classe-repositoryparam)
        - [Classe cluster](#classe-cluster)
        - [Classe clusterRepository](#classe-clusterrepository)
    - [Messages](#messages)
        - [Création et modification de dépôt](#création-et-modification-de-dépôt)
        - [Création et modification de grappe de dépôts](#création-et-modification-de-grappe-de-dépôts)
        - [Stockage de ressource](#stockage-de-ressource)
        - [Ajout et suppression de copie de ressource](#ajout-et-suppression-de-copie-de-ressource)
        - [Migration de grappe de dépôts](#migration-de-grappe-de-dépôts)
    - [Interface Homme-Machine](#interface-homme-machine)
    - [Classes participantes](#classes-participantes)
        - [Contrôleur resource](#contrôleur-resource)
        - [Contrôleur store](#contrôleur-store)
        - [Contrôleur retrieve](#contrôleur-retrieve)
        - [Contrôleur modify](#contrôleur-modify)
        - [Contrôleur delete](#contrôleur-delete)
        - [Contrôleur copy](#contrôleur-copy)
        - [Contrôleur migrate](#contrôleur-migrate)
        - [Package dependency/sdo](#package-dependencysdo)
        - [Package dependency/repository](#package-dependencyrepository)

<!-- /MarkdownTOC -->

# Spécifications fonctionnelles
Ce chapitre fournit la description de l’implémentation fournie par Maarch Bundle Digital Resource.

## Modèle statique
Le diagramme ci-dessous représente les classes statiques qui entrent en jeu dans les différentes activités liées à la gestion des ressources numériques.

![Static Model](img/staticModel.png)  

### Classe digitalResource
Cette classe représente la ressource numérique avec les informations techniques liées.

|Propriété      |Description                                        |Type       |Requis |
|---------------|---------------------------------------------------|-----------|:-----:|
|**resId        ** |Clé primaire. Identifiant unique de la ressource   |id         |O      |
|**clusterId    ** |Identifiant de la grappe de dépôts de stockage     |id         |O      |
|**size         ** |Taille de la ressource en octets                   |integer    |O      |
|**puid         ** |Identifiant PRONOM du format                       |string     |N      |
|**mimetype     ** |Type MIME                                          |string     |O      |
|**fileExtension** |Extension de fichier associée                      |string     |N      |
|**fileName     ** |Nom de fichier associé                             |string     |N      |
|**hash         ** |Empreinte numérique de la ressource                |string     |O      |
|**hashAlgorithm** |Algorithme de calcul de l’empreinte (cf Annexe 1)  |string     |O      |
|**created      ** |Date et heure du stockage dans le système          |timestamp  |O      |
|**createdBy    ** |Identifiant de l’utilisateur qui a stocké          |id         |N      |
|**modified     ** |Date et heure de dernière modification             |timestamp  |N      |
|**modifiedBy   ** |Identifiant de l’utilisateur qui a modifié         |id         |N      |


### Classe format
Cette classe représente le format d’une ressource numérique, c’est-à-dire les informations qui permettent de décoder et convertir le contenu ou d’en fournir une représentation à l’utilisateur.

|Propriété      |Description                                        |Type       |Requis |
|---------------|---------------------------------------------------|-----------|:-----:|
|**puid       **|Identifiant unique PRONOM du format                |string     |O      |
|**name       **|Nom du format                                      |string     |O      |
|**version    **|Version du format                                  |string     |N      |
|**mimetype   **|Types MIME associés au format                      |string     |N      |
|**extension  **|Extensions de fichier associées au format          |string     |N      |


### Classe address
Cette classe représente les adresses logiques de stockage des ressources sur les dépôts.

|Propriété       |Description                                |Type       |Requis |
|----------------|-------------------------------------------|-----------|:-----:|
|**resId       **|Identifiant de la ressource stockée        |id         |O      |
|**repositoryId**|Identifiant du dépôt                       |id         |O      |
|**address     **|Version du format                          |string     |O      |
|**created     **|Date et heure du stockage dans le dépôt    |timestamp  |O      |
|**createdBy   **|Identifiant de l’utilisateur qui a stocké  |id         |N      |

### Classe repository
Cette classe représente le site de stockage ou dépôt.

|Propriété      |Description                                    |Type       |Requis |
|---------------|-----------------------------------------------|-----------|:-----:|
|**repositoryId  **|Identifiant de l’entrepôt de stockage       |id         |O      |
|**repositoryType**|Type d’entrepôt, désignant la classe de service associée dans la dépendance repository  |string |O      |
|**repositoryName**|Nom d’instanciation du service d’entrepôt : chaîne de connexion, chemin répertoire, URL |string |O      |
|**enabled       **|Indique si le dépôt est disponible et actif |boolean    |N      |

### Classe repositoryParam
Paramètre d’instanciation du service de dépôt.

|Propriété       |Description                                |Type       |Requis |
|----------------|-------------------------------------------|-----------|:-----:|
|**repositoryId**|Identifiant de l’entrepôt de stockage      |id         |O
|**paramName   **|Nom du paramètre                           |string     |O
|**paramValue  **|Valeur du paramètre                        |string     |O

### Classe cluster
Grappe de dépôts.

|Propriété      |Description                                |Type       |Requis |
|---------------|-------------------------------------------|-----------|:-----:|
|**clusterId  **|Identifiant de la grappe d’entrepôts de stockage   |id     |O
|**name       **|Nom de la grappe                                   |string |O

### Classe clusterRepository
Dépôt de la grappe.

|Propriété      |Description                                |Type       |Requis |
|---------------|-------------------------------------------|-----------|:-----:|
|**clusterId     **|Identifiant de la grappe de dépôts de stockage |id     |O
|**repositoryId  **|Identifiant du dépôt de stockage               |id     |O
|**writePriority **|Priorité en écriture dans la grappe            |integer|O
|**readPriority  **|Priorité en lecture dans la grappe             |integer|O
|**deletePriority**|Priorité en suppression dans la grappe         |integer|O


## Messages
Ce chapitre décrit la structure des objets échangés dans les interactions entre les composants, notamment entre l’interface homme-machine et le contrôle métier au travers des couches d’interprétation et de présentation.

Il ne décrit que les membres de messages de types complexes qui n'appartiennent pas au modèle statique. Les arguments simples sont échangés tels quels entre les composants et ne requièrent pas de description particulière autre que celle des APIs.

### Création et modification de dépôt
Le message contient un objet qui représente le dépôt ainsi qu’un tableau associatif ou un objet qui contient la liste des paramètres associés :
 

Exemple en notation JSON:

    {
        'repository' : {
            'repositoryId' : 'myrepositoryid,
            'repositoryType' : 'fileSystem',
            'repositoryName' : '/srv/repositories/myrepo',
            'repositoryParam' : {
                'myparam1' : 'myvalue1',
                'myparam2' : 'myvalue2'
            }
        }
    }

 
### Création et modification de grappe de dépôts
Le message contient un objet qui représente la grappe de dépôts ainsi qu’un tableau d'objets qui des dépôts de la grappe associés :
 
Exemple en notation JSON:

    { 
        'cluster': {
            'clusterId' : 'myclusterid',
            'clusterName' : 'mycluster',
            'clusterRepository' : [
                {
                    'repositoryId' : 'myrepository1',
                    'readPriority' : 1,
                    'writePriority' : 1,
                    'deletePriority' : 1
                },
                {
                    'repositoryId' : 'myrepository2',
                    'readPriority' : 2,
                    'writePriority' : 1,
                    'deletePriority' : 2
                }
            ]
        }
    }

 
### Stockage de ressource
Le message contient trois éléments:
*   La représentation de la ressource à stocker
*   Optionnel: un objet de métadonnées associées
*   L'identifiant de la grappe de dépôts à utiliser

Les modes de représentation de la ressource sont les suivants :
*   Un chemin vers le fichier dans le système de fichiers
*   Une chaîne binaire du contenu
*   Une ressource PHP vers le flux de données binaire
*   Un objet de classe digitalResource restreint aux propriétés suivantes :
 

Exemples en notation JSON:

    {
        'filename' : '/path/to_my/file',
        'metadata' : {
            /** Business specific model **/
        },
        'clusterId' : 'myclusterid'
    }

    {
        'resource' : {
                'size' : 82735,
                'puid' : 'fmt/95',
                'mimetype' : 'application/pdf',
                'fileExtension' : 'pdf',
                'filename' : 'mypdffile'
        },
        'metadata' : {
            /** Business specific model **/
        },
        'clusterId' : 'myclusterid'
    }
 
### Ajout et suppression de copie de ressource

    {
        'resId : 'myresid',
        'clusterId' : 'myclusterid'
    }

 
### Migration de grappe de dépôts


  
## Interface Homme-Machine
Ce chapitre décrit le modèle d'interface homme-machine, c'est-à-dire la présentation faire à l'utilisateur des messages réponse de l'application ainsi que les opérations de navigation possibles à partir de cette présentation.

## Classes participantes
Ce chapitre effectue la jonction entre la modélisation (les cas d'utilisation, le modèle du domaine et l'IHM) et la conception logicielle utilisée pour l'implémentation mais non détaillée dans ce document (classes de conception, diagrammes d'interaction).

![Classes participantes](img/involvedClasses.png) 

### Contrôleur resource
### Contrôleur store
### Contrôleur retrieve
### Contrôleur modify
### Contrôleur delete
### Contrôleur copy
### Contrôleur migrate

### Package dependency/sdo
Ce package fournit les classes de service nécessaire à la gestion du stockage persistant des données  structurées. 

Il est utilisé par les contrôleurs du package digitalResource pour toutes les opérations de création, de lecture, de modification et de suppression des objets de données et de leurs propriétés, à l’exclusion des ressources et de leurs métadonnées qui ne sont pas vues dans le package comme des informations structurées mais des paquets de données.

Il s’agit de la couche d’abstraction qui utilise le modèle de données de l’application pour gérer les opérations de base sur le stockage persistant indépendamment de la technologie de stockage : moteur de base de données relationnelle, base noSQL, XML, fichier à plat, etc.

Le service principalement utilisé est Factory et les méthodes associées aux opérations de base sur la persistance des données structurées :
*   Create : crée une nouvelle représentation d’un objet dans le stockage persistant
*   Read : lit un objet à partir du stockage persistant
*   Update : modifie un objet dans le stockage persistant
*   Delete : supprime un objet du stockage persistant

### Package dependency/repository
Ce package fournit les classes de service nécessaires à la gestion du stockage persistant des données non structurées.

Il est utilisé par les contrôleurs du package digitalResource pour toutes les opérations de création, de lecture, de modification et de suppression des ressources et de leurs métadonnées.

Le service utilisé dépend de la technologie de stockage associée à la configuration des dépôts, mais tous doivent utiliser une interface commune aux services de dépôts et fournir les méthodes associées aux opérations de base sur la persistance des données non structurées:
*   Create : crée un objet dans le stockage persistant
*   Read : lit un objet à partir du stockage persistant
*   Update : modifie un objet dans le stockage persistant
*   Delete : supprime un objet du stockage persistant