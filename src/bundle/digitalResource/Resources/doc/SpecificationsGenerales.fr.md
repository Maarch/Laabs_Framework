<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications Générales](#spécifications-générales)
    - [Introduction](#introduction)
        - [Domaine d'application](#domaine-dapplication)
        - [Termes et définitions](#termes-et-définitions)
    - [Contextualisation et modélisation](#contextualisation-et-modélisation)
        - [Les acteurs](#les-acteurs)
        - [Les fonctions](#les-fonctions)
        - [Les entités](#les-entités)

<!-- /MarkdownTOC -->
# Spécifications Générales

## Introduction
### Domaine d'application
Le présent document décrit les spécifications du paquet Digital Resource développé par Maarch pour l'implémentation des fonctionnalités liées à la gestion des ressources numériques dans les applications de gestion électronique de documents ou toute autre application qui requiert des fonctionnalités de stockage et restitution de ressources numériques.

Le but de cette documentation est de fournir toutes les informations nécessaires à la compréhension du fonctionnement du paquet Maarch Digital Resource afin 
* de valider l'adéquation de la solution aux besoins de l‘organisation
* de démontrer comment elle peut être utilisée dans le développement d'une application qui utilise des ressources numériques

Le document est destiné :
*   aux directions informatiques des organisations souhaitant mettre en œuvre un tel système 
*   aux développeurs qui utiliseront le paquet dans leurs applications

### Termes et définitions
__Ressource numérique (ou ressource électronique) :__ Document encodé utilisable par le système d’information et considéré comme une unité de traitement. Une ressource électronique peut contenir du texte, de l'image fixe ou animée, du son ou tout autre type d’information qui peut être décodée et exploitée par le système informatique. 

__Métadonnée :__ Littéralement « les données sur les données ». Ce sont les informations sur la ressource numérique et son contenu. Les métadonnées varient en nombre et en nature en fonction des besoins adressés par l’application. On peut globalement les classer en catégories :
*   Les métadonnées techniques fournissent les informations nécessaires à l’utilisation de la ressource par le système : format de fichier, encodage, taille
*   Les métadonnées descriptives décrivent et identifient la ressource pour la recherche et la qualification fonctionnelle du contenu
*   Les métadonnées structurelles décrivent la structure interne de la ressource (pages, titres et références, index…) ainsi que les relations externes entre les ressource (versions d’un même document, utilisation de l’une dans l’autre)
*   Les métadonnées administratives permettent la gestion à court, moyen ou long terme des ressources par le système ou la personne qui a en charge leur gestion : autorisations d’accès, contrôle de qualité, actions de conservation, etc.


## Contextualisation et modélisation

### Les acteurs
Un acteur représente un ensemble cohérent de rôles vis-à-vis du système d’information. L'acteur ne doit pas être confondu avec l'utilisateur qui est une personne physique ou morale identifiée dans le dispositif. L’utilisateur peut en effet assurer différents rôles suivant la transaction utilisée, et ainsi être représenté par plusieurs acteurs. 

#### Propriétaire
Il détient les droits intellectuels sur la ressource. Ceci peut inclure les droits de diffusion et d’exploitation du contenu de la ressource. Il s’agit par exemple la société de production ou l’organisme d’origine, l’auteur ou l’éditeur.

#### Lecteur
C’est l’ensemble des personnes morales ou physiques qui ont accès à la ressource numérique et à son contenu. Ce sont, dans notre contexte, les utilisateurs de l’application qui ont les autorisations nécessaires pour afficher ou charger la ressource sur leur système d’information.


### Les fonctions

![Use cases](img/useCases.png)

#### Stocker
C’est l’action par le propriétaire de mettre les ressources numériques dans le système d’information afin qu’elles soient conservées pour leur utilisation.
Cette procédure permet au propriétaire de la ressource d’effectuer un versement sécurisé de cette dernière dans le système. 

![Use case - Stocker](img/useCase-Stocker.png)

##### Flux standard
Lors du stockage, le propriétaire transmet au système les informations nécessaires au stockage d’une nouvelle ressource :
*   Les ressources numériques à archiver
*   Les métadonnées associées
*   L’adresse logique de stockage souhaité

Les ressources peuvent être transmises sous plusieurs formes :
*   Un chemin UNC vers un fichier dans un système de fichiers ou une URL
*   Le contenu de la ressource en chaîne de données binaires
*   Le contenu en ressource de type flux de données binaires

Le système contrôle la validité de la demande par rapport aux exigences techniques et fonctionnelles définies dans le système : 
*   validité du format des ressources à stocker, 
*   respect de quotas éventuels portant sur la volumétrie ou la quantité d’informations transmises, etc.
*   disponibilité du système pour assurer le stockage dans le respect des normes et procédures décrites

Une fois la demande contrôlée et acceptée, le système fournit un identifiant unique à la ressource, calcule son empreinte numérique et procède à l’enregistrement sécurisé des informations :
*   Stockage de la ressource numérique et de ses métadonnées sur les dépôts désignés
*   Enregistrement de la ressource numérique : identifiant unique, adresse logique de stockage sur les dépôts, métadonnées techniques
*   Le cas échéant écriture de l’événement dans le journal d’audit

Le stockage dans le système est alors effectif.

##### Flux alternatif
Versement par lot 
Le propriétaire transmet des informations qui représentent un lot cohérent de nouvelles ressources. Dans ce cas les opérations du flux standard sont réitérées pour chaque paquet d’information. Si une exception est rencontrée, c’est l’intégralité du lot qui est rejetée.

##### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement, d’une ressource unitaire ou du lot entier. Aucune information ne sera inscrite : la ou les ressources électroniques et leurs métadonnées ne seront pas écrites sur les dépôts et il n’y aura aucune entrée dans le journal d’audit.

###### Format non déterminé ou non autorisé
Le format des données numériques de la ressource fournie n’a pu être déterminé, ou bien il ne fait pas partie des formats autorisés.
###### Taille de fichier excédant la taille maximale autorisée
La taille de la ressource numérique de la ressource  excède la taille maximale autorisée par les accords. 
###### Métadonnées invalides
Les métadonnées transmises ne respectent pas le format attendu. 
###### Système indisponible
Le système n’est pas en mesure d’assurer toutes les fonctions nécessaires au respect des accords, normes ou procédures définies (dépôts définis et disponibles, métadonnées). 
###### Données non inscriptibles
Les données transmises n’ont pas pu être enregistrées dans au moins l’un des dépôts, qui a rencontré une erreur lors du processus d’écriture. 

##### Points d’inclusion
###### Stocker un fichier
La ressource à stocker est fournie sous la forme d’un chemin sur le système de fichiers.
###### Stocker un contenu
La ressource à stocker est fournie sous la forme d’un flot de caractères binaire.
###### Stocker un flux
La ressource à stocker est fournie sous la forme d’un gestionnaire de flux de données.


#### Consulter
C’est l’action par un lecteur d’accéder à la ressource pour en consulter le contenu et/ou de la télécharger afin d’en conserver une copie sur son propre système.

![Use case - Consulter](img/useCase-Consulter.png)

##### Flux standard
Lors de la consultation, le lecteur demande au système de lui présenter les informations constituant  la ressource :
*   Le contenu encodé
*   Les métadonnées associées
*   Les informations techniques

Le système effectue la récupération des informations et les contrôle : 
*   L’empreinte de la ressource numérique est conforme à celle enregistrée
*   Les métadonnées sont présentes

Il transmet en retour au lecteur la ressource.

##### Flux alternatif
###### Consultation par lot 
Le lecteur demande à accéder à un lot cohérent de ressources. Dans ce cas les opérations du flux standard sont réitérées pour chaque paquet d’information. Si une exception est rencontrée, c’est l’intégralité du lot qui est non retourné.

##### Flux d’exception
Tous les flux d’exception provoquent une interruption du traitement, d’une ressource unitaire ou du lot entier. Aucune information ne sera délivrée : la ou les ressources électroniques et leurs métadonnées ne seront pas retournées.
###### Empreinte non valide
L’empreinte numérique de la ressource récupérée sur les dépôts ne correspond pas à celle de la ressource stockée.
###### Système indisponible
Le système n’est pas en mesure d’assurer toutes les fonctions nécessaires au respect des accords, normes ou procédures définies (dépôts définis et disponibles, métadonnées). 
###### Données non lisibles ou absentes
Les données demandées n’ont pas pu être récupérées dans au moins l’un des dépôts.  

####  Modifier
Dans ce cas d’utilisation, le propriétaire va soumettre une nouvelle version de la ressource numérique ou de ses métadonnées qui doivent remplacer celles stockées précédemment.

 ![Use case - Modifier](img/useCase-Modifier.png)

##### Flux standard
Il est relativement similaire au flux de stockage, à deux différences près :
*   L’acteur doit fournir l’identifiant unique de la ressource à modifier
*   Il peut fournir la ressource numérique ET/OU les métadonnées à modifier

##### Flux alternatif
La modification ne permet pas de traitement de lot.

##### Flux d’exception
Ce cas inclut tous les flux d’exception du stockage. 

 
####  Détruire
Dans ce cas d’utilisation, le propriétaire demande la destruction de la ressource numérique par le système.

![Use case - Détruire](img/useCase-Détruire.png)

##### Flux standard
Le propriétaire demande au système la suppression de la ressource en lui fournissant l’identifiant unique de ressource associé.

Le système accède aux dépôts et supprime la ressource et les métadonnées, ainsi que l’enregistrement des informations concernant la ressource dans le système.

##### Flux alternatif
La suppression ne permet pas de traitement par lot.

##### Flux d’exception
###### Système indisponible
Le système n’est pas en mesure d’assurer toutes les fonctions nécessaires au respect des accords, normes ou procédures définies (dépôts définis et disponibles, métadonnées). 
###### Données non lisibles ou absentes
Les données à supprimer n’ont pas pu être retrouvées dans au moins l’un des dépôts.

 

####  Administrer le stockage
Ces procédures permettent à l’administrateur d’administrer et paramétrer les aspects techniques liés au stockage et à la gestion des ressources numériques en trois points :
*   La déclaration et le paramétrage des dépôts
*   La définition des grappes de dépôts
*   La gestion des formats de fichiers

![Use case - Administrer](img/useCase-Administrer.png)

##### Administrer les dépôts
###### Flux standards
L’application permet à l’administrateur d’ajouter, de modifier et de supprimer des dépôts. Pour l’ajout et la modification on fournit les informations relatives au dépôt :
*   Son nom
*   Son type
*   Les paramètres du service associé

###### Flux d’exception
Les informations de dépôt fournies sont invalides.
Le dépôt ne peut être supprimé car il est utilisé dans une grappe.

##### Administrer les grappes de dépôts
###### Flux standards
L’application permet à l’administrateur d’ajouter, de modifier et de supprimer des grappes de dépôts. Pour l’ajout et la modification on fournit les informations relatives à la grappe de dépôt :
*   Son nom
*   La liste des dépôts associés avec leurs priorités en lecture, écriture et suppression

###### Flux d’exception
* Les informations de grappe de dépôt fournies sont invalides.
* La grappe ne peut être supprimée car elle est utilisée par au moins une ressource numérique.




#### Gérer le stockage
Ces cas d’utilisation permettent à l’administrateur du stockage de faire évoluer dans le temps les sites de stockage des ressources de trois manières :
*   En créant de nouvelles copies dans la grappe de dépôts définie pour la ressource
*   En supprimant des copies dans la grappe de dépôts définie pour la ressource
*   En migrant la ressource vers une nouvelle grappe

![Use case - Gérer](img/useCase-Gérer.png) 

##### Afficher les informations de stockage
L'administrateur 

##### Créer une copie
L’administrateur demande au système de créer une nouvelle copie de la ressource sur les dépôts de la grappe.

Le système vérifie la disponibilité des services pour assurer le stockage d’au moins une nouvelle copie dans le respect des normes et procédures décrites.

Une fois la demande contrôlée et acceptée, le système procède à l’enregistrement sécurisé des informations :
*   Stockage de la ressource numérique et de ses métadonnées sur les nouveaux dépôts trouvés
*   Ajout des adresses correspondantes

##### Supprimer une copie
L’administrateur demande au système de supprimer une copie de la ressource sur les dépôts de la grappe.

Le système vérifie la disponibilité de la ressource sur au moins deux adresses de priorité de suppression différentes pour assurer que le stockage conserve au moins une copie de la ressource, puis supprimer le stockage et l’adresse correspondante. En effet, la suppression définitive de toutes les copies de la ressource n’est disponible qu’au travers du cas d’utilisation « Supprimer ».

##### Migrer de grappe
L’administrateur demande au système de modifier la grappe de dépôts utilisée pour une ressource.

Cette opération s’apparente au cas « Stocker » à la différence qu’il porte sur une ressource existante.

Le système vérifie la disponibilité des services pour assurer le nouveau stockage, effectue le versement des données et supprimer les anciennes adresses de stockage. 



### Les entités

Les entités sont les représentations dans le système des informations manipulées, structurées sous forme d’objets.

![Business Model](img/businessModel.png)  
 
#### Ressource numérique
La classe représente une ressource numérique gérée par le système. Elle comporte un identifiant unique ainsi que les données techniques associées au contenu, notamment :
*   l’empreinte numérique associée
*   la taille des données (en octets)
*   le type MIME
*   l’extension de fichier
*   le nom de fichier associé si disponible
*   l’identifiant de format si disponible

#### Les métadonnées
Ce sont les informations sur la ressource numérique et son contenu. Les métadonnées varient en nombre et en nature en fonction des besoins adressés par l’application. Le paquet ne s’occupe pas de leur constitution, leur validation ou de leur gestion. Elles sont vues comme un ensemble opaque destiné à être manipulé selon les mêmes règles lors des opérations sur les données : Stockage, modification, consultation, suppression.


#### Format
Cette classe représente un format de ressource numérique. Elle comporte un identifiant unique, un nom de format, une version, la liste des types MIME associés et la liste des extensions de fichier associés. 

L’identifiant unique de format utilisé par Maarch est celui du registre PRONOM du Département de Conservation Numérique des Ressources Nationales du Royaume-Uni (Digital Preservation Department of the UK National Ressources).

Une ressource numérique peut être reliée avec son format si l’identifiant de format PRONOM a été enregistré dans ses données techniques.

#### Adresse de stockage
Cette classe représente une adresse logique de stockage de la ressource numérique sur les dépôts. Elle comporte l’identifiant unique de la ressource, l’identifiant du dépôt ainsi que l’adresse logique fournie par le site lors du dépôt. 

Chaque ressource numérique sera stockée sur au moins un site de stockage lors du versement initial, et par la suite d’autres copies pourront être faites durant le cycle de vie de la ressource. Ainsi une ressource possède potentiellement plusieurs adresses logiques de stockage durant sa vie.

#### Dépôt
Cette classe représente un dépôt de stockage persistant des ressources numériques et de leurs métadonnées. Elle comporte un identifiant unique, un code de type de technologie de stockage et son adresse ou son nom.

Ces deux dernières informations permettent d’accéder et d’instancier la classe de service qui implémente la technologie de stockage.

Quelques exemples de type de dépôt:
*   Système de fichiers (NAS)
*   Serveur Centera EMC
*   WORM
*   Base de données capable de stocker des objets binaires larges ou BLOBs
*   Service de stockage sécurisé en ligne, dans le nuage (oodrive, Hexabuilder)

Un dépôt possède en plus un tableau de paramètres, qui seront utilisés lors de l’instanciation du service de stockage correspondant.

La ressource numérique est donc liée le dépôt de stockage par une adresse de stockage sur ce dernier.

#### Grappe de dépôts
Cette classe décrit un groupe sur lequel pourra être stockée la ressource numérique. Elle présente en particulier une association avec un certain nombre de dépôts, en attribuant à chacun des priorités de stockage, de lecture et de suppression. 

Il doit exister au moins une association avec un dépôt par grappe. 
