<meta charset="UTF-8">
<!-- MarkdownTOC -->

- [Spécifications techniques](#spécifications-techniques)
    - [Description des APIs](#description-des-apis)
        - [Contrôleur resource](#contrôleur-resource)
        - [Contrôleur store](#contrôleur-store)
        - [Contrôleur retrieve](#contrôleur-retrieve)
        - [Contrôleur modify](#contrôleur-modify)
        - [Contrôleur delete](#contrôleur-delete)
        - [Contrôleur copy](#contrôleur-copy)
        - [Contrôleur migrate](#contrôleur-migrate)
    - [Configuration](#configuration)
        - [Directives de configuration](#directives-de-configuration)
        - [Définition](#définition)

<!-- /MarkdownTOC -->
# Spécifications techniques

## Description des APIs

![Involved Model](img/involvedModel.png)  

### Contrôleur resource
C’est le contrôleur métier qui gère la création de nouvelles ressources sous la forme d’instances de la classe digitalResource.

#### Action file
Crée une nouvelle ressource à partir d’un fichier dont le chemin complet et accessible est fourni en paramètre. Elle collecte les informations sur la ressource identifiée par le chemin, crée une nouvelle ressource en mémoire et retourne un nouvel objet digitalResource.

##### Description
    digitalResource/digitalResource file ( string $filename [, boolean $withPuid = false] )

##### Paramètres 
* **filename** Chaîne de caractères. Nom complet, chemin UNC ou URL du fichier de ressource à créer
* **withPuid** Booléen. Si actif, déclenche la détection de format PRONOM. Par défaut inactif 

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource qui comporte toutes les propriétés techniques demandées ainsi que le pointeur vers la ressource.


#### Action contents
Crée une nouvelle ressource à partir d’un contenu fourni en paramètre. Elle collecte les informations sur la ressource et retourne un nouvel objet digitalResource.

##### Description
    digitalResource/digitalResource contents ( string $contents [, boolean $withPuid = false] )

##### Paramètres 
* **contents** : Chaîne de caractère. Contenu binaire de la ressource à créer | O
* **withPuid** : Booleén. Si actif, déclenche la détection de format PRONOM. Défaut: inactif.

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource qui comporte toutes les propriétés techniques demandées ainsi que le pointeur vers la ressource en mémoire.

#### Action stream
Crée une nouvelle ressource à partir d’un gestionnaire de flux fourni en paramètre. Elle collecte les informations sur la ressource et retourne un nouvel objet digitalResource.

##### Description
    digitalResource/digitalResource stream ( resource $stream [, boolean $withPuid = false] )
    
##### Paramètres 
* **stream** : Resource. Gestionnaire de flux vers la ressource à créer
* **withPuid** : Booleén. Si actif, déclenche la détection de format PRONOM

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource qui comporte toutes les propriétés techniques demandées ainsi que le pointeur vers la ressource en mémoire.

 
### Contrôleur store
C’est le contrôleur métier qui gère le stockage des ressources.

#### Action file
Stocke une nouvelle ressource à partir d’un fichier dont le chemin complet et accessible est fourni en paramètre. 

##### Description
    digitalResource/digitalResource file ( string $filename, object $metadata = false, string $clusterId )

##### Paramètres 
* **filename** : Chaîne de caractère. Nom complet, chemin UNC ou URL du fichier de ressource à stocker.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les métadonnées de la ressource.
* **clusterId** : id. Identifiant de la grappe de dépôts dans laquelle stocker la ressource.

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource avec ses adresses de stockage.


#### Action contents
Stocke une nouvelle ressource à partir du contenu binaire fourni en paramètre.

##### Description
    digitalResource/digitalResource contents ( string $contents, object $metadata = false, string $clusterId )

##### Paramètres 
* **contents** : Chaîne de caractère. Contenu binaire de la ressource à stocker.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les métadonnées de la ressource.
* **clusterId** : identifiant. Identifiant de la grappe de dépôts dans laquelle stocker la ressource.

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource avec ses adresses de stockage.

#### Action stream
Stocke une nouvelle ressource à partir d’un flot de données fourni en paramètre. 

##### Description
    digitalResource/digitalResource stream ( resource $stream, object $metadata = false, string $clusterId )

##### Paramètres 
* **stream** : Ressource. Flot de données de la ressource à stocker.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les métadonnées de la ressource.
* **clusterId** : identifiant. Identifiant de la grappe de dépôts dans laquelle stocker la ressource.

##### Valeur de retour
La méthode retourne un nouvel objet digitalResource avec ses adresses de stockage.

#### Action resource
Stocke une ressource fournie en paramètre. 

##### Description
    digitalResource/digitalResource stream ( digitalResource/digitalResourceInfo $resource, object $metadata = false, string $clusterId )

##### Paramètres 
* **resource** : digitalResource/digitalResourceInfo. Les informations disponibles sur la ressource à stocker. Doit contenir une propriété 'handler' qui est la resource php vers le gestionnaire de flux.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les métadonnées de la ressource.
* **clusterId** : identifiant. Identifiant de la grappe de dépôts dans laquelle stocker la ressource.

##### Valeur de retour
La méthode retourne l’objet digitalResource avec ses adresses de stockage.


#### Action fileCollection
Stocke une collection de nouvelles ressources à partir d’un tableau contenant des chemins de fichiers et des métadonnées. 
##### Paramètres 
* **files** : Tableau. En index 0, nom complet, chemin UNC ou URL du fichier de ressource à stocker. En index 1, métadonnées associées.
* **clusterId** : id. Identifiant de la grappe de dépôts dans laquelle stocker les ressources.

##### Valeur de retour
La méthode retourne un tableau des nouveaux objets digitalResource avec leurs adresses de stockage.

#### Action contentsCollection
Stocke une collection de nouvelles ressources à partir d’un tableau contenant des contenus binaires et des métadonnées. 
##### Paramètres 
* **contents** : Tableau. En index 0, contenu binaire de la ressource à stocker. En index 1, métadonnées associées.
* **clusterId** : id. Identifiant de la grappe de dépôts dans laquelle stocker les ressources.

##### Valeur de retour
La méthode retourne un tableau des nouveaux objets digitalResource avec leurs adresses de stockage.

#### Action resourceCollection
Stocke une collection de ressources à partir d’un tableau contenant des ressources et des métadonnées. 
##### Paramètres 
* **resources** : Tableau. En index 0, la ressource à stocker. En index 1, métadonnées associées.
* **clusterId** : id. Identifiant de la grappe de dépôts dans laquelle stocker les ressources.

##### Valeur de retour
La méthode retourne un tableau des objets digitalResource avec leurs adresses de stockage.

 
### Contrôleur retrieve
Fournit les méthodes de récupération des ressources à partir du stockage persistant.

#### Action resource
Récupère un objet ressource à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource à récupérer.

##### Valeur de retour
La méthode retourne l’objet digitalResource avec ses adresses de stockage et le contenu de la ressource sous la forme d’un flot de données en mémoire.

#### Action contents
Récupère le contenu d’un objet ressource à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource dont le contenu est à récupérer.

##### Valeur de retour
La méthode retourne le contenu de la ressource sous la forme d’une chaîne de caractères binaire.

#### Action stream
Récupère le gestionnaire de flux de données d’un objet ressource à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource dont le contenu est à récupérer.

##### Valeur de retour
La méthode retourne le contenu de la ressource sous la forme d’un flot de données.

 
### Contrôleur modify
C’est le contrôleur métier qui gère la modification des ressources stockées
#### Action file
Modifie la ressource à partir d’un fichier dont le chemin complet et accessible est fourni en paramètre. 
##### Paramètres 
* **resId** : Identifiant de la ressource à modifier.
* **filename** : Chaîne de caractère. Nom complet, chemin UNC ou URL du fichier de ressource à stocker.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les nouvelles métadonnées de la ressource.

##### Valeur de retour
La méthode retourne un booléen.

#### Action contents
Modifie la ressource à partir du contenu binaire fourni en paramètre. 
##### Paramètres 
* **resId** : Identifiant de la ressource à modifier.
* **contents** : Chaîne de caractère. Contenu binaire de la ressource à modifier.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les nouvelles métadonnées de la ressource.

##### Valeur de retour
La méthode retourne un booléen.

#### Action stream
Modifie la ressource à partir d’un flot de données fourni en paramètre. 
##### Paramètres 
* **resId** : Identifiant de la ressource à modifier.
* **stream** : Ressource. Flot de données de la ressource à modifier.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les nouvelles métadonnées de la ressource.

##### Valeur de retour
La méthode retourne un booléen.

#### Action resource
Modifie la ressource fournie en paramètre. 
##### Paramètres 
* **resource** : digitalResource/digitalResource. La ressource à modifier.
* **metadata** : Optionnel. Objet. Instance d’une classe du modèle de données de l’application qui représente les nouvelles métadonnées de la ressource.

##### Valeur de retour
La méthode retourne un booléen.

#### Action metadata
Modifie les métadonnées de la ressource.
##### Paramètres 
* **resId** : Identifiant de la ressource à modifier.
* **metadata** : Objet. Instance d’une classe du modèle de données de l’application qui représente les nouvelles métadonnées de la ressource.

##### Valeur de retour
La méthode retourne un booléen.
 
### Contrôleur delete
Fournit les méthodes de suppression des ressources dans le stockage persistant.
#### Action resource
Supprime un objet ressource à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource à récupérer.

##### Valeur de retour
La méthode retourne un booléen.

 
### Contrôleur copy
Fournit les méthodes de gestion des copies des ressources dans le stockage persistant.
#### Action create
Copie un objet ressource vers une nouvelle adresse de stockage à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource à copier.

##### Valeur de retour
La méthode retourne l’objet digitalResource/digitalResource avec les nouvelles adresses de stockage.

#### Action remove
Supprimer une copie de la ressource dans une adresse à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource.

##### Valeur de retour
La méthode retourne l’objet digitalResource/digitalResource avec les nouvelles adresses de stockage.

 
### Contrôleur migrate
Fournit les méthodes de gestion des migrations de grappe des ressources dans le stockage persistant.
#### Action resource
Déplace l’objet ressource d’une grappe de dépôt vers une nouvelle.
##### Paramètres 
* **resId** : Chaîne de caractère. Identifiant de la ressource à migrer.
* **clusterId** : identifiant. Identifiant de la nouvelle grappe de dépôts dans laquelle stocker la ressource.

##### Valeur de retour
La méthode retourne l’objet digitalResource/digitalResource avec les nouvelles adresses de stockage.

#### Action cluster
Supprimer une copie de toutes les ressources stockées dans une grappe à partir de son identifiant unique fourni en paramètre.
##### Paramètres 
* **fromClusterId** : identifiant. Identifiant de la grappe de dépôts à migrer
* **toClusterId** : identifiant. Identifiant de la nouvelle grappe de dépôts dans laquelle stocker les ressources.

##### Valeur de retour
La méthode retourne le nombre de ressources migrées.

 
## Configuration
### Directives de configuration
#### Configuration du paquet
Le paquet fournit un jeu de paramètres de configuration qui permettent de modifier le comportement de certaines fonctions lors de l’exécution.

Les directives correspondantes sont inscrites dans un fichier de configuration de l’application qui utilise le package sous la forme de paires clés/valeur au format Laabs Ini.

##### Paramètre hashAlgorithm
Chaîne de caractères. Indique l’algorithme à utiliser pour le calcul des empreintes numériques des nouvelles ressources stockées. Il doit figurer parmi les algorithmes supportés listes dans l’annexe 1.
##### Paramètre droidSignatureFile
Chaîne de caractères. Indique le chemin complet vers un fichier de signatures de formats PRONOM utilisé par le plugin de détection automatique des formats de fichiers. S’il est absent, le plugin ne sera pas utilisé, ce qui peut provoquer une exception si la détection automatique du format est requise.
##### Paramètre magicDatabaseFile
Chaîne de caractères. Indique le chemin complet vers un fichier de base de données au format magic utilisé par le module PHP de détermination du type MIME des fichiers. S’il est absent, la base de données par défaut configurée pour PHP est utilisée.
##### Paramètre allowedFormats
Chaîne de caractères. Liste les identifiants de format pronom autorisés pour les nouvelles ressources à stocker, séparés par un espace. Si la détection de format est désactivée, cela signifie que seul le versement de ressource dont le format est déjà identifié sera autorisé.

#### Configuration des dépendances
##### Dépendance Service Data Objects
##### Dépendance Repository 

### Définition
#### Routes

#### Jobs


