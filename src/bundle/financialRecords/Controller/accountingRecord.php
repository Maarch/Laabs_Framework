<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\financialRecords\Controller;

/**
 * Class accountingRecord records
 *
 * @package RecordsManagement
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class accountingRecord implements \bundle\recordsManagement\Controller\archiveDescriptionInterface
{
    /* Properties */

    public $sdoFactory;

    /**
     * Constructor of access control class
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get a  search result
     * @param string $docType
     * @param string $reference
     * @param date   $dateMin
     * @param date   $dateMax
     * @param int    $year
     * @param string $companyName
     * @param string $companyCode
     * @param string $thirdPartyCode
     * @param string $thirdPartyName
     * @param string $accountNumber
     *
     * @return bool
     */
    public function find($docType = null, $reference = null, $dateMin = null, $dateMax = null, $year = null, $companyName = null, $companyCode = null, $thirdPartyCode = null, $thirdPartyName = null, $accountNumber = null)
    {
        $queryParts = array();
        $queryParams = array();

        //$queryParts[] = \laabs::callService('auth/userAccount/readAccessrule_objectClass_', 'financialRecords/accountingRecord');

        if ($docType) {
            $queryParams['docType'] = $docType;
            $queryParts['docType'] = "docType = :docType";
        }
        if ($reference) {
            $queryParams['reference'] = $reference;
            $queryParts['reference'] = "reference = :reference";
        }
        if ($dateMin) {
            $queryParts['date'] = "date >= '$dateMin'";
        }
        if ($dateMax) {
            $queryParts['date'] = "date <= '$dateMax'";
        }
        if ($year) {
            $queryParts['year'] = "year = '$year'";
        }
        if ($companyName) {
            $queryParts['companyName'] = "companyName = '*$companyName*'";
        }
        if ($companyCode) {
            $queryParts['companyCode'] = "companyCode = '$companyCode'";
        }
        if ($thirdPartyName) {
            $queryParts['thirdPartyName'] = "thirdPartyName = '*$thirdPartyName*'";
        }
        if ($thirdPartyCode) {
            $queryParts['thirdPartyCode'] = "thirdPartyCode = '$thirdPartyCode'";
        }
        if ($accountNumber) {
            $queryParts['accountNumber'] = "accountNumber = '$accountNumber'";
        }

        $queryString = implode(' AND ', $queryParts);

        $accountingRecords = $this->sdoFactory->find("financialRecords/accountingRecordArchive", $queryString, $queryParams, false, 0, 100);

        return $accountingRecords;
    }

    /**
     * Create the requested accountingRecord
     * @param object $accountingRecord Acc document object
     *
     * @return boolean status of the query
     */
    public function create($accountingRecord)
    {
        $accountingRecord->docId = \laabs::newId();
        $accountingRecord->docType = "accountReceivableInvoice";
        if (!\laabs::validate($accountingRecord)) {
            throw \laabs::newException("financialRecords/accoutingRecordException", "Invalid accountingRecord document data.");
        }
        try {
            $this->sdoFactory->create($accountingRecord);
        } catch (\Exception $e) {
            throw \laabs::newException("financialRecords/accoutingRecordException", "Accouting record not created.");
        }

        return $accountingRecord;
    }

    /**
     * Read an accountingRecord with its archive identifier
     * @param id $docId
     *
     * @return financialRecords/accountingRecord
     */
    public function read($docId)
    {
        return $this->sdoFactory->read("financialRecords/accountingRecord", $docId);
    }

    /**
     * Update an accountingRecord
     * @param financialRecords/accountingRecord $accountingRecord
     *
     * @return bool
     */
    public function update($accountingRecord)
    {
        return false;
    }

    /**
     * Delete an accountingRecord with its doc identifier
     * @param id $docId
     *
     * @return boolean
     */
    public function delete($docId)
    {
        try {
            return $this->sdoFactory->delete($docId, "financialRecords/accountingRecord");
        } catch (\Exception $e) {
            throw \laabs::newException("financialRecords/accoutingRecordException", "Document not deleted.");
        }
    }

    /**
     * Initialise the description class with the rules of archival profile
     * @param Array $profileDescriptions Array of recordsManagement/profileDescription classe
     *
     * @return object accountingRecord
     */
    public function form($profileDescriptions)
    {
        $accountingRecord = \laabs::newInstance("financialRecords/accountingRecord");

        if ($profileDescriptions) {
            $requiredProperties = new \stdClass();
            $usedProperties = array();
            foreach ($profileDescriptions as $profileDescription) {
                $usedProperties[] = $profileDescription->propertyName;
                if ($profileDescription->required) {
                    $temp = (string) $profileDescription->propertyName;
                    $requiredProperties->$temp = true;
                }
            }

            foreach ($accountingRecord as $property => $value) {
                if (!in_array($property, $usedProperties)) {
                    unset($accountingRecord->$property);
                }
            }

            $accountingRecord->requiredProperties = $requiredProperties;
        }

        return $accountingRecord;
    }
}
