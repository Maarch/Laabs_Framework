<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\financialRecords\Controller;

/**
 * Class invoice
 *
 * @package RecordsManagement
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class invoice implements \bundle\recordsManagement\Controller\archiveDescriptionInterface
{
    /* Properties */

    public $sdoFactory;

    /**
     * Constructor of access control class
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get a  search result
     * @param string $documentType                 The document type
     * @param string $reference                    The reference
     * @param string $companyName                  The company name
     * @param string $companyRegistrationNumber    The company registration number
     * @param string $companyTaxIdentifier         The company tax indentifier
     * @param string $thirdPartyName               The third party name
     * @param string $thirdPartyContact            the third party contact
     * @param string $thirdPartyRegistrationNumber The third party registration number
     * @param string $thirdPartyTaxIdentifier      The third party tax identifier
     * @param string $currency                     The currency
     * @param string $purchaseOrderReference       The purchase order reference
     * @param string $accountingPeriod             The accounting period
     * @param string $minAmount                    The min amount
     * @param string $maxAmount                    the max amount
     * @param string $minDate                      The min date
     * @param string $maxDate                      The max date
     *
     * @return bool
     */
    public function find($documentType = null, $reference = null, $companyName = null, $companyRegistrationNumber = null, $companyTaxIdentifier = null, $thirdPartyName = null, $thirdPartyContact = null, $thirdPartyRegistrationNumber = null, $thirdPartyTaxIdentifier = null, $currency = null, $purchaseOrderReference = null, $accountingPeriod = null, $minAmount = null, $maxAmount = null, $minDate = null, $maxDate = null)
    {
        $queryParts = array();

        $queryParts[] = $this->auth->getUserAccessRule('financialRecords/invoice');

        if ($documentType) {
            $queryParts['documentType'] = "documentType = '*$documentType*'";
        }
        if ($reference) {
            $queryParts['reference'] = "reference = '*$reference*'";
        }
        if ($companyName) {
            $queryParts['companyName'] = "companyName = '*$companyName*'";
        }
        if ($thirdPartyName) {
            $queryParts['thirdPartyName'] = "thirdPartyName = '*$thirdPartyName*'";
        }
        if ($minDate) {
            $queryParts['minDate'] = "date >= '$minDate'";
        }
        if ($maxDate) {
            $queryParts['maxDate'] = "date <= '$maxDate'";
        }

        $queryString = implode(' AND ', $queryParts);

        $invoices = $this->sdoFactory->find("financialRecords/invoice", $queryString, null, false, 0, 100);

        return $invoices;
    }

    /**
     * Create the requested invoice
     * @param object $invoice Invoice object
     *
     * @return boolean status of the query
     */
    public function create($invoice)
    {
        $this->sdoFactory->create($invoice);

        return $invoice;
    }

    /**
     * Read an invoice with its archive identifier
     * @param id $archiveId
     *
     * @return financialRecords/invoice
     */
    public function read($archiveId)
    {
        return $this->sdoFactory->read("financialRecords/invoice", $archiveId);
    }

    /**
     * Update an invoice
     * @param financialRecords/invoice $invoice
     *
     * @return bool
     */
    public function update($invoice)
    {
        return false;
    }

    /**
     * Delete an invoice with its archive identifier
     * @param id $archiveId
     *
     * @return boolean
     */
    public function delete($archiveId)
    {
        return $this->sdoFactory->delete($archiveId, "financialRecords/invoice");
    }

    /**
     * Initialise the description class with the rules of archival profile
     * @param Array $profileDescriptions Array of recordsManagement/profileDescription classe
     *
     * @return object invoice
     */
    public function form($profileDescriptions)
    {
        $invoice = \laabs::newInstance("financialRecords/invoice");

        if ($profileDescriptions) {
            $requiredProperties = new \stdClass();
            $usedProperties = array();
            foreach ($profileDescriptions as $profileDescription) {
                $usedProperties[] = $profileDescription->propertyName;
                if ($profileDescription->required) {
                    $temp = (string) $profileDescription->propertyName;
                    $requiredProperties->$temp = true;
                }
            }

            foreach ($invoice as $property => $value) {
                if (!in_array($property, $usedProperties)) {
                    unset($invoice->$property);
                }
            }

            $invoice->requiredProperties = $requiredProperties;
        }

        return $invoice;
    }

    /**
     * Get archive by archive identifier
     * @param string $archiveId The archive identifier
     * @return type
     */
    public function getArchiveByArchiveId($archiveId)
    {
        return $this->sdoFactory->read("recordsManagement/archive", $archiveId);
    }
}
