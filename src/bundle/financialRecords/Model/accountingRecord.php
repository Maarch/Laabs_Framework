<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords\Model;
/**
 * Class model that represents accounting document indexes
 *
 * @package FinancialRecords
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 * 
 * @pkey [docId]
 * @xmlns fr maarch.org:laabs:financialRecords
 */
class accountingRecord
{
    /**
     * The doc identifier
     *
     * @var id
     */
    public $docId;
    
    /**
     * The type of document
     *
     * @var string
     */
    public $docType;

    /**
     * The document reference
     *
     * @var string
     * @xpath fr:reference
     */
    public $reference;

    /**
     * The document date
     *
     * @var date
     * @xpath fr:date
     */
    public $date;

    /**
     * The document year
     *
     * @var integer
     * @xpath fr:year
     */
    public $year;

    /**
     * The compagny code
     *
     * @var string
     * @xpath fr:companyCode
     */
    public $companyCode;

    /**
     * The compagny name
     *
     * @var string
     * @xpath fr:companyName
     */
    public $companyName;

    /**
     * The third party code
     *
     * @var string
     * @xpath fr:thirdPartyCode
     */
    public $thirdPartyCode;

    /**
     * The third party name
     *
     * @var string
     * @xpath fr:thirdPartyName
     */
    public $thirdPartyName;

    /**
     * The account number
     *
     * @var string
     * @xpath fr:accountNumber
     */
    public $accountNumber;


} // END class accounting 