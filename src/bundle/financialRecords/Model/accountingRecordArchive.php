<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle accountingRecord
 *
 * Bundle accountingRecord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle accountingRecord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle accountingRecord.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\financialRecords\Model;

/**
 * Classe du modele qui représente un contenu de description
 *
 * @package Financial records
 * @author  Prosper DE LAURE (Maarch) <prosper.delaure@maarch.org>
 *
 * @pkey [docId]
 */
class accountingRecordArchive
    extends accountingRecord
{
    /**
     * Register number of originator organisation
     *
     * @var string
     */
    public $originatorOrgRegNumber;

    /**
     * Register number of originator organisation
     *
     * @var string
     */
    public $archiveId;

    /**
     * Register number of depositor organisation
     *
     * @var string
     */
    public $depositorOrgRegNumber;

    /**
     * Register number of archiver organisation
     *
     * @var string
     */
    public $archiverOrgRegNumber;

    /**
     * The status
     *
     * @var string
     * @notempty
     * @enumeration [received, pending, preserved, frozen, disposable, disposed, restitued]
     */
    public $status;

    /**
     * The name of archival profile
     *
     * @var string
     */
    public $archivalProfileReference;

    /**
     * The name of archival agreement
     *
     * @var string
     */
    public $archivalAgreementReference;

    /**
     * The action to execute when the retention rule is over
     *
     * @var string
     */
    public $finalDisposition;

    /**
     * The disposal date of the archive
     *
     * @var date
     */
    public $disposalDate;

    /**
     * The name of description identifier
     *
     * @var id
     */
    public $descriptionId;
}
