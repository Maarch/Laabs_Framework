<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords\Model;
/**
 * Class model that represents an digital records of an invoice (accounts payable or receivable)
 *
 * @package financialRecords
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 * 
 * @pkey [archiveId]
 */
class invoice
{
    /**
     * The record identifier
     *
     * @var id
     */
    public $archiveId;
    
    /**
     * The type of document
     *
     * @var string
     */
    public $documentType;

    /**
     * The purchase order reference
     *
     * @var string
     */
    public $reference;

    /**
     * The invoice date
     *
     * @var date
     */
    public $date;

    /**
     * The compagny name
     *
     * @var string
     */
    public $companyName;

    /**
     * The third party name
     *
     * @var string
     */
    public $thirdPartyName;

} // END class invoice 