<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords\Parser\xml;
/**
 * Parser for XML accountingRecord documents
 */
class accountingRecord
{
    protected $xmlDocument;

    public function __construct(\dependency\xml\Document $xmlDocument) 
    {
        $this->xmlDocument = $xmlDocument;
    }

    public function extract($xml)
    {
        return $this->create($xml);
    }

    public function create($xml)
    {

        $this->xmlDocument->loadXml($xml);

        $accountingRecord = $this->xmlDocument->export('financialRecords/accountingRecord');

        return $accountingRecord;
    }

}