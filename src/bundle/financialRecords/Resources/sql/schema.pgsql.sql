DROP SCHEMA IF EXISTS "financialRecords" CASCADE;

CREATE SCHEMA "financialRecords"
  AUTHORIZATION postgres; 

-- Table: "financialRecords"."invoice"

-- DROP TABLE "financialRecords"."invoice";

CREATE TABLE "financialRecords"."invoice"
(
  "archiveId" text NOT NULL,
  "documentType" text NOT NULL,
  "reference" text,
  "date" date,
  "companyName" text,
  "thirdPartyName" text,
  PRIMARY KEY ("archiveId")
)
WITH (
  OIDS=FALSE
);

-- Table: "financialRecords"."accountingRecord"

-- DROP TABLE "financialRecords"."accountingRecord";

CREATE TABLE "financialRecords"."accountingRecord"
(
  "docId" text NOT NULL,
  "docType" text NOT NULL,
  
  "reference" text,
  "date" date,
  "year" integer,
  
  "companyCode" text,
  "companyName" text,
  
  "thirdPartyCode" text,
  "thirdPartyName" text,
  
  "accountNumber" text,
  
  PRIMARY KEY ("docId")
)
WITH (
  OIDS=FALSE
);

