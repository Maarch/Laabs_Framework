CREATE VIEW "financialRecords"."accountingRecordArchive" AS
SELECT "accountingRecord".*,
	"archive"."archiveId",
    "archive"."originatorOrgRegNumber",
    "archive"."originatorOwnerOrgId",
    "archive"."depositorOrgRegNumber",
    "archive"."archiverOrgRegNumber",
    "archive"."status",
    "archive"."archivalProfileReference",
    "archive"."archivalAgreementReference",
    "archive"."finalDisposition",
    "archive"."disposalDate",
    "archive"."descriptionId",
	"archive"."parentArchiveId",
    "document"."count" as "documentCount",
    "document"."copies" as "documentCopies"
FROM "financialRecords"."accountingRecord"
     JOIN "recordsManagement"."archive" ON "archive"."descriptionClass" = 'financialRecords/accountingRecord' AND "archive"."descriptionId" = "accountingRecord"."docId"
     LEFT JOIN ( SELECT "archiveId", count(1) as "count", sum(case when "copy"=true then 1 else 0 end)  as "copies" FROM "documentManagement"."document" WHERE "type" = 'CDO' GROUP BY "archiveId") "document" ON "archive"."archiveId" = "document"."archiveId"; 
