<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:fr="maarch.org:laabs:financialRecords"
    exclude-result-prefixes="xsl xsd fr">
    
    <xsl:template match="fr:accountingRecord">
        <h4 data-translate-catalog="financialRecords/messages">Accounting record</h4>
        <dl class="dl-horizontal" data-translate-catalog="financialRecords/messages">
            <dd></dd>
            <dt>Date</dt>
            <dd translate="no"><xsl:value-of select="fr:date"/></dd>

            <dt>Reference</dt>
            <dd translate="no"><xsl:value-of select="fr:reference"/></dd>

            <dt>Third party name</dt>
            <dd translate="no"><xsl:value-of select="fr:thirdPartyName"/></dd>

            <dt>Company name</dt>
            <dd translate="no"><xsl:value-of select="fr:companyName"/></dd>

            <dt>Year</dt>
            <dd translate="no"><xsl:value-of select="fr:year"/></dd>

            <dt>Company code</dt>
            <dd translate="no"><xsl:value-of select="fr:companyCode"/></dd>
        </dl>
    </xsl:template>
    
</xsl:stylesheet>