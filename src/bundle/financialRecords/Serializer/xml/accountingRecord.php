<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords\Serializer\xml;
/**
 * accounting record Xml serializer
 *
 * @package RecordsManagement
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
class accountingRecord
{

    protected $xml;

    /**
     * Constructor of accountingRecord class
     * @param \dependency\xml\Document $xml
     */
    public function __construct(\dependency\xml\Document $xml)
    {
        $this->xml = $xml;
        $this->xml->formatOutput = true;
    }

    /**
     * Serialize accounting record as XML
     * @param recordsManagement/accountingRecord $accountingRecord
     *
     * @return string
     */
    public function read($accountingRecord)
    {
        $fragment = $this->xml->createDocumentFragment();
        $fragment->appendFile('financialRecords/xml/accountingRecord.xml');
        $this->xml->appendChild($fragment);

        $this->xml->setSource('accountingRecord', $accountingRecord);

        $this->xml->merge();

        return $this->xml->saveXml($this->xml->documentElement);
    }

}
