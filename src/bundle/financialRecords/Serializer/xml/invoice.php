<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords\Serializer\xml;
/**
 * invoice Xml serializer
 *
 * @package RecordsManagement
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class invoice
{

    protected $xml;

    /**
     * Constructor of admin archival profile class
     * @param \dependency\xml\Document $xml
     */
    public function __construct(\dependency\xml\Document $xml)
    {
        $this->xml = $xml;
        $this->xml->formatOutput = true;
    }

    /**
     * Serialize invoice as XML
     * @param recordsManagement/invoice $invoice
     *
     * @return string
     */
    public function read($invoice)
    {
        $fragment = $this->xml->createDocumentFragment();
        $fragment->appendFile('financialRecords/xml/invoice.xml');
        $this->xml->appendChild($fragment);

        $this->xml->setSource('invoice', $invoice);

        $this->xml->merge();

        return $this->xml->saveXml();
    }

}

