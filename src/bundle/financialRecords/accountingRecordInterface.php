<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords;
/**
 * Standard interface for accountingRecord archive description class
 */
interface accountingRecordInterface
{
    /**
     * Get a  search result
     * @param string $docType
     * @param string $reference
     * @param date   $dateMin
     * @param date   $dateMax
     * @param int    $year
     * @param string $companyName
     * @param string $companyCode
     * @param string $thirdPartyCode
     * @param string $thirdPartyName
     * @param string $accountNumber
     *
     * @action financialRecords/accountingRecord/find
     */
    public function readList($docType = null, $reference = null, $dateMin = null, $dateMax = null, $year = null, $companyName = null, $companyCode = null, $thirdPartyCode = null, $thirdPartyName = null, $accountNumber = null);
    
    /**
     * Create the description of archive
     *
     * @action financialRecords/accountingRecord/create
     */
    public function create($accountingRecord);

    /**
     * Read the description associated with the archive
     * 
     * @action financialRecords/accountingRecord/read
     */
    public function read_accountingRecordId_();

    /**
     * Update accountingRecord : get start date for disposal
     * 
     * @action financialRecords/accountingRecord/update
     */
    public function update_accountingRecordId_($archive);

    /**
     * Delete accountingRecord. Delete archive
     * 
     * @action financialRecords/accountingRecord/delete(.+)
     */
    public function delete_accountingRecordId_();
    
    /**
     * Get the description form
     *
     * @action financialRecords/accountingRecord/description/form
     */
    public function readForm($profileDescriptions);
}