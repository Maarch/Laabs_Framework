<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle financialRecords.
 *
 * Bundle financialRecords is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle financialRecords is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle financialRecords.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\financialRecords;
/**
 * Standard interface for invoice archive description class
 */
interface invoiceInterface
{
    /**
     * @request READ financialRecords/invoice/search/form
     * @prompt
     */
    public function search();

    /**
     * @request READ financialRecords/invoice
     */
    public function find();
    
    /**
     * Create the description of archive
     * @param financialRecords/invoice $invoice
     * 
     * @request CREATE financialRecords/invoice
     */
    public function create($invoice);

    /**
     * Read the description associated with the archive
     * @param id $archiveId
     * 
     * @return financialRecords/invoice
     * 
     * @request READ financialRecords/invoice/([^\/]+)
     */
    public function read($archiveId);

    /**
     * Update invoice : get start date for disposal
     * @param string $archiveId
     * 
     * @request UPDATE financialRecords/invoice/([^\/]+)
     */
    public function update($archiveId);

    /**
     * Delete invoice. Delete archive
     * @param string $archiveId
     * 
     * @request DELETE financialRecords/invoice/(.+)
     */
    public function delete($archiveId);
    
    /**
     * Get the description form
     * @param Array $profileDescriptions Array of recordsManagement/profileDescription classe
     *
     * @return object invoice
     *
     * @request READ financialRecords/invoice/description/form
     */
    public function form($profileDescriptions);
}