<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle lifeCycle.
 *
 * Bundle lifeCycle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle lifeCycle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle lifeCycle.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\lifeCycle\Batch;

/**
 * Class for batch job to chain journals
 *
 */
class chainJournal
{
    // Parameters
    protected $depositorOrgRegNumber;

    protected $serviceLevel;

    // Other attributes
    protected $batchId;

    // Controllers
    protected $archiveController;

    protected $lifeCycleJournalController;

    protected $orgController;

    // Services
    protected $logger;



    /**
     * Construct the batch job instance
     * @param string $depositorOrgRegNumber The depositor organization registration number
     * @param string $serviceLevel          The service level
     */
    public function __construct($depositorOrgRegNumber, $serviceLevel)
    {
        // Instanciate controllers
        $this->archiveController = \laabs::newController('recordsManagement/archive');

        $this->digitalResourceController = \laabs::newController('digitalResource/digitalResource');

        $this->lifeCycleJournalController = \laabs::newController('lifeCycle/journal');

        $this->auditController = \laabs::newController('audit/entry');

        $this->orgController = \laabs::newController('organization/organization');

        // Get references
        $this->serviceLevel = $this->archiveController->useServiceLevel("logServiceLevel", 'deposit');

        // Prepare for import
        umask(0022);

        $this->tmpdir = \laabs\tempdir();

        $this->batchId = basename($this->tmpdir);

        mkdir($this->tmpdir.DIRECTORY_SEPARATOR.'documents', 0770);
        mkdir($this->tmpdir.DIRECTORY_SEPARATOR.'descriptions', 0770);
        mkdir($this->tmpdir.DIRECTORY_SEPARATOR.'success', 0770);
        mkdir($this->tmpdir.DIRECTORY_SEPARATOR.'error', 0770);

        $logger = \laabs::newService('dependency/logger/Logger');
        $logger->configure(
            $rootLogger = array(
                'appenders' => array('default'),
            ),
            $appenders = array(
                'default' => array(
                    'class' => 'LoggerAppenderFile',
                    'layout' => array(
                        'class' => 'LoggerLayoutSimple',
                    ),
                    'params' => array(
                        'file' => $this->tmpdir.DIRECTORY_SEPARATOR.'log.txt',
                        'append' => true,
                    ),
                ),
            )
        );

        $this->logger = $logger->getLogger('main');

        $this->start = \laabs::newTimestamp();

        $this->logger->info("Starting batch chain journal at ".(string) $this->start);

        $this->journalDirectory = \laabs::configuration("lifeCycle")["journalDirectory"];

        $this->depositorOrgRegNumber = $depositorOrgRegNumber;
    }

    /**
     * Get the current journal
     *
     * @return object $journal
     */
    public function getCurrentJournal()
    {
        $journal = $this->lifeCycleJournalController->getCurrentJournal();

        return $journal;
    }

    /**
     * Chain journal
     *
     * @return object $journalFile
     */
    public function chainJournal()
    {
        $journalFile = $this->lifeCycleJournalController->chainJournal();

        return $journalFile;
    }

    /**
     * Create journal resource
     * @param object $journal     The journal
     * @param object $journalFile The journal file
     *
     * @return object $journalArchive
     */
    public function createJournalArchive($journal, $journalFile)
    {
        $this->logger->info("Creating life cycle journal archive...");

        // Create archive
        $archive = $this->archiveController->newArchive();

        $archive->archiveId = $journal->journalId;

        // Create resource
        $journalResource = $this->digitalResourceController->createFromFile($journalFile);

        if (!$journalResource) {
            $this->logger->error('Journal file nt found !');

            return;
        }

        $archive->accesRulesDuration = null;
        $archive->retentionDuration = '0D';

        $this->digitalResourceController->getHash($journalResource, "SHA256");

        // Add document
        $document = \laabs::newInstance('recordsManagement/document');
        $document->archiveId = $archive->archiveId;
        $document->resId = $journalResource->resId;
        $document->type = "CDO";
        $document->digitalResource = $journalResource;
        $archive->document[] = $document;
        $archive->hasDigitalResource = true;

        // Add description
        $journalDescription = \laabs::newInstance('recordsManagement/log');
        $journalDescription->fromDate = $journal->timestamp;
        $journalDescription->toDate = $this->start;
        $journalDescription->type = "lifeCycle";

        $archive->descriptionObject = $journalDescription;
        $archive->descriptionObject->archiveId = $archive->archiveId;
        $archive->descriptionClass = 'recordsManagement/log';

        if (!isset($this->orgController->getOrgsByRole('owner')[0])) {
            throw \laabs::newException("lifeCycle/journalException", "Owner organization not found.");
        }

        $depositorOrg = $this->orgController->getOrgsByRole('owner')[0];
        $archive->originatorOrgRegNumber = $depositorOrg->registrationNumber;

        $archive->serviceLevelReference = $this->serviceLevel->reference;
        $archive->accessRuleCode = '';

        $this->logger->info('Archive created for life cycle journal');

        return $archive;
    }

    /**
     * Deposit archives
     * @param object $journalArchive
     *
     * @return array $certificates
     */
    public function depositArchives($journalArchive)
    {
        $archive = $this->archiveController->receive($journalArchive, $this->depositorOrgRegNumber);

        $this->archiveController->deposit($archive, $this->depositorOrgRegNumber);

        return true;
    }
}
