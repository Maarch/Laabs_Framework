<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle lifeCycle.
 *
 * Bundle lifeCycle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle lifeCycle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle lifeCycle.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\lifeCycle\Controller;

/**
 * Class of archives life cycle journal
 *
 * @author Prosper DE LAURE <prosper.delaure@maarch.org>
 */
class journal
{

    protected $archiveController;
    protected $journalDirectory;
    protected $interval;
    protected $currentJournalFile;
    protected $currentJournalId;
    protected $journalCursor;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory       The sdo factory
     * @param string                  $journalDirectory The identifier of a digital resource cluster to store journal archives
     * @param string                  $interval         The time bewteen 2 journal changes
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, $journalDirectory, $interval = 86400)
    {
        $this->journalDirectory = $journalDirectory;

        if (!is_dir($journalDirectory)) {
            mkdir($journalDirectory, 777, true);
        }

        $this->interval = $interval;

        $this->sdoFactory = $sdoFactory;

        $this->currentJournalFile = null;

        $this->currentJournalId = null;

        $this->currentOffset = 0;
    }

    /**
     * Get event tpye list
     *
     * @return array The eventType list
     */
    public function listEventType()
    {
        return $this->sdoFactory->index('lifeCycle/eventFormat', 'type');
    }

    /**
     * Add an event to the journal
     * @param string $eventType        The type of the event
     * @param string $objectClass      The aimed object class
     * @param string $objectId         The aimed object id
     * @param array  $eventItems       The description of the event
     * @param string $description      The description of the event
     * @param bool   $operationResult  The operation result
     *
     * @return lifeCycle/event The new event
     */
    public function logEvent($eventType, $objectClass, $objectId, $eventItems = null, $description = null, $operationResult = true)
    {
        // Get current journal or create a new one
        if (!$journal = $this->getCurrentJournal()) {
            $journal = $this->newJournal();
        }

        $event = \laabs::newInstance('lifeCycle/event');

        $event->eventId = \laabs::newId();

        $event->timestamp = \laabs::newTimestamp();
        $event->eventType = $eventType;
        $event->objectClass = $objectClass;
        $event->objectId = $objectId;
        $event->operationResult = $operationResult;
        $event->description = $description;
        
        if ($account = \laabs::getToken('AUTH')) {
            $event->accountId = $account->accountId;
        } else {
            $event->accountId = '__system__';
        }

        $event->journalId = $journal->journalId;
        $eventInfo = array();

        // Event info
        if ($eventItems) {
            try {
                $eventFormat = $this->sdoFactory->read('lifeCycle/eventFormat', $event->eventType);
                $eventFormat = explode(' ', $eventFormat->format);

                foreach ($eventFormat as $item) {
                    if (isset($eventItems[$item])) {
                        $eventInfo[] = $eventItems[$item];
                    } else {
                        $eventInfo[] = "";
                    }
                }
            } catch (\Exception $e) {
                throw \laabs::newException("lifeCycle/journalException", "Invalid event format.");
            }
        }

        if (!empty($eventInfo)) {
            $event->eventInfo = json_encode($eventInfo);
        }

        $this->sdoFactory->create($event);

        return $event;
    }

    /**
     * Get the events for a given object id and class
     * @param string $objectId    The identifier of the object
     * @param string $objectClass The class of the object
     * @param mixed  $eventType   An event type or an array of event types to retrieve
     *
     * @return lifeCycle/event[]
     */
    public function getObjectEvents($objectId, $objectClass, $eventType = null)
    {
        $query = "";
        if ($eventType) {
            if (is_array($eventType)) {
                $query = " AND eventType=['".\laabs\implode("', '", $eventType)."']";
            } else {
                $query = " AND eventType='$eventType'";
            }
        }

        $events = $this->sdoFactory->find('lifeCycle/event', "objectId='$objectId' AND objectClass='$objectClass'".$query, null, 'timestamp');

        foreach ($events as $event) {
            $event = $this->getEvent($event);
        }

        return $events;
    }

    /**
     * Get an events by id
     * @param mixed $eventId The event or the identifier of the event
     *
     * @return string
     */
    public function getEvent($eventId)
    {
        if (is_scalar($eventId) || get_class($eventId) == 'core\Type\Id') {
            $event = $this->sdoFactory->read('lifeCycle/event', $eventId);
        } else {
            $event = $eventId;
        }

        // Read journal file
        $journalReference = $this->sdoFactory->find('lifeCycle/journal', "timestamp <= '$event->timestamp'", null, ">timestamp", 0, 1);
        $journalReference = end($journalReference);

        if ($journalReference->closingTimestamp) {
            $this->openJournal($journalReference->journalId);

            // Get the event
            $this->currentOffset = strpos($this->currentJournalFile, (string) $event->eventId);

            if ($this->currentOffset) {
                $endOffset = strpos($this->currentJournalFile, "\n", $this->currentOffset);
                $eventLine = substr($this->currentJournalFile, $this->currentOffset, $endOffset - $this->currentOffset);
                $this->currentOffset = $this->currentOffset + strlen($eventLine) + 1;

                $event = $this->getEventFromLine($eventLine);
            } else {
                throw \laabs::newException("lifeCycle/journalException", "Event can't be find.");
            }
        } else {
            if (isset($event->eventInfo)) {
                $event->eventInfo = json_decode($event->eventInfo);
                $eventFormat = $this->sdoFactory->read('lifeCycle/eventFormat', $event->eventType);
                $eventFormat = explode(' ', $eventFormat->format);
                $i = 0;

                foreach ($eventFormat as $item) {
                    if (isset($event->eventInfo[$i])) {
                        $event->{$item} = $event->eventInfo[$i];
                    } else {
                        $event->{$item} = null;
                    }
                    $i++;
                }
            }
            unset($event->eventInfo);
        }

        return $event;
    }

    /**
     * Get the next event or get the next event whitch contain a givven item
     * @param string $eventItem The event item to search
     * @param bool   $chain     Chain to the next journal
     *
     * @return string
     */
    public function getNextEvent($eventItem = null, $chain = true)
    {
        $event = null;

        // Open a journal if there is not any
        if (!$this->currentJournalFile) {
            return false;
        }

        // Place the cursor to the first event if it not positioned yet
        if ($this->currentOffset == 0) {
            $this->currentOffset = strpos($this->currentJournalFile, "\n") + 1;
        }

        // Search the event
        if ($eventItem) {
            $offset = strpos($this->currentJournalFile, $eventItem, $this->currentOffset);
        } else {
            $offset = $this->currentOffset;
        }

        // Read the event
        if ($offset != false) {
            $journalLength = strlen($this->currentJournalFile);

            $startOffset = strrpos($this->currentJournalFile, "\n", -$journalLength + $offset) + 1;
            $endOffset = strpos($this->currentJournalFile, "\n", $startOffset);
            $eventLine = substr($this->currentJournalFile, $startOffset, $endOffset - $startOffset);

            $this->currentOffset = $startOffset + strlen($eventLine) + 1;

            $event = $this->getEventFromLine($eventLine);
        }

        // Search on the next journal
        if ($chain && $event == null) {
            if ($this->openNextJournal()) {
                $event = $this->getNextEvent($eventItem);
            }
        }

        return $event;
    }

    /**
     * Get the previous event or get the previous event whitch contain a givven item
     * @param string $eventItem The event item to search
     * @param bool   $chain     Chain to the previous journal
     *
     * @return string
     */
    public function getPreviousEvent($eventItem = null, $chain = true)
    {
        $event = null;

        // Open a journal if there is not any
        if (!$this->currentJournalFile) {
            return false;
        }

        // Place the cursor to the first event if it not positioned yet
        if ($this->currentOffset == 0) {
            if ($this->openPreviousJournal()) {
                return $event = $this->getPreviousEvent($eventItem);
            } else {
                return null;
            }
        }

        $journalLength = strlen($this->currentJournalFile);

        // Search the event
        if ($eventItem) {
            $offset = strrpos($this->currentJournalFile, $eventItem, $this->currentOffset - $journalLength);
        } else {
            $offset = $this->currentOffset - 2;
        }

        // Read the event
        if ($offset != false) {
            $startOffset = strrpos($this->currentJournalFile, "\n", $offset - $journalLength) + 1;
            $endOffset = strpos($this->currentJournalFile, "\n", $startOffset);
            $eventLine = substr($this->currentJournalFile, $startOffset, $endOffset - $startOffset);

            $this->currentOffset = $startOffset - 1;

            $event = $this->getEventFromLine($eventLine);
        }

        // Search on the next journal
        if ($chain && $event == null) {
            if ($this->openPreviousJournal()) {
                $event = $this->getPreviousEvent($eventItem);
            }
        }

        return $event;
    }

    /**
     * Delete the events for a given id
     * @param string $archiveId The identifier of the archive object
     *
     * @return boolean
     */
    public function deleteArchiveEvents($archiveId)
    {
        $events = $this->sdoFactory->find('lifeCycle/event', "archiveId='$archiveId'");

        $this->sdoFactory->deleteCollection($events, 'lifeCycle/event');
    }

    /**
     * Get the current usable journal
     *
     * @return lifeCycle/journal The journal object
     */
    public function getCurrentJournal()
    {
        $journals = $this->sdoFactory->find('lifeCycle/journal', "", null, ">timestamp", 0, 1);

        if (empty($journals)) {
            return null;
        }

        $journal = end($journals);

        return $journal;
    }

    /**
     * Get the oldest usable journal
     *
     * @return lifeCycle/journal The journal object
     */
    public function getOldestJournal()
    {
        $journals = $this->sdoFactory->find('lifeCycle/journal', "", null, "timestamp", 0, 1);

        if (empty($journals)) {
            return null;
        }

        $journal = end($journals);

        return $journal;
    }

    /**
     * Load a journal
     * @param string $journalReference The id of the journal or the journal object
     *
     * @return boolean The result of the operation
     */
    public function openJournal($journalReference)
    {
        if (is_scalar($journalReference) || get_class($journalReference) == 'core\Type\Id') {
            $journalReference = $this->sdoFactory->read('lifeCycle/journal', $journalReference);
        }

        if (isset($journalReference->closingTimestamp)) {
            $archiveController = \laabs::newController('recordsManagement/archive');
            $journalDocuments = $archiveController->getDocuments($journalReference->journalId);
            $journalFile = null;

            foreach ($journalDocuments as $document) {
                if ($document->type == "CDO") {
                    $journalFile = $document->digitalResource->getContents();
                    $this->journalCursor = 0;
                    break;
                }
            }

            if ($journalFile == null) {
                throw \laabs::newException("lifeCycle/journalException", "The journal file can't be opened");
            } else {
                $this->currentJournalFile = $journalFile;
                $this->currentJournalId = $journalReference->journalId;
            }
        }

        $this->currentOffset = 0;

        return true;
    }

    /**
     * Load the previous journal
     *
     * @return string The opened journalId
     */
    public function openPreviousJournal()
    {
        $journalId = null;

        if ($this->currentJournalFile) {
            $this->currentOffset = strpos($this->currentJournalFile, "\n");
            $eventLine = substr($this->currentJournalFile, 0, -2);

            $journalArray = str_getcsv($eventLine);

            if (count($journalArray)) {
                $journalId = $journalArray[2];
                $hashAlgorithm = $journalArray[3];
                $hash = $journalArray[4];
                $this->openJournal($journalId);

                $currentHash = hash($hashAlgorithm, $currentJournalFile);
                if ($currentHash != $hash) {
                    throw \laabs::newException("lifeCycle/journalException", "Journal hash is incorrect.");
                }

                $this->currentOffset = strrpos($this->currentJournalFile, "\n", -2);
            }
        }

        return $journalId;
    }

    /**
     * Load the next journal
     *
     * @return string The opened jounalId
     */
    public function openNextJournal()
    {
        $journalId = null;

        if ($this->currentJournalId) {
            $this->currentOffset = strpos($this->currentJournalFile, "\n");
            $eventLine = substr($this->currentJournalFile, 0, -2);
            $journalArray = str_getcsv($eventLine);
            $journalTimestamp = $journalArray[1];

            $journalList = $this->sdoFactory->find('lifeCycle/journal', "timestamp >= '$journalTimestamp'", null, "timestamp", 0, 1);
            $this->openJournal($journalList[0]->journalId);
        }

        return $journalId;
    }

    /**
     * Get the current journal
     * @param string  $journalId The journal identifier
     * @param integer $offset    The reading offset
     * @param integer $limit     The maximum number of event to load
     *
     * @return lifeCycle/event[]
     */
    public function readJournal($journalId, $offset = 0, $limit = 300)
    {
        $this->openJournal($journalId);

        $events = array();

        while ($limit > 0 && $event = $this->getNextEvent(null, false)) {
            $events[] = $event;
            $limit--;
        }

        return $events;
    }

    /**
     * Chain the last journal
     *
     * @return string The chained journal file name
     */
    public function chainJournal()
    {
        $journalReference = $this->getCurrentJournal();
        $journalEndTimestamp = $journalReference->timestamp->shift(\laabs::newDuration("PT".$this->interval."S"));

        $events = $this->sdoFactory->find('lifeCycle/event', "timestamp >= '$journalReference->timestamp' and timestamp < '$journalEndTimestamp'");
        $journalFile = fopen($this->journalDirectory.DIRECTORY_SEPARATOR.(string) $journalReference->journalId.".csv", "w");

        $eventLine = array();
        $eventLine[] = (string) $journalReference->journalId;
        $eventLine[] = (string) $journalReference->timestamp;

        // Write previous jounal informations
        if (isset($journalReference->previousJournal)) {
            $archiveController = \laabs::newController('recordsManagement/archive');
            $journalDocuments = $archiveController->getDocuments($journalReference->journalId);
            $previousJournal = null;

            foreach ($journalDocuments as $document) {
                if ($document->type != "CDO") {
                    $previousJournal = $document->digitalResource->getContents();
                    break;
                }
            }

            if (!$previousJournal) {
                throw new \core\Exception\NotFoundException("Journal archive not found");
            }

            $journalReference->hashAlgorithm = "SHA256";
            $journalReference->hash = hash("SHA256", $previousJournal);

            $eventLine[] = (string) $journalReference->previousJournal;
            $eventLine[] = (string) $journalReference->hashAlgorithm;
            $eventLine[] = (string) $journalReference->hash;
        }

        fputcsv($journalFile, $eventLine);

        // Write events
        foreach ($events as $event) {
            $eventLine = array();

            $eventLine[] = (string) $event->eventId;
            $eventLine[] = (string) $event->eventType;
            $eventLine[] = (string) $event->timestamp;
            $eventLine[] = (string) $event->objectClass;
            $eventLine[] = (string) $event->objectId;
            $eventLine[] = (string) $event->operationResult;
            $eventLine[] = (string) $event->description;

            $event->eventInfo = json_decode($event->eventInfo);
            $eventLine = array_merge($eventLine, $event->eventInfo);

            fputcsv($journalFile, $eventLine);
        }

        $journalReference->closingTimestamp = \laabs::newTimestamp();

        $this->sdoFactory->update($journalReference);

        // Deposit archive

        // Open new journal
        $newJournal = $this->newJournal($journalReference);
        fputcsv($journalFile, array($newJournal->journalId));

        fclose($journalFile);

        return $this->createJournalArchive($journalReference, $this->journalDirectory.DIRECTORY_SEPARATOR.(string) $journalReference->journalId.".csv");
    }

    /**
     * Create a new journal
     * @param lifeCycle/journal $previousJournal
     *
     * @return lifeCycle/journal The created journal object
     */
    public function newJournal($previousJournal = null)
    {
        $journal = \laabs::newInstance("lifeCycle/journal");
        $journal->journalId = \laabs::newId();
        $journal->timestamp = \laabs::newTimestamp();

        if ($previousJournal) {
            $journal->previousJournalId = $previousJournal->journalId;
        }

        try {
            $this->sdoFactory->create($journal);
        } catch (\Exception $e) {
            throw \laabs::newException("lifeCycle/journalException", "Journal not created.");
        }

        return $journal;
    }

    /**
     * Create a new journal
     *
     * @return lifeCycle/journal The created journal object
     */
    public function getJournalList()
    {
        return $this->sdoFactory->find('lifeCycle/journal', "", null, ">timestamp");
    }

    /**
     * Search a journal event
     * @param string $eventType   The type of the event
     * @param string $objectClass The object class
     * @param string $objectId    The identifier of the object
     * @param string $minDate     The minimum date of the event
     * @param string $maxDate     The maximum date of the event
     *
     * @return array The result of the request
     */
    public function searchEvent($eventType = false, $objectClass = false, $objectId = false, $minDate = false, $maxDate = false)
    {
        $predicate = array();

        if ($eventType) {
            array_push($predicate, "eventType = '$eventType'");
        }

        if ($objectClass) {
            array_push($predicate, "objectClass = '$objectClass'");
        }

        if ($objectId) {
            array_push($predicate, "objectId = '$objectId'");
        }

        if ($minDate) {
            array_push($predicate, "timestamp >= '$minDate'");
        }

        if ($maxDate) {
            array_push($predicate, "timestamp <= '$maxDate'");
        }

        $queryString = implode(' AND ', $predicate);

        $events = $this->sdoFactory->find('lifeCycle/event', $queryString);

        return $events;
    }

    /**
     * Get an event from a csv line
     * @param string $eventLine The scv line from the journal
     *
     * @return lifeCycle/event The event
     */
    private function getEventFromLine($eventLine)
    {
        $eventArray = str_getcsv($eventLine);

        $event = null;

        if (count($eventArray) >= 6) {
            $event = \laabs::newInstance('lifeCycle/event');

            $event->eventId = \laabs::newId($eventArray[0]);
            $event->eventType = $eventArray[1];
            $event->timestamp = \laabs::newTimestamp($eventArray[2]);
            $event->objectClass = $eventArray[3];
            $event->objectId = $eventArray[4];
            $event->operationResult = $eventArray[5] === '0' ? false : true;
            $event->description = $eventArray[6];

            try {
                $i = 7;
                $eventFormat = $this->sdoFactory->read('lifeCycle/eventFormat', $event->eventType);
                $eventFormat = explode(' ', $eventFormat->format);

                foreach ($eventFormat as $item) {
                    if (isset($eventArray[$i])) {
                        $event->{$item} = $eventArray[$i];
                    } else {
                        $event->{$value} = null;
                    }
                    $i++;
                }
            } catch (\Exception $e) {
            }
        }

        return $event;
    }

    /**
     * Create journal resource
     * @param lifeCycle/journal $journal     The journal
     * @param stream            $journalFile The journal file
     *
     * @return object $journalArchive
     */
    protected function createJournalArchive($journal, $journalFile)
    {
        $archiveController = \laabs::newController('recordsManagement/archive');
        $digitalResourceController = \laabs::newController('digitalResource/digitalResource');
        $orgController = \laabs::newController('organization/organization');


        // Create archive
        $archive = $archiveController->newArchive();

        $archive->archiveId = $journal->journalId;

        // Create resource
        $journalResource = $digitalResourceController->createFromFile($journalFile);

        $archive->accesRulesDuration = null;
        $archive->accesRulesCode = null;
        $archive->retentionDuration = '0D';
        $archive->finalDisposition = 'preserved';

        $digitalResourceController->getHash($journalResource, "SHA256");

        // Add document
        $document = \laabs::newInstance('documentManagement/document');
        $document->archiveId = $archive->archiveId;
        $document->resId = $journalResource->resId;
        $document->type = "CDO";
        $document->digitalResource = $journalResource;
        $archive->document[] = $document;
        $archive->hasDigitalResource = true;

        // Add description
        $journalDescription = \laabs::newInstance('recordsManagement/log');
        $journalDescription->fromDate = $journal->timestamp;
        $journalDescription->toDate = \laabs::newTimestamp();
        $journalDescription->type = "lifeCycle";

        $archive->descriptionObject = $journalDescription;
        $archive->descriptionObject->archiveId = $archive->archiveId;
        $archive->descriptionClass = 'recordsManagement/log';

        if (!isset($orgController->getOrgsByRole('owner')[0])) {
            throw \laabs::newException("lifeCycle/journalException", "Owner organization not found.");
        }

        $depositorOrg = $orgController->getOrgsByRole('owner')[0];
        $archive->originatorOrgRegNumber = $depositorOrg->registrationNumber;

        $archive->serviceLevelReference = $archiveController->useServiceLevel("deposit", "logServiceLevel")->reference;

        return $archiveController->deposit($archive, $depositorOrg->registrationNumber);
    }
}
