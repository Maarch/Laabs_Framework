<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle lifeCycle.
 *
 * Bundle lifeCycle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle lifeCycle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle lifeCycle.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\lifeCycle\Model;

/**
 * Class model that represents a journal of event for digital resources
 *
 * @package lifeCycle
 * @author  Prosper DE LAURE (Maarch) <prosper.delaure@maarch.org>
 *
 * @pkey [journalId]
 * @fkey [previousJournalId] lifeCycle/journal [journalId]
 */
class journal
{
    /**
     * The universal identifier
     *
     * @var id
     */
    public $journalId;

    /**
     * The universal identifier of the previous journal
     *
     * @var id
     */
    public $previousJournalId;

    /**
     * The timestamp of the creation
     *
     * @var timestamp
     */
    public $timestamp;

    /**
     * The timestamp of the last writing
     *
     * @var timestamp
     */
    public $closingTimestamp;

    /**
     * The journal's events
     *
     * @var lifeCycle/event[]
     */
    public $event;
}
