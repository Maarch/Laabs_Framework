<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona\Batch;

/**
 * Class for batch job to archive messages
 *
 */
class archiveCertificate
{
    protected $messageController;
    protected $sdoFactory;

    /**
     * Construct the batch job instance
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->messageController = \laabs::newController('medona/message');
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Receive the message
     *
     * @return medona/message $message
     */
    public function archive()
    {
        $certificates = $this->sdoFactory->find('medona/message', 'archived = false');
        foreach ($certificates as $certificate) {
            $this->messageController->recordCertificate($certificate);
        }

        return true;
    }
}
