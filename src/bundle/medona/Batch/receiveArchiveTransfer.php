<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona\Batch;

/**
 * Class for batch job to deposit messages
 *
 */
class receiveArchiveTransfer
{
    // Controllers
    protected $messageController;

    protected $zip;

    // Loaded data
    protected $messagefile;

    protected $tmpdir;

    protected $start;


    /**
     * Construct the batch job instance
     * @param string $messagefile The message path, directory or xml file or zip file
     * @param bool   $compressed  Indicates that the message is compressed
     */
    public function __construct($messagefile, $compressed = false)
    {
        // Controlers
        $this->messageController = \laabs::newController('medona/message');

        if ($compressed) {
            $this->zip = \laabs::newService('dependency/fileSystem/plugins/zip');

            $tmpdir = sys_get_temp_dir().\laabs::uniqid();
            $this->zip->extract($messagefile, $tmpdir);

            $messagefile = glob($tmpdir.DIRECTORY_SEPARATOR.'*.xml')[0];
        }

        $this->messagefile = $messagefile;

        $this->start = \laabs::newTimestamp();
    }

    /**
     * Receive the message
     *
     * @return medona/message $message
     */
    public function receive()
    {
        $message = $this->messageController->receiveArchiveTransfer($this->messagefile);

        $this->messageController->validateArchiveTransfer($message);
    }
}
