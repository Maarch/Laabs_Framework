<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Conversion request message
 *
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class ArchiveConversionRequest extends abstractMessage
{
    /**
     * Send a new a new delivery request
     * @param string $reference    The message identifier
     * @param object $senderOrg    The requesting org
     * @param object $recipientOrg The originating org
     * @param array  $documents    An array of document objects
     *
     * @return The request message generated
     */
    public function send($reference, $senderOrg, $recipientOrg, $documents)
    {
        if (!is_array($documents)) {
            $documents = array($documents);
        }

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();

        $message->schema = "medona";
        $message->type = "ArchiveConversionRequest";
        $message->status = 'accepted';
        $message->date = \laabs::newDateTime();
        $message->reference = $reference;
        $message->unitIdentifier = \laabs::newTokenList();

        foreach ($documents as $document) {
            $message->unitIdentifier[] = (string) $document->docId;
        }


        $message->senderOrgRegNumber = $senderOrg->registrationNumber;
        $message->recipientOrgRegNumber = $recipientOrg->registrationNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        $this->create($message);

        return $message;
    }

    /**
     * Process all archive destructions
     *
     * @return the result of process
     */
    public function processAll()
    {
        $index = $this->sdoFactory->index('medona/message', array('messageId'), 'type = "ArchiveConversionRequest" AND status = "accepted"');

        foreach ($index as $messageId) {
            $this->process($messageId);
        }
    }

    /**
     * Process archive destruction
     * @param medona/message $messageId
     *
     * @return the result of process
     */
    public function process($messageId)
    {
        if (is_scalar($messageId)) {
            $message = $this->read($messageId);
        } else {
            $message = $messageId;
        }

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $archives = $this->archiveController->convert((array) $message->unitIdentifier);

            $message->status = "processed";
            $message->operationDate = \laabs::newDatetime();

            $this->update($message);

        } catch (\Exception $e) {
            throw $e;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }
}
