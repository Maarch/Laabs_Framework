<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for archiveDelivery
 * 
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class ArchiveDeliveryRequest
    extends abstractMessage
{
    /**
     * Get received archive delivery message
     *
     * @return array Array of medona/message object
     */
    public function listReception()
    {
        $queryParts = array();
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveDeliveryRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get sending archive delivery message
     * @param string $sender
     * @param string $recipient
     * @param string $fromDate
     * @param string $toDate
     * @param string $reference
     * 
     * @return array Array of medona/message object
     */
    public function listSending($sender = false, $recipient = false, $fromDate = false, $toDate = false, $reference = false)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveDeliveryRequest'";
        $queryParts[] = "senderOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get processed archive delivery message
     * @param string $sender
     * @param string $recipient
     * @param string $fromDate
     * @param string $toDate
     * @param string $reference
     *
     * @return array Array of medona/message object
     */
    public function search($sender = null, $recipient = null, $fromDate = null, $toDate = null, $reference = null)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }
        $queryParts[] = "type='ArchiveDeliveryRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active=false";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Count archive delivery message
     *
     * @return array Number of received and sent messages
     */
    public function count()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }
        
        $res = array();
        $queryParts = array();

        $queryParts["type"] = "type='ArchiveDeliveryRequest'";
        $queryParts["registrationNumber"] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts["active"] = "active='true'";
        $res['received'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        $queryParts["registrationNumber"] = "senderOrgRegNumber='$registrationNumber'";
        $res['sent'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        return $res;
    }

    /**
     * Send a new a new delivery request
     * @param string  $reference  The message identifier
     * @param array   $archives   An array of archives
     * @param boolean $derogation Ask for an authorization
     * @param string  $comment    The request comment
     * @param object  $senderOrg  The requesting org
     *
     * @return The reply message generated
     */
    public function send($reference, $archives, $derogation = false, $comment = false, $senderOrg = false)
    {
        if (!is_array($archives)) {
            $archives = array($archives);
        }       

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        
        $schema = "medona";
        if (\laabs::hasBundle('seda') && $archives[0]->descriptionClass == 'archivesPubliques/contentDescription') {
            $schema = "seda";
        }
        $message->schema = $schema;
        $message->type = "ArchiveDeliveryRequest";
        $message->status = 'new';
        $message->date = \laabs::newDateTime();
        $message->reference = $reference;
        
        $message->comment[] = $comment;

        if (!$senderOrg) {
            $senderOrg = \laabs::getToken('ORGANIZATION');
            if (!$senderOrg) {
                throw \laabs::newException('medona/invalidMessageException', "No current organization choosen");
            }
        }

        $message->senderOrgRegNumber = $senderOrg->registrationNumber;
        $message->recipientOrgRegNumber = $archives[0]->archiverOrgRegNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        $message->derogation = $derogation;

        if ($derogation) {
            $message->status = "derogation";
        } else {
            $message->status = "accepted";
        }       

        try {
            if ($message->schema != 'medona') {
                $archiveDeliveryRequestController = \laabs::newController($message->schema.'/ArchiveDeliveryRequest');
                $archiveDeliveryRequestController->send($message);

            } else {
                $this->generate($message);
                $this->save($message);
            }
            $operationResult = true;
            $this->create($message);

            $message->unitIdentifier = array();

            foreach ($archives as $archive) {
                $unitIdentifier = \laabs::newInstance("medona/unitIdentifier");
                $unitIdentifier->messageId = $message->messageId;
                $unitIdentifier->objectClass = "recordsManagement/archive";
                $unitIdentifier->objectId = (string) $archive->archiveId;

                $this->sdoFactory->create($unitIdentifier);
                $message->unitIdentifier[] = $unitIdentifier;
            }

        } catch (\Exception $e) {
            $message->status = "invalid";
            $this->create($message);
            $operationResult = false;

            var_dump($e);
            throw $e;
        }



        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDeliveryRequestSending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        if ($message->status == "accepted") {
            $replyMessage = $this->process($message);

            $message->replyMessage = $replyMessage;
        }

        return $message;
    }

    /**
     * Get received archive tranfer message
     *
     * @return array Array of medona/message object
     */
    public function listToValidate()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        return $this->sdoFactory->find("medona/message", "type='ArchiveCommunication' AND recipientOrgRegNumber='".$registrationNumber."' AND status='derogation' active=true");
    }

    /**
     * Accep archive delivery request message
     * @param string $messageId The message identifier
     */
    public function accept($messageId)
    {
        $this->changeStatus($messageId, "accepted");

        if (\laabs::hasBundle('lifeCycle')) {
            $message = $this->sdoFactory->read('medona/message', array('messageId' => $messageId));
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDeliveryRequestAcceptance',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Acceptance of the message",
                true
            );
        }
    }

    /**
     * Reject the message identifier
     * @param type $messageId The message identifier
     * 
     * @return object The reply message
     */
    public function reject($messageId)
    {
        $this->changeStatus($messageId, "rejected");

        $archiveDeliveryRequestReplyController = \laabs::newController('medona/ArchiveDeliveryRequestReply');

        if (\laabs::hasBundle('lifeCycle')) {
            $message = $this->sdoFactory->read('medona/message', array('messageId' => $messageId));
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDeliveryRequestRejection',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Rejection of the message",
                true
            );
        }
        
        return $archiveDeliveryRequestReplyController->send($messageId, "REJECT");
    }

    /**
     * Validate message against schema and rules
     * @param medona/message $message
     * @param string         $replyCode
     *
     * @return the result of process
     */
    public function process($message, $replyCode = "000")
    {
        if (is_scalar($message)) {
            $messageId = $message;
            $message = $this->sdoFactory->read('medona/message', $messageId);
        }

        $archives = array();

        foreach ((array) $message->unitIdentifier as $unitIdentifier) {
            $archives[] = $this->archiveController->retrieve($unitIdentifier->objectId );
        }

        try {
            $archiveDeliveryRequestReplyController = \laabs::newController('medona/ArchiveDeliveryRequestReply');
            $replyMessage = $archiveDeliveryRequestReplyController->send($message, $archives, $replyCode);
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "error";
            $operationResult = false;
            $this->sdoFactory->update($message);
            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDeliveryRequestProcessing',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Processing of the message",
                $operationResult
            );
        }

        $message->status = "processed";
        $this->sdoFactory->update($message);

    }

}
