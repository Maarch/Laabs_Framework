<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for archiveDelivery
 * 
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class ArchiveDeliveryRequestReply
    extends abstractMessage
{

    /**
     * Send a new delivery request reply
     * @param string $requestMessage The request message identifier
     * @param object $archives       The archives to deliver
     * @param string $replyCode      The reply code
     *
     * @return The reply message generated
     */
    public function send($requestMessage, $archives, $replyCode = "000")
    {
        if (is_scalar($requestMessage)) {
            $messageId = $requestMessage;
            $requestMessage = $this->sdoFactory->read('medona/message', $messageId);

            //$this->load($message);
        } 

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "ArchiveDeliveryRequestReply";
        $message->schema = $requestMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();

        $message->reference = $requestMessage->reference.'_Reply';
        $message->requestReference = $requestMessage->reference;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $message->recipientOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        $message->replyCode = $replyCode;

        if ($message->replyCode == "000" || $message->replyCode == "001") {
            foreach ($archives as $archive) {
                $archive->lifeCycleEvent = $this->lifeCycleJournalController->getObjectEvents($archive->archiveId, 'recordsManagement/archive');
            }

            $message->archive = $archives;
            $message->dataObjectCount = count($archives);
        }

        try {
            mkdir($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId, 0777, true);

            if ($message->schema != 'medona') {

                $archiveDeliveryRequestReplyController = \laabs::newController($message->schema.'/ArchiveDeliveryRequestReply');
                $archiveDeliveryRequestReplyController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
                
            }
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "invalid";
            $operationResult = false;

            $this->create($message);

            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDeliveryRequestReplySending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        $this->create($message);


        return $message;
    }
}
