<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Trait for destruction message
 * 
 * @author Prosper DE LAURE <prosper.delaure@maarch.org>
 */
class ArchiveDestructionNotification
    extends ArchiveNotification
{
    /**
     * Send a new transfer reply
     * @param medona/message $destructionRequest The destruction request message
     * @param array          $archives           The destroyed archives
     *
     * @return The message generated
     */
    public function send($destructionRequest, $archives)
    {
        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "ArchiveDestructionNotification";

        $schema = "medona";
        if (\laabs::hasBundle('seda') && $archives[0]->descriptionClass == 'archivesPubliques/contentDescription') {
            $schema = "seda";
        }
        $message->schema = $schema;
        
        $message->status = "sent";
        $message->date = \laabs::newDatetime();

        $message->reference = $destructionRequest->reference . "_DestructionNotification";

        $message->senderOrgRegNumber = $archives[0]->archiverOrgRegNumber;
        $message->recipientOrgRegNumber = $archives[0]->originatorOrgRegNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        $message->archive = $archives;

        $message->dataObjectCount = count($message->archive);

        try {
            if ($message->schema != 'medona') {

                $archiveModificationNotificationController = \laabs::newController($message->schema.'/ArchiveModificationNotification');
                $archiveModificationNotificationController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
            }
            $operationResult = true;
            
        } catch (\Exception $e) {
            $message->status = "error";
            $operationResult = false;

            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveDestructionNotificationSending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        $this->create($message);

        return $message;
    }
}
