<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Destruction request message
 * 
 * @author Prosper DE LAURE <prosper.delaure@maarch.org>
 */
class ArchiveDestructionRequest
    extends abstractMessage
{
     /**
     * Get received archive delivery message
     *
     * @return array Array of medona/message object
     */
    public function listReception()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts = [];
        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get sending archive delivery message
     * 
     * @return array Array of medona/message object
     */
    public function listSending()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts = [];
        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "senderOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get processed archive delivery message
     * @param string $sender
     * @param string $recipient
     * @param string $fromDate
     * @param string $toDate
     * @param string $reference
     *
     * @return array Array of medona/message object
     */
    public function search($sender = null, $recipient = null, $fromDate = null, $toDate = null, $reference = null)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }
        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active=false";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Count archive delivery message
     *
     * @return array Number of received and sent messages
     */
    public function count()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }
        
        $res = array();
        $queryParts = array();

        $queryParts["type"] = "type='ArchiveDestructionRequest'";
        $queryParts["registrationNumber"] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts["active"] = "active='true'";
        $res['received'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        $queryParts["registrationNumber"] = "senderOrgRegNumber='$registrationNumber'";
        $res['sent'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        return $res;
    }

    /**
     * Send a new a new delivery request
     * @param string $reference The message identifier
     * @param array  $archives  An array of archives
     * @param string $comment   The request comment
     * @param object $senderOrg The requesting org
     *
     * @return The request message generated
     */
    public function send($reference, $archives, $comment=null, $senderOrg=null)
    {
        if (!is_array($archives)) {
            $archives = array($archives);
        }       

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        
        $schema = "medona";
        if (\laabs::hasBundle('seda') && $archives[0]->descriptionClass == 'archivesPubliques/contentDescription') {
            $schema = "seda";
        }
        $message->schema = $schema;
        $message->type = "ArchiveDestructionRequest";
        $message->status = 'sent';
        $message->date = \laabs::newDateTime();
        $message->reference = $reference;
        
        $message->comment[] = $comment;

        if (!$senderOrg) {
            $senderOrg = \laabs::getToken('ORGANIZATION');
            if (!$senderOrg) {
                throw \laabs::newException('medona/invalidMessageException', "No current organization choosen");
            }
        }

        $message->senderOrgRegNumber = $senderOrg->registrationNumber;
        $message->recipientOrgRegNumber = $archives[0]->archiverOrgRegNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        try {
            if ($message->schema != 'medona') {

                $archiveDestructionRequestController = \laabs::newController($message->schema.'/ArchiveDestructionRequest');
                $archiveDestructionRequestController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
            } 

            $this->create($message);

            $message->unitIdentifier = array();
            foreach ($archives as $archive) {

                $unitIdentifier = \laabs::newInstance("medona/unitIdentifier");
                $unitIdentifier->messageId = $message->messageId;
                $unitIdentifier->objectId = (string) $archive->archiveId;
                $unitIdentifier->objectClass = "recordsManagement/archive";

                $this->sdoFactory->create($unitIdentifier);
                $message->unitIdentifier[] = $unitIdentifier;
            }

        } catch (\Exception $e) {
            $message->status = "error";
            $this->create($message);

            throw $e;
        }
        
        if ($message->senderOrgRegNumber != $archives[0]->originatorOrgRegNumber) {
            // Requested by archiver: send auth request to originator
            $authorizationOriginatingAgencyRequestController = \laabs::newController('medona/AuthorizationOriginatingAgencyRequest');
            $authorizationOriginatingAgencyRequestController->send($message, $archives[0]->originatorOrgRegNumber);
            
            $message->status == "authorization_wait";

        } else {
            // Requested by originator 
            $controlAuthorities = $this->orgController->getOrgsByRole('controlAuthority');

            // Check if control authority is set on system
            if (count($controlAuthorities)) {
                $message->status == "authorization_wait";

                $controlAuthority = reset($controlAuthorities);

                $authorizationControlAuthorityRequestController = \laabs::newController('medona/AuthorizationControlAuthorityRequest');
                $authorizationControlAuthorityRequestController->send($message);

            } else {
                
                $this->accept($message);
            }
        }

        return $message;
    }

    /**
     * Accept 
     * @param object $message
     */
    public function accept($message)
    {
        if (is_scalar($message)) {
            $message = $this->read($message);
        }

        $message->status = "accepted";

        $archiveController = \laabs::newController('recordsManagement/archive');
        $archiveController->setStatus((array) $message->unitIdentifier[0]->objectId, 'disposed');
        
        $this->sdoFactory->update($message);
    }

    /**
     * Process all archive destructions
     *
     * @return the result of process
     */
    public function processAll()
    {
        $index = $this->sdoFactory->index('medona/message', array('messageId'), 'type = "ArchiveDestructionRequest" AND status = "accepted"');

        foreach ($index as $messageId) {
            $this->process($messageId);
        }
    }

    /**
     * Process archive destruction
     * @param medona/message $messageId
     *
     * @return the result of process
     */
    public function process($messageId)
    {
        if (is_scalar($messageId)) {
            $message = $this->read($messageId);
        } else {
            $message = $messageId;
        }

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $archives = $this->archiveController->destruct((array) $message->unitIdentifier->objectId);

            $message->status = "processed";
            $message->operationDate = \laabs::newDatetime();

            $this->update($message);

        } catch (\Exception $e) {
            throw $e;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        $archiveDestructionNotificationController = \laabs::newController("medona/ArchiveDestructionNotification");
        $archiveDestructionNotificationController->send($message, $archives);

        return true;
    }
}
