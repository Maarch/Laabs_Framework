<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for ArchiveNotification
 * 
 * @author Prosper DE LAURE <prosper.delaure@maarch.org>
 */
class ArchiveNotification
    extends abstractMessage
{
     /**
     * Get received notification messages
     *
     * @return array Array of medona/message object
     */
    public function listReception()
    {
        $queryParts = array();
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "(type='ArchiveModificationNotification' OR type='ArchiveDestructionNotification' OR type='ArchivalProfileModificationNotification')";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Search archive modification notification messages
     * @param string $type               The type of notifiaction messages
     * @param string $senderOrgRegNumber The sender of notification messages
     * @param string $fromDate           The fromDate of notification messages
     * @param string $toDate             The toDate of notification messages
     * @param string $reference          The reference of notification message
     *
     * @return array Array of medona/message object
     */
    public function search($type = null, $senderOrgRegNumber = null, $fromDate = null, $toDate = null, $reference = null)
    {
        $queryParts = $this->searchMessage($senderOrgRegNumber, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        if ($reference) {
            $queryParts[] = "reference='$reference'";
        }
        if ($type) {
            $queryParts[] = "type='$type'";
        } else {
            $queryParts[] = "(type='ArchiveModificationNotification' OR type='ArchiveDestructionNotification' OR type='ArchivalProfileModificationNotification')";
        }
        if ($senderOrgRegNumber) {
            $queryParts[] = "senderOrgRegNumber = '$senderOrgRegNumber'";
        }
        if ($fromDate) {
            $queryParts[] = "date >='$fromDate'";
        }
        if ($toDate) {
            $queryParts[] = "date <='$toDate'";
        }

        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active = FALSE";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Count notification message
     *
     * @return array Number of notification messages
     */
    public function count()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $res = array();
        $queryParts = array();

        $queryParts[] = "(type='ArchiveModificationNotification' OR type='ArchiveDestructionNotification' OR type='ArchivalProfileModificationNotification')";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";
        $res['received'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        return $res;
    }
}