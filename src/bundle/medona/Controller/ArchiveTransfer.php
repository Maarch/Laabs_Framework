<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for archive transfer
 *
 * @package Medona
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
class ArchiveTransfer extends abstractMessage
{
    protected $archiveParser;

    protected $droid;

    protected $jhove;

    /**
     * Get received archive tranfer message
     *
     * @return array Array of medona/message object
     */
    public function listReception()
    {
        $queryParts = array();
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveTransfer'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active=true";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get sending archive tranfer message
     *
     * @return array Array of medona/message object
     */
    public function listSending()
    {
        $queryParts = array();
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveTransfer'";
        $queryParts[] = "senderOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active=true";

        return $this->sdoFactory->find("medona/message", implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get processed archive tranfer message
     * @param string $sender
     * @param string $recipient
     * @param string $fromDate
     * @param string $toDate
     * @param string $reference
     *
     * @return array Array of medona/message object
     */
    public function search($sender = null, $recipient = null, $fromDate = null, $toDate = null, $reference = null)
    {
        $queryParts = array();
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveTransfer'";
        $queryParts[] = "(recipientOrgRegNumber='$registrationNumber' OR senderOrgRegNumber='$registrationNumber')";
        $queryParts[] = "status='processed'";
        $queryParts[] = "active=false";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Count archive tranfer message
     *
     * @return array Number of received and sent messages
     */
    public function count()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $res = array();
        $queryParts = array();

        $queryParts["type"] = "type='ArchiveTransfer'";
        $queryParts["registrationNumber"] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts["active"] = "active='true'";
        $res['received'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        $queryParts["registrationNumber"] = "senderOrgRegNumber='$registrationNumber'";
        $res['sent'] = $this->sdoFactory->count('medona/message', implode(' and ', $queryParts));

        return $res;
    }

    /**
     * Receive message with all contents embedded
     * @param string $messageFile The message binary contents OR a filename
     * @param array  $attachments An array of attachment binary data
     *
     * @return medona/message
     * 
     * @todo Remove files from sas when error on reception
     */
    public function receive($messageFile, $attachments = array())
    {
        $messageId = \laabs::newId();
        $message = \laabs::newInstance('medona/message');
        $message->messageId = $messageId;
        $message->type = "ArchiveTransfer";
        $message->receptionDate = \laabs::newTimestamp();

        $messageDir = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId;

        if (is_file($messageFile)) {
            $this->receiveFile($message, $messageFile, $attachments);
        } elseif (is_string($messageFile)) {
            if (strtolower(substr($messageFile, 0, 5)) != "<?xml") {
                $messageFile = base64_decode($messageFile);
            }

            $this->receiveXml($message, $messageFile, $attachments);
        }

        // Extract basic information for medona/message object
        try {
            if ($message->schema != 'medona') {

                $archiveTransferController = \laabs::newController($message->schema.'/ArchiveTransfer');
                $archiveTransferController->receive($message);

            } else {
                // Validate SEDA xsd
                $schemaFile = LAABS_BUNDLE.DIRECTORY_SEPARATOR.'medona'.DIRECTORY_SEPARATOR.LAABS_RESOURCE.DIRECTORY_SEPARATOR.'xml'.DIRECTORY_SEPARATOR.'medona_V1.0.xsd';

                libxml_use_internal_errors(true);
                $valid = $message->xml->schemaValidate($schemaFile);
                if ($valid == false) {
                    $exception = \laabs::newException('medona/invalidMessageException', "Message not received: It doesn't respect the schema.");
                    $exception->errors = libxml_get_errors();

                    libxml_clear_errors();
                    libxml_use_internal_errors(false);

                    throw $exception;
                }

                $message->object = $message->xml->export('medona/ArchiveTransfer');

                //$this->receiveDescriptiveMetadata($message);

                $message->date = $message->object->date;

                $message->senderOrgRegNumber = $message->object->transferringAgency->identifier->value;
                $message->recipientOrgRegNumber = $message->object->archivalAgency->identifier->value;

                $message->reference = $message->object->messageIdentifier->value;
                $message->archivalAgreementReference = $message->object->archivalAgreement->value;

                $binaryDataObjects = $message->object->dataObjectPackage->binaryDataObject;
                $physicalDataObjects = $message->object->dataObjectPackage->physicalDataObject;

                $message->dataObjectCount = count($binaryDataObjects) + count($physicalDataObjects);

                $message->size = 0;
                for ($i = 0, $l = count($binaryDataObjects); $i < $l; $i++) {
                    $binaryDataObject = $binaryDataObjects[$i];
                    $size = $binaryDataObject->size;

                    $message->size += (integer) $size;
                }
            }

            $message->status = "received";

            $this->create($message);

        } catch (\Exception $e) {
            if (\laabs::hasBundle('lifeCycle')) {
                $eventInfo = array();
                $operationResult = false;

                $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
                $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
                $eventInfo['reference'] = $message->reference;

                $event = $this->lifeCycleJournalController->logEvent(
                    'medona/archiveTransferReception',
                    'medona/message',
                    $message->messageId,
                    $eventInfo,
                    "Reception of the message",
                    $operationResult
                );
            }            
            
            // Remove files from sas

            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $operationResult = true;

            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveTransferReception',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Reception of the message",
                $operationResult
            );
        }

        // Small message (less than 2Mb) are validated synchronously
        if ($message->size < 2097152) {
            $this->validate($message);
        }

        return $message->messageId;
    }

    protected function receiveFile($message, $messageFile, $attachments=array())
    {
        $messageDir = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId;

        // Load Xml from file
        $this->loadXmlFile($message, $messageFile);

        // Save to message directory
        $this->save($message);
        
        if (count($attachments)) {
            foreach ($attachments as $attachment) {
                if (is_file($attachment)) {
                    copy($attachment, $messageDir.DIRECTORY_SEPARATOR.basename($attachment));
                } elseif (is_dir($attachment)) {
                    $filenames = glob(dirname($messageFile).DIRECTORY_SEPARATOR."*.*");
                    foreach ($filenames as $filename) {
                        if (basename($filename) === basename($messageFile)) {
                            continue;
                        }
                        copy($filename, $messageDir.DIRECTORY_SEPARATOR.basename($filename));
                    }
                }
            }
        }
    }

    protected function receiveXml($message, $messageFile, $attachments=array())
    {
        $messageDir = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId;

        // Load file xml
        $this->loadXml($message, $messageFile);

        // Save to message directory
        $this->save($message);

        if (count($attachments)) {
            foreach ($attachments as $attachment) {
                $attachment->data = base64_decode($attachment->data);
                file_put_contents($messageDir.DIRECTORY_SEPARATOR.$attachment->filename, $attachment->data);
            }
        }
    }

    /**
     * Validate the messages
     *
     * @return medona/message $message
     */
    public function validateBatch()
    {
        $messages = $this->sdoFactory->find("medona/message", "status='received' AND type='ArchiveTransfer' AND active=true");
        foreach ($messages as $message) {
            $this->loadXmlFile($message);
            $this->validate($message);
        }
    }

    /**
     * Validate message against schema and rules
     * @param mixed $message The message to validate or the message identifier
     *
     * @return boolean
     */
    public function validate($message)
    {
        $this->errors = array();
        $this->replyCode = null;

        if (is_scalar($message)) {
            $messageId = $message;
            $message = $this->sdoFactory->read('medona/message', $messageId);

            $this->loadXmlFile($message);
        }

        if ($message->status == "error") {
            throw new \bundle\medona\Exception\invalidStatusException("Le message de transfert est en erreur.");
        }

        try {
            // Check sender (depositor) roles
            $this->validateDepositor($message);

            // Check recipient (archiver) roles
            $this->validateArchiver($message);

            if (isset($message->archivalAgreementReference)) {
                $this->validateArchivalAgreement($message);
            }

            // Get the validator for schema (medona/seda/other)
            if ($message->schema != 'medona') {
                $archiveTransferController = \laabs::newController($message->schema.'/ArchiveTransfer');
                $archiveTransferController->validate($message, $this->currentArchivalAgreement);

                if ($archiveTransferController->replyCode && !$this->replyCode) {
                    $this->replyCode = $archiveTransferController->replyCode;
                }
                $this->errors = array_merge($this->errors, $archiveTransferController->errors);
            } else {
                //$this->validateOriginators($message, $archivalAgreement);

                // Validation du profil métier
                $this->validateProfile($message, $this->currentArchivalAgreement);

                // Contrôle des documents attachés
                $this->validateAttachments($message, $this->currentArchivalAgreement);
            }

            // Non blocking errors
            if (count($this->errors) > 0) {
                throw \laabs::newException('medona/invalidMessageException', "Invalid message");
            }

            $message->status = "valid";
            $this->sdoFactory->update($message);
        } catch (\Exception $e) {
            $message->status = "invalid";
            foreach ((array) $this->errors as $error) {
                $message->comment[] = $error->getMessage();
            }
            $this->sdoFactory->update($message);

            $archiveTransferReplyController = \laabs::newController('medona/ArchiveTransferReply');
            $archiveTransferReplyController->send($message, $this->replyCode);

            $exception = \laabs::newException('medona/invalidMessageException', "Invalid message");
            if (count($this->errors) > 0) {
                $exception->errors = $this->errors;
            } else {
                $exception->errors[] = new \core\Error($e->getMessage());
            }

            throw $exception;
        }

        if ($this->currentArchivalAgreement->autoTransferAcceptance) {
            $this->accept((string) $message->messageId);

            if ($this->currentArchivalAgreement->processSmallArchive && ($message->size < 2097152)) {
                $this->process($message);
            }
        }
    }

    protected function validateDepositor($message)
    {
        try {
            $senderOrg = $this->orgController->getOrgByRegNumber($message->senderOrgRegNumber);
        } catch (\Exception $e) {
            $this->sendError("202", "Le service versant identifié par '".$message->senderOrgRegNumber."' est inconnu du système.");

            throw \laabs::newException('medona/invalidMessageException', "Invalid message");
        }

        $senderRoles = (array) $senderOrg->orgRoleCodes;
        if (!in_array("depositor", $senderRoles)) {
            $this->sendError("202", "Le service versant identifié par '".$message->senderOrgRegNumber."' ne possède pas le rôle d'acteur adéquat dans le système.");
        }

        return true;
    }

    protected function validateArchiver($message)
    {
        try {
            $recipientOrg = $this->orgController->getOrgByRegNumber($message->recipientOrgRegNumber);
        } catch (\Exception $e) {
            $this->sendError("201", "Le service d'archive identifié par '".$message->recipientOrgRegNumber."' est inconnu du système.");

            throw \laabs::newException('medona/invalidMessageException', "Invalid message");
        }

        $recipientRoles = (array) $recipientOrg->orgRoleCodes;
        if (!in_array("archiver", $recipientRoles)) {
            $this->sendError("202", "Le service d'archives identifié par '".$message->recipientOrgRegNumber."' ne possède pas le rôle d'acteur adéquat dans le système.");
        }

        return true;
    }

    protected function validateArchivalAgreement($message)
    {

        try {
            $this->useArchivalAgreement($message->archivalAgreementReference);

        } catch (\Exception $e) {
            $this->sendError("300", "L'accord de versement '$message->archivalAgreementReference' n'a pas pu être lu.");

            return;
        }

        // Check actor orgnizations
        if ($this->currentArchivalAgreement->depositorOrgRegNumber != $message->senderOrgRegNumber) {
            $this->sendError("303", "Le service versant n'est pas conforme à celui indiqué dans l'accord de versement.");
        }

        if ($this->currentArchivalAgreement->archiverOrgRegNumber != $message->recipientOrgRegNumber) {
            $this->sendError("304", "Le service d'archives n'est pas conforme à celui indiqué dans l'accord de versement.");
        }

        if (empty($this->currentArchivalAgreement->originatorOrgIds)) {
            $this->sendError("300", "Il n'y a pas de service producteur.");
        }

        // Check dates and activity
        $today = \laabs::newDate();
        if ($this->currentArchivalAgreement->beginDate->diff($today)->invert) {
            $this->sendError("312", "L'accord de versement n'est pas encore en cours de validité.");
        }
        if (!$this->currentArchivalAgreement->endDate->diff($today)->invert) {
            $this->sendError("312", "L'accord de versement n'est plus en cours de validité.");
        }
        if ($this->currentArchivalAgreement->enabled != true) {
            $this->sendError("300", "L'accord de versement n'est pas actif.");
        }

        $das = $this->sdoFactory->das;
        $query = "SELECT SUM(size) FROM medona.message WHERE date > TO_DATE (:date, 'YYYY-MM-DD H-M-S')";
        $stmt = $das->prepare($query);

        if ($message->size > ($this->currentArchivalAgreement->maxSizeTransfer*1048576) && $this->currentArchivalAgreement->maxSizeTransfer > 0) {
            $this->sendError("301", "La taille maximale par tranfert de l'accord de versement est dépassée.");
        }

        $stmt->execute(array("date" => $today->sub(new \core\Type\Duration("P1D"))));
        if ($stmt->fetchColumn() > ($this->currentArchivalAgreement->maxSizeDay*1048576) && $this->currentArchivalAgreement->maxSizeDay > 0) {
            $this->sendError("301", "La taille maximale par jour de l'accord de versement est dépassée.");
        }

        $stmt->execute(array("date" => $today->sub(new \core\Type\Duration("P7D"))));
        if ($stmt->fetchColumn() > ($this->currentArchivalAgreement->maxSizeWeek*1048576) && $this->currentArchivalAgreement->maxSizeWeek > 0) {
            $this->sendError("301", "La taille maximale par semaine de l'accord de versement est dépassée.");
        }

        $stmt->execute(array("date" => $today->sub(new \core\Type\Duration("P1M"))));
        if ($stmt->fetchColumn() > ($this->currentArchivalAgreement->maxSizeMonth*1048576) && $this->currentArchivalAgreement->maxSizeMonth > 0) {
            $this->sendError("301", "La taille maximale par mois de l'accord de versement est dépassée.");
        }

        $stmt->execute(array("date" => $today->sub(new \core\Type\Duration("P1Y"))));
        if ($stmt->fetchColumn() > ($this->currentArchivalAgreement->maxSizeYear*1048576) && $this->currentArchivalAgreement->maxSizeYear > 0) {
            $this->sendError("301", "La taille maximale par année de l'accord de versement est dépassée.");
        }

        $stmt->execute(array("date" => $this->currentArchivalAgreement->beginDate));
        if ($stmt->fetchColumn() > ($this->currentArchivalAgreement->maxSizeAgreement*1048576) && $this->currentArchivalAgreement->maxSizeAgreement > 0) {
            $this->sendError("301", "La taille maximale de l'accord de versement est dépassée.");
        }

        return true;
    }

    protected function validateOriginators($message, $archivalAgreement)
    {
        // Contrôle du producteur
        $originatorOrgRegNumberElements = $message->xPath->query('medona:OriginatingAgency/medona:Identification');
        $originatorOrgs = array();

        foreach ($originatorOrgRegNumberElements as $originatorOrgRegNumberElement) {
            $originatorOrgRegNumber = $originatorOrgRegNumberElement->nodeValue;
            if (!isset($originatorOrgs[$originatorOrgRegNumber])) {
                try {
                    $originatorOrg = $this->orgController->getOrgByRegNumber($originatorOrgRegNumber);
                    $originatorOrgs[$originatorOrgRegNumber] = $originatorOrg;
                } catch (\Exception $e) {
                    $this->sendError("200", "Le producteur de l'archive n'est pas référencé dans le système.");

                    continue;
                }
            } else {
                $originatorOrg = $originatorOrgs[$originatorOrgRegNumber];
            }

            if (!in_array('originator', (array) $originatorOrg->orgRoleCodes)) {
                $this->sendError("302", "Le producteur indiqué pour l'archive n'est pas référencé comme producteur dans le système.");
            }

            if (!in_array((string) $originatorOrg->orgId, (array) $archivalAgreement->originatorOrgIds)) {
                $this->sendError("302", "Le producteur de l'archive n'est pas conforme à celui indiqué dans l'accord de versement.");
            }
        }
    }

    protected function validateProfile($message, $archivalAgreement)
    {
        if (!$message->object->dataObjectPackage->managementMetadata->archivalProfile
            || $message->object->dataObjectPackage->managementMetadata->archivalProfile->value == "") {
            $this->sendError("203", "Aucun profil d'archivage spécifié dans le bordereau");

            return;
        }

        $archivalprofileReference = $message->object->dataObjectPackage->managementMetadata->archivalProfile->value;

        if ($archivalprofileReference != $archivalAgreement->archivalProfileReference) {
            $this->sendError("203", "Le profil d'archivage du bordereau $archivalprofileReference ne correspond pas à celui de l'accord de versement");

            return;
        }

        $profileFiles = glob($this->profilesDirectory.DIRECTORY_SEPARATOR.$archivalprofileReference.'.*');

        if (count($profileFiles) == 0) {
            $this->sendError("203", "Le message fait référence à un profil d'archivage $archivalprofileReference non enregistré dans le système.");

            return;
        }

        foreach ($profileFiles as $profileFile) {
            $extension = pathinfo($profileFile, \PATHINFO_EXTENSION);

            switch ($extension) {
                case 'rng':
                    //if ($validationMode == 'jing') {
                    if (true) {
                        $jing = \laabs::newService('dependency/xml/plugins/jing/jing');
                        $xmlFile = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.".xml";
                        $valid = $jing->validate($profileFile, $xmlFile);
                        if (!$valid) {
                            $this->sendError("203");

                            foreach ($jing->getErrors() as $errorMessage) {
                                $this->errors[] = new \core\error($errorMessage);
                            }
                        }

                    } else {
                        libxml_use_internal_errors(true);
                        $valid = $message->xml->relaxNGValidate($profileFile);
                        if ($valid == false) {
                            $this->sendError("203");
                            foreach (libxml_get_errors() as $libxmlError) {
                                $this->errors[] = new \core\error($libxmlError->message, null, $libxmlError->code);
                            }
                        }
                        libxml_clear_errors();
                        libxml_use_internal_errors(false);
                    }
                    break;

                case 'xsd':
                    libxml_use_internal_errors(true);
                    $valid = $message->xml->schemaValidate($profileFile);
                    if ($valid == false) {
                        foreach (libxml_get_errors() as $libxmlError) {
                            $this->errors[] = new \core\error($libxmlError->message, null, $libxmlError->code);
                        }
                    }
                    libxml_clear_errors();
                    libxml_use_internal_errors(false);
                    break;

                default:
                    $this->sendError("203", "Le message fait référence à un profil d'archivage dont le format n'est pas géré.");
            }
        }
    }

    protected function validateAttachments($message, $archivalAgreement)
    {
        $serviceLevelController = \laabs::newController("recordsManagement/serviceLevel");
        $serviceLevel = $serviceLevelController->getByReference($archivalAgreement->serviceLevelReference);

        if (!isset($this->dfi)) {
            $this->dfi = \laabs::newService('dependency/fileSystem/plugins/dfi');
        }

        // Retrieve format information
        $dfimodules = 0;
        if (strpos($serviceLevel->control, 'formatDetection') !== false) {
            $dfimodules += DFI_USE_DROID;
        }
        if (strpos($serviceLevel->control, 'formatValidation') !== false) {
            $dfimodules += DFI_USE_JHOVE;
        }
        if (strpos($serviceLevel->control, 'formatCaracterization') !== false) {
            $dfimodules += DFI_USE_MEDIAINFO;
        }


        $allowedFormats = \laabs\explode(' ', $archivalAgreement->allowedFormats);

        $binaryDataObjects = $message->object->dataObjectPackage->binaryDataObject;

        $fromdir = dirname($message->xml->uri);

        // List received files
        $receivedFiles = glob($fromdir.DIRECTORY_SEPARATOR."*.*");
        // List actual message files, starting with message xml itself
        $messageFiles = array($message->xml->uri);

        for ($i = 0, $l = count($binaryDataObjects); $i < $l; $i++) {
            $binaryDataObject = $binaryDataObjects[$i];
            $dataObjectId = $binaryDataObject->id;

            $attachment = $binaryDataObject->attachment;

            if ($attachment->filename) {
                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$attachment->filename;
                if (!is_file($filepath)) {
                    $this->sendError("211", "Le document identifié par le nom '$filename' n'a pas été trouvé.");

                    continue;
                }

                $contents = file_get_contents($filepath);

                $messageFiles[] = $attachment->filename;

            } elseif ($attachment->uri) {
                $contents = file_get_contents($attachment->getAttribute('uri'));

                if (!$contents) {
                    $this->sendError("211", "Le document à l'adresse '$uri' est indisponible.");

                    continue;
                }

                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$dataObjectId;
                file_put_contents($filepath, $contents);
            } else {
                if (strlen($attachmentElement->value) == 0) {
                    $this->sendError("211", "Le contenu du document n'a pas été transmis.");

                    continue;
                }

                $contents = base64_decode($attachment->value);

                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$dataObjectId;
                file_put_contents($filepath, $contents);
            }


            // Get file format information
            $dfiInfo = $this->dfi->getInfo($filepath, $dfimodules);

            if (strpos($serviceLevel->control, 'formatDetection') !== false) {
                if (!isset($dfiInfo->format->puid)) {
                    $this->sendError("205", "Le format du document '".basename($filepath)."' n'a pas pu être déterminé");
                } else {
                    $puid = $dfiInfo->format->puid;

                    /*if ($format->puid != $attachment->format) {
                        $this->sendError("205 Format de document non conforme au format déclaré.");
                        array_push($this->errors, new \core\Error("Le format du document '".basename($filepath)."' ".$format->puid." ne correspond pas à celui indiqué (".$attachment->format.")."));
                    }*/

                    file_put_contents(
                        $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$dataObjectId.'.info',
                        json_encode($dfiInfo, \JSON_PRETTY_PRINT)
                    );
                }
            }

            // Validate format is allowed
            if (count($allowedFormats) && isset($puid) && !in_array($puid, $allowedFormats)) {
                $this->sendError("307", "Le format du document '".basename($filepath)."' ".$format->puid." n'est pas autorisé par l'accord de versement.");
            }

            // Validate format
            if (!isset($dfiInfo->status) || $dfiInfo->status->code != 0) {
                $this->sendError("307", "Le format du document '".basename($filepath)."' ".$format->puid." n'est pas valide : " . $dfiInfo->status->info);
            }

            // Validate hash
            $messageDigest = $binaryDataObject->messageDigest;
            if ($messageDigest->value != hash($messageDigest->algorithm, $contents)) {
                $this->sendError("207", "L'empreinte numérique du document '".basename($filepath)."' ne correspond pas à celle transmise.");
            }
        }

        // Check all files received are part of the message
        foreach ($receivedFiles as $receivedFile) {
            if (!in_array(basename($receivedFile), $messageFiles) && basename($receivedFile) != basename($message->xml->uri)) {
                $this->sendError("101", "Le fichier '".basename($receivedFile)."' ,'est pas référencé dans le bordereau.");

                return;
            }
        }
    }

    /**
     * Process the messages
     *
     * @return medona/message $message
     */
    public function processBatch()
    {
        $messages = $this->sdoFactory->find("medona/message", "status='accepted' AND type='ArchiveTransfer' AND active=true");
        foreach ($messages as $message) {
            $this->loadXmlFile($message);
            $this->readOrgs($message);

            if (!empty($message->data)) {
                $message->object = json_decode($message->data);
            }

            $this->process($message);
        }
    }

    /**
     * Validate message against schema and rules
     * @param medona/message $messageId
     *
     * @return the result of process
     */
    public function process($messageId)
    {
        if (is_scalar($messageId)) {
            $message = $this->read($messageId);

            $this->loadXmlFile($message);
        } else {
            $message = $messageId;
        }

        try {
            if ($message->schema != 'medona') {
                $archiveTransferController = \laabs::newController($message->schema.'/ArchiveTransfer');
                $archiveTransferController->process($message);

            } else {
                $message->xPath->registerNamespace('recordsManagement', 'maarch.org:laabs:recordsManagement');
                $message->xPath->registerNamespace('documentManagement', 'maarch.org:laabs:documentManagement');
                $message->xPath->registerNamespace('digitalResource', 'maarch.org:laabs:digitalResource');
                $message->xPath->registerNamespace('organization', 'maarch.org:laabs:organization');

                // Extract ->dataObjectPackage->managementMetadata->accessRule (recordsManagament/accessRule)
                $accessRule = \laabs::newInstance('recordsManagement/accessRule');
                $message->object->dataObjectPackage->managementMetadata->accessRule = $accessRule;
                $accessRuleElement = $message->xPath->query("medona:DataObjectPackage/medona:ManagementMetadata/medona:AccessRule")->item(0);
                if ($codeElement = $message->xPath->query('recordsManagement:accessRule/recordsManagement:code', $accessRuleElement)->item(0)) {
                    $accessRule->code = $codeElement->nodeValue;
                }
                if ($startDateElement = $message->xPath->query('recordsManagement:accessRule/recordsManagement:startDate', $accessRuleElement)->item(0)) {
                    $accessRule->startDate = \laabs::newDate($startDateElement->nodeValue);
                }
                if ($durationElement = $message->xPath->query('recordsManagement:accessRule/recordsManagement:duration', $accessRuleElement)->item(0)) {
                    $accessRule->duration = \laabs::newDuration($durationElement->nodeValue);
                }
                if ($originatingAgencyElement = $message->xPath->query('recordsManagement:accessRule/recordsManagement:originatingAgency', $accessRuleElement)->item(0)) {
                    if ($originatorOrgRegNumberElement = $message->xPath->query('recordsManagement:Identifier', $originatingAgencyElement)->item(0)) {
                        $accessRule->originatorOrgRegNumber = $originatorOrgRegNumberElement->nodeValue;
                    }

                    /*if ($orgNameElement = $message->xPath->query('recordsManagement:OrganizationDescriptiveMetadata', $originatingAgencyElement)->item(0)) {
                        $object->originatorOrgName = $orgNameElement->nodeValue;
                    }*/
                }

                // Extract ->dataObjectPackage->descriptiveMetadata (recordsManagament/archive)
                $descriptionElement = $message->xPath->query('medona:DataObjectPackage/medona:DescriptiveMetadata/*')->item(0);
                switch ($descriptionElement->localName) {
                    case 'archivePackage':
                        $message->object->dataObjectPackage->descriptiveMetadata = array();
                        $archiveParser = \laabs::newParser('recordsManagement/archive', 'xml');
                        $archiveElements = $message->xPath->query('//recordsManagement:archive', $descriptionElement);
                        foreach ($archiveElements as $archiveElement) {
                            $archive = $archiveParser->create($message->xml->saveXml($archiveElement));

                            $message->object->dataObjectPackage->descriptiveMetadata[] = $archive;
                        }
                        break;

                    default:
                        break;
                }

                foreach ($message->object->dataObjectPackage->descriptiveMetadata as $archive) {
                    $archive = $this->processArchive($message, $archive);
                }

            }
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "error";
            $operationResult = false;
            $this->sdoFactory->update($message);

            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveTransferProcessing',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Processing of the message",
                $operationResult
            );
        }

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $message->unitIdentifier = array();

            if ($message->schema != 'medona') {
                $archives = $message->archive;
            } else {
                $archives = $message->object->dataObjectPackage->descriptiveMetadata;
            }

            foreach ($archives as $archive) {
                $this->archiveController->deposit($archive);
                $unitIdentifier = \laabs::newInstance("medona/unitIdentifier");
                $unitIdentifier->messageId = $message->messageId;
                $unitIdentifier->objectId = (string) $archive->archiveId;
                $unitIdentifier->objectClass = "recordsManagement/archive";

                $this->sdoFactory->create($unitIdentifier);
                $message->unitIdentifier[] = $unitIdentifier;
            }

            $message->status = "processed";
            $this->sdoFactory->update($message);

        } catch (\Exception $e) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            $message->status = "error";
            $this->sdoFactory->update($message);

            throw $e;
        }



        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        $archiveTransferReplyController = \laabs::newController('medona/ArchiveTransferReply');

        $archiveTransferReplyController->send($message, "000", $archives);
    }

    protected function processArchive($message, $medonaArchive)
    {
        $medonaArchive->archiveId = \laabs::newId();
        $medonaArchive->archiverOrgRegNumber = $message->recipientOrgRegNumber;
        $medonaArchive->depositorOrgRegNumber = $message->senderOrgRegNumber;
        $medonaArchive->archivalAgreementReference = $message->archivalAgreementReference;
        $medonaArchive->originatorOrgRegNumber =  $message->object->dataObjectPackage->managementMetadata->accessRule->originatorOrgRegNumber;


        if ($message->object->dataObjectPackage->managementMetadata->archivalProfile) {
            $medonaArchive->archivalProfileReference = $message->object->dataObjectPackage->managementMetadata->archivalProfile->value;
        }

        if ($message->object->dataObjectPackage->managementMetadata->serviceLevel) {
            $medonaArchive->serviceLevelReference = $message->object->dataObjectPackage->managementMetadata->serviceLevel->value;
        }

        if (isset($message->object->dataObjectPackage->managementMetadata->appraisalRule)) {
            $medonaArchive = $this->processAppraisalRule($medonaArchive, $message->object->dataObjectPackage->managementMetadata->appraisalRule);
        }

        if (isset($message->object->dataObjectPackage->managementMetadata->accessRule)) {
            $medonaArchive = $this->processAccessRestrictionRule($medonaArchive, $message->object->dataObjectPackage->managementMetadata->accessRule);
        }

        if (isset($medonaArchive->document)) {
            foreach ($medonaArchive->document as $medonaDocument) {
                $docName = key($medonaArchive->document);
                $medonaDocument = $this->processDocument($medonaDocument, $docName, $message);
            }
        }

        return $medonaArchive;
    }

    protected function processAppraisalRule($object, $medonaAppraisalRule)
    {
        if (isset($medonaAppraisalRule->appraisalCode)) {
            switch ((string) $medonaAppraisalRule->appraisalCode) {
                case 'conserver':
                    $object->finalDisposition = 'preservation';
                    break;

                case 'detruire':
                    $object->finalDisposition = 'destruction';
                    break;
            }
        }

        if (isset($medonaAppraisalRule->startDate)) {
            $object->retentionStartDate = \laabs::cast((string) $medonaAppraisalRule->startDate, 'datetime');
        }

        if (isset($medonaAppraisalRule->code)) {
            $object->retentionRuleCode = $medonaAppraisalRule->code;
        }

        if (isset($medonaAppraisalRule->duration)) {
            $object->retentionDuration = \laabs::cast((string) $medonaAppraisalRule->duration, 'duration');
        }

        return $object;
    }

    protected function processAccessRestrictionRule($object, $medonaAccessRestrictionRule)
    {
        if (isset($medonaAccessRestrictionRule->code)) {
            $object->accessRuleCode = (string) $medonaAccessRestrictionRule->code;
        }

        if (isset($medonaAccessRestrictionRule->duration)) {
            $object->accessRuleDuration = \laabs::cast((string) $medonaAccessRestrictionRule->duration, 'duration');
        }

        if (isset($medonaAccessRestrictionRule->startDate)) {
            $object->accessRuleStartDate = \laabs::cast((string) $medonaAccessRestrictionRule->startDate, 'datetime');
        }

        return $object;
    }

    protected function processDocument($medonaDocument, $docName, $message)
    {
        $resource = \laabs::newInstance('digitalResource/digitalResource');
        $resource->resId = \laabs::newId();

        foreach ($message->object->dataObjectPackage->binaryDataObject as $binaryDataObject) {
            if ($binaryDataObject->id == $docName) {
                
                $resource->size = (string) $binaryDataObject->size;
                $resource->mimetype = $binaryDataObject->format;
                $resource->hash = $binaryDataObject->messageDigest->value;
                $resource->hashAlgorithm = $binaryDataObject->messageDigest->algorithm;
                $resource->fileName = $binaryDataObject->attachment->filename;
                $resource->fileExtension = pathinfo($resource->fileName, \PATHINFO_EXTENSION);

                $riString = file_get_contents($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$docName.'.info');
                $ri = json_decode($riString);
                $resource->puid = $ri->format->puid;
                $resource->mediaInfo = $riString;

                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$resource->fileName;
                $contents = file_get_contents($filepath);
                $resource->setContents($contents);
            }
        }
        $medonaDocument->digitalResource = $resource;

        return $medonaDocument;
    }

    /**
     * Accept a message
     * @param string $messageId the message identifier
     * @param string $comment   a comment
     */
    public function accept($messageId, $comment = null)
    {
        $this->changeStatus($messageId, "accepted", $comment);

        if (\laabs::hasBundle('lifeCycle')) {
            $message = $this->sdoFactory->read('medona/message', array('messageId' => $messageId));
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveTransferAcceptance',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Acceptance of the message",
                true
            );
        }
    }

    /**
     * Reject a message
     * @param string $messageId The message identifier
     * @param string $comment   a comment
     */
    public function reject($messageId, $comment = null)
    {
        $this->changeStatus($messageId, "rejected", $comment);
        $archiveTransferReplyController = \laabs::newController('medona/ArchiveTransferReply');
        $archiveTransferReplyController->send($messageId, "REJECTED", $comment);

        if (\laabs::hasBundle('lifeCycle')) {
            $message = $this->sdoFactory->read('medona/message', array('messageId' => $messageId));
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveTransferRejection',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Rejection of the message",
                true
            );
        }
    }
}
