<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for archive transfer
 * 
 * @package Medona
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
class ArchiveTransferReply
    extends abstractMessage
{

    /**
     * Send a new transfer reply
     * @param string $transferMessage The request message identifier
     * @param string $replyCode       The reply code
     * @param array  $archives        The archives to send
     *
     * @return The reply message generated
     */
    public function send($transferMessage, $replyCode = "OK", $archives = array())
    {
        if (is_scalar($transferMessage)) {
            $messageId = $transferMessage;
            $transferMessage = $this->sdoFactory->read('medona/message', $messageId);
        }

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "ArchiveTransferReply";
        $message->schema = $transferMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->replyCode = $replyCode;

        $message->reference = $transferMessage->reference.'_Reply';
        $message->comment = $transferMessage->comment;
        $message->requestReference = $transferMessage->reference;
        $message->operationDate = \laabs::newDatetime();

        $message->senderOrgRegNumber = $transferMessage->recipientOrgRegNumber;
        $message->recipientOrgRegNumber = $transferMessage->senderOrgRegNumber;
        $this->readOrgs($message); // read org names, addresses, communications, contacts

        
        $message->unitIdentifier = $transferMessage->unitIdentifier;
        if (!is_array($archives)) {
            $archives = array($archives);
        }

        $message->archive = $archives;

        /*$message->lifeCycleEventId = \laabs::newTokenList();
        foreach ($archives as $archive) {
            foreach ($archives as $archive) {
                if ($archive->lifeCycleEvent) {
                    foreach ($archive->lifeCycleEvent as $event) {
                        $message->lifeCycleEventId[] = (string) $event->eventId;
                    }
                }
            }
        }*/

        try {
            if ($message->schema != 'medona') {

                $archiveTransferReplyController = \laabs::newController($message->schema.'/ArchiveTransferReply');
                $archiveTransferReplyController->send($message);

            } else {
                $this->generate($message);
                $this->save($message);
            }           
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "error";
            $operationResult = false;

            throw $e;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/archiveTransferReplySending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        $this->create($message);

        return $message;
    }
}
