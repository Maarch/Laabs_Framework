<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for control authority authorization request message
 * 
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class AuthorizationControlAuthorityRequest
    extends AuthorizationRequest
{
    /**
     * Send a new authorization control authority request
     * @param mixed $requestMessage The message identifier
     *
     * @return medona/message The message generated
     */
    public function send($requestMessage)
    {        
        if (is_scalar($requestMessage)) {
            $requestMessage = $this->read($requestMessage);
        }
      
        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->schema = $requestMessage->schema;
        $message->type = "AuthorizationControlAuthorityRequest";
        $message->status = 'sent';
        $message->date = \laabs::newDateTime();
        $message->reference = $requestMessage->reference."_AuthorizationControlAuthority";

        $message->authorizationReference = $requestMessage->reference;
        $message->authorizationRequesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $message->authorizationReason = $requestMessage->type;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;

        $recipientOrg = $this->orgController->getOrgsByRole("controlAuthority")[0];
        $message->recipientOrgRegNumber = $recipientOrg->registrationNumber;
        $this->readOrgs($message);

        $message->authorizationRequestContent = \laabs::newInstance('medona/AuthorizationRequestContent');
        $message->authorizationRequestContent->authorizationReason = $requestMessage->type;
        $message->authorizationRequestContent->requestDate = $requestMessage->date;
        $message->authorizationRequestContent->unitIdentifier = $requestMessage->unitIdentifier;
        $message->authorizationRequestContent->requester = $requestMessage->senderOrg;

        /*
        $message->originatingAgencyAuthorization = new \stdClass();
        $message->originatingAgencyAuthorization->identifier = (string) $originatingOrg->orgId;
        $message->originatingAgencyAuthorization->comment = null;
        $message->originatingAgencyAuthorization->originatingAgency = $originatingOrg->orgName;
        */

        try {
            
            if ($message->schema != 'medona') {

                $authorizationControlAuthorityRequestController = \laabs::newController($message->schema.'/AuthorizationControlAuthorityRequest');
                $authorizationControlAuthorityRequestController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
            }     
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "invalid";
            $operationResult = false;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationControlAuthorityRequestSending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }
        $this->create($message);

        return $message;
    }

    /**
     * Accepte the authorization control authority request
     *
     * @param string $messageId The message identifier
     */
    public function accept($messageId)
    {
        if (is_scalar($messageId)) {
            $message = $this->read($messageId);
        } else {
            $message = $messageId; 
        }

        $this->changeStatus($messageId, "accepted");

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));

        $requestMessageController = \laabs::newController('medona/'.$requestMessage->type);
        $requestMessageController->accept($requestMessage);

        \laabs::newController('medona/AuthorizationControlAuthorityRequestReply')->send($message);
        
        if (\laabs::hasBundle('lifeCycle')) {
            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationControlAuthorityRequestSending',
                'medona/message',
                $message->messageId,
                null,
                "Acceptance of the message",
                true
            );
        }
    }

    /**
     * Reject the authorization control authority request
     *
     * @param string $messageId The message identifier
     */
    public function reject($messageId)
    {
        if (is_scalar($messageId)) {
            $message = $this->read($messageId);
        } else {
            $message = $messageId; 
        }

        $this->changeStatus($messageId, "rejected");

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));
        $this->archiveController->setStatus($requestMessage->messageId, "rejected");

        \laabs::newController('medona/AuthorizationControlAuthorityRequestReply')->send($message, 'REJECT');

        if (\laabs::hasBundle('lifeCycle')) {
            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationControlAuthorityRequestRejection',
                'medona/message',
                $message->messageId,
                null,
                "Rejection of the message",
                true
            );
        }
    }
}
