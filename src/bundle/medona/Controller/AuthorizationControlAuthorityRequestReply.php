<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for control authority authorization request message
 * 
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class AuthorizationControlAuthorityRequestReply
    extends abstractMessage
{
     /**
     * Send a new authorization authority request reply
     * @param string $requestMessage The request message identifier
     * @param string $replyCode      The reply code
     *
     * @return The reply message generated
     */
    public function send($requestMessage, $replyCode = "000")
    {
        if (is_scalar($requestMessage)) {
            $requestMessage = $this->read($requestMessage);
        } 

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "AuthorizationControlAuthorityRequestReply";
        $message->schema = $requestMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->replyCode = $replyCode;

        $message->reference = $requestMessage->reference.'_Reply';
        $message->requestReference = $requestMessage->reference;
        
        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $message->recipientOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $this->readOrgs($message);
        
        try {

            if ($message->schema != 'medona') {

                $authorizationControlAuthorityRequestReplyController = \laabs::newController($message->schema.'/AuthorizationControlAuthorityRequestReply');
                $authorizationControlAuthorityRequestReplyController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
            }     
            $operationResult = true;
            
        } catch (\Exception $e) {
            $message->status = "invalid";
            $operationResult = false;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationOriginatingAgencyRequestReplySending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        $this->create($message);

        return $message;
    }
}
