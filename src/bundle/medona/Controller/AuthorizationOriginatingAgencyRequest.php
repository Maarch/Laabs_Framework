<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for originating agency request auhorisation message
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class AuthorizationOriginatingAgencyRequest
    extends AuthorizationRequest
{
    /**
     * Send authorization originating agency request
     * @param string $requestMessage        The request message identifier
     * @param string $recipientOrgRegNumber The originating agency reg number
     *
     * @return medona/message
     */
    public function send($requestMessage, $recipientOrgRegNumber)
    {
        if (is_scalar($requestMessage)) {
            $requestMessage = $this->read($requestMessage);
        }

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->schema = $requestMessage->schema;
        $message->type = "AuthorizationOriginatingAgencyRequest";
        $message->status = 'sent';
        $message->date = \laabs::newDateTime();
        $message->reference = $requestMessage->reference."_AuthorizationOriginatingAgency";

        $message->authorizationReference = $requestMessage->reference;
        $message->authorizationRequesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $message->authorizationReason = $requestMessage->type;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $message->recipientOrgRegNumber = $recipientOrgRegNumber;
        $this->readOrgs($message);

        $message->authorizationRequestContent = \laabs::newInstance('medona/AuthorizationRequestContent');
        $message->authorizationRequestContent->authorizationReason = $requestMessage->type;
        $message->authorizationRequestContent->requestDate = $requestMessage->date;
        $message->authorizationRequestContent->unitIdentifier = $requestMessage->unitIdentifier;
        $message->authorizationRequestContent->requester = $requestMessage->senderOrg;

        try {
            
            if ($message->schema != 'medona') {
                $authorizationOriginatingAgencyRequestController = \laabs::newController($message->schema.'/AuthorizationOriginatingAgencyRequest');
                $authorizationOriginatingAgencyRequestController->send($message);

            } else {

                $this->generate($message);
                $this->save($message);
            }     
            $operationResult = true;

        } catch (\Exception $e) {
            $message->status = "invalid";
            $operationResult = false;
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $eventInfo = array();
            $eventInfo['senderOrgRegNumber'] = $message->senderOrgRegNumber;
            $eventInfo['recipientOrgRegNumber'] = $message->recipientOrgRegNumber;
            $eventInfo['reference'] = $message->reference;

            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationOriginatingAgencyRequestSending',
                'medona/message',
                $message->messageId,
                $eventInfo,
                "Sending of the message",
                $operationResult
            );
        }

        $this->create($message);
        
        return $message;
    }

    /**
     * Accepte the authorization originating agency request
     *
     * @param string $messageId
     */
    public function accept($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        
        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));
        $requestMessage = $this->read($requestMessage->messageId);

        $this->changeStatus($messageId, "accepted");
        $this->archiveController->setStatus((array) $message->unitIdentifier, 'disposed');

        // Send reply
        \laabs::newController('medona/AuthorizationOriginatingAgencyRequestReply')->send($message, '000');

        // Requested by originator 
        $controlAuthorities = $this->orgController->getOrgsByRole('controlAuthority');
        // Check if control authority is set on system
        if (count($controlAuthorities)) {
            $message->status == "authorization_wait";

            $controlAuthority = reset($controlAuthorities);

            $authorizationControlAuthorityRequestController = \laabs::newController('medona/AuthorizationControlAuthorityRequest');
            $authorizationControlAuthorityRequestController->send($requestMessage);
        } else {
            $requestMessageController = \laabs::newController('medona/'.$requestMessage->type);
            $requestMessageController->accept($requestMessage);
        }

        if (\laabs::hasBundle('lifeCycle')) {
            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationOriginatingAgencyRequestAcceptance',
                'medona/message',
                $message->messageId,
                null,
                "Acceptance of the message",
                true
            );
        }

    }

    /**
     * Reject the authorization originating agency request
     *
     * @param string $messageId
     */
    public function reject($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));
        $this->changeStatus($requestMessage->messageId, "rejected");

        $this->changeStatus($messageId, "rejected");
        $this->archiveController->setStatus((array) $message->unitIdentifier, 'preserved');

        \laabs::newController('medona/AuthorizationOriginatingAgencyRequestReply')->send($messageId, 'REJECT');

        if (\laabs::hasBundle('lifeCycle')) {
            $event = $this->lifeCycleJournalController->logEvent(
                'medona/authorizationOriginatingAgencyRequestRejection',
                'medona/message',
                $message->messageId,
                null,
                "Rejection of the message",
                true
            );
        }
    }
}
