<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for originating agency request auhorisation message
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class AuthorizationOriginatingAgencyRequestReply
    extends abstractMessage
{
    /**
     * Send a new authorization originating agency request reply
     * @param string $requestIdentifier The request message identifier
     * @param string $replyCode         The reply code
     *
     * @return The reply message generated
     */
    public function send($requestIdentifier, $replyCode = "000")
    {
        $requestMessage = $this->read($requestIdentifier);

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "AuthorizationOriginatingAgencyRequestReply";
        $message->schema = $requestMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->replyCode = $replyCode;

        $message->reference = $requestMessage->reference.'_Reply';
        $message->requestReference = $requestMessage->reference;
        $requestMessage->replyReference = $message->reference;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $senderOrg = $this->orgController->getOrgByRegNumber($message->senderOrgRegNumber);
        $message->senderOrgName = $senderOrg->orgName;

        $message->recipientOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $recipientOrg = $this->orgController->getOrgByRegNumber($message->recipientOrgRegNumber);
        $message->recipientOrgName = $recipientOrg->orgName;

        try {
            $this->generate($message);
            $this->save($message);
            
            $this->sdoFactory->update($requestMessage, "medona/message");
        } catch (\Exception $e) {
            var_dump($e);
            $message->status = "invalid";
        }

        $this->create($message);

        return $message;
    }
}
