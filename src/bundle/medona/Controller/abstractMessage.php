<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Trait for all types of messages
 *
 * @author Maarch Prosper DE LAURE <prosper.delaure@maarch.org>
 */
abstract class abstractMessage
    extends message
{

    protected $message;

    /**
     * Load a message
     * @param medona/message $message
     */
    public function loadMessage($message)
    {
        $this->message = $message;

        if (!isset($message->xPath)) {
            $message->xPath = \laabs::newService('dependency/xml/XPath', $message->xml);
        }

        $message->xPath->registerNamespace('medona', 'org:afnor:medona:1.0');
        $message->xPath->registerNamespace('recordsManagement', 'maarch.org:laabs:recordsManagement');
        $message->xPath->registerNamespace('digitalResource', 'maarch.org:laabs:digitalResource');
    }

    protected function sendReplyCode($code)
    {
        $replyCode = \laabs::newInstance('medona/Code', $code);

        $replyCode->name = $this->getReplyMessage($code);

        return $replyCode;
    }

    protected function sendError($code, $message=false)
    {
        if ($message) {
            array_push($this->errors, new \core\Error($message, null, $code));
        } else {
            array_push($this->errors, new \core\Error($this->getReplyMessage($code), null, $code));
        }

        if ($this->replyCode == null) {
            $this->replyCode = $code;
        }
    }

    protected function getReplyMessage($code)
    {
        switch ((string) $code) {
            case "000":
                $name = "OK";
                break;
            case "001":
                $name = "OK (consulter les commentaires pour information)";
                break;
            case "002":
                $name = "OK (Demande aux autorités de contrôle effectuée)";         
                break;
                
            case "101":
                $name = "Message mal formé.";
                break;
            case "102":
                $name = "Système momentanément indisponible.";
                break;
                
            case "200":
                $name = "Service producteur non reconnu.";
                break;
            case "201":
                $name = "Service d'archive non reconnu.";
                break;
            case "202":
                $name = "Service versant non reconnu.";
                break;
            case "203":
                $name = "Dépôt non conforme au profil de données.";
                break;
            case "204":
                $name = "Format de document non géré.";
                break;
            case "205":
                $name = "Format de document non conforme au format déclaré.";
                break;
            case "206":
                $name = "Signature du message invalide.";
                break;
            case "207":
                $name = "Empreinte(s) invalide(s).";
                break;
            case "208":
                $name = "Archive indisponible. Délai de communication non écoulé.";
                break;
            case "209":
                $name = "Archive absente (élimination, restitution, transfert)";
                break;
            case "210":
                $name = "Archive inconnue";
                break;
            case "211":
                $name = "Pièce attachée absente.";
                break;
            case "212":
                $name = "Dérogation refusée.";
                break;
                
            case "300":
                $name = "Convention invalide.";
                break;
            case "301":
                $name = "Dépôt non conforme à la convention. Quota des versements dépassé.";
                break;
            case "302":
                $name = "Dépôt non conforme à la convention. Identifiant du producteur non conforme.";
                break;
            case "303":
                $name = "Dépôt non conforme à la convention. Identifiant du service versant non conforme.";
                break;
            case "304":
                $name = "Dépôt non conforme à la convention. Identifiant du service d'archives non conforme.";
                break;
            case "305":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) absente(s).";
                break;
            case "306":
                $name = "Dépôt non conforme à la convention. Volume non conforme.";
                break;
            case "307":
                $name = "Dépôt non conforme à la convention. Format non conforme.";
                break;
            case "308":
                $name = "Dépôt non conforme à la convention. Empreinte(s) non transmise(s).";
                break;
            case "309":
                $name = "Dépôt non conforme à la convention. Absence de signature du message.";
                break;
            case "310":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) non valide(s).";
                break;
            case "311":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) non vérifiée(s).";
                break;
            case "312":
                $name = "Dépôt non conforme à la convention. Dates de début ou de fin non respectées."; 
                break;

            default:
                $name = null;
        }

        return $name;
    }

    /**
     * Extract data object package
     */
    public function extractDataObjectPackage()
    {
        $dataObjectPackageElement = $this->message->xPath->query("medona:DataObjectPackage")->item(0);

        $this->extractBinaryDataObjects($dataObjectPackageElement);

        $this->extractRelationships($dataObjectPackageElement);

        // Extract Management Metadata
        $managementMetadataElement = $this->message->xPath->query("medona:ManagementMetadata", $dataObjectPackageElement)->item(0);
        $this->extractManagementMetadata($managementMetadataElement);

        $this->archiveParser = \laabs::newParser('recordsManagement/archive', 'xml');

        $descriptiveMetadataElement = $this->message->xPath->query("medona:DescriptiveMetadata", $dataObjectPackageElement)->item(0);
        $this->extractDescriptiveMetadata($descriptiveMetadataElement);
    }

    protected function useArchivalAgreement($archivalAgreementReference)
    {
        $archivalAgreementController = \laabs::newController('medona/archivalAgreement');

        if (!isset($this->archivalAgreements[$archivalAgreementReference])) {
            $this->currentArchivalAgreement = $archivalAgreementController->getByReference($archivalAgreementReference);

            $this->archivalAgreements[$archivalAgreementReference] = $this->currentArchivalAgreement;
        } else {
            $this->currentArchivalAgreement = $this->archivalAgreements[$archivalAgreementReference];
        }

        return $this->currentArchivalAgreement;
    }

    protected function getArchivalAgreement()
    {
        if ($archivalAgreementElement = $this->message->xPath->query("medona:ArchivalAgreement")->item(0)) {
            return $archivalAgreementElement->nodeValue;
        }
    }

    protected function getUnitIdentifiers()
    {
        $unitIdentifierElements = $this->message->xPath->query("medona:UnitIdentifier");

        $unitIdentifier = \laabs::newTokenList();

        foreach ($unitIdentifierElements as $unitIdentifierElement) {
            $unitIdentifier[] = $unitIdentifierElement->nodeValue;
        }

        return $unitIdentifier;
    }

    protected function extractBinaryDataObjects($dataObjectPackageElement)
    {
        $binaryDataObjectElements = $this->message->xPath->query('medona:BinaryDataObject', $dataObjectPackageElement);
        $this->dataObjects = array();
        for ($i = 0, $l = $binaryDataObjectElements->length; $i < $l; $i++) {
            $binaryDataObjectElement = $binaryDataObjectElements->item($i);
            $dataObjectId = $binaryDataObjectElement->getAttribute('xml:id');

            $resource = \laabs::newInstance('digitalResource/digitalResource');
            $resource->resId = \laabs::newId();

            $this->dataObjects[$dataObjectId] = $resource;

            $resourceOneToOneProperties = array(
                'medona:Format'                     => 'mimetype',
                'medona:MessageDigest'              => 'hash',
                'medona:MessageDigest/@algorithm'   => 'hashAlgorithm',
                'medona:Size'                       => 'size',

            );
            foreach ($resourceOneToOneProperties as $path => $property) {
                if ($binaryDataObjectPropertyElement = $this->message->xPath->query($path, $binaryDataObjectElement)->item(0)) {
                    $resource->$property = $binaryDataObjectPropertyElement->nodeValue;
                }
            }

            $fileinfo = $this->messageDirectory. DIRECTORY_SEPARATOR . (string) $this->message->messageId.DIRECTORY_SEPARATOR.$dataObjectId.'.info';
            if (is_file($fileinfo)) {
                $fileinfo = json_decode(file_get_contents($fileinfo));

                $resource->puid = $fileinfo->puid;

                $resource->mediaInfo = $fileinfo->mediaInfo;

            }

            $attachmentElement = $this->message->xPath->query('medona:Attachment', $binaryDataObjectElement)->item(0);
            $this->extractAttachment($resource, $attachmentElement);

        }
    }

    protected function extractAttachment($resource, $attachmentElement)
    {
        if ($attachmentElement->hasAttribute('filename')) {
            $resource->fileName = $attachmentElement->getAttribute('filename');
        }

        // Get contents
        if (isset($resource->fileName)) {
            $filepath = $this->messageDirectory. DIRECTORY_SEPARATOR . (string) $this->message->messageId.DIRECTORY_SEPARATOR.$resource->fileName;
            $resource->fileExtension = pathinfo($resource->fileName, \PATHINFO_EXTENSION);
            $resource->fileName = basename($resource->fileName);

            $contents = file_get_contents($filepath);
        } else {
            $contents = $attachmentElement->nodeValue;
        }

        $resource->setContents($contents);
    }

    protected function extractRelationships($dataObjectPackageElement)
    {
        $binaryDataObjectElements = $this->message->xPath->query('medona:BinaryDataObject', $dataObjectPackageElement);

        for ($i = 0, $l = $binaryDataObjectElements->length; $i < $l; $i++) {
            $binaryDataObjectElement = $binaryDataObjectElements->item($i);
            $dataObjectId = $binaryDataObjectElement->getAttribute('xml:id');

            // Get relationships to switch on archive, document or digitalResource creation
            $relationshipElements = $this->message->xPath->query('medona:Relationship', $binaryDataObjectElement);

            if ($relationshipElements->length > 0) {
                foreach ($relationshipElements as $relationshipElement) {
                    $relationshipTypeCode = $relationshipElement->getAttribute('type');
                    $relationshipTarget = $relationshipElement->getAttribute('target');

                    $dataObject = $this->dataObjects[$dataObjectId];
                    $relatedDataObject = $this->dataObjects[$relationshipTarget];

                    $digitalResourceRelationship = \laabs::newInstance('digitalResource/digitalResourceRelationship');
                    $digitalResourceRelationship->resId = $dataObject->resId;
                    $digitalResourceRelationship->relatedResId = $relatedDataObject->resId;
                    $digitalResourceRelationship->typeCode = $relationshipTypeCode;
                    $digitalResourceRelationship->relatedResource = $relatedDataObject;

                    $dataObject->relationship[] = $digitalResourceRelationship;
                }
            }
        }
    }

    protected function extractManagementMetadata($managementMetadataElement)
    {
        $managementMetadata = \laabs::newInstance('recordsManagement/archive');
        $this->managementMetadata = $managementMetadata;
        $managementMetadataOneToOneProperties = array(
            'medona:ArchivalProfile'          => 'archivalProfileReference',
            'medona:ServiceLevel'             => 'serviceLevelReference',
        );
        foreach ($managementMetadataOneToOneProperties as $path => $property) {
            if ($archivePropertyElement = $this->message->xPath->query($path, $managementMetadataElement)->item(0)) {
                $managementMetadata->$property = $archivePropertyElement->nodeValue;
            }
        }

        $apparaisalRuleElement = $this->message->xPath->query("medona:AppraisalRule", $managementMetadataElement)->item(0);
        $this->extractAppraisalRule($managementMetadata, $apparaisalRuleElement);

        $accessRuleElement = $this->message->xPath->query("medona:AccessRule", $managementMetadataElement)->item(0);
        $this->extractAccessRule($managementMetadata, $accessRuleElement);

        if (isset($this->currentArchivalAgreement)) {
            $managementMetadata->originatorOrg = $this->currentArchivalAgreement->originatorOrg;
        }
    }

    protected function extractAccessRule($object, $accessRuleElement)
    {
        if ($codeElement = $this->message->xPath->query('recordsManagement:accessRule/recordsManagement:code', $accessRuleElement)->item(0)) {
            $object->accessRuleCode = $codeElement->nodeValue;
        }

        if ($startDateElement = $this->message->xPath->query('recordsManagement:accessRule/recordsManagement:startDate', $accessRuleElement)->item(0)) {
            $object->accessRuleStartDate = \laabs::newDate($startDateElement->nodeValue);
        }

        if ($durationElement = $this->message->xPath->query('recordsManagement:accessRule/recordsManagement:duration', $accessRuleElement)->item(0)) {
            $object->accessRuleDuration = \laabs::newDuration($durationElement->nodeValue);
        }

        if ($originatingAgencyElement = $this->message->xPath->query('recordsManagement:accessRule/recordsManagement:originatingAgency', $accessRuleElement)->item(0)) {
            $this->extractOriginatingAgency($object, $originatingAgencyElement);
        }
    }

    protected function extractOriginatingAgency($object, $originatingAgencyElement)
    {
        if ($originatorOrgRegNumberElement = $this->message->xPath->query('recordsManagement:Identifier', $originatingAgencyElement)->item(0)) {
            $object->originatorOrgRegNumber = $originatorOrgRegNumberElement->nodeValue;
        }

        if ($orgNameElement = $this->message->xPath->query('recordsManagement:OrganizationDescriptiveMetadata', $originatingAgencyElement)->item(0)) {
            $object->originatorOrgName = $orgNameElement->nodeValue;
        }
    }

    protected function extractAppraisalRule($object, $apparaisalRuleElement)
    {
        if ($codeElement = $this->message->xPath->query('medona:AppraisalCode', $apparaisalRuleElement)->item(0)) {
            switch ($codeElement->nodeValue) {
                case 'conserver':
                    $object->finalDisposition = 'preservation';
                    break;

                case 'détruire':
                    $object->finalDisposition = 'destruction';
                    break;
            }
        }
        if ($startDateElement = $this->message->xPath->query('medona:StartDate', $apparaisalRuleElement)->item(0)) {
            $object->retentionStartDate = \laabs::newDate($startDateElement->nodeValue);
        }

        if ($durationElement = $this->message->xPath->query('medona:Duration', $apparaisalRuleElement)->item(0)) {
            $object->retentionDuration = \laabs::newDuration($durationElement->nodeValue);
        }
    }

    protected function extractDescriptiveMetadata($descriptiveMetadataElement)
    {
        $descriptionElement = $this->message->xPath->query('*', $descriptiveMetadataElement)->item(0);
        switch ($descriptionElement->localName) {
            case 'archivePackage':
                $this->extractDescriptionPackage($descriptionElement);
                break;

            default:
                break;
        }
    }

    protected function extractDescriptionPackage($descriptionPackageElement)
    {
        $archiveElements = $this->message->xPath->query('recordsManagement:archive', $descriptionPackageElement);
        foreach ($archiveElements as $archiveElement) {
            $archive = $this->extractArchive($archiveElement);
        }
    }

    protected function extractArchive($archiveElement)
    {
        $archive = $this->archiveParser->create($this->message->xml->saveXml($archiveElement));
        
        $this->message->archive[] = $archive;

        $this->inheritance($archive, $this->managementMetadata);

        return $archive;
    }

    protected function inheritance($archive, $parentManagementMetadata)
    {
        $archive->archiverOrgRegNumber = $this->message->recipientOrgRegNumber;
        $archive->depositorOrgRegNumber = $this->message->senderOrgRegNumber;
        $archive->archivalAgreementReference = $this->message->archivalAgreementReference;

        foreach ($archive as $name => $value) {
            if (is_null($archive->$name) && isset($parentManagementMetadata->$name)) {
                $archive->$name = $this->managementMetadata->$name;
            }
        }

        foreach ($archive->document as $oid => $document) {
            $document->digitalResource = $this->dataObjects[$oid];
            $document->resId = $document->digitalResource->resId;
        }

        if (isset($archive->contents) && count($archive->contents)) {
            foreach ($archive->contents as $childArchive) {
                $this->inheritance($childArchive, $archive);
            }
        }
    }

    
}
