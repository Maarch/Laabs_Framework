<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * Class for message management
 *
 * @author Maarch Prosper DE LAURE <prosper.delaure@maarch.org>
 */
class message
{
    
    use messageDestructionTrait,
        messageAuthorizationTrait;

    protected $orgControler;
    protected $archiveController;
    protected $lifeCycleJournalController;
    protected $digitalResourceController;
    protected $archivalAgreementController;
    protected $sdoFactory;
    protected $messageTypeController;
    protected $messageTypeParser;
    protected $messageTypeSerializer;
    protected $messageDirectory;
    protected $profilesDirectory;
    protected $messageStandard;
    protected $archivalAgreements = array();
    protected $currentArchivalAgreement;

    protected $errors;
    protected $replyCode;

    protected $message;

    protected $orgController;


    /**
     * The configuration for the notification
     * @var bool
     */
    protected $sendNotification;

    /**
     * The configuration for the compliance
     * @var bool
     */
    protected $sendCompliance;

    /**
     * Constructor
     * @param string                  $messageDirectory  The message directory
     * @param string                  $profilesDirectory The profiles directory
     * @param \dependency\sdo\Factory $sdoFactory        The dependency Sdo Factory
     * @param string                  $messageStandard   The standard to use : MEDONA / SEDA
     * @param bool                    $sendNotification  The state of the fonction of notification modification
     * @param bool                    $sendCompliance    The state of the fonction of compliance
     */
    public function __construct($messageDirectory, $profilesDirectory, \dependency\sdo\Factory $sdoFactory, $messageStandard = 'medona', $sendNotification = false, $sendCompliance = false)
    {
        $this->orgController = \laabs::newController('organization/organization');
        $this->archiveController = \laabs::newController('recordsManagement/archive');
        $this->lifeCycleJournalController = \laabs::newController('lifeCycle/journal');
        $this->digitalResourceController = \laabs::newController('digitalResource/digitalResource');
        $this->archivalAgreementController = \laabs::newController('medona/archivalAgreement');
        $this->profilesDirectory = $profilesDirectory;
        $this->messageDirectory = $messageDirectory;

        if (!is_dir($messageDirectory)) {
            mkdir($messageDirectory, 0777, true);
        }

        $this->sdoFactory = $sdoFactory;
        $this->messageStandard = $messageStandard;
        $this->sendNotification = (bool) $sendNotification;
        $this->sendCompliance = (bool) $sendCompliance;
    }

    /**
     * Extract message info the specific schema model objects
     * @param medona/message $message
     */
    public function extract($message)
    {
        if (is_scalar($message)) {
            $messageId = $message;
            $message = $this->sdoFactory->read('medona/message', $messageId);

            $this->loadXml($message);
        }

        $message->object = $message->xml->export($message->schema.'/'.$message->type);
    }

    /**
     * Load file
     * @param medona/message $message  The medona message object
     * @param string         $filename The path to a SEDA message xml file
     *
     * @return object The Xml document
     */
    public function loadXmlFile($message, $filename = null)
    {
        if (!$filename) {
            $filename = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.'.xml';
        }

        if (is_file($filename)) {

            $message->xml = \laabs::newService('dependency/xml/Document');

            $message->xml->load($filename);

            $message->xPath = new \DOMXPath($message->xml);
            $message->xPath->registerNamespace('medona', 'org:afnor:medona:1.0');

            if (!isset($message->schema)) {
                $this->getMessageSchema($message);
            }

            $message->xml->uri = $filename;
        }
       
    }

    /**
     * Load string
     * @param medona/message $message  The medona message object
     * @param string         $contents The xml contents of a message xml file
     *
     * @return object The Xml document
     */
    public function loadXml($message, $contents = null)
    {
        if (!$contents) {
            $contents = file_get_contents($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.'.xml');
        }

        $message->xml = \laabs::newService('dependency/xml/Document');

        $message->xml->loadXml($contents);

        $message->xPath = new \DOMXPath($message->xml);
        $message->xPath->registerNamespace('medona', 'org:afnor:medona:1.0');

        if (!isset($message->schema)) {
            $this->getMessageSchema($message);
        }

        $message->xml->uri = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.'.xml';
    }


    /**
     * Search message by sender / recipient / reference / date
     * @param string $sender
     * @param string $recipient
     * @param date   $fromDate
     * @param date   $toDate
     * @param string $reference
     *
     * @return medona/message[]
     */
    public function searchMessage($sender = false, $recipient = false, $fromDate = false, $toDate = false, $reference = false)
    {
        $queryParts = array();
        if ($sender) {
            $queryParts[] = "senderOrgName='$sender'";
        }
        if ($recipient) {
            $queryParts[] = "recipientOrgName='$recipient'";
        }
        if ($fromDate) {
            $queryParts[] = "date >='$fromDate'";
        }
        if ($toDate) {
            $queryParts[] = "date <='$toDate'";
        }
        if ($reference) {
            $queryParts[] = "reference='$reference'";
        }

        return $queryParts;
    }

    /**
     * Save xml file
     * @param medona/message $message
     */
    public function save($message)
    {
        if (isset($message->xml)) {

            $messageFile = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.'.xml';

            if (!is_dir($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId)) {
                mkdir($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId, 0777, true);
            }    

            $message->xml->save($messageFile);

            $message->xml->uri = $messageFile;
        }
    }

    /**
     * Create a message
     * @param medona/message $message The message object
     *
     * @throws \Exception
     *
     * @return string The new message identifier
     */
    public function create($message)
    {
        // Check unique
        $unique = array(
            'type' => $message->type,
            'senderOrgRegNumber' => $message->senderOrgRegNumber,
            'reference' => $message->reference,
        );

        if ($this->sdoFactory->exists("medona/message", $unique)) {
            throw \laabs::newException("medona/invalidMessageException", "The message has already been received ('%s' Ref. '%s' from '%s')", null, null, array($message->type, $message->reference, $message->senderOrgRegNumber));
        }

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            if ($account = \laabs::getToken('AUTH')) {
                $message->accountId = $account->accountId;
            }

            if (isset($message->object)) {
                $message->data = json_encode($message->object);
            }

            $this->sdoFactory->create($message, 'medona/message');

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw $exception;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return $message->messageId;
    }

    /**
     * Update a message
     * @param medona/message $message The message object
     *
     * @return string
     *
     * @throws \Exception
     * @throws \dependency\sdo\Exception
     */
    public function update($message)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {

            if (isset($message->object)) {
                $message->data = json_encode($message->object);
            }

            $this->sdoFactory->update($message, 'medona/message');

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw $exception;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return $message->messageId;
    }

    /**
     * Read a message
     * @param string $messageId The message identifier
     *
     * @return medona/message The message object
     */
    public function read($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        $message->unitIdentifier = $this->sdoFactory->readChildren('medona/unitIdentifier', $message);
        $this->loadXmlFile($message);
        // Read organization data
        $this->readOrgs($message);

        if (!empty($message->data)) {
            $message->object = json_decode($message->data);
        }

        // Parent request to child reply
        try {
            $replyMessages = $this->sdoFactory->find('medona/message', "type='".$message->type."Reply' AND senderOrgRegNumber='".$message->recipientOrgRegNumber."' AND requestReference='".$message->reference."'");
            if (count($replyMessages) > 0) {
                $replyMessage = $replyMessages[0];
                $this->readOrgs($replyMessage);
                if (!empty($replyMessage->data)) {
                    $replyMessage->object = json_decode($replyMessage->data);
                }
                $message->replyMessage = $replyMessage;
            }
        } catch (\Exception $e) {

        }

        // Related authorization messages for communication and destruction requests
        try {
            $authorizationMessages = $this->sdoFactory->find('medona/message', "type=['AuthorizationOriginatingAgencyRequest', 'AuthorizationControlAuthorityRequest'] AND authorizationReason='".$message->type."' AND authorizationRequesterOrgRegNumber='".$message->senderOrgRegNumber."' AND authorizationReference='".$message->reference."'", null, "date");
            if (count($authorizationMessages) > 0) {
                foreach ($authorizationMessages as $authorizationMessage) {
                    if ($message->messageId != $authorizationMessage->messageId) {
                        $authorization = $this->read($authorizationMessage->messageId);
                        
                        $message->authorizationMessages[] = $authorization;

                        // Read reply
                        if (isset($authorization->replyMessage)) {
                            $message->authorizationMessages[] = $authorization->replyMessage;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            
        }

        return $message;
    }

    protected function readOrgs($message)
    {
        $message->recipientOrg = $this->readOrg($message->recipientOrgRegNumber);
        $message->recipientOrgName = $message->recipientOrg->orgName;

        $message->senderOrg = $this->readOrg($message->senderOrgRegNumber);
        $message->senderOrgName = $message->senderOrg->orgName;   
    }

    protected function readOrg($regNumber)
    {
        $organization = $this->orgController->getOrgByRegNumber($regNumber);

        // Address
        $organization->address = $this->orgController->getAddresses($organization->orgId);

        // Communication
        $organization->communication = $this->orgController->getCommunications($organization->orgId);

        // Contact
        $organization->contact = $this->orgController->getContacts($organization->orgId);

        return $organization;
    }

    /**
     * Compose a new message
     * @param medona/message $message
     */
    public function generate($message)
    {
        $message->xml = \laabs::newService('dependency/xml/Document');
        $message->xPath = new \DOMXPath($message->xml);

        if (!\laabs::getXmlNamespace($message->schema)) {
            throw new \Exception('Unknown message schema '.$message->schema);
        }

        $messageTypeSerializer = $this->getMessageTypeSerializer($message);

        $messageTypeSerializer->generate($message);

        $this->extract($message);

        if (isset($message->object)) {
            $jsonfilename = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.'.json';

            file_put_contents($jsonfilename, json_encode($message->object, \JSON_PRETTY_PRINT));
        }
    }

    /**
     * End a transaction
     * @param string $messageId The message identifier
     *
     * @return boolean The result of the operation
     */
    public function endTransaction($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);

        $type = substr($message->type, -5);

        if ($type != "Reply") {
            // EXCEPTION
        }

        $message->active = false;
        if (isset($message->requestReference)) {
            $parentKey = array(
                "reference" => $message->requestReference,
                "senderOrgRegNumber" => $message->recipientOrgRegNumber,
                "type" => substr($message->type, 0, -5),
            );

            $parentMessage = $this->sdoFactory->read('medona/message', $parentKey);
            $parentMessage->active = false;
            $this->sdoFactory->update($parentMessage);
        }

        $this->sdoFactory->update($message);

        return true;
    }

    /**
     * Export a message
     * @param string $messageId The message identifier
     *
     * @return string The zip of message xml + attachments
     */
    public function export($messageId)
    {
        $message = $this->read($messageId);

        $zip = \laabs::newService('dependency/fileSystem/plugins/zip');

        $messageDirectory = $this->messageDirectory.DIRECTORY_SEPARATOR.$message->messageId;

        $zipfile = $this->messageDirectory.DIRECTORY_SEPARATOR.$message->messageId.".zip";
        if (is_dir($messageDirectory)) {
            $zip->add($zipfile, $messageDirectory.DIRECTORY_SEPARATOR."*");
        }
        \laabs::setResponseType('bin');

        $zipContents = file_get_contents($zipfile);

        unlink($zipfile);

        return $zipContents;
    }

    /**
     * Get the data object attachment
     * @param string $messageId    The message identifier
     * @param string $attachmentId The attachment identifier
     * @param string $format       The format of message
     *
     * @return string
     */
    public function getDataObjectAttachment($messageId, $attachmentId, $format = "xml")
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        $message->object = json_decode($message->data);

        if ($message->schema != 'medona') {
            $messageController = \laabs::newController($message->schema . '/' . $message->type);

            $resource = $messageController->getAttachment($message, $attachmentId);

            return $resource;
        }

        // Medona attachment
        foreach ($message->object->dataObjectPackage->binaryDataObject as $binaryDataObject) {
            if ($binaryDataObject->oid == $attachmentId) {
                $attachment = $binaryDataObject->attachment;
            }
        }

        if (!$attachment) {
            return false;
        }

        $resource = \laabs::newInstance('digitalResource/digitalResource');

        switch(true) {
            case isset($attachment->filename) :
                $filepath = $this->messageDirectory . DIRECTORY_SEPARATOR . (string) $message->messageId . DIRECTORY_SEPARATOR . $attachment->filename;
                $contents = file_get_contents($filepath);

                $resource->fileExtension = pathinfo($attachment->filename, \PATHINFO_EXTENSION);
                $resource->fileName = basename($attachment->filename);
                break;

            case isset($attachment->uri) :
                $contents = file_get_contents($attachment->uri);
                break;

            case isset($attachment->value):
                $contents = base64_decode($attachment->value);
                break;

            default:
                return false;
        }

        $finfo = new \finfo(\FILEINFO_MIME_TYPE);
        $resource->mimetype = $finfo->buffer($contents);

        $resource->setContents($contents);

        return $resource;

    }

    /**
     * Record a message as a certificate
     * @param medona/message $message The message to record
     *
     * @return array
     */
    public function recordCertificate($message)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            // Create archive
            $archive = $this->archiveController->newArchive();
            $archive->archiveId = $message->messageId;
            $archive->accesRulesDuration = null;

            // Create resource
            $certificate = $this->digitalResourceController->createFromFile($this->messageDirectory.DIRECTORY_SEPARATOR.$message->messageId.DIRECTORY_SEPARATOR.$message->messageId.'.xml');
            $this->digitalResourceController->getHash($certificate, "SHA256");

            // Add document
            $document = \laabs::newInstance('documentManagement/document');
            $document->archiveId = $archive->archiveId;
            $document->resId = $certificate->resId;
            $document->type = "CDO";
            $document->digitalResource = $certificate;
            $archive->document[] = $document;
            $archive->hasDigitalResource = true;

            $archive->descriptionClass = 'medona/message';
            $archive->descriptionId = $message->messageId;

            $archive->originatorOrgRegNumber = $message->senderOrgRegNumber;

            $this->archiveController->deposit($archive, $message->senderOrgRegNumber);

            $messageStatus = new \stdClass();
            $messageStatus->messageId = $message->messageId;
            $messageStatus->archived = true;

            $this->sdoFactory->update($messageStatus, 'medona/message');

        } catch (\Exception $exception) {
            throw $exception;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }

    protected function useArchivalAgreement($archivalAgreementReference)
    {
        if (!isset($this->archivalAgreements[$archivalAgreementReference])) {
            $this->currentArchivalAgreement = $this->archivalAgreementController->getByReference($archivalAgreementReference);

            $this->archivalAgreements[$archivalAgreementReference] = $this->currentArchivalAgreement;
        } else {
            $this->currentArchivalAgreement = $this->archivalAgreements[$archivalAgreementReference];
        }

        return $this->currentArchivalAgreement;
    }

    /**
     * Get the message schema
     * @param medona/message $message The message object
     *
     * @return string The bundle
     */
    protected function getMessageSchema($message)
    {
        $messageNamespace = $message->xml->documentElement->namespaceURI;
        if (!$messageSchema = \laabs::resolveXmlNamespace($messageNamespace)) {
            throw new \Exception('Unknown message namespace'.$messageNamespace);
        }
        $message->schema = $messageSchema;
    
        return $messageSchema;
    }

    /**
     * Get the message type and schema specific controller
     * @param medona/message $message The message object
     *
     * @return The controller
     */
    protected function getMessageTypeController($message)
    {
        if (!isset($message->type)) {
            $message->type = $message->xml->documentElement->nodeName;
        }

        if (!isset($message->schema)) {
            $messageNamespace = $message->xml->documentElement->namespaceURI;
            if (!$messageSchema = \laabs::resolveXmlNamespace($messageNamespace)) {
                throw new \Exception('Unknown message namespace'.$messageNamespace);
            }
            $message->schema = $messageSchema;
        }

        $this->messageTypeController = \laabs::newController($message->schema.LAABS_URI_SEPARATOR.$message->type);

        return $this->messageTypeController;
    }

    /**
     * Get the message type and schema specific parser
     * @param medona/message $message The message object
     * @param string         $format  The implementation format
     *
     * @return The parser
     */
    protected function getMessageTypeParser($message, $format = "xml")
    {
        if (!isset($message->type)) {
            $message->type = $message->xml->documentElement->nodeName;
        }

        if (!isset($message->schema)) {
            $messageNamespace = $message->xml->documentElement->namespaceURI;
            if (!$messageSchema = \laabs::resolveXmlNamespace($messageNamespace)) {
                throw new \Exception('Unknown message namespace '.$messageNamespace);
            }
            $message->schema = $messageSchema;
        }

        $this->messageTypeParser = \laabs::newParser($message->schema.LAABS_URI_SEPARATOR.$message->type, $format);

        return $this->messageTypeParser;
    }

    /**
     * Get the message type and schema specific serializer
     * @param medona/message $message The message object
     * @param string         $format  The implementation format
     *
     * @return The parser
     */
    protected function getMessageTypeSerializer($message, $format = "xml")
    {
        if (!isset($message->type)) {
            $message->type = $message->xml->documentElement->nodeName;
        }

        if (!isset($message->schema)) {
            $messageNamespace = $message->xml->documentElement->namespaceURI;
            if (!$messageSchema = \laabs::resolveXmlNamespace($messageNamespace)) {
                throw new \Exception('Unknown message namespace'.$messageNamespace);
            }
            $message->schema = $messageSchema;
        }

        $this->messageTypeSerializer = \laabs::newSerializer($message->schema.LAABS_URI_SEPARATOR.$message->type, $format);

        return $this->messageTypeSerializer;
    }

    /**
     * Change status of message
     * @param string $messageId The message identifier
     * @param string $status    The new status for message
     * @param string $comment   A comment
     *
     * @return boolean The result of the operation
     */
    protected function changeStatus($messageId, $status, $comment = null)
    {
        $messageStatus = \laabs::newMessage('medona/messageStatus');
        $messageStatus->messageId = $messageId;
        $messageStatus->status = strtolower($status);

        if ($comment) {
            $messageStatus->comment[] = $comment;
        }

        $this->sdoFactory->update($messageStatus, 'medona/message');

        return true;
    }
}
