<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for archiveAuthorizationTrait
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
trait messageAuthorizationTrait
{

    /**
     * Get received authorization request message
     *
     * @return array Array of medona/message object
     */
    public function listArchiveAuthorizationReception()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $messages = $this->sdoFactory->find("medona/message", "type=['AuthorizationOriginatingAgencyRequest', 'AuthorizationControlAuthorityRequest'] AND recipientOrgRegNumber='".$registrationNumber."' AND status='sent' AND active=true");

        return $messages;
    }

    /**
     * Get received authorization request message
     *
     * @return array Array of medona/message object
     */
    public function listArchiveAuthorizationSending()
    {
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        return $this->sdoFactory->find("medona/message", "type=['AuthorizationOriginatingAgencyRequest', 'AuthorizationControlAuthorityRequest'] AND senderOrgRegNumber='".$registrationNumber."' AND status='sent' AND active=true");
    }

    /**
     * Send authorization originating agency request
     * @param string $requestMessageId The request message identifier
     *
     * @return medona/message
     */
    public function sendAuthorizationOriginatingAgencyRequest($requestMessageId)
    {
        $requestMessage = $this->read($requestMessageId);

        $archivesByOriginator = array();
        foreach ($requestMessage->unitIdentifier as $archiveId) {
            $originatorOrgRegNumber = $this->archiveController->getArchiveOriginatorOrgRegNumber($archiveId);
            if (!isset($archivesByOriginator[$originatorOrgRegNumber])) {
                $archivesByOriginator[$originatorOrgRegNumber] = array();
            }
            $archivesByOriginator[$originatorOrgRegNumber][] = $archiveId;
        }

        foreach ($archivesByOriginator as $originatorOrgRegNumber => $archiveIds) {
            $authorizationRequest = new \stdClass();
            $authorizationRequest->requestDate = $requestMessage->date;
            $authorizationRequest->unitIdentifier = $archiveIds;
            $authorizationRequest->requesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
            $authorizationRequest->authorizationReason = $requestMessage->type;

            $message = \laabs::newInstance('medona/message');
            $message->messageId = \laabs::newId();
            $message->schema = $requestMessage->schema;
            $message->type = "AuthorizationOriginatingAgencyRequest";
            $message->status = 'sent';
            $message->date = \laabs::newDateTime();

            $message->reference = $requestMessage->reference."_AuthorizationOriginating";

            $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
            $senderOrg = $this->orgController->getOrgByRegNumber($message->senderOrgRegNumber);
            $message->senderOrgName = $senderOrg->orgName;

            $message->recipientOrgRegNumber = $originatorOrgRegNumber;
            $recipientOrg = $this->orgController->getOrgByRegNumber($originatorOrgRegNumber);
            $message->recipientOrgName = $recipientOrg->orgName;

            $message->authorizationRequestContent = $authorizationRequest;

            $message->authorizationReference = $requestMessage->reference;
            $message->authorizationReason = $requestMessage->type;

            $message->authorizationRequesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
            try {
                $this->generate($message);
                $this->save($message);
            } catch (\Exception $e) {
                $message->status = "invalid";
            }

            $this->create($message);
        }

        $this->changeStatus($requestMessageId, "authorization_wait");

        return $message;
    }

    /**
     * Send a new authorization originating agency request reply
     * @param string $requestIdentifier The request message identifier
     * @param string $replyCode         The reply code
     *
     * @return The reply message generated
     */
    public function sendAuthorizationOriginatingAgencyRequestReply($requestIdentifier, $replyCode = "000")
    {
        $requestMessage = $this->read($requestIdentifier);

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "AuthorizationOriginatingAgencyRequestReply";
        $message->schema = $requestMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->replyCode = $replyCode;

        $message->reference = $requestMessage->reference.'_Reply';
        $message->requestReference = $requestMessage->reference;
        $requestMessage->replyReference = $message->reference;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $senderOrg = $this->orgController->getOrgByRegNumber($message->senderOrgRegNumber);
        $message->senderOrgName = $senderOrg->orgName;

        $message->recipientOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $recipientOrg = $this->orgController->getOrgByRegNumber($message->recipientOrgRegNumber);
        $message->recipientOrgName = $recipientOrg->orgName;

        try {
            $this->generate($message);
            $this->save($message);
            
            $this->sdoFactory->update($requestMessage, "medona/message");
        } catch (\Exception $e) {
            $message->status = "invalid";
        }

        $this->create($message);

        return $message;
    }

    /**
     * Send a new authorization control authority request
     * @param type $requestMessageId The message identifier
     *
     * @return medona/message              The message generated
     */
    public function sendAuthorizationControlAuthorityRequest($requestMessageId)
    {
        $recipientOrg = $this->orgController->getOrgsByRole("controlAuthority")[0];
        $requestMessage = $this->read($requestMessageId);

        $originatingOrg = $this->orgController->getOrgByRegNumber($requestMessage->senderOrgRegNumber);
        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->schema = $requestMessage->schema;
        $message->type = "AuthorizationControlAuthorityRequest";
        $message->status = 'sent';
        $message->date = \laabs::newDateTime();
        $message->reference = $requestMessage->reference."_AuthorizationControlAuthority";

        $message->authorizationReference = $requestMessage->reference;
        $message->authorizationRequesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $message->authorizationReason = $requestMessage->type;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        //$senderOrg = $this->orgController->getOrgByRegNumber($message->senderOrgRegNumber);
        $message->senderOrgName = $requestMessage->recipientOrgName;
        $message->recipientOrgRegNumber = $recipientOrg->registrationNumber;
        $message->recipientOrgName = $recipientOrg->orgName;

        $message->authorizationRequestContent = new \stdClass();
        $message->authorizationRequestContent->authorizationReason = $requestMessage->type;
        $message->authorizationRequestContent->requestDate = \laabs::newDateTime();
        $message->authorizationRequestContent->unitIdentifier = $requestMessage->unitIdentifier;
        $message->authorizationRequestContent->requesterOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $message->originatingAgencyAuthorization = new \stdClass();
        $message->originatingAgencyAuthorization->identifier = (string) $originatingOrg->orgId;
        $message->originatingAgencyAuthorization->comment = null;
        $message->originatingAgencyAuthorization->originatingAgency = $originatingOrg->orgName;

        try {
            
            $this->generate($message);
            $this->save($message);

        } catch (\Exception $e) {
            $message->status = "invalid";
        }

        $this->create($message);

        return $message;
    }

    /**
     * Send a new authorization authority request reply
     * @param string $requestIdentifier The request message identifier
     * @param string $replyCode         The reply code
     *
     * @return The reply message generated
     */
    public function sendAuthorizationControlAuthorityRequestReply($requestIdentifier, $replyCode = "000")
    {
        $requestMessage = $this->read($requestIdentifier);

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "AuthorizationControlAuthorityRequestReply";
        $message->schema = $requestMessage->schema;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->replyCode = $replyCode;

        $message->reference = $requestMessage->reference.'_Reply';
        $message->requestReference = $requestMessage->reference;
        $requestMessage->replyReference = $message->reference;

        $message->senderOrgRegNumber = $requestMessage->recipientOrgRegNumber;
        $message->senderOrgName = $requestMessage->recipientOrgName;

        $message->recipientOrgRegNumber = $requestMessage->senderOrgRegNumber;
        $message->recipientOrgName = $requestMessage->senderOrgName;

        try {

            $this->generate($message);
            $this->save($message);
            
            $this->update($requestMessage);
        } catch (\Exception $e) {
            $message->status = "invalid";
        }

        $this->create($message);

        return $message;
    }

    /**
     * Accepte the authorization originating agency request
     *
     * @param string $messageId
     */
    public function acceptAuthorizationOriginatingAgencyRequest($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));

        $this->changeStatus($messageId, "accepted");
        $this->sendAuthorizationOriginatingAgencyRequestReply($messageId);

        $this->sendAuthorizationControlAuthorityRequest($requestMessage->messageId);
    }

    /**
     * Reject the authorization originating agency request
     *
     * @param string $messageId
     */
    public function rejectAuthorizationOriginatingAgencyRequest($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));
        $this->changeStatus($requestMessage->messageId, "rejected");

        $this->changeStatus($messageId, "rejected");
        $this->sendAuthorizationOriginatingAgencyRequestReply($messageId, "REJECT");
    }

    /**
     * Accepte the authorization control authority request
     *
     * @param string $messageId The message identifier
     */
    public function acceptAuthorizationControlAuthorityRequest($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        $this->changeStatus($messageId, "accepted");

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));


        if ($requestMessage->type == "ArchiveDestructionRequest") {
            $archiveController = \laabs::newController('recordsManagement/archive');
            $archiveController->setStatus((array) $requestMessage->unitIdentifier, 'disposed');
        }

        $this->changeStatus($requestMessage->messageId, "accepted");

        $this->sendAuthorizationControlAuthorityRequestReply($messageId);
    }

    /**
     * Reject the authorization control authority request
     *
     * @param string $messageId The message identifier
     */
    public function rejectAuthorizationControlAuthorityRequest($messageId)
    {
        $message = $this->sdoFactory->read('medona/message', $messageId);
        $this->changeStatus($messageId, "rejected");

        $requestMessage = $this->sdoFactory->read('medona/message', array('reference' => $message->authorizationReference, 'type' => $message->authorizationReason, 'senderOrgRegNumber' => $message->authorizationRequesterOrgRegNumber));
        $this->changeStatus($requestMessage->messageId, "rejected");

        $this->sendAuthorizationControlAuthorityRequestReply($messageId, "REJECT");
    }
}
