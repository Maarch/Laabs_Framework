<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for archiveDelivery
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
trait messageComplianceTrait
{
    /**
     * Send a new transfer reply
     * @param string $reference The notification message identifier
     * @param array  $archives  The archives to send
     * @param string $schema    The schema of the message
     *
     * @return The message generated
     */
    public function sendCompliance($reference, $archives = array(), $schema = 'medona')
    {
        if (!$this->sendCompliance) {
            return null;
        }

        $archivesByOriginator = array();
        foreach ($archives as $archive) {
            if (!isset($archivesByOriginator[$archive->originatorOrgRegNumber])) {
                $archivesByOriginator[$archive->originatorOrgRegNumber] = array();
            }

            $archivesByOriginator[$archive->originatorOrgRegNumber][] = $archive;
        }

        $messages = array();

        foreach ($archivesByOriginator as $originatorOrgRegNumber => $archives) {
            $message = \laabs::newInstance('medona/message');
            $message->messageId = \laabs::newId();
            $message->type = "ArchiveCompliance";
            $message->schema = $schema;
            $message->status = "sent";
            $message->date = \laabs::newDatetime();

            $message->reference = $reference."_".$originatorOrgRegNumber;

            $senderOrg = $this->orgController->getOrgsByRole('archiver')[0];
            $message->senderOrgRegNumber = $senderOrg->registrationNumber;
            $message->senderOrgName = $senderOrg->orgName;

            $recipientOrg = $this->orgController->getOrgByRegNumber($originatorOrgRegNumber);
            $message->recipientOrgRegNumber = $originatorOrgRegNumber;
            $message->recipientOrgName = $recipientOrg->orgName;

            $message->archive = $archives;

            $message->unitIdentifier = array();
            $message->lifeCycleEventId = \laabs::newTokenList();
            foreach ($archives as $archive) {
                $unitIdentifier = \laabs::newInstance("medona/unitIdentifier");
                $unitIdentifier->messageId = $message->messageId;
                $unitIdentifier->objectId = (string) $archive->archiveId;
                $unitIdentifier->objectClass = "recordsManagement/archive";
                
                $this->sdoFactory->create($unitIdentifier);
                $message->unitIdentifier[] = $unitIdentifier;
                
                if ($archive->lifeCycleEvent) {
                    foreach ($archive->lifeCycleEvent as $event) {
                        $message->lifeCycleEventId[] = (string) $event->eventId;
                    }
                }
            }

            $message->dataObjectCount = count($message->archive);

            try {
                
                $this->generate($message);
                $this->save($message);

            } catch (\Exception $e) {
                $message->status = "error";
            }

            $this->create($message);

            $messages[] = $message;
        }

        return $messages;
    }

    /**
     * Get received archive modification notification messages
     *
     * @return array Array of medona/message object
     */
    public function listmessageComplianceReception($sender = false, $recipient = false, $fromDate = false, $toDate = false, $reference = false)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveCompliance'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }
}
