<?php

/* 
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\medona\Controller;

/**
 * trait for destruction message
 * @author Prosper DE LAURE <prosper.delaure@maarch.org>
 */
trait messageDestructionTrait
{
    /**
     * Get received archive destruction message
     *
     * @return array Array of medona/message object
     */
    public function listArchiveDestructionRequestReception()
    {
        $queryParts = array();
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get sending archive destruction message
     * @param string $sender    The sender
     * @param string $recipient The recipient
     * @param string $fromDate  The start date
     * @param string $toDate    The final date
     * @param string $reference The reference
     *
     * @return array Array of medona/message object
     */
    public function listArchiveDestructionSending($sender = false, $recipient = false, $fromDate = false, $toDate = false, $reference = false)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }

        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "senderOrgRegNumber='$registrationNumber'";
        $queryParts[] = "active='true'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Get processed archive tranfer message
     * @param string $sender    The sender
     * @param string $recipient The recipient
     * @param string $fromDate  The start date
     * @param string $toDate    The final date
     * @param string $reference The reference
     *
     * @return array Array of medona/message object
     */
    public function listArchiveDestructionHistory($sender = false, $recipient = false, $fromDate = false, $toDate = false, $reference = false)
    {
        $queryParts = $this->searchMessage($sender, $recipient, $fromDate, $toDate, $reference);
        $organization = \laabs::getToken("ORGANIZATION");
        if (!$organization) {
            $registrationNumber = "";
        } else {
            $registrationNumber = $organization->registrationNumber;
        }
        $queryParts[] = "type='ArchiveDestructionRequest'";
        $queryParts[] = "recipientOrgRegNumber='$registrationNumber'";
        $queryParts[] = "status='processed'";

        return $this->sdoFactory->find('medona/message', implode(' and ', $queryParts), null, false, false, 300);
    }

    /**
     * Create a new a new delivery request
     * @param string                      $reference The message identifier
     * @param recordsManagmeent/archive[] $archives  An array of archive identifiers
     *
     * @return The request message generated
     */
    public function newDestructionRequest($reference, $archives, $schema='medona')
    {
        $archivesByOriginator = array();

        foreach ($archives as $archive) {
            if (!isset($archivesByOriginator[$archive->originatorOrgRegNumber])) {
                $archivesByOriginator[$archive->originatorOrgRegNumber] = array();
            }

            $archivesByOriginator[$archive->originatorOrgRegNumber][] = $archive;
        }

        $messages = array();

        foreach ($archivesByOriginator as $originatorOrgRegNumber => $archives) {
            $message = \laabs::newInstance('medona/message');
            $message->messageId = \laabs::newId();
            $message->schema = $schema;
            $message->type = "ArchiveDestructionRequest";
            $message->status = 'derogation';
            $message->date = \laabs::newDateTime();
            $message->reference = $reference."_".$originatorOrgRegNumber;

            $message->archive = $archives;

            $message->unitIdentifier = \laabs::newTokenList(array());
            foreach ($archives as $archive) {
                $message->unitIdentifier[] = (string) $archive->archiveId;
            }

            $message->dataObjectCount = count($message->archive);

            $senderOrg = \laabs::getToken('ORGANIZATION');
            if (!$senderOrg) {
                throw \laabs::newException('medona/invalidMessageException', "No current organization choosen");
            }

            $message->senderOrgRegNumber = $senderOrg->registrationNumber;
            $message->senderOrgName = $senderOrg->orgName;

            $recipientOrg = $this->orgController->getOrgsByRole('archiver')[0];
            $message->recipientOrgRegNumber = $recipientOrg->registrationNumber;
            $message->recipientOrgName = $recipientOrg->orgName;

            $message->derogation = true;

            try {
                $this->generate($message);
                $this->save($message);

            } catch (\Exception $e) {
                $message->status = "error";
            }

            $this->create($message);
            $this->sendAuthorizationOriginatingAgencyRequest($message->messageId);
            $messages[] = $message;
        }

        return $messages;
    }

    /**
     * Process archive destruction
     * @param medona/message $message
     *
     * @return the result of process
     */
    public function processArchiveDestruction($message)
    {
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $archives = $this->archiveController->destruct((array) $message->unitIdentifier);

            $this->sendDestructionNotification($message, $archives);

            $message->status = "processed";
            $message->operationDate = \laabs::newDatetime();
            $this->update($message);

        } catch (\Exception $e) {
            throw $e;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }


    /**
     * Send a new transfer reply
     * @param medona/message            $destructionMessage The destruction request message
     * @param recordsManagement/archive $archives           The destroyed archives
     *
     * @return The message generated
     */
    public function sendDestructionNotification($destructionMessage, $archives)
    {
        if (!$this->sendNotification) {
            return null;
        }

        $message = \laabs::newInstance('medona/message');
        $message->messageId = \laabs::newId();
        $message->type = "ArchiveDestructionNotification";
        $message->schema = $this->messageStandard;
        $message->status = "sent";
        $message->date = \laabs::newDatetime();
        $message->reference =  "DestructionNotification".$destructionMessage->reference;


        $senderOrg = $this->orgController->getOrgsByRole('archiver')[0];
        $message->senderOrgRegNumber = $senderOrg->registrationNumber;
        $message->senderOrgName = $senderOrg->orgName;

        $message->recipientOrgRegNumber = $destructionMessage->senderOrgRegNumber;
        $message->recipientOrgName = $destructionMessage->senderOrgName;

        $message->unitIdentifier = $destructionMessage->unitIdentifier;
        $message->dataObjectCount = $destructionMessage->dataObjectCount;

        $message->archive = $archives;
        try {

            $this->generate($message);
            $this->save($message);
            
        } catch (\Exception $e) {
            $message->status = "error";
        }

        $this->create($message);


        return $message;
    }
}
