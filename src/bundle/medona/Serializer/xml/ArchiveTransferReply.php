<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona\Serializer\xml;

/**
 * Class for ArchiveTransferReply message handling
 */
class ArchiveTransferReply 
    extends abstractBusinessReplyMessage
{

    /**
     * Generate a new archive delivery request reply
     * @param medona/message $message
     */
    public function generate($message)
    {
        parent::generate($message);       

        $this->addDataObjectPackage($withAttachments = false);

        $this->setReplyCode($message->replyCode);
       
        $this->setMessageRequestIdentifier($message->requestReference);

        $this->setGrantDate($message->operationDate);

        $this->setOrganization($message->senderOrgRegNumber, "ArchivalAgency");

        $this->setOrganization($message->recipientOrgRegNumber, "TransferringAgency");

    }

    /**
     * Add binary data objects attachments for transfer reply as a certificate of deposit
     * @param object $resource
     * @param object $binaryDataObjectElement
     */
    /*protected function addAttachment($resource, $binaryDataObjectElement)
    {
        $attachmentElement = $this->message->xml->createElement('Attachment');
        $binaryDataObjectElement->appendChild($attachmentElement);

        $address = $resource->address[0];
        $uri = str_replace('\\', LAABS_URI_SEPARATOR, $address->path);
        if (isset($address->repository)) {
            $uri = str_replace('\\', LAABS_URI_SEPARATOR, $address->repository->repositoryUri) . LAABS_URI_SEPARATOR . $uri;
        }

        $attachmentElement->setAttribute('uri', $uri);
    }*/

    /**
     * Add archive identifiers
     * @param recordsManagement/archive $archive
     * @param object                    $parentElement
     */
/*    protected function addArchiveDescriptiveMetadata($archive, $parentElement)
    {
        $archiveElement = $this->message->xml->createElement('archive');
        $archiveElement->setAttribute('xml:id', $archive->archiveId);

        $parentElement->appendChild($archiveElement);

        if (count($archive->document)) {
            foreach ($archive->document as $document) {
                if ($document->type == 'CDO') {
                    $archiveElement->setAttribute('oid', (string) $document->digitalResource->resId);
                }
            }
        }

        if (isset($archive->contents) && count($archive->contents)) {
            foreach ($archive->contents as $subArchive) {
                $this->addArchiveDescriptiveMetadata($subArchive, $archiveElement);
            }
        }
    }
*/
    protected function setGrantDate($date)
    {
        $dateText = $this->message->xml->createTextNode($date->format('Y-m-d\TH:i:s'));

        if (!$dateElement = $this->message->xPath->query("medona:GrantDate")->item(0)) {
            $dateElement = $this->message->xml->createElement('GrantDate');
            $this->message->xml->documentElement->appendChild($dateElement);
        } else {
            $dateElement->nodeValue = "";
        }

        $dateElement->appendChild($dateText);
    }
}
