<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona;

/**
 * Archive delivery interface
 *
 * @package Medona
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
interface archiveDeliveryInterface 
    extends messageInterface
{
    /**
     * Get ingoing delivery messages
     * 
     * @action medona/ArchiveDeliveryRequest/listReception
     */
    public function readIncominglist();
    
    /**
     * Get outgoing delivery messages
     * 
     * @action medona/ArchiveDeliveryRequest/listSending
     */
    public function readOutgoinglist();

    /**
     * Count delivery messages
     * 
     * @action medona/ArchiveDeliveryRequest/count
     */
    public function readCount();
    
    /**
     * Accept archive delivery request
     * 
     * @action medona/ArchiveDeliveryRequest/accept
     */
    public function updateRequestacceptance_messageId_();
    
    /**
     * Reject archive delivery request
     * 
     * @action medona/ArchiveDeliveryRequest/reject
     */
    public function updateRequestrejection_messageId_();
        
    /**
     * Search message by sender / recipient / reference / date
     * @param string $sender
     * @param string $recipient
     * @param date   $fromDate
     * @param date   $toDate
     * @param string $reference
     *
     * @action medona/ArchiveDeliveryRequest/listSending
     */
    public function readSearchoutgoing($sender = null, $recipient = null, $fromDate = null, $toDate = null, $reference = null);
    
    /**
     * Search message by sender / recipient / reference / date
     * @param string $sender
     * @param string $recipient
     * @param date   $fromDate
     * @param date   $toDate
     * @param string $reference
     *
     * @action medona/ArchiveDeliveryRequest/search
     */
    public function readSearch($sender = null, $recipient = null, $fromDate = null, $toDate = null, $reference = null);
    
    /**
     * Get validate archives delivery request
     *
     * @action medona/ArchiveDeliveryRequest/listToValidate
     */
    public function readValidate();
}
