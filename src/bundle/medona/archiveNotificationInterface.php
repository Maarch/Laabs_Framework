<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona;

/**
 * archiveNotificationInterface
 *
 * @package Medona
 * @author  Alexandre MORIN <alexandre.morin@maarch.org>
 */
interface archiveNotificationInterface 
    extends messageInterface
{
    /**
     * Search form
     * 
     * @action medona/ArchiveNotification/listReception
     */
    public function readList();
    
    /**
     * Search message by sender / recipient / reference / date
     * @param string $type               The type of notifiaction messages
     * @param string $senderOrgRegNumber The sender of notification messages
     * @param string $fromDate           The fromDate of notification messages
     * @param string $toDate             The toDate of notification messages
     * @param string $reference          The reference of notification message
     *
     * @action medona/ArchiveNotification/search
     */
    public function readSearch($type = null, $senderOrgRegNumber = null, $fromDate = null, $toDate = null, $reference = null);

    /**
     * Count notification messages
     * 
     * @action medona/ArchiveNotification/count
     */
    public function readCount();
}
