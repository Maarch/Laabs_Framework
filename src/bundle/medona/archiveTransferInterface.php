<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\medona;

/**
 * Archive transfer interface
 *
 * @package Medona
 * @author  Alexis Ragot <alexis.ragot@maarch.org>
 */
interface archiveTransferInterface 
    extends messageInterface
{
    /**
     * Search message by sender / recipient / reference / date
     * @param string $sender
     * @param string $recipient
     * @param date   $fromDate
     * @param date   $toDate
     * @param string $reference
     *
     * @action medona/ArchiveTransfer/search
     */
    public function readSearch($sender = null,$recipient = null, $fromDate = null, $toDate = null, $reference = null);
    
    /**
     * Get ingoing transfer messages
     * 
     * @action medona/ArchiveTransfer/listReception
     */
    public function readIncominglist();
    
    /**
     * Get outgoing transfer messages
     * 
     * @action medona/ArchiveTransfer/listSending
     */
    public function readOutgoinglist();

    /**
     * Count transfer messages
     * 
     * @action medona/ArchiveTransfer/count
     */
    public function readCount();

    /**
     * Receive message with all contents embedded
     * @param string $messageFile The message binary contents OR a filename
     * @param array  $attachments An array of filenames for attachments
     *
     * @action medona/ArchiveTransfer/receive
     */
    public function create($messageFile, $attachments = array());

    /**
     * Validate messages against schema and rules
     * 
     * @action medona/ArchiveTransfer/validateBatch
     */
    public function updateValidateBatch();

    /**
     * Validate messages against schema and rules
     * 
     * @action medona/ArchiveTransfer/processBatch
     */
    public function updateProcessBatch();

    /**
     * Accept archive transfer
     * 
     * @action medona/ArchiveTransfer/accept
     */
    public function updateRequestacceptance_messageId_();
    
    /**
     * Reject archive transfer
     * 
     * @action medona/ArchiveTransfer/reject
     */
    public function updateRequestrejection_messageId_();
    
    /**
     * Process archive transfer
     * 
     * @action medona/ArchiveTransfer/process
     */
    public function updateProcess_messageId_();
}
