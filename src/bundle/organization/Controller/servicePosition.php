<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle organization.
 *
 * Bundle organization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle organization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle organization.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class servicePosition
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Add service position
     * @param string $serviceAccountId The service account identifier
     * @param string $orgId            Organization identifier
     *
     * @return bool
     */
    public function create($serviceAccountId, $orgId)
    {
        $servicePosition = new \stdClass();
        $servicePosition->serviceAccountId = $serviceAccountId;
        $servicePosition->orgId = $orgId;

        return $this->sdoFactory->create($servicePosition, "organization/servicePosition");
    }

    /**
     * Add service position
     * @param string $serviceAccountId The service account identifier
     *
     * @return bool
     */
    public function get($serviceAccountId)
    {
        $servicePositions = $this->sdoFactory->find("organization/servicePosition", "serviceAccountId='$serviceAccountId'");
        
        if (!count($servicePositions)) {
            return;
        }
        $servicePosition = $servicePositions[0];
        $saController = \laabs::newController("auth/serviceAccount");
        $serviceAccount = $saController->read((string) $servicePosition->serviceAccountId);
        $servicePosition->accountName = $serviceAccount->accountName;

        return $servicePosition;
    }

    /**
     * List user positions organization ids as well as theirs childrens
     *
     * @return array The list of organizations
     */
    public function listDescendants()
    {
        $orgs = array();
        $organization = \laabs::getToken('ORGANIZATION');

        if (!$organization) {
            return array();
        }

        $orgs[(string) $organization->orgId] = $organization;

        $descendants = $this->sdoFactory->readDescendants('organization/organization', $organization);
        foreach ($descendants as $descendant) {
            if (!isset($orgs[(string) $descendant->orgId])) {
                $orgs[(string) $descendant->orgId] = $descendant;
            }
        }

        return $orgs;
    }

}
