<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle organization.
 *
 * Bundle organization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle organization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle organization.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\organization\Controller;

/**
 * Control of the organization types
 *
 * @package Organization
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org> 
 */
class userPosition
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory) 
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get my all positions
     *
     * @return array The list of my position's
     */
    public function getMyPositions()
    {
        $user = \laabs::getToken('AUTH');
        $currentOrg = \laabs::getToken('ORGANIZATION');


        if (!$user) {
            return array();
        }

        $positions = $this->sdoFactory->find('organization/userPosition', "userAccountId = '$user->accountId'");

        $organizations = array();

        foreach ($positions as $position) {
            $organization = $this->sdoFactory->read('organization/organization', $position->orgId);

            $organizations[(string) $position->orgId] = $organization;
            $position->organization = $organization;

            $position->organization->orgName = $organization->displayName;

            if ($position->default && !$currentOrg) {
                \laabs::setToken("ORGANIZATION", $organization, 86400);
            }
        }

        return $positions;
    }

    /**
     * Set my working positions
     * @param organization/organization $orgId The organization identifier 
     * 
     * @return bool The resutl of the operation
     */
    public function setCurrentPosition($orgId)
    {
        if ($organization = $this->sdoFactory->read('organization/organization', $orgId)) {
            \laabs::setToken("ORGANIZATION", $organization, 86400);

            return true;
        }

        throw \laabs::Bundle('organization')->newException('workingPositionException', 'This position is not defined for this user');

        return false;
    }

    /**
     * List user positions organization ids
     *
     * @return array The list of organization ids
     */
    public function listMyOrgIds()
    {
        $user = \laabs::getToken('AUTH');
        
        if (!$user) {
            return array();
        }
        $orgs = array();
        $orgIds = array();

        $orgUnits = $this->sdoFactory->index('organization/userPosition',array("orgId"), "userAccountId = '$user->accountId'");
        
        foreach ($orgUnits as $orgUnitId) {
            $orgUnit = $this->sdoFactory->read('organization/organization', $orgUnitId);
            $organization = $this->sdoFactory->read('organization/organization', $orgUnit->ownerOrgId);

            $orgs[] = $organization;
            $orgs = array_merge($this->sdoFactory->readDescendants('organization/organization', $organization), $orgs);
        }

        foreach ($orgs as $org) {
            if (! $org->isOrgUnit) {
                $orgIds[] = (string) $org->orgId;
            }
        }

        $orgIds = array_unique($orgIds);

        return $orgIds;
    }


    /**
     * List user positions organization ids as well as theirs childrens
     *
     * @return array The list of organization ids
     */
    public function listMyDescendantsOrgRegNumbers()
    {
        $orgRegNumbers = array();
        $orgs = array();
        $user = \laabs::getToken('AUTH');

        if (!$user) {
            return array();
        }

        $positions = $this->sdoFactory->find('organization/userPosition', "userAccountId = '$user->accountId'");

        foreach ($positions as $position) {
            $organization = $this->sdoFactory->read('organization/organization', $position->orgId);
            $orgs[] = $organization;

            $orgs = array_merge($this->sdoFactory->readDescendants('organization/organization', $organization), $orgs);
        }

        foreach ($orgs as $org) {
            $orgRegNumbers[] = $org->registrationNumber;
        }

        $orgRegNumbers = array_unique($orgRegNumbers);

        return $orgRegNumbers;
    }

    /**
     * List user positions organization ids as well as theirs childrens
     *
     * @return array The list of organizations
     */
    public function listMyDescendantsOrgs()
    {
        $orgs = array();
        $user = \laabs::getToken('AUTH');

        if (!$user) {
            return array();
        }

        $userPositions = $this->sdoFactory->find('organization/userPosition', "userAccountId = '$user->accountId'");
        foreach ($userPositions as $userPosition) {
            $organization = $this->sdoFactory->read('organization/organization', $userPosition->orgId);
            $orgs[(string) $organization->orgId] = $organization;

            $descendants = $this->sdoFactory->readDescendants('organization/organization', $organization);
            foreach ($descendants as $descendant) {
                if (!isset($orgs[(string) $descendant->orgId])) {
                    $orgs[(string) $descendant->orgId] = $descendant;
                }
            }
        }

        return $orgs;
    }

    /**
     * List user positions organization ids as well as theirs sisters
     *
     * @return array The list of organization ids
     */
    public function listMySistersOrgIds()
    {
        $orgIds = array();
        $sisterOrgs = array();
        $user = \laabs::getToken('AUTH');

        if (!$user) {
            return array();
        }

        $myOrgs = $this->sdoFactory->find('organization/userPosition', "userAccountId = '$user->accountId'");

        foreach ($myOrgs as $organization) {
            if (isset($organization->parentOrgId)) {
                $sisterOrgs = array_merge($sisterOrgs, $this->sdoFactory->find('organization/organization', "parentOrgId = '$organization->parentOrgId'"));
            }
        }

        foreach ($sisterOrgs as $organization) {
            $orgIds[] = (string) $sisterOrgs->orgIds;
        }

        return $orgIds;
    }
}
