<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Batch;

/**
 * Description of validateIntegrity
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class validateIntegrity {
    
    // Services
    protected $sdoFactory;
    
    //Controllers
    private $lifeCycleController;
    private $archiveController;
    
    //Params
    private $limit;
    private $delayDate;
    
    /**
     * Construct the batch job instance
     */
    public function __construct($limit, $delay)
    {
        // Service
        $this->sdoFactory = \laabs::newService('dependency/sdo/Factory');
        
        // Controller
        $this->lifeCycleController = \laabs::newController('lifeCycle/journal');
        $this->archiveController = \laabs::newController('recordsManagement/archive');
    
        //Params
        $this->limit = $limit;
        $this->delayDate = \laabs::newTimestamp()->sub(\laabs::newDuration($delay));
    }
    
    /**
     * Return the start event for begin the validation integrity
     *
     * @return lifeCycle/event $startEvent The event 
     */
    public function getStartEvent()
    {
        //Get the most newest event of periodic integrity
        $startEvent = $this->sdoFactory->find("lifeCycle/event", "eventType='recordsManagement/integrityCheck'", "timestamp DESC", null, 1);
        
        if (count($startEvent) == 0) {
            //Get the latest event of deposit
            $startEvent = $this->sdoFactory->find("lifeCycle/event", "eventType='recordsManagement/reception'", "timestamp ASC", null, 1);
            
            if (count($startEvent) == 0) {
                // TODO Exception
            }

            $startEvent = $this->lifeCycleController->getEvent((string) $startEvent[0]->eventId);
        } else {
            $startEvent = $this->lifeCycleController->getEvent((string) $startEvent[0]->eventId);
            $startEvent = $this->lifeCycleController->getEvent((string) $startEvent->endEventId);
        }
        
        return $startEvent;
    }
    
    /**
     * Select archives for periodic integrity
     * @param lifeCycle/event $startEvent The first event to begin the validation of integrity
     *
     * @return array $events
     */
    public function getEventsToCheck($startEvent)
    {
        $currentEvent = $startEvent;
        $events = array();
        $i = 0;
        while($i < $this->limit) {
            if ($i != 0) {
                $currentEvent = $this->lifeCycleController->getNextEvent("recordsManagement/reception");
                
                if ($currentEvent == null) {
                    continue;
                }
                
                if ((strcmp((string)$currentEvent->eventId, (string)$startEvent->eventId) == 0) && ($i != 0)) {
                    break;
                }
            }
            if ($currentEvent->timestamp->diff($this->delayDate)->invert == 1) {
                $i++;
                continue;
            }
            
            if (!in_array($currentEvent, $events)) {
                $events[] = $currentEvent;
            } else {
                break;
            }
            
            $i++;
        }

        return $events;
    }
    
    /**
     * Extract archive information from events
     * @param type $events
     *
     * @return array $archives
     */
    public function extractInformationFromEvent($events)
    {
        $archives = array();
        
        foreach ($events as $key => $event) {
            $archive = new \stdClass();
            $archive->hash = $event->hash;
            $archive->hashAlgorithm = $event->hashAlgorithm;
            $archive->archiveId = $event->objectId;
            $archive->eventTimestamp = $event->timestamp;
            $archive->eventId = $event->eventId;
            $archives[] = $archive;
        }

        return $archives;
    }
    
    /**
     * Check integrity of archives
     * @param object $archives
     * @param lifeCycle/event $startEvent
     */
    public function checkIntegrity($archives, $startEvent) 
    {
        foreach ($archives as $key => $archive) {
            $this->archiveController->verifyIntegrity($archive, $archive->hash);
        }
        
        $eventItems = array('startEventDate' => ((string)$startEvent->timestamp), 'endEventDate' => ((string) end($archives)->eventTimestamp), 'endEventId' => ((string) end($archives)->eventId));
        $this->lifeCycleController->logEvent('recordsManagement/integrityCheck', 'recordsManagement/archive', ((string) end($archives)->eventId), $eventItems, null, "Periodic validation of integrity");
    }
}
