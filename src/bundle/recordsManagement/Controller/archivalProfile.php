<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\recordsManagement\Controller;

/**
 * Class of adminArchivalProfile
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class archivalProfile
{
    protected $sdoFactory;

    protected $lifeCycleJournalController;

    protected $certificateController;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory         The sdo factory
     * @param bool                    $notifyModification The state of the fonction of notification modification
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, $notifyModification)
    {
        $this->sdoFactory = $sdoFactory;
        if (\laabs::hasBundle('lifeCycle')) {
            $this->lifeCycleJournalController = \laabs::newController('lifeCycle/journal');
            //$this->certificateController = \laabs::newController('recordsManagement/certificate');
        }

        $this->notifyModification = $notifyModification;
    }

    /**
     * List archival profiles
     *
     * @return recordManagement/archivalProfile[] The list of archival profiles
     */
    public function index()
    {
        return $this->sdoFactory->find('recordsManagement/archivalProfile');
    }

    /**
     * New empty archival profile with default values
     *
     * @return recordsManagement/archivalProfile The archival profile object
     */
    public function newProfile()
    {
        $archivalProfile = \laabs::newInstance("recordsManagement/archivalProfile");

        return $archivalProfile;
    }

    /**
     * Edit an archival profile
     * @param string $archivalProfileId The archival profile's identifier
     *
     * @return recordsManagement/archivalProfile The profile object
     */
    public function read($archivalProfileId)
    {
        $archivalProfile = $this->sdoFactory->read('recordsManagement/archivalProfile', $archivalProfileId);
        $archivalProfile->profileDescription = $this->sdoFactory->readChildren('recordsManagement/profileDescription', $archivalProfile);
        if ($archivalProfile->retentionRuleCode) {
            $archivalProfile->retentionRule = $this->sdoFactory->read('recordsManagement/retentionRule', $archivalProfile->retentionRuleCode);
        }

        if (\laabs::hasBundle('medona')) {
            $profileDirectory = \laabs::configuration('seda')['profilesDirectory'];
            $profileFile = $profileDirectory.DIRECTORY_SEPARATOR.$archivalProfile->reference.'.rng';
            if (file_exists($profileFile)) {
                $archivalProfile->profileFile = $profileFile;
            }
        }

        return $archivalProfile;
    }

    /**
     * get an archival profile by reference
     * @param string $archivalProfileReference The archival profile reference
     *
     * @return recordsManagement/archivalProfile The profile object
     */
    public function getByReference($archivalProfileReference)
    {
        $archivalProfile = $this->sdoFactory->read('recordsManagement/archivalProfile', array('reference' => $archivalProfileReference));
        if (!$archivalProfile) {
            throw \laabs::newException('recordsManagemnt/unknownArchivalProfileException', "Archival profile '$archivalProfileReference' not found");
        }
        $archivalProfile->profileDescription = $this->sdoFactory->readChildren('recordsManagement/profileDescription', $archivalProfile);
        if (!empty($archivalProfile->retentionRuleCode)) {
            $archivalProfile->retentionRule = $this->sdoFactory->read('recordsManagement/retentionRule', $archivalProfile->retentionRuleCode);
        }
        if (!empty($archivalProfile->accessRuleCode)) {
            $archivalProfile->accessRule = $this->sdoFactory->read('recordsManagement/accessRule', $archivalProfile->accessRuleCode);
        }

        return $archivalProfile;
    }

    /**
     * get array of archival profile by description class
     * @param string $archivalProfileDescriptionClass The archival profile reference
     *
     * @return Array $archivalProfiles Array of recordsManagement/archivalProfile object
     */
    public function getByDescriptionClass($archivalProfileDescriptionClass)
    {
        $archivalProfiles = $this->sdoFactory->find('recordsManagement/archivalProfile', "descriptionClass='$archivalProfileDescriptionClass'");

        foreach ($archivalProfiles as $archivalProfile) {
            $archivalProfile->retentionRule = $this->sdoFactory->read('recordsManagement/retentionRule', $archivalProfile->retentionRuleCode);
            $archivalProfile->accessRule = $this->sdoFactory->read('recordsManagement/accessRule', $archivalProfile->accessRuleCode);
        }

        return $archivalProfiles;
    }

    /**
     * create an archival profile
     * @param recordsManagement/archivalProfile $archivalProfile The archival profile object
     *
     * @return boolean The result of the request
     */
    public function create($archivalProfile)
    {
        $archivalProfile->archivalProfileId = \laabs::newId();
        
        $this->sdoFactory->create($archivalProfile, 'recordsManagement/archivalProfile');

        if (!empty($archivalProfile->profileDescription)) {
            foreach ($archivalProfile->profileDescription as $description) {
                $description->archivalProfileId = $archivalProfile->archivalProfileId;
                $this->sdoFactory->create($description, 'recordsManagement/profileDescription');
            }
        }


        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            $eventItems = array('archivalProfileReference' => $archivalProfile->reference);
            $this->lifeCycleJournalController->logEvent('recordsManagement/profileCreation', 'recordsManagement/archivalProfile', $archivalProfile->archivalProfileId, $eventItems, "Creation of profile ".$archivalProfile->archivalProfileId);
        }

        return true;
    }

    /**
     * update an archival profile
     * @param recordsManagement/archivalProfile $archivalProfile The archival profile object
     *
     * @return boolean The request of the request
     */
    public function update($archivalProfile)
    {
        // profile description
        $currentProfileDescription = $this->sdoFactory->find('recordsManagement/profileDescription', "archivalProfileId = '$archivalProfile->archivalProfileId'");
        foreach ($currentProfileDescription as $description) {
            $this->sdoFactory->delete($description);
        }

        if (is_array($archivalProfile->profileDescription)) {
            foreach ($archivalProfile->profileDescription as $description) {
                $description->archivalProfileId = $archivalProfile->archivalProfileId;
                $this->sdoFactory->create($description, 'recordsManagement/profileDescription');
            }
        }

        // archival profile
        $this->sdoFactory->update($archivalProfile, "recordsManagement/archivalProfile");
        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            $eventItems = array('archivalProfileReference' => $archivalProfile->reference);
            $this->lifeCycleJournalController->logEvent('recordsManagement/profileModification', 'recordsManagement/archivalProfile', $archivalProfile->archivalProfileId, $eventItems, "Modification of profile " . $archivalProfile->archivalProfileId);
        }

        // Send profil modification notification
        if (\laabs::hasBundle("medona")) {
            $archivalProfileModificationNotificationController = \laabs::newController("medona/ArchivalProfileModificationNotification");
            $archivalProfileModificationNotificationController->send($reference = (string) \laabs::newId(), $archivalProfile);
        }

        return true;
    }

    /**
     * delete an archival profile
     * @param string $archivalProfileId The identifier of the archival profile
     *
     * @return boolean The request of the request
     */
    public function delete($archivalProfileId)
    {
        $archivalProfile = $this->sdoFactory->read('recordsManagement/archivalProfile', $archivalProfileId);
        $profileDescription = $this->sdoFactory->readChildren('recordsManagement/profileDescription', $archivalProfile);

        foreach ($profileDescription as $description) {
            $this->sdoFactory->delete($description);
        }

        $this->sdoFactory->delete($archivalProfile);

        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            $eventItems = array('archivalProfileReference' => $archivalProfile->reference);
            $this->lifeCycleJournalController->logEvent('recordsManagement/profileDestruction', 'recordsManagement/archivalProfile', $archivalProfile->archivalProfileId, $eventItems, "Destruction of profile ".$archivalProfile->archivalProfileId);
        }

        return true;
    }

    /**
     * Get form of teh description class
     * @param type $archivalProfileReference The reference of the archival profile
     *
     * @return object The description class object parsed with the profile descriptions
     */
    public function descriptionForm($archivalProfileReference)
    {
        $archivalProfile = $this->getByReference($archivalProfileReference);
        $descriptionObject = \laabs::newController($archivalProfile->descriptionClass)->form($archivalProfile->profileDescription);
        $descriptionObject->descriptionClass =  $archivalProfile->descriptionClass;

        return $descriptionObject;
    }

    /**
     * Upload a profile file
     * @param string $profileReference The profile reference
     * @param string $archivalProfile  The profile binary file
     *
     * @return boolean The result of the operation
     */
    public function uploadArchivalProfile($profileReference, $archivalProfile)
    {
        if (!\laabs::hasBundle('medona')) {
            return false;
        }

        $profileDirectory = \laabs::configuration('seda')['profilesDirectory'];
        $profileDirectory .= DIRECTORY_SEPARATOR.$profileReference.'.rng';

        $archivalProfile = base64_decode($archivalProfile);
        file_put_contents($profileDirectory, $archivalProfile);

        return true;
    }
}
