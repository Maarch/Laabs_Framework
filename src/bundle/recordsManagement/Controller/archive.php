<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Class for Records Management archives
 */
class archive
{

    use archiveDepositTrait,
        archiveCommunicationTrait,
        archiveModificationTrait,
        archiveRestitutionTrait,
        archiveComplianceTrait,
        archiveConversionTrait,
        archiveDestructionTrait;

    /**
     * Sdo Factory for management of archive persistance
     * @var dependency/sdo/Factory
     */
    protected $sdoFactory;

    /**
     * The path to save pending archive files
     * @var dependency/sdo/Factory
     */
    protected $pendingArchiveDirectory;

    /**
     * The path to save restituted archive
     * @var dependency/sdo/Factory
     */
    protected $restitutionDirectory;

    /**
     * Controller for archive documents
     * @var documentManagement/Controller/document
     */
    protected $documentController;

    /**
     * Controller for access rules
     * @var recordsManagement/Controller/accessRule
     */
    protected $accessRuleController;

    /**
     * Controller for archive relationships
     * @var recordsManagement/Controller/archiveRelationship
     */
    protected $archiveRelationshipController;

    /**
     * Controller for archival profiles
     * @var recordsManagement/Controller/archivalProfile
     */
    protected $archivalProfileController;

    /**
     * Controller for service levels
     * @var recordsManagement/Controller/serviceLevel
     */
    protected $serviceLevelController;

    /**
     * Controller for certificates
     * @var recordsManagement/Controller/certificate
     */
    protected $certificateController;

    /**
     * Controller for life cycle journal events
     * @var recordsManagement/Controller/lifeCycleJournal
     */
    protected $lifeCycleJournalController;

    /**
     * Controller for life cycle journal events
     * @var recordsManagement/Controller/lifeCycleJournal
     */
    protected $organizationController;

    /**
     * Previously loaded archival profiles, indexed by reference
     * @var array
     */
    protected $archivalProfiles;

    /**
     * Currently used archival profile
     * @var recordsManagement/archivalProfile
     */
    protected $currentArchivalProfile;

    /**
     * Previously loaded service levels, indexed by reference
     * @var array
     */
    protected $serviceLevels;

    /**
     * Currently used service level
     * @var recordsManagement/serviceLevel
     */
    protected $currentServiceLevel;

    /**
     * Previously loaded description object controllers, indexed by reference
     * @var array
     */
    protected $descriptionControllers;

    /**
     * Currently description object controller
     * @var object
     */
    protected $currentDescriptionController;

    /**
     * The hash algo for resources
     * @var string
     */
    protected $hashAlgorithm;

    /**
     * The depositor organization
     * @var object
     */
    protected $depositorOrg;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory              The dependency sdo factory service
     * @param string                  $hashAlgorithm           The hash algorithm for digital archives
     * @param string                  $pendingArchiveDirectory The pending archives directory
     * @param string                  $restitutionDirectory    The archives restitution directory
     * @param bool                    $notifyModification      The state of the fonction of notification modification
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, $hashAlgorithm = 'SHA256', $pendingArchiveDirectory = false, $restitutionDirectory = false)
    {

        $this->hashAlgorithm = $hashAlgorithm;

        if (!is_dir($pendingArchiveDirectory)) {
            mkdir($pendingArchiveDirectory, 777, true);
        }

        /*
        $this->restitutionDirectory = $restitutionDirectory;
        if (!is_dir($restitutionDirectory)) {
            mkdir($restitutionDirectory, 777, true);
        }
        */

        $this->sdoFactory = $sdoFactory;

        $this->documentController = \laabs::newController("documentManagement/document");

        $this->archiveRelationshipController = \laabs::newController("recordsManagement/archiveRelationship");

        $this->archivalProfileController = \laabs::newController("recordsManagement/archivalProfile");

        $this->serviceLevelController = \laabs::newController("recordsManagement/serviceLevel");

        if (\laabs::hasBundle("lifeCycle")) {
            //$this->certificateController = \laabs::newController("recordsManagement/certificate");
            $this->lifeCycleJournalController = \laabs::newController("lifeCycle/journal");
        }

        $this->accessRuleController = \laabs::newController('recordsManagement/accessRule');

        $this->organizationController = \laabs::newController('organization/organization');
    }

    /**
     * Load archive references
     * @param object $archive   The archive
     * @param string $operation The requested operation: deposit, communication, modification, restitution, destruction
     */
    public function useReferences($archive, $operation)
    {

        if (!empty($archive->archivalProfileReference)) {
            $this->useArchivalProfile($archive->archivalProfileReference);
        }

        if (!empty($archive->serviceLevelReference)) {
            $this->useServiceLevel($operation, $archive->serviceLevelReference);
        } else {
            $this->useServiceLevel($operation);
        }

        if (!empty($archive->descriptionClass)) {
            $this->useDescriptionController($archive->descriptionClass);
        } elseif (!empty($archive->descriptionSchema)) {
            $documentRootNamespaceUri = $archive->descriptionXml->documentElement->namespaceURI;
            $this->useDescriptionController($documentRootNamespaceUri);
        }
    }

    /**
     * Select an archival profile for use
     * @param string $archivalProfileReference
     *
     * @return recordsManagement/archivalProfile
     */
    public function useArchivalProfile($archivalProfileReference)
    {
        if (!isset($this->archivalProfiles[$archivalProfileReference])) {
            $this->currentArchivalProfile = $this->archivalProfileController->getByReference($archivalProfileReference);
            $this->archivalProfiles[$archivalProfileReference] = $this->currentArchivalProfile;
        } else {
            $this->currentArchivalProfile = $this->archivalProfiles[$archivalProfileReference];
        }

        return $this->currentArchivalProfile;
    }

    /**
     * Select a service level for use
     * @param string $operation
     * @param string $serviceLevelReference
     *
     * @return recordsManagement/serviceLevel
     */
    public function useServiceLevel($operation, $serviceLevelReference=null)
    {
        if (!$serviceLevelReference) {
            $this->currentServiceLevel = $this->serviceLevelController->getDefault();

            $this->serviceLevels[$this->currentServiceLevel->reference] = $this->currentServiceLevel;
        } else {
            if (!isset($this->serviceLevels[$serviceLevelReference])) {
                $this->currentServiceLevel = $this->serviceLevelController->getByReference($serviceLevelReference);

                $this->serviceLevels[$serviceLevelReference] = $this->currentServiceLevel;
            } else {
                $this->currentServiceLevel = $this->serviceLevels[$serviceLevelReference];
            }
        }

        switch ($operation) {
            case 'deposit':
                $mode = 'write';
                $limit = true;
                break;

            case 'destruction':
                $mode = 'delete';
                $limit = false;
                break;

            case 'restitution':
            case 'communication':
            default:
                $mode = "read";
                $limit = true;
                break;
        }

        $digitalResourceCluster = $this->documentController->useDigitalResourceCluster($this->currentServiceLevel->digitalResourceClusterId, $mode, $limit);

        $control = explode(" ", $this->currentServiceLevel->control);

        if (in_array("storeMetadata", $control) && !$digitalResourceCluster->storeMetadata) {
            throw \laabs::newException('recordsManagement/serviceLevelException', "The Service level requires the digital resource cluster to store metadata, but the selected cluster does not.");
        }

        if (in_array("storeMetadata", $control) && count($digitalResourceCluster->clusterRepository) < 2) {
            throw \laabs::newException('recordsManagement/serviceLevelException', "The Service level requires a redundant storage, but the selected cluster does not have sufficiant repositories.");
        }

        return $this->currentServiceLevel;
    }

    /**
     * Select a description controller
     * @param string $descriptionClass
     *
     * @return object
     */
    public function useDescriptionController($descriptionClass)
    {
        if (!isset($this->descriptionControllers[$descriptionClass])) {
            $this->currentDescriptionController = \laabs::newController($descriptionClass);

            $this->descriptionControllers[$descriptionClass] = $this->currentDescriptionController;
        } else {
            $this->currentDescriptionController = $this->descriptionControllers[$descriptionClass];
        }

        return $this->currentDescriptionController;
    }

    /**
     * Retrieve an archive by its id
     * @param string $archiveId
     *
     * @return recordsManagement/archive
     */
    public function retrieve($archiveId)
    {
        $archive = $this->sdoFactory->read('recordsManagement/archive', $archiveId);
        
        $this->getArchiveComponents($archive, true);
        
        return $archive;
    }

    /**
     * Get the archive by description class and id
     * @param string $descriptionClass
     * @param string $descriptionId
     * 
     * @return object
     */
    public function getByDescription($descriptionClass, $descriptionId)
    {
        $archive = $this->sdoFactory->read('recordsManagement/archive', array('descriptionClass ' => $descriptionClass, 'descriptionId' => $descriptionId));

        $this->getArchiveComponents($archive, false);

        return $archive;
    }

    /**
     * Get the archives by originator
     * @param string $originatorOrgRegNumber
     * 
     * @return array
     */
    public function getArchiveByOriginator($originatorOrgRegNumber)
    {
        $archives = $this->sdoFactory->read("recordsManagement/archive", array('originatorOrgRegNumber' => $originatorOrgRegNumber));

        return $archives;
    }

    /**
     * Get the archive originator
     * @param string $archiveId
     * 
     * @return string
     */
    public function getArchiveOriginatorOrgRegNumber($archiveId)
    {
        $archive = $this->sdoFactory->read("recordsManagement/archive", $archiveId);

        return $archive->originatorOrgRegNumber;
    }

    /**
     * Get archives by status
     * @param string $status
     *
     * @return recordsManagement/archive[]
     */
    public function getByStatus($status)
    {
        $archives = $this->sdoFactory->find('recordsManagement/archive', "status='$status'");

        return $archives;
    }

    /**
     * Retrieve an archive description by its archive id
     * @param string $archiveId The archive identifer
     *
     * @return recordsManagement/archive
     */
    public function getDescription($archiveId)
    {
        if (!$this->sdoFactory->exists('recordsManagement/archive', $archiveId)) {
            throw \laabs::newException("recordsManagement/unknownArchive", "The archive identifier '$archiveId' not exist");
        }
        
        $archive = $this->sdoFactory->read('recordsManagement/archive', $archiveId);
        if (!empty($archive->descriptionClass)) {
            $descriptionController = $this->useDescriptionController($archive->descriptionClass);
            $archive->descriptionObject = $descriptionController->read($archive->descriptionId);
        }

        if ($archive->descriptionObject == null) {
            throw \laabs::newException("recordsManagement/invalidArchiveDescriptionException", "Invalid description for this archive with archive identifier : '$archiveId'");
        }

        if (\laabs::hasBundle("lifeCycle")) {
            $archive->lifeCycleEvent = $this->lifeCycleJournalController->getObjectEvents($archive->archiveId, 'recordsManagement/archive');
        }
        
        $archive->document = $this->documentController->getArchiveDocuments($archive->archiveId, $withContents = false);
        $archive->originatorOrg = $this->organizationController->getOrgByRegNumber($archive->originatorOrgRegNumber);
        $archive->archiverOrg = $this->organizationController->getOrgByRegNumber($archive->archiverOrgRegNumber);
        $archive->depositorOrg = $this->organizationController->getOrgByRegNumber($archive->depositorOrgRegNumber);
        
        $this->getParentArchive($archive);
        $this->getChildrenArchives($archive);
        
        $archive->childrenRelationships = $this->archiveRelationshipController->getByArchiveId($archive->archiveId);
        $archive->parentRelationships = $this->archiveRelationshipController->getByRelatedArchiveId($archive->archiveId);
        
        return $archive;
    }
    
    /**
     * Get the parent archive
     * @param recordsManagement/archive $archive The archive
     * 
     * @return recordsManagement/archive
     */
    protected function getParentArchive($archive) 
    {
        if (isset($archive->parentArchiveId)) {
            $archive->parentArchive = $this->sdoFactory->read("recordsManagement/archive", $archive->parentArchiveId);
        }
        
        return $archive;
    }
    
    /**
     * Get the children archives
     * @param recordsManagement/archive $archive The parent archive
     * 
     * @return recordsManagement/archive Archive with children archives
     */
    protected function getChildrenArchives($archive)
    {
        $archive->childrenArchives = $this->sdoFactory->find("recordsManagement/archive", "parentArchiveId='". (string) $archive->archiveId ."'");

        foreach ($archive->childrenArchives as $child) {
            $this->getChildrenArchives($child);
        }
        
        return $archive;
    }
    
    protected function getArchiveComponents($archive, $withContents=false)
    {
        $this->getAccessRule($archive);

        if (\laabs::hasBundle("lifeCycle")) {
            $archive->lifeCycleEvent = $this->lifeCycleJournalController->getObjectEvents($archive->archiveId, 'recordsManagement/archive');
        }

        if (!empty($archive->descriptionClass)) {
            $descriptionController = $this->useDescriptionController($archive->descriptionClass);
            $archive->descriptionObject = $descriptionController->read($archive->descriptionId);
        }

        $archive->document = $this->documentController->getArchiveDocuments($archive->archiveId, $withContents);

        $archive->contents = $this->sdoFactory->find('recordsManagement/archive', "parentArchiveId = '" . $archive->archiveId . "'");
        foreach ($archive->contents as $content) {
            $this->getArchiveComponents($content, $withContents);
        }

        $archive->relatedArchives = $this->archiveRelationshipController->getByArchiveId($archive->archiveId);
        $archive->relatedArchives = $this->archiveRelationshipController->getByRelatedArchiveId($archive->archiveId);

        $archive->originatorOrg = $this->organizationController->getOrgByRegNumber($archive->originatorOrgRegNumber);

    }

    /**
     * Change the status of an archive
     * @param mixed  $archiveIds    Identifiers of the archives to update
     * @param string $status        New status to set
     *
     * @return array Archives ids separate by successfully updated archives ['success'] and not updated archives ['error']
     */
    public function setStatus($archiveIds, $status)
    {
        $statusList = array();
        $statusList['preserved'] = array('frozen');
        $statusList['restituable'] = array('preserved');
        $statusList['restituted'] = array('restituable', 'restituable');
        $statusList['frozen'] = array('preserved', 'restituable', 'disposable');
        $statusList['disposable'] = array('preserved');
        $statusList['disposed'] = array('disposable');

        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }

        $res = array('success' => array(), 'error' => array());

        if (!isset($statusList[$status])) {
            $res['error'] = $archiveIds;
            return $res;
        }

        foreach ($archiveIds as $archiveId) {
            $archiveStatus = $this->sdoFactory->read('recordsManagement/archiveStatus', $archiveId);

            if (!in_array($archiveStatus->status, $statusList[$status])) {
                array_push($res['error'], $archiveId);
            } else {
                $archiveStatus->status = $status;

                $childrenArchives = $this->sdoFactory->index('recordsManagement/archive', "archiveId", "parentArchiveId = '$archiveId'");
                $this->setStatus($childrenArchives, $status);

                $this->sdoFactory->update($archiveStatus);
                array_push($res['success'], $archiveId);
            }
        }

        return $res;
    }

    /**
     * Calculate the communication date of an archive
     * @param timestamp $startDate The start date
     * @param duration  $duration  The duration
     *
     * @return date
     */
    public function calculateDate($startDate, $duration)
    {
        if (empty($startDate) || empty($duration)) {
            return null;
        }
        if ($duration == "P999999999Y") {
            return $duration;
        }
        
        return $startDate->shift($duration);
    }

    /**
     * Calculate access rule from archive
     * @param recordsManagement/archive         $archive
     * @param recordsManagement/archivalProfile $archivalProfile
     *
     * @return recordsManagement/accessRule[]
     */
    public function getAccessRule($archive, $archivalProfile = false)
    {
        $accessRules = array();
        if (!empty($archive->accessRuleCode)) {
            $accessRuleCode = $archive->accessRuleCode;
        } elseif (!empty($archive->archivalProfileReference)) {
            $archivalProfile = $this->archivalProfileController->getByReference($archive->archivalProfileReference);
            $accessRuleCode = $archivalProfile->accessRuleCode;
        } else {
            return;
        }
        if (!empty($accessRuleCode)) {
            $archive->accessRule = $this->accessRuleController->edit($accessRuleCode);
        }
    }

    /**
     * Check the current user access to an archive for the given operation (deposit, retrieve, modify, destruct)
     * @param recordsManagement/archive $archive
     *
     * @return boolean
     *
     * @throws recordsManagement/accessDeniedException If access if denied
     */
    public function checkCommunicability($archive)
    {
        if (! $archive->accessRuleComDate) {
            return true;
        }
        
        // Calc diff between communicability date and curent date
        $communicationDelay = $archive->accessRuleComDate->diff(\laabs::newTimestamp());
        // If date is in the past, public communication is allowed
        if ($communicationDelay->invert == 0) {
            return true;
        }
        
        // Check user / service orgs with descendants
        $account = \laabs::getToken('AUTH');
        switch ($account->accountType) {
            case 'user':
                $userPositionController = \laabs::newController('organization/userPosition');
                $accountOrgs = $userPositionController->listMyDescendantsOrgs();
                break;        
            case 'service':
                $servicePositionController = \laabs::newController('organization/servicePosition');
                $accountOrgs = $servicePositionController->listDescendants();
                break;
        }

        $accountOrgRegNumbers = array();
        foreach ($accountOrgs as $accountOrg) {
            // Account has ONWER org role, i.e. third party archiver
            if (in_array('owner', (array) $accountOrg->orgRoleCodes)) {
                return true;
            }

            $accountOrgRegNumbers[] = (string) $accountOrg->registrationNumber;
        }

        if (in_array((string) $archive->originatorOrgRegNumber, $accountOrgRegNumbers)) {
            return true;
        }

        if (in_array($archive->archiverOrgRegNumber, $accountOrgRegNumbers)) {
            return true;
        }

        throw \laabs::newException('recordsManagement/accessDeniedException', "Permission denied");
        
    }

    /**
     * Check if archive exists
     * @param string $archiveId The archive identifier
     *
     * @return object Object with archiveId and a boolean 'exist'
     */
    public function exists($archiveId)
    {
        $result = new \stdClass();
        $result->archiveId = $archiveId;
        $result->exist = false;
        if ($this->sdoFactory->exists("recordsManagement/archive", array("archiveId" => $archiveId))) {
            $result->exist = true;
        }

        return $result;
    }

    /**
     * VerifyIntegrityByArchiveId
     * @param object  $archiveIds         An array of archive identifier or an archive identifier
     * @param boolean $integrityByJournal Validate integrity by life cycle journal or by the data sytem
     * 
     * @return array Array of archive object
     */
    public function verifyIntegrity($archiveIds, $integrityByJournal = true)
    {
        if (!is_array($archiveIds)) {
            $tmp = $archiveIds;
            $archiveIds = array();
            $archiveIds[] = $tmp;
        }

        $archiveIdsString = "'".implode("', '", $archiveIds)."'";
        $archives = $this->sdoFactory->find("recordsManagement/archive", "archiveId=[$archiveIdsString]");

        foreach ($archives as $key => $archive) {
            if ($integrityByJournal) {
                $event = $this->sdoFactory->find("lifeCycle/event", "eventType='recordsManagement/deposit' AND objectId='$archive->archiveId'")[0];

                $event = $this->lifeCycleJournalController->getEvent((string) $event->eventId);
                $archive = $this->integrityCheck($archive, $event->hash);
            } else {
                $archive = $this->integrityCheck($archive);
            }
        }

        return $archives;
    }

    /**
     * Verify archives integrity
     * @param string $archive Archive identifier
     * @param string $hash    The hash of the archive in life cycle journal
     *
     * @return array Array of archive
     */
    public function integrityCheck($archive, $hash = null)
    {
        $digitalResourceController = \laabs::newController("digitalResource/digitalResource");

        $archive->document = $this->sdoFactory->find("documentManagement/document", "archiveId='".(string) $archive->archiveId."'");

        if (count($archive->document) == 0) {
            throw new \bundle\recordsManagement\Exception\noDocumentException('The archive with identifier '.(string) $archive->archiveId."doesn't have documents for integrity validation");
        }

        foreach ($archive->document as $document) {
            if ($document->type != "CDO") {
                continue;
            }

            $document->digitalResource = $this->sdoFactory->read("digitalResource/digitalResource", $document->resId);

            if (!$document->digitalResource) {
                throw \laabs::newException("digitalResource/resourceNotFoundException");
            }

            if ($hash != null && $document->digitalResource->hash != $hash) {
                throw \laabs::newException("recordsManagement/invalidHashException : $hash  with ".(string) $document->digitalResource->hash);
            }

            $digitalResourceController->verifyResource($document->digitalResource);
        }

        if (\laabs::hasBundle("lifeCycle")) {
            $eventInfo = array();
            $result = true;
            foreach ($archive->document as $document) {
                if ($document->type != "CDO") {
                    continue;
                }

                $eventInfo['resId'] = $document->resId;
                $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                $eventInfo['hash'] = $document->digitalResource->hash;

                $address = $document->digitalResource->address[0];
                $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                $eventInfo['requesterOrgRegNumber'] = $this->organizationController->getOrgsByRole("owner")[0]->registrationNumber;

                foreach ($document->digitalResource->address as $address) {
                    if ($address->integrityCheckResult == false) {
                        $result = $address->integrityCheckResult;
                        break 2;
                    }
                }
            }

            if ($hash != null) {
                $this->lifeCycleJournalController->logEvent('recordsManagement/integrity', 'recordsManagement/archive', $archive->archiveId, $eventInfo, "Validate integrity of archive by life cycle journal ".(string) $archive->archiveId, $result);
            } else {
                $this->lifeCycleJournalController->logEvent('recordsManagement/integrity', 'recordsManagement/archive', $archive->archiveId, $eventInfo, "Validate integrity of archive by data system ".(string) $archive->archiveId, $result);
            }
        }

        return $archive;
    }
}
