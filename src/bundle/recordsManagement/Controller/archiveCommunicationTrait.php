<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archives communication
 */
trait archiveCommunicationTrait
{

    /**
     * Search archives by profile / dates / agreement
     * @param string $profileReference
     * @param date   $fromDate
     * @param date   $toDate
     * @param string $status
     * @param string $agreementReference
     * @param string $archiveId
     * @param string $archiveExpired
     * @param string $finalDisposition
     * @param string $origniatorOrgRegNumber
     * @param string $archiveIdOriginator
     *
     * @return recordsManagement/archive[]
     */
    public function search($profileReference = false, $status = false, $agreementReference = false, $archiveId = false, $archiveExpired = null, $finalDisposition = false, $origniatorOrgRegNumber = false, $archiveIdOriginator = false)
    {
        $currentDate = \laabs::newDate();
        $currentDateString = $currentDate->format('Y-m-d');

        $queryParts = array();
        if ($profileReference) {
            $queryParts[] = "archivalProfileReference='$profileReference'";
        }
        if ($agreementReference) {
            $queryParts[] = "archivalAgreementReference='$agreementReference'";
        }
        if ($archiveId) {
            $queryParts[] = "archiveId='$archiveId'";
        }
        if ($status) {
            $queryParts[] = "status='$status'";
        } else {
            $queryParts[] = "status=['preserved', 'disposable', 'restituable', 'restitution', 'restitued', 'frozen']";
        }
        if ($archiveExpired == "true") {
            $queryParts[] = "disposalDate<='$currentDateString'";
        }
        if ($archiveExpired == "false") {
            $queryParts[] = "disposalDate>='$currentDateString'";
        }
        if ($finalDisposition) {
            $queryParts[] = "finalDisposition='$finalDisposition'";
        }
        if ($origniatorOrgRegNumber) {
            $queryParts[] = "originatorOrgRegNumber='$origniatorOrgRegNumber'";
        }

        $originators = array();
        foreach ((array) $this->organizationController->getOrgsByRole('originator') as $originator) {
            $originators[$originator->registrationNumber] = $originator;
        }

        $archives = $this->sdoFactory->find('recordsManagement/archive', implode(' and ', $queryParts), null, false, false, 100);
        foreach ($archives as $archive) {
            if (!empty($archive->disposalDate) && $archive->disposalDate <= $currentDate) {
                $archive->disposable = true;
            }
            
            if (isset($originators[$archive->originatorOrgRegNumber])) {
                $archive->originator = $originators[$archive->originatorOrgRegNumber];
            }
        }
        
        return $archives;
    }

    /**
     * Retrieve an archive resource contents
     * @param string $archiveId The archive identifier
     *
     * @return documentManagement/document
     */
    public function getContents($archiveId)
    {
        $archive = $this->getDescription($archiveId);

        $this->checkCommunicability($archive);

        if (count($archive->document) == 0) {
            return;
        }
        
        $contentDocument = null;

        foreach ($archive->document as $document) {
            if ($document->type == 'CDO') {
                if (is_null($contentDocument) || (string) $document->digitalResource->created > (string) $contentDocument->digitalResource->created) {
                    $contentDocument = $document;
                }
            }
        }

        if ($contentDocument) {
            $this->documentController->getContent($contentDocument);

            return $contentDocument;
        }
    }

    /**
     * Retrieve an archive resource contents
     * @param string $descriptionClass The description object class
     * @param string $descriptionId    The description object id
     *
     * @return documentManagement/document
     */
    public function getContentsByDescription($descriptionClass, $descriptionId)
    {
        $archive = $this->sdoFactory->read('recordsManagement/archive', array('descriptionClass ' => $descriptionClass, 'descriptionId' => $descriptionId));

        $archive = $this->getByDescription($descriptionClass, $descriptionId);
        
        $this->checkCommunicability($archive);

        if (count($archive->document) == 0) {
            return;
        }
        
        $contentDocument = null;

        foreach ($archive->document as $document) {
            if ($document->type == 'CDO') {
                if (is_null($contentDocument) || (string) $document->digitalResource->created > (string) $contentDocument->digitalResource->created) {
                    $contentDocument = $document;
                }
            }
        }
        
        if ($contentDocument) {
            $this->documentController->getContent($contentDocument);

            return $contentDocument;
        }
    }

    /**
     * Retrieve an archive resource contents
     * @param string $docId The archive identifier
     *
     * @return documentManagement/document
     */
    public function getDocument($docId)
    {
        $document = $this->documentController->getById($docId);

        $archive = $this->getDescription($document->archiveId);

        $this->checkCommunicability($archive);

        return $document;
    }

    /**
     * Retrieve an archive resource contents
     * @param string $archiveId The archive identifier
     *
     * @return documentManagement/document[]
     */
    public function getDocuments($archiveId)
    {
        $documents = $this->documentController->getArchiveDocuments($archiveId);

        return $documents;
    }
    
   
    /**
     * Deliver an archive
     * @param mixed $archiveIds The identifier of archive or a list of identifiers
     * @param string $reference The medona message reference
     * @param string $comment   The message comment
     * @param string $senderOrg The sender organization registration number
     * 
     * @throws \bundle\recordsManagement\Exception\notCommunicableException
     */
    public function deliver($archiveIds, $reference=null, $comment = null, $senderOrg = null)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        } else {
            $reference = null;
        }

        if (!\laabs::hasBundle("medona")) {
            return;
        }

        $archiveDeliveryRequestController = \laabs::newController("medona/ArchiveDeliveryRequest");
        $archivesByOriginator = array();
        $messages = array();

        foreach ($archiveIds as $archiveId) {
            $archive = $this->retrieve($archiveId);

            if (isset($archivesByOriginator[$archive->originatorOrgRegNumber])) {
                $archivesByOriginator[$archive->originatorOrgRegNumber] = array();
            }

            $archivesByOriginator[$archive->originatorOrgRegNumber][] = $archive;
        }

        foreach ($archivesByOriginator as $originatorOrgRegNumber => $archives) {

            if (!$reference) {
                $reference = "archiveDeliveryRequest_" . \laabs::newId();
            }

            $derogation = false;
            try {
                $communicable = $this->checkCommunicability($archives[0]);
            } catch (\Exception $e) {
                $derogation = true;
            }

            $message = $archiveDeliveryRequestController->send($reference, $archives, $derogation, $comment);
            $messages[] = $message;
        }

        return $messages;
    }

    /**
     * Deliver the archive by description class and id
     * @param string $descriptionClass The description object class
     * @param string $descriptionId    The description object id
     * @param string $reference        The medona message reference
     * @param string $comment          The message comment
     * @param string $senderOrg        The sender organization registration number
     * 
     * @return object
     */
    public function deliverByDescription($descriptionClass, $descriptionId, $reference=null, $comment = null, $senderOrg = null)
    {

        $archive = $this->sdoFactory->read('recordsManagement/archive', array('descriptionClass ' => $descriptionClass, 'descriptionId' => $descriptionId));
        
        //$archive = $this->retrieve($archive->archiveId);
       
        if (!$reference) {
            $reference = "archiveDeliveryRequest_" . \laabs::newId();
        }

        $derogation = false;
        try {
            $communicable = $this->checkCommunicability($archive);
        } catch (\Exception $e) {
            $derogation = true;
        }
        
        if (\laabs::hasBundle("medona")) {
            $archiveDeliveryRequestController = \laabs::newController("medona/ArchiveDeliveryRequest");

            $message = $archiveDeliveryRequestController->send($reference, array($archive), $derogation, $comment, $senderOrg);
            
            return $message;
        }
    }
}