<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archives compliance
 */
trait archiveComplianceTrait
{
    //Params
    private $limit;
    private $delayDate;

    /**
     * Periodic integrity compliance method
     * @param string $limit The limit
     * @param string $delay The delay
     */
    public function periodicIntegrityCompliance($limit, $delay)
    {
        $this->limit = $limit;
        $this->delayDate = \laabs::newTimestamp()->sub(\laabs::newDuration($delay));

        $startEvent = $this->getStartEvent();
        $events = $this->getEventsToCheck($startEvent);
        $archives = $this->extractInformationFromEvent($events);
        $this->checkIntegrity($archives, $startEvent);
    }
    /**
     * Return the start event for begin the validation integrity
     *
     * @return lifeCycle/event $startEvent The event
     */
    public function getStartEvent()
    {
        //Get the most newest event of periodic integrity
        $startEvent = $this->sdoFactory->find("lifeCycle/event", "eventType='recordsManagement/integrityCheck'", null, "timestamp DESC", null, 1);

        if (count($startEvent) == 0) {
            //Get the latest event of deposit
            $startEvent = $this->sdoFactory->find("lifeCycle/event", "eventType='recordsManagement/deposit'", null, "timestamp ASC", null, 1);

            if (count($startEvent) == 0) {
                exit;
            }

            $startEvent = $this->lifeCycleJournalController->getEvent((string) $startEvent[0]->eventId);
        } else {
            $startEvent = $this->lifeCycleJournalController->getEvent((string) $startEvent[0]->eventId);
            $startEvent = $this->lifeCycleJournalController->getEvent((string) $startEvent->endEventId);
        }

        return $startEvent;
    }

    /**
     * Select archives for periodic integrity
     * @param lifeCycle/event $startEvent The first event to begin the validation of integrity
     *
     * @return array $events
     */
    public function getEventsToCheck($startEvent)
    {
        $currentEvent = $startEvent;
        $events = array();
        $i = 0;
        while ($i < $this->limit) {
            if ($i != 0) {
                $currentEvent = $this->lifeCycleJournalController->getNextEvent("recordsManagement/deposit");
                if ($currentEvent === false) {
                    exit;
                }

                if ($currentEvent == null) {
                    continue;
                }

                if ((strcmp((string) $currentEvent->eventId, (string) $startEvent->eventId) == 0) && ($i != 0)) {
                    break;
                }
            }
            if ($currentEvent->timestamp->diff($this->delayDate)->invert == 1) {
                $i++;
                continue;
            }

            if (!in_array($currentEvent, $events)) {
                $events[] = $currentEvent;
            } else {
                break;
            }

            $i++;
        }

        return $events;
    }

    /**
     * Extract archive information from events
     * @param type $events
     *
     * @return array $archives
     */
    public function extractInformationFromEvent($events)
    {
        $archives = array();

        foreach ($events as $key => $event) {
            $archive = new \stdClass();
            $archive->hash = $event->hash;
            $archive->hashAlgorithm = $event->hashAlgorithm;
            $archive->archiveId = $event->objectId;
            $archive->eventTimestamp = $event->timestamp;
            $archive->eventId = $event->eventId;
            $archives[] = $archive;
        }

        return $archives;
    }

    /**
     * Check integrity of archives
     * @param object          $archives
     * @param lifeCycle/event $startEvent
     */
    public function checkIntegrity($archives, $startEvent)
    {
        foreach ($archives as $key => $archive) {
            $this->verifyIntegrity($archive->archiveId, $archive->hash);
        }

        $eventItems = array('startEventDate' => ((string) $startEvent->timestamp), 'endEventDate' => ((string) end($archives)->eventTimestamp), 'endEventId' => ((string) end($archives)->eventId));
        $this->lifeCycleJournalController->logEvent('recordsManagement/integrityCheck', 'recordsManagement/archive', ((string) end($archives)->eventId), $eventItems, "Periodic validation of integrity");
    }
}
