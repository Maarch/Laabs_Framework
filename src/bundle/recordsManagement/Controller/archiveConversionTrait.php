<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archives conversion
 */
trait archiveConversionTrait
{
    /**
     * Flag for converison
     * @param array $documentIds Array of document identifier
     *
     * @return bool
     */
    public function conversion($documentIds)
    {
        // Medona
        if (\laabs::hasBundle("medona")) {
            $archiveConversionRequestController = \laabs::newController("medona/ArchiveConversionRequest");

            $archiveIds = array();

            $documentsByOriginator = array();
            foreach ($documentIds as $documentId) {
                $archiveDocumentDigitalResource = $this->sdoFactory->find('recordsManagement/archiveDocumentDigitalResource', "docId='".(string) $documentId."'")[0];
                $archiveIds[] = $archiveDocumentDigitalResource->archiveId;

                if (!isset($documentsByOriginator[$archiveDocumentDigitalResource->originatorOrgRegNumber])) {
                    $documentsByOriginator[$archiveDocumentDigitalResource->originatorOrgRegNumber] = array();
                }

                $documentsByOriginator[$archiveDocumentDigitalResource->originatorOrgRegNumber][] = $archiveDocumentDigitalResource;
            }

            $senderOrg = \laabs::getToken('ORGANIZATION');
            if (!$senderOrg) {
                throw \laabs::newException('medona/invalidMessageException', "No current organization choosen");
            }

            foreach ($documentsByOriginator as $originatorOrgRegNumber => $documents) {
                $recipientOrg = $this->organizationController->getOrgByRegNumber($originatorOrgRegNumber);

                $archiveConversionRequestController->send((string) \laabs::newId(), $senderOrg, $recipientOrg, $documents);
            }
        }

        return $documentIds;
    }

    /**
     * Convert archive
     * @param id $documentIds The document identifier or an identifier list
     *
     * @return array The convert documents
     */
    public function convert($documentIds)
    {
        if (!is_array($documentIds)) {
            $documentIds = array($documentIds);
        }

        // Store document and resources
        if (!$this->currentServiceLevel) {
            if (isset($archive->serviceLevelReference)) {
                $this->useServiceLevel('deposit', $archive->serviceLevelReference);
            } else {
                $this->useServiceLevel('deposit');
            }
        }

        $newDocument = array();
        $digitalResourceController = \laabs::newController("digitalResource/digitalResource");

        foreach ($documentIds as $documentId) {
            $document = $this->sdoFactory->find("recordsManagement/archiveDocumentDigitalResource", "docId='".$documentId."'")[0];
            $document->digitalResource = $digitalResourceController->retrieve($document->resId);

            if ($convertedDocument = $this->documentController->convertDocument($document)) {
                $convertedDocument->archiveId = $document->archiveId;

                $this->documentController->store($convertedDocument, $this->currentServiceLevel->digitalResourceClusterId, $convertedDocument->archiveId);
                $newDocument[] = $convertedDocument;
            }
        }

        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            //$eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null);
            $eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null, 'softwareName' => null, 'softwareVersion' => null);

            foreach ($newDocument as $document) {
                $address = $document->digitalResource->address[0];
                $eventInfo['resId'] = $document->resId;
                $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                $eventInfo['hash'] = $document->digitalResource->hash;
                $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                //$eventInfo['softwareName'] = $document->digitalResource->mediaInfo->name;
                $eventInfo['softwareName'] = "libreOffice";
                //$eventInfo['softwareVersion'] = $document->digitalResource->mediaInfo->version;
                $eventInfo['softwareVersion'] = "1.0";

                $this->lifeCycleJournalController->logEvent('recordsManagement/convert', 'recordsManagement/document', $document->docId, $eventInfo, "Conversion of document ".$document->docId);
            }
        }

        return $newDocument;
    }
}
