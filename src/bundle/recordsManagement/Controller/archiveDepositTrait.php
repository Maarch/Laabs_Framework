<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archive deposit
 */
trait archiveDepositTrait
{

    /**
     * Instanciate a new archive from a file
     *
     * @return recordsManagement/archive
     */
    public function newArchive()
    {
        // Use archive xml parser for archive creation
        $archive = \laabs::newInstance('recordsManagement/archive');

        // Generate archive id
        if (!isset($archive->archiveId)) {
            $archive->archiveId = \laabs::newId();
        }

        $archive->timestamp = \laabs::newTimestamp();
        $archive->status = 'received';

        // Use current profile
        if (isset($this->currentArchivalProfile)) {
            $archive->archivalProfileReference = $this->currentArchivalProfile->reference;

            $archive->descriptionClass = $this->currentArchivalProfile->descriptionClass;

            $archive->accessRuleCode = $this->currentArchivalProfile->accessRuleCode;
            $archive->retentionRuleCode = $this->currentArchivalProfile->retentionRuleCode;
        }

        // Use current service level
        if (isset($this->currentServiceLevel)) {
            $archive->serviceLevelReference = $this->currentServiceLevel->reference;
        }

        return $archive;
    }

    /**
     * Receive a new archive and store the pending archive if an import directory is given
     * @param recordsManagement/archive $archive               The archive
     *
     * @return recordsManagement/archive
     */
    public function receive($archive)
    {
        $this->depositorOrg = $this->organizationController->getOrgByRegNumber($archive->depositorOrgRegNumber);
        $this->useReferences($archive, 'deposit');

        if (!isset($archive->archiveId)) {
            $archive->archiveId = \laabs::newId();
        }
        $archive->status = "received";

        // Use current profile
        if (isset($this->currentArchivalProfile)) {
            $archive->archivalProfileReference = $this->currentArchivalProfile->reference;

            $archive->descriptionClass = $this->currentArchivalProfile->descriptionClass;
            $archive->descriptionId = \laabs::objectId($archive->descriptionObject, $archive->descriptionClass);
            $archive->retentionRuleCode = $this->currentArchivalProfile->retentionRuleCode;

            if (empty($archive->accessRuleCode)) {
                $archive->accessRuleCode = $this->currentArchivalProfile->accessRuleCode;
            }
        }

        // Use current service level
        if (isset($this->currentServiceLevel)) {
            $archive->serviceLevelReference = $this->currentServiceLevel->reference;
        }

        $this->getManagementRules($archive);
                
        // Documents
        if ($archive->document) {
            foreach ($archive->document as $document) {
                if (empty($document->digitalResource->resId)) {
                    $document->digitalResource->resId = \laabs::newId();
                    $document->resId = $document->digitalResource->resId;
                }
                if (empty($document->archiveId)) {
                    $document->archiveId = $archive->archiveId;
                }

                if (empty($document->digitalResource->hash) || empty($document->digitalResource->hashAlgorithm)) {
                    $this->documentController->getResourceHash($document, $this->hashAlgorithm);
                }

                if (empty($archive->descriptionObject->archiveId)) {
                    $archive->descriptionObject->archiveId = $archive->archiveId;
                }
            }
        }

        // Store archive in pendingDirectory
        $archiveDirectory = $this->pendingArchiveDirectory . DIRECTORY_SEPARATOR . (string) $archive->archiveId;

        mkdir($archiveDirectory, 0775, true);

        // Store documents' json
        $archiveJson = json_encode($archive);
        file_put_contents($archiveDirectory . DIRECTORY_SEPARATOR . "archive", $archiveJson);

        $filesDirectory = $archiveDirectory . DIRECTORY_SEPARATOR . "files";
        mkdir($filesDirectory, 0775, true);
 
        if ($archive->document) {
            foreach ($archive->document as $document) {
                $resource = $document->digitalResource->resId . "." . $document->type;
                file_put_contents($filesDirectory . DIRECTORY_SEPARATOR . $resource, $document->digitalResource->getContents());
            }
        }

        // Create archive
        $this->sdoFactory->create($archive);

        // Life cycle jounral
        if (\laabs::hasBundle('lifeCycle')) {
            // Certificate of reception
            $eventItems = array('hashAlgorithm' => null, 'hash' => null, 'depositorOrgRegNumber' => $archive->depositorOrgRegNumber);

            if ($archive->document) {
                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $eventItems['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventItems['hash'] = $document->digitalResource->hash;
                        break;
                    }
                }
            }

            $this->lifeCycleJournalController->logEvent('recordsManagement/reception', 'recordsManagement/archive', $archive->archiveId, $eventItems, "Reception of archive " . $archive->archiveId);
        }

        return $archive;
    }

    /**
     * Get management rules
     * @param object $archive
     *
     * @return disposalRule
     */
    public function getManagementRules($archive)
    {
        $conf = \laabs::configuration('recordsManagement');
        if (isset($conf['archivalProfileType']) && $conf['archivalProfileType'] > 1) {
            $this->getManagementRulesFromProfile($archive);
        }

        if (isset($archive->retentionRuleCode) && !isset($archive->retentionDuration)) {
            try {
                $retentionRule = $this->retentionRuleController->read($archive->retentionRuleCode);
                $archive->retentionDuration = $retentionRule->duration;
            } catch (\Exception $e) {

            }
        }

        if (isset($archive->retentionStartDate) && isset($archive->retentionDuration)) {
            $archive->disposalDate = $archive->retentionStartDate->shift($archive->retentionDuration);
        }

        if (isset($archive->accessRuleCode) && !isset($archive->accessRuleDuration)) {
            try {
                $accessRule = $this->accessRuleController->edit($archive->accessRuleCode);
                $archive->accessRuleDuration = $accessRule->duration;
            } catch (\Exception $e) {

            }
        }

        if (isset($archive->retentionStartDate) && isset($archive->accessRuleDuration)) {
            $archive->accessRuleComDate = $archive->accessRuleStartDate->shift($archive->accessRuleDuration);
        }
    }

    protected function getManagementRulesFromProfile($archive)
    {
        // Access & communication rules
        if ($archive->archivalProfileReference) {
            try {
                $archivalProfile = $this->useArchivalProfile($archive->archivalProfileReference);
            } catch (\Exception $e) {

            }
        }

        // Retention rule
        if (
            $archive->retentionDuration == null 
            && $archive->finalDisposition == null 
            && !empty($archive->retentionStartDate) 
            && isset($archive->retentionRuleCode)
        ) {
                $retentionRule = $this->sdoFactory->read('recordsManagement/retentionRule', $archive->retentionRuleCode);
                $archive->retentionDuration = $retentionRule->duration;
                $archive->finalDisposition = $retentionRule->finalDisposition;
        }

        // Access rule
        if (
            $archive->accessRuleDuration == null 
            && !empty($archive->accessRuleStartDate) 
            && isset($archive->accessRuleCode)
        ) {
                $accessRule = $this->sdoFactory->read('recordsManagement/accessRule', $archive->accessRuleCode);
                $archive->accessRuleDuration = $accessRule->duration;
        }

        if (
            $archive->retentionDuration == null 
            && $archive->finalDisposition == null 
            && $archive->retentionStartDate == null 
            && $archivalProfile
        ) {
            $archive->retentionRuleCode = $archivalProfile->retentionRule->code;
            $archive->retentionDuration = $archivalProfile->retentionRule->duration;
            $archive->finalDisposition = $archivalProfile->retentionRule->finalDisposition;
            $dateRule = strtok($archivalProfile->retentionStartDate, LAABS_URI_SEPARATOR);
            switch ($dateRule) {
                case 'definedLater' : 
                    $archive->retentionStartDate = null;
                    break;
                case 'depositDate' : 
                    $archive->retentionStartDate = \laabs::newDate();
                    break;
                case 'createdDate' : // TO DO
                    break;
                case 'publicatedDate' : // TO DO
                    break;
                case 'receptionDate' : // TO DO
                    break;
                case 'lastCommunicationDate' : // TO DO
                    break;
                
                case 'description':
                    $path = strtok(LAABS_URI_SEPARATOR);
                    $archive->retentionStartDate = $archive->descriptionObject->{$path};
                    break;
            }
        }
    }

    /**
     * Add a document to the archive
     * @param object $archive
     *
     * @return object
     */
    public function addDocument($archive)
    {
        // Load agreement, profile and service level
        $this->useReferences($archive, 'deposit');

        $document = $this->documentController->newDocument();

        $archive->document[] = $document;

        return $document;
    }

    /**
     * Validate an archive VS agreement, profile and service level
     * @param mixed $archive Archive object or archive identifer
     *
     * @return boolean
     */
    public function validate($archive)
    {
        // Read archive
        if (is_string($archive)) {
            $archive = $this->readPendingArchive($archive);
        }

        // Load agreement, profile and service level
        $this->useReferences($archive, 'deposit');

        // Generate archive id
        if (!isset($archive->archiveId)) {
            $archive->archiveId = \laabs::newId();
        }

        try {
            // Validations
            $archive = $this->validateArchiveManagementInformation($archive);
            $archive = $this->validateArchiveDocument($archive);
            $archive = $this->validateArchiveDescriptionObject($archive);

            // Life cycle jounral
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of validation
                $eventItems = array('hashAlgorithm' => null, 'hash' => null);

                if ($archive->document) {
                    foreach ($archive->document as $document) {
                        if ($document->type == "CDO" && $document->type != true) {
                            $eventItems['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                            $eventItems['hash'] = $document->digitalResource->hash;
                            break;
                        }
                    }
                }

                $this->lifeCycleJournalController->logEvent('recordsManagement/validation', 'recordsManagement/archive', $archive->archiveId, $eventItems, "Validation of archive " . $archive->archiveId);
            }

        } catch (\Exception $exception) {
            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of validation
                $eventItems = array('hashAlgorithm' => null, 'hash' => null);

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->type != true) {
                        $eventItems['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventItems['hash'] = $document->digitalResource->hash;
                        break;
                    }
                }

                $this->lifeCycleJournalController->logEvent('recordsManagement/validation', 'recordsManagement/archive', $archive->archiveId, $eventItems, "Validation failure of archive " . $archive->archiveId, false);
            }

            $archive->status = "invalid";
            $this->sdoFactory->update($archive);
            throw $exception;
        }

        return true;
    }

    private function validateArchiveManagementInformation($archive)
    {
        // Validate retention rule
        if (!$archive->retentionRuleCode && (!$archive->retentionDuration && !$archive->finalDisposition)) {
            throw new \bundle\recordsManagement\Exception\invalidArchiveException("The archive is invalid: no retention rule defined.");
        }

        if (empty($archive->accessRuleCode)) {
            throw new \bundle\recordsManagement\Exception\invalidArchiveException("The archive is invalid: no access rule defined.");
        }

        return $archive;
    }

    private function validateArchiveDocument($archive)
    {
        if (!$archive->document) {
            return $archive;
        }

        foreach ($archive->document as $document) {
            // processs content data objects
            if ($document->type == "CDO") {
                // Use service level
                if (isset($this->currentServiceLevel)) {
                    if ($this->currentServiceLevel->formatDetection) {
                        $format = $this->documentController->getResourceFormat($document);
                    }

                    if ($this->currentServiceLevel->mediaInfo) {
                        $this->documentController->getResourceMediaInfo($document);
                    }

                    if ($this->currentServiceLevel->formatValidation) {
                        $this->documentController->validateResourceFormat($document);
                    }   
                }
            }

            if (empty($document->digitalResource->hash) || empty($document->digitalResource->hashAlgorithm)) {
                throw new \bundle\recordsManagement\Exception\invalidArchiveException("The archive is invalid: no hash on document.");
            } else {
                $myHash = hash($document->digitalResource->hashAlgorithm, $document->digitalResource->getContents());

                if ($document->digitalResource->hash != $myHash) {
                    throw new \bundle\recordsManagement\Exception\invalidArchiveException("The archive is invalid: invalid hash.");
                }
            }
        }

        return $archive;
    }

    private function validateArchiveDescriptionObject($archive)
    {
        if (!empty($archive->archivalProfileReference)) {
            if (!empty($archive->descriptionClass)) {
                $this->validateDescriptionObject($archive->descriptionObject, $this->currentArchivalProfile);
            }
        }

        return $archive;
    }

    /**
     * Check if an object correspond to an archival profile
     * @param mixed                             $object          The metadata object to check
     * @param recordsManagement/archivalProfile $archivalProfile The reference of the profile
     *
     * @return boolean The result of the validation
     */
    public function validateDescriptionObject($object, $archivalProfile)
    {
        if (\laabs::getClass($object)->getName() != $archivalProfile->descriptionClass) {
            return false;
        }

        foreach ($archivalProfile->profileDescription as $description) {
            $propertyName = explode(LAABS_URI_SEPARATOR, $description->propertyName);
            $propertiesList = array($object);

            foreach ($propertyName as $name) {
                $newPropertiesList = array();
                foreach ($propertiesList as $propertyValue) {
                    if (isset($propertyValue->{$name})) {
                        if (is_array($propertyValue->{$name})) {
                            foreach ($propertyValue->{$name} as $value) {
                                $newPropertiesList[] = $value;
                            }
                        } else {
                            $newPropertiesList[] = $propertyValue->{$name};
                        }
                    } else {
                        $newPropertiesList[] = null;
                    }
                }
                $propertiesList = $newPropertiesList;
            }

            foreach ($propertiesList as $propertyValue) {
                if ($description->required && $propertyValue == null) {
                    throw new \bundle\recordsManagement\Exception\archiveDoesNotMatchProfileException('The description class does not match with the archival profile.');
                }
            }
        }

        return true;
    }

    /**
     * Generate a new package
     * @param recordsManagement/archive $archive The archive to deposit
     *
     * @return boolean The result of the operation
     */
    private function generatePackage($archive)
    {
        $digitalResourceCDO = null;
        $digitalResourceController = \laabs::newController("digitalResource/digitalResource");
        $formatController = \laabs::newController("digitalResource/format");

        foreach ($archive->document as $document) {
            if ($document->type == "CDO") {
                $digitalResourceCDO = $document->digitalResource;
            }
        }

        if (!isset($digitalResourceCDO->format) && isset($digitalResourceCDO->puid)) {
            $digitalResourceCDO->format = $formatController->get($digitalResourceCDO->puid);
        }

        // Representation information
        $RI = $this->generateRI($digitalResourceCDO);
        
        $resRI = $digitalResourceController->createFromContents($RI);
        $resRI->fileName = $archive->archiveId . '.ri.xml';
        $resRI->fileExtension = 'xml';
        $resRI->puid = 'fmt/101';
        $resRI->mimetype = 'text/xml';
        $resRI->hash = md5($RI);
        $resRI->hashAlgorithm = 'md5';

        $documentRI = $this->documentController->newDocument();
        $documentRI->type = "RI";
        $documentRI->digitalResource = $resRI;
        

        // Preservation Description Information
        $PDI = $this->generatePDI($digitalResourceCDO, $archive);

        $resPDI = $digitalResourceController->createFromContents($PDI);
        $resPDI->fileName = $archive->archiveId . '.pdi.xml';
        $resPDI->fileExtension = 'xml';
        $resPDI->puid = 'fmt/101';
        $resPDI->mimetype = 'text/xml';
        $resPDI->hash = md5($PDI);
        $resPDI->hashAlgorithm = 'md5';

        $documentPDI = $this->documentController->newDocument();
        $documentPDI->type = "PDI";
        $documentPDI->digitalResource = $resPDI;
        
        $archive->document[] = $documentRI;
        $archive->document[] = $documentPDI;

        return $archive;
    }

    private function generateRI($digitalResourceCDO)
    {
        $xml = "<RepresentationInformation>";
            $xml .= "<fileName>" . $digitalResourceCDO->fileName . "</fileName>";
            $xml .= "<fileExtension>" . $digitalResourceCDO->fileExtension . "</fileExtension>";
            $xml .= "<mimetype>" . $digitalResourceCDO->mimetype . "</mimetype>";
            $xml .= "<mediaInfo>" . $digitalResourceCDO->mediaInfo . "</mediaInfo>";
            $xml .= "<size>" . $digitalResourceCDO->size . "</size>";
            $xml .= "<format>";
                $xml .= "<puid>" . $digitalResourceCDO->puid . "</puid>";
                if (isset($digitalResourceCDO->format)) {
                    $xml .= "<name>" . $digitalResourceCDO->format->name . "</name>";
                    $xml .= "<version>" . $digitalResourceCDO->format->version . "</version>";
                }
            $xml .= "</format>";
        $xml .= "</RepresentationInformation>";

        return $xml;
    }

    private function generatePDI($digitalResourceCDO, $archive)
    {
        $xml = "<PreservationDescriptionInformation>";

            $xml .= "<ReferenceInformation>";
                $xml .= "<archiveId>" . $archive->archiveId . "</archiveId>";
            $xml .= "</ReferenceInformation>";

            $xml .= "<FixityInformation>";
                $xml .= "<hash algorithm='" . $digitalResourceCDO->hashAlgorithm . "' >" . $digitalResourceCDO->hash . "</hash>";
            $xml .= "</FixityInformation>";

            $xml .= "<ContextInformation>";
            if (isset($archive->archiveRelationship) && count($archive->archiveRelationship)) {
                foreach ($archive->archiveRelationship as $archiveRelationship) {
                    $xml .= "<relationship type='" . $archiveRelationship->typeCode . "'>" . $archiveRelationship->relatedArchiveId . "</relationship>";
                }
            }
            $xml .= "</ContextInformation>";

            $xml .= "<ProvenanceInformation>";
                $xml .= "<originatorOrg>".$archive->originatorOrgRegNumber."</originatorOrg>";
            $xml .= "</ProvenanceInformation>";

        $xml .= "</PreservationDescriptionInformation>";

        return $xml;
    }

    /**
     * Deposit a new archive
     * @param recordsManagement/archive $archive The archive to deposit
     *
     * @return boolean The result of the operation
     */
    public function deposit($archive)
    {
        $originatorOrg = $this->organizationController->getOrgByRegNumber($archive->originatorOrgRegNumber); 

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            /*$exist = $this->sdoFactory->exists("recordsManagement/archive", $archive->archiveId);

            if ($exist) {
                $archive = $this->readPendingArchive($archive);
            } else {
                if (!isset($archive->archiveId)) {
                    $archive->archiveId = \laabs::newId();
                }
            }*/

            $archive->originatorOwnerOrgId = $originatorOrg->ownerOrgId;
            $archive->status = 'preserved';
            $archive->depositDate = \laabs::newTimestamp();
            
            if ($archive->document) {
                $this->generatePackage($archive);
            }

            $this->getManagementRules($archive);  

            // Record description metadata
            if (!empty($archive->descriptionClass) && !empty($archive->descriptionObject)) {
                $descriptionController = $this->useDescriptionController($archive->descriptionClass);
                $descriptionController->create($archive->descriptionObject);
                $archive->descriptionId = \laabs::objectId($archive->descriptionObject);
            }

            // Create archive
            /*if ($exist) {
                $this->sdoFactory->update($archive);
            } else {*/
                $this->sdoFactory->create($archive, 'recordsManagement/archive');
            //}

            // Store document and resources
            if (!$this->currentServiceLevel) {
                if (isset($archive->serviceLevelReference)) {
                    $this->useServiceLevel('deposit', $archive->serviceLevelReference);
                } else {
                    $this->useServiceLevel('deposit');
                }
            }

            if (isset($archive->document) && count($archive->document)) {
                foreach ($archive->document as $document) {
                    $document->archiveId = $archive->archiveId;
                    
                    $this->documentController->store($document, $this->currentServiceLevel->digitalResourceClusterId, $archive->archiveId);
                    
                    if ($convertedDocument = $this->documentController->convertDocument($document)) {
                        $archive->document[] = $convertedDocument;
                        
                        $convertedDocument->archiveId = $archive->archiveId;
                        
                        $this->documentController->store($convertedDocument, $this->currentServiceLevel->digitalResourceClusterId, $archive->archiveId);
                        
                        if (\laabs::hasBundle('lifeCycle')) {
                            $eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null, 'softwareName' => null, 'softwareVersion' => null);

                            $address = $convertedDocument->digitalResource->address[0];
                            $eventInfo['resId'] = $convertedDocument->resId;
                            $eventInfo['hashAlgorithm'] = $convertedDocument->digitalResource->hashAlgorithm;
                            $eventInfo['hash'] = $convertedDocument->digitalResource->hash;
                            $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                            //$eventInfo['softwareName'] = $document->digitalResource->mediaInfo->name;
                            $eventInfo['softwareName'] = "libreOffice";
                            //$eventInfo['softwareVersion'] = $document->digitalResource->mediaInfo->version;
                            $eventInfo['softwareVersion'] = "1.0";

                            $this->lifeCycleJournalController->logEvent('recordsManagement/convert', 'recordsManagement/document', $convertedDocument->docId, $eventInfo, "Conversion of document ".$convertedDocument->docId);
                        }
                    }
                }
            }

            if (isset($archive->contents) && count($archive->contents)) {
                foreach ($archive->contents as $content) {
                    $content->parentArchiveId = $archive->archiveId;
                    $this->deposit($content);
                }
            }

        } catch (\Exception $exception) {
            if ($archive->document) {
                foreach ($archive->document as $document) {
                    $this->documentController->rollbackStorage($document);
                }
            }

            if ($transactionControl) {
                $this->sdoFactory->rollback();

                if ($exist) {
                    $archive->status = "invalid";
                    $this->sdoFactory->update($archive);
                }
            }

            throw $exception;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            // get certificate of deposit
            $eventInfo = array('depositorOrgRegNumber' => $archive->depositorOrgRegNumber);

            if (isset($archive->document) && count($archive->document)) {
                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];

                        $eventInfo['resId'] = (string) $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri . "/" . $address->path;
                        break;
                    }
                }
            }

            $event = $this->lifeCycleJournalController->logEvent('recordsManagement/deposit', 'recordsManagement/archive', $archive->archiveId, $eventInfo, "Deposit of archive");
            $archive->lifeCycleEvent[] = $event;
        }
        
        return true;
    }

    /**
     * Receive, validate and deposit a new archive
     * @param type $archive               The archive to deposit
     * @param type $depositorOrgRegNumber The depositor organization registration number
     */
    public function interactiveArchive($archive, $depositorOrgRegNumber)
    {
        $archive = $this->receive($archive, $depositorOrgRegNumber);
        $this->validate($archive);
        $this->deposit($archive, $depositorOrgRegNumber);
    }

    /**
     * Read pending archive
     * @param mixed $archive The archive or the archive identifier
     *
     * @return recordsManagement/archive
     */
    public function readPendingArchive($archive)
    {
        if (is_string($archive)) {
            $archive = $this->sdoFactory->read('recordsManagement/archive', $archive);
        }

        $pendingArchive = $this->pendingArchiveDirectory . DIRECTORY_SEPARATOR . $archive->archiveId;

        $archiveJson = file_get_contents($pendingArchive . DIRECTORY_SEPARATOR . "archive");

        $archive = \laabs::castObject(json_decode($archiveJson), 'recordsManagement/archive');
        $archive->descriptionObject = \laabs::castObject($archive->descriptionObject, $archive->descriptionClass);

        if ($archive->document) {
            foreach ($archive->document as $document) {
                if ($document->type != "CDO") {
                    continue;
                }

                $resourceFileName = $document->digitalResource->resId . "." . $document->type;
                $resourceFile = $pendingArchive . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $resourceFileName;

                $document->digitalResource->setContents(file_get_contents($resourceFile));
            }
        }

        return $archive;
    }

    /**
     * Delete an archive directory
     * @param mixed $archive The archive or the path to delete
     *
     * @return boolean The result of the request
     */
    protected function removeArchiveDirectory($archive)
    {
        if (!is_string($archive)) {
            $dir = $this->pendingArchiveDirectory . DIRECTORY_SEPARATOR . (string) $archive->archiveId;
        } else {
            $dir = $archive;
        }

        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->removeArchiveDirectory($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }

        return true;
    }
}
