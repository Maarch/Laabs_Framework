<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archives destruction
 */
trait archiveDestructionTrait
{
    /**
     * Flag for disposal
     * @param array $archiveIds Array of archive identifier
     *
     * @return bool
     */
    public function dispose($archiveIds)
    {
        $currentDate = \laabs::newTimestamp();

        foreach ($archiveIds as $archiveId) {
            $archive = $this->sdoFactory->read('recordsManagement/disposalDate', $archiveId);

            if ($archive->finalDisposition != "destruction") {
                throw new \bundle\recordsManagement\Exception\notDisposableArchiveException("Archive not set for destruction.");
            }

            if ($archive->disposalDate > $currentDate) {
                throw new \bundle\recordsManagement\Exception\notDisposableArchiveException("Archive disposal date not reached yet.");
            }
        }

        // Medona
        if (\laabs::hasBundle("medona")) {
            $archiveDestructionController = \laabs::newController("medona/ArchiveDestructionRequest");

            $archivesByOriginator = array();
            foreach ($archiveIds as $archiveId) {
                $archive = $this->retrieve($archiveId);
                if (!isset($archivesByOriginator[$archive->originatorOrgRegNumber])) {
                    $archivesByOriginator[$archive->originatorOrgRegNumber] = array();
                }

                $archivesByOriginator[$archive->originatorOrgRegNumber][] = $archive;
            }

            foreach ($archivesByOriginator as $originatorOrgRegNumber => $archives) {
                $archiveDestructionController->send((string) \laabs::newId(), $archives);
            }
        }

        return $this->setStatus($archiveIds, 'disposable');
    }

    /**
     * Cancel destruction
     * @param array $archiveIds Array of archive identifier
     *
     * @return bool
     */
    public function cancelDestruction($archiveIds)
    {
        return $this->setStatus($archiveIds, 'preserved');
    }

    /**
     * Delete archive and his related archive
     * @param id $archiveIds The archive identifier or an identifier list
     *
     * @return recordsManagement[] The destroyed archives
     */
    public function destruct($archiveIds)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }

        $archives = array();

        foreach ($archiveIds as $archiveId) {
            $archive = $this->retrieve($archiveId);

            $destroyedArchives =  $this->destructArchive($archive);
            $archiveIds = array_diff($archiveIds, $destroyedArchives);

            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                $eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null);

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];
                        $eventInfo['resId'] = $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                        break;
                    }
                }

                $this->lifeCycleJournalController->logEvent('recordsManagement/destruction', 'recordsManagement/archive', $archive->descriptionId, $eventInfo, "Destruction of archive ".$archive->descriptionId);
            }

            $archives[] = $archive;
        }

        return $archives;
    }

    /**
     * Destruct an archive
     * @param recordsManagement/archive $archive The archive
     *
     * @return array the destroyed archives identifiers
     **/
    private function destructArchive($archive)
    {
        $destroyedArchiveId = array();

        // Load agreement, profile and service level
        $this->useReferences($archive, 'destruction');

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            // Delete description
            if (isset($archive->descriptionClass)) {
                $this->currentDescriptionController->delete($archive->descriptionId);
            }

            // Relationship
            $relationships = $this->sdoFactory->readChildren('recordsManagement/archiveRelationship', $archive);
            foreach ($relationships as $relationship) {
                $this->sdoFactory->delete($relationship);
            }

            // Children archives
            $childrenArchives = $this->sdoFactory->readChildren('recordsManagement/archive', $archive);
            if (count($childrenArchives)) {
                foreach ($childrenArchives as $child) {
                    $destroyedArchiveId = array_merge($this->destructArchive($child), $destroyedArchiveId);
                }
            }

            $this->documentController->deleteArchiveDocuments($archive->archiveId);
            $this->sdoFactory->delete($archive);
            $destroyedArchiveId[] = $archive->archiveId;

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }
            throw $exception;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return $destroyedArchiveId;
    }
}
