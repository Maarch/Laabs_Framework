<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Trait for archives modification
 */
trait archiveModificationTrait
{

    /**
     * Read the retention rule of an archive
     * @param string $archiveId The archive identifier
     *
     * @return recordsManagement/archive[]
     */
    public function editArchiveRetentionRule($archiveId)
    {
        $archive = $this->sdoFactory->read('recordsManagement/archive', $archiveId);

        $this->getAccessRule($archive);

        return \laabs::castMessage($archive, 'recordsManagement/archiveRetentionRule');

    }

    /**
     * Read the access rule of an archive
     * @param string $archiveId The archive identifier
     *
     * @return recordsManagement/archive[]
     */
    public function editArchiveAccessRule($archiveId)
    {
        $archive = $this->sdoFactory->read('recordsManagement/archive', $archiveId);

        $this->getAccessRule($archive);

        return \laabs::castMessage($archive, 'recordsManagement/archiveAccessRule');
    }

    /**
     * Modify the archive retention
     * @param recordsManagement/archiveRetentionRule $retentionRule The retention rule object
     * @param mixed                                  $archiveIds    The archives ids
     *
     * @return bool
     */
    public function modifyRetentionRule($retentionRule, $archiveIds)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }

        $res = array('success' => array(), 'error' => array());

        $archives = array();
        $events = array();

        if (!$currentOrg = \laabs::getToken("ORGANIZATION")) {
            throw \laabs::newException('recordsManagement/noOrgUnitException', "Permission denied: You have to choose a working organization unit to proceed this action.");
        }

        foreach ($archiveIds as $archiveId) {
            $archive = $this->getDescription($archiveId);

            if (!in_array($archive->status, array("preserved", "frozen"))) {
                array_push($res['error'], $archiveId);

                $operationResult = false;

            } else {
                $retentionRule->archiveId = $archiveId;
                $retentionRule->disposalDate = $this->calculateDate($retentionRule->retentionStartDate, $retentionRule->retentionDuration);

                $this->sdoFactory->update($retentionRule, 'recordsManagement/archive');

                // Update current object for caller
                $archive->retentionStartDate = $retentionRule->retentionStartDate;
                $archive->retentionDuration = $retentionRule->retentionDuration;
                $archive->disposalDate = $retentionRule->disposalDate;

                array_push($res['success'], $archiveId);

                $operationResult = true;

                $archives[] = $archive;
            }

            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of modication
                $eventInfo = array();

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];
                        $eventInfo['resId'] = $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                        break;
                    }
                }

                $eventInfo['retentionStartDate'] = (string) $retentionRule->retentionStartDate;
                $eventInfo['retentionDuration'] = (string) $retentionRule->retentionDuration;
                $eventInfo['finalDisposition'] = (string) $retentionRule->finalDisposition;

                $event = $this->lifeCycleJournalController->logEvent(
                    'recordsManagement/retentionRuleModification',
                    'recordsManagement/archive',
                    $archive->archiveId,
                    $eventInfo,
                    "Modification of retention rule",
                    $operationResult
                );

                $archive->lifeCycleEvent = array($event);
            }
        }

        // Send modification notification
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return $res;
    }

    /**
     * Modify the archive access
     * @param recordsManagement/archiveAccessCode $accessRule The access rule object
     * @param array                               $archiveIds The archives ids
     *
     * @return bool
     */
    public function modifyAccessRule($accessRule, $archiveIds)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }

        $res = array('success' => array(), 'error' => array());

        $archives = array();
        $operationResult = null;


        foreach ($archiveIds as $archiveId) {
            $archive = $this->getDescription($archiveId);
            
            if (!in_array($archive->status, array("preserved", "frozen"))) {
                array_push($res['error'], $archiveId);

                $operationResult = false;
            } else {
                $accessRule->archiveId = $archiveId;

                if ($accessRule->accessRuleDuration != null && $accessRule->accessRuleStartDate != null) {
                    $accessRule->accessRuleComDate = $this->calculateDate($accessRule->accessRuleStartDate, $accessRule->accessRuleDuration);
                }
                
                $this->sdoFactory->update($accessRule, 'recordsManagement/archive');

                $archive->accessRuleStartDate = $accessRule->accessRuleStartDate;
                $archive->accessRuleDuration = $accessRule->accessRuleDuration;
                $archive->accessRuleComDate = $accessRule->accessRuleComDate;

                array_push($res['success'], $archiveId);

                $operationResult = true;

                $archives[] = $archive;
            }

            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of modication
                $eventInfo = array();

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];
                        $eventInfo['resId'] = $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                        break;
                    }
                }

                $eventInfo['accessRuleStartDate'] = (string) $accessRule->accessRuleStartDate;
                $eventInfo['accessRuleDuration'] = (string) $accessRule->accessRuleDuration;

                $event = $this->lifeCycleJournalController->logEvent(
                    'recordsManagement/accessRuleModification',
                    'recordsManagement/archive',
                    $archive->archiveId,
                    $eventInfo,
                    "Modification of access rule",
                    $operationResult
                );

                $archive->lifeCycleEvent = array($event);
            }
        }

        // Send modification notification
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return $res;
    }

    /**
     * Suspend archives
     * @param mixed $archiveIds Array of archive identifier
     *
     * @return array
     */
    public function freeze($archiveIds)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }
        $res = $this->setStatus($archiveIds, 'frozen');

        $archives = array();

        foreach ($archiveIds as $archiveId) {
            $archive = $this->getDescription($archiveId);

            $operationResult = true;

            $archives[] = $archive;

            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of modication
                $eventInfo = array();

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];
                        $eventInfo['resId'] = $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                        break;
                    }
                }

                $event = $this->lifeCycleJournalController->logEvent(
                    'recordsManagement/freeze',
                    'recordsManagement/archive',
                    $archive->archiveId,
                    $eventInfo,
                    "Frezze of life cycle",
                    $operationResult
                );

                $archive->lifeCycleEvent = array($event);
            }
        }

        // Send modification notification
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return $res;
    }

    /**
     * Liberate archives
     * @param mixed $archiveIds Array of archive identifier
     *
     * @return array
     */
    public function unfreeze($archiveIds)
    {
        if (!is_array($archiveIds)) {
            $archiveIds = array($archiveIds);
        }
        $res = $this->setStatus($archiveIds, 'preserved');

        $archives = array();
        foreach ($archiveIds as $archiveId) {
            $archive = $this->getDescription($archiveId);

            $operationResult = true;

            $archives[] = $archive;

            // Life cycle journal
            if (\laabs::hasBundle('lifeCycle')) {
                // Certificate of modication
                $eventInfo = array();

                foreach ($archive->document as $document) {
                    if ($document->type == "CDO" && $document->copy != true) {
                        $address = $document->digitalResource->address[0];
                        $eventInfo['resId'] = $document->resId;
                        $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                        $eventInfo['hash'] = $document->digitalResource->hash;
                        $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                        break;
                    }
                }

                $event = $this->lifeCycleJournalController->logEvent(
                    'recordsManagement/unfreeze',
                    'recordsManagement/archive',
                    $archive->archiveId,
                    $eventInfo,
                    "Unfrezze of life cycle",
                    $operationResult
                );

                $archive->lifeCycleEvent = array($event);
            }
        }

        // Send modification notification
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return $res;
    }

    /**
     * Add a relationship to the archive
     * @param recordsManagement/archiveRelationship $archiveRelationship The relationship of the archive
     *
     * @return bool The result of the operation
     */
    public function addRelationship($archiveRelationship)
    {
        $this->archiveRelationshipController->createRelationship($archiveRelationship);

        $archive = $this->getDescription($archiveRelationship->archiveId);

        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            // Certificate of modication
            $eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null);

            foreach ($archive->document as $document) {
                if ($document->type == "CDO" && $document->copy != true) {
                    $address = $document->digitalResource->address[0];
                    $eventInfo['resId'] = $document->resId;
                    $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                    $eventInfo['hash'] = $document->digitalResource->hash;
                    $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                    break;
                }
            }

            $this->lifeCycleJournalController->logEvent('recordsManagement/modification', 'recordsManagement/archive', $archive->archiveId, $eventInfo, "Relationship added with archive ".$archiveRelationship->relatedArchiveId);
        }

        // Send modification notification
        $messageController = \laabs::newController("medona/message");
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return true;
    }

    /**
     * delete a relationship
     * @param recordsManagement/archiveRelationship $archiveRelationship The archive relationship object
     *
     * @return recordsManagement/archiveRelationship
     */
    public function deleteRelationship($archiveRelationship)
    {
        $this->archiveRelationshipController->deleteRelationship($archiveRelationship);

        $archive = $this->getDescription($archiveRelationship->archiveId);

        // Life cycle journal
        if (\laabs::hasBundle('lifeCycle')) {
            // Certificate of modication
            $eventInfo = array('resId' => null, 'hashAlgorithm' => null, 'hash' => null, 'address' => null);

            foreach ($archive->document as $document) {
                if ($document->type == "CDO" && $document->copy != true) {
                    $address = $document->digitalResource->address[0];
                    $eventInfo['resId'] = $document->resId;
                    $eventInfo['hashAlgorithm'] = $document->digitalResource->hashAlgorithm;
                    $eventInfo['hash'] = $document->digitalResource->hash;
                    $eventInfo['address'] = $address->repository->repositoryUri."/".$address->path;
                    break;
                }
            }

            $this->lifeCycleJournalController->logEvent('recordsManagement/modification', 'recordsManagement/archive', $archive->archiveId, $eventInfo, "Relationship with archive ".$archiveRelationship->relatedArchiveId."deleted");
        }

         // Send modification notification
        if (\laabs::hasBundle("medona")) {
            $this->sendModificationNotification($archives);
        }

        return true;
    }

    protected function sendModificationNotification($archives)
    {
        $archivesByOriginator = array();
        foreach ($archives as $archive) {
            if (!isset($archivesByOriginator[$archive->originatorOrgRegNumber])) {
                $archivesByOriginator[$archive->originatorOrgRegNumber] = array();
            }

            $archivesByOriginator[$archive->originatorOrgRegNumber][] = $archive;
        }

        $archiveModificationNotificationController = \laabs::newController("medona/ArchiveModificationNotification");

        foreach ($archivesByOriginator as $originatorOrgRegNumber => $archives) {
            $message = $archiveModificationNotificationController->send((string) \laabs::newId(), $archives);
        }
    }
}
