<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\recordsManagement\Controller;

/**
 * Class log
 *
 * @package RecordsManagement
 * @author  Cyril Vazquez <cyril.vazquez@maarch.org>
 */
class log 
    implements \bundle\recordsManagement\logInterface
{
    /* Properties */

    public $sdoFactory;

    /**
     * Constructor of access control class
     * @param \dependency\sdo\Factory $sdoFactory The factory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory) 
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get a  search result
     * @param string $archiveId   The archive identifier
     * @param string $type        The type
     * @param string $date        The date
     * @param string $processName The process name
     * @param string $processId   The process identifier
     *
     * @return array Array of logs
     */
    public function find($archiveId = null, $type = null, $date = null, $processName = null, $processId = null) {
        $queryParts = array();

        //$queryParts[] = $this->auth->getUserAccessRule('recordsManagement/log');

        if ($archiveId) {
            $queryParts['archiveId'] = "archiveId = '*$archiveId*'";
        }
        if ($type) {
            $queryParts['type'] = "type = '*$type*'";
        }
        if ($date) {
            $queryParts['fromDate'] = "fromDate <= '$date'";
            $queryParts['toDate'] = "toDate >= '$date'";
        }
        if ($processName) {
            $queryParts['processName'] = "processName = '*$processName*'";
        }
        if ($processId) {
            $queryParts['processId'] = "processIds = '*$processId*'";
        }

        $queryString = implode(' AND ', $queryParts);

        $logs = $this->sdoFactory->find("recordsManagement/log", $queryString, null, false, 0, 100);

        return $logs;
    }

    /**
     * Create the requested log
     * @param object $log log object
     *
     * @return boolean status of the query
     */
    public function create($log) {
        if (!\laabs::validate($log)) {
            $e = new \core\Exception('Invalid log data');

            $e->errors = \laabs::getValidationErrors();
            throw $e;
        }
        $this->sdoFactory->create($log);

        return $log;
    }

    /**
     * Read an log with its archive identifier
     * @param id $archiveId
     *
     * @return recordsManagement/log
     */
    public function read($archiveId) {
        return $this->sdoFactory->read("recordsManagement/log", $archiveId);
    }

    /**
     * Update an log
     * @param object $log log object
     *
     * @return boolean
     */
    public function update($log) {
        return false;
    }

    /**
     * Delete an log with its archive identifier
     * @param id $archiveId
     *
     * @return boolean
     */
    public function delete($archiveId) {
        return $this->sdoFactory->delete($archiveId, "recordsManagement/log");
    }

    /**
     * Get a form
     * @param Array $profileDescriptions Array of recordsManagement/profileDescription classe
     *
     * @return boolean
     */
    public function form($profileDescriptions) {
        return true;
    }

}
