<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle recordsManagement.
 *
 * Bundle recordsManagement is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle recordsManagement is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle recordsManagement.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\recordsManagement;
/**
 * Standard interface for log archive description class
 */
interface logInterface
{
    /**
     * @request READ recordsManagement/log
     */
    public function find();

    /**
     * Read the description associated with the archive
     * @param id $archiveId
     * 
     * @return recordsManagement/log
     * 
     * @request READ recordsManagement/log/([^\/]+)
     */
    public function read($archiveId);

    /**
     * Create the description of archive
     * @param recordsManagement/log $log
     * 
     * @request CREATE recordsManagement/log
     */
    public function create($log);

    /**
     * Update log : get start date for disposal
     * @param string $archiveId
     * 
     * @request UPDATE recordsManagement/log/([^\/]+)
     */
    public function update($archiveId);

    /**
     * Delete log. Delete archive
     * @param string $archiveId
     * 
     * @request DELETE recordsManagement/log/(.+)
     */
    public function delete($archiveId);
    
    /**
     * Get a form
     * @param Array $profileDescriptions Array of recordsManagement/profileDescription classe
     */
    public function form($profileDescriptions);
    
}