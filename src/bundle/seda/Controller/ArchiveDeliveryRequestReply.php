<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\seda\Controller;

/**
 * Class for DeliveryRequestReply message handling
 */
class ArchiveDeliveryRequestReply 
    extends abstractMessage
{

    /**
     * Send message with all contents embedded
     * @param string $message The message
     */
    public function send($message)
    {
        $this->message = $message;

        $archiveDeliveryRequestReply = abstractMessage::send($message);

        $archiveDeliveryRequestReply->replyCode = $this->sendReplyCode($message->replyCode);
       
        $archiveDeliveryRequestReply->deliveryRequestIdentifier = \laabs::newInstance('seda/ID', $message->requestReference);

        $archiveDeliveryRequestReply->deliveryRequestReplyIdentifier = \laabs::newInstance('seda/ID', $message->reference);

        $archiveDeliveryRequestReply->archivalAgency = $this->sendOrganization($message->recipientOrg);
        
        $archiveDeliveryRequestReply->requester = $this->sendOrganization($message->senderOrg);

        foreach ($message->archive as $archive) {
            $archiveDeliveryRequestReply->archive[] = $this->sendArchive($archive, $withAttachment = true);
        }

        $this->sendXml($message, $withData = true);

    }


}