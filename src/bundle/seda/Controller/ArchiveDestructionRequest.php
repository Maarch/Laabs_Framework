<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\seda\Controller;

/**
 * Class for ArchiveDestructionNotification message handling
 */
class ArchiveDestructionRequest
    extends abstractMessage
{
    /**
     * Send message with all contents embedded
     * @param string $message The message
     */
    public function send($message)
    {
        $archiveDestructionRequest = abstractMessage::send($message);

        $archiveDestructionRequest->destructionRequestIdentifier = \laabs::newInstance('seda/ID', $message->reference);
        $archiveDestructionRequest->destructionRequestIdentifier->schemeID = 'SIREN';

        $archiveDestructionRequest->archivalAgency = $this->sendOrganization($message->recipientOrg);

        $archiveDestructionRequest->requester = $this->sendOrganization($message->senderOrg);
                
        $message->unitIdentifier = $this->sdoFactory->readChildren('medona/unitIdentifier', $message);
        
        foreach ($message->unitIdentifier as $unitIdentifier) {
            $id = \laabs::newInstance('seda/ID');
            $id->schemeID = $unitIdentifier->objectId;
            $id->schemeName = $unitIdentifier->objectClass;
            $archiveDestructionRequest->unitIdentifier[] = $id;
        }
    }
}