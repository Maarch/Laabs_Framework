<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle seda.
 *
 * Bundle seda is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle seda is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle seda.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\seda\Controller;

/**
 * Class for ArchiveModificationNotification message handling
 * 
 * @author Alexandre Morin (Maarch) <alexandre.morin@maarch.org>
 */
class ArchiveModificationNotification 
    extends abstractMessage
{
    /**
     * Send message with all contents embedded
     * @param string $message The message
     */
    public function send($message)
    {
        $archiveModificationNotification = abstractMessage::send($message);
      
        $archiveModificationNotification->modificationNotificationIdentifier = \laabs::newInstance('seda/ID', $message->reference);

        $archiveModificationNotification->archivalAgency = $this->sendOrganization($message->senderOrg);

        $archiveModificationNotification->originatingAgency = $this->sendOrganization($message->recipientOrg);

        foreach ($message->archive as $archive) {
            $archiveModificationNotification->archive[] = $this->sendArchive($archive, $withAttachment = false);
        }

        $this->sendXml($message, $withData = false);
    }
}