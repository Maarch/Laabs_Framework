<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle seda.
 *
 * Bundle seda is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle seda is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle seda.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\seda\Controller;

/**
 * Class ArchiveTransfer
 *
 * @package Seda
 */
class ArchiveTransfer 
    extends abstractMessage
{
    public $droid;

    public $dfi;

    public $errors;
    public $replyCode;

    /**
     * Receive message with all contents embedded
     * @param string $message The message
     */
    public function receive($message)
    {
        $this->loadMessage($message);

        // Conformité au schéma SEDA
        $schemaFile = LAABS_BUNDLE.DIRECTORY_SEPARATOR.'seda'.DIRECTORY_SEPARATOR.LAABS_RESOURCE.DIRECTORY_SEPARATOR.'xsd'.DIRECTORY_SEPARATOR.'seda'.DIRECTORY_SEPARATOR.'seda_v1-0.xsd';

        libxml_use_internal_errors(true);
        $valid = $message->xml->schemaValidate($schemaFile);
        if ($valid == false) {
            $exception = \laabs::newException('seda/invalidMessageException', 'Bordereau non reçu : il ne respecte pas le standard SEDA 1.0.');
            $exception->errors = libxml_get_errors();

            libxml_clear_errors();
            libxml_use_internal_errors(false);

            throw $exception;
        }

        $message->object = $message->xml->export('seda/ArchiveTransfer');

        // Si conforme, récupération des informations de l'objet medona/messsage
        $message->date = $message->object->date;

        $message->senderOrgRegNumber = $message->object->transferringAgency->identification->value;

        $message->recipientOrgRegNumber = $message->object->archivalAgency->identification->value;

        $message->reference = $message->object->transferIdentifier->value;
        $message->archivalAgreementReference = $message->object->archive[0]->archivalAgreement->value;

        foreach ($message->object->archive as $archive) {
            $this->receiveArchiveObject($message, $archive);
        }

    }

    protected function receiveArchiveObject($message, $archiveObject)
    {
        if (isset($archiveObject->document)) {
            foreach ($archiveObject->document as $document) {
                $message->size += (integer) filesize($this->messageDirectory . DIRECTORY_SEPARATOR . $message->messageId . DIRECTORY_SEPARATOR . $document->attachment->filename);
                $message->dataObjectCount++;
                /*
                if (isset($document->size)) {
                    $message->size += (integer) $document->size->value;
                    $message->dataObjectCount++;
                }
                */
            }
        }
        if (isset($archiveObject->archiveObject)) {
            foreach ($archiveObject->archiveObject as $subObject) {
                $this->receiveArchiveObject($message, $subObject);
            }
        }
    }

    /**
     * Validate against profile
     * @param seda/message $message           The message object with the xml
     * @param object       $archivalAgreement The archival agreement
     *
     * @return boolean
     */
    public function validate($message, $archivalAgreement)
    {
        $this->errors = array();
        $this->replyCode = null;

        $this->loadMessage($message);

        $this->validateOriginators($message, $archivalAgreement);

        if ($archivalAgreement->signed && $this->message->xPath->query('seda:ArchiveTransfer/seda:NonRepudiation')->length == 0) {
            $this->sendError("309");
            //array_push($this->errors, new \core\Error("Le Bordereau n'est pas signé"));
        }

        // Validation du profil métier
        $this->validateProfile($this->message, $archivalAgreement);

        // Contrôle des documents attachés
        $this->validateAttachments($this->message, $archivalAgreement);

        return true;
    }

    protected function getOriginatorIdentifications($archiveObject)
    {
        $originatorIdentifications = array();

        if (isset($archiveObject->contentDescription) && isset($archiveObject->contentDescription->originatingAgency)) {
            foreach ($archiveObject->contentDescription->originatingAgency as $originatingAgency) {
                $originatorIdentifications[] = (string) $originatingAgency->identification;
            }
        }

        if (isset($archiveObject->archiveObject)) {
            foreach ($archiveObject->archiveObject as $subObject) {
                $originatorIdentifications = array_merge($originatorIdentifications, $this->getOriginatorIdentifications($subObject));
            }
        }

        return array_unique($originatorIdentifications);
    }

    protected function validateOriginators($message, $archivalAgreement)
    {
        // Contrôle du producteur
        $originatorIdentifications = array();
        foreach ($message->object->archive as $archive) {
            $originatorIdentifications = array_merge($originatorIdentifications, $this->getOriginatorIdentifications($archive));
        }

        $originatorIdentifications = array_unique($originatorIdentifications);

        //$originatorOrgRegNumberElements = $this->message->xPath->query('//seda:OriginatingAgency/seda:Identification');
        $originatorOrgs = array();

        foreach ($originatorIdentifications as $originatorIdentification) {
            //$originatorOrgRegNumber = $originatorOrgRegNumberElement->nodeValue;
            if (!isset($originatorOrgs[$originatorIdentification])) {
                try {
                    $originatorOrg = $this->orgController->getOrgByRegNumber($originatorIdentification);
                    $originatorOrgs[$originatorIdentification] = $originatorOrg;
                } catch (\Exception $e) {
                    $this->sendError("200", "Le producteur de l'archive identifié par '$originatorIdentification' n'est pas référencé dans le système.");

                    continue;
                }

                if (!in_array('originator', (array) $originatorOrg->orgRoleCodes)) {
                    $this->sendError("302", "Le service identifié par '$originatorIdentification' n'est pas référencé comme producteur dans le système.");
                }

                if (!in_array((string) $originatorOrg->orgId, (array) $archivalAgreement->originatorOrgIds)) {
                    $this->sendError("302", "Le producteur de l'archive identifié par '$originatorIdentification' n'est pas indiqué dans l'accord de versement.");
                }

            }
        }
    }


    protected function validateProfile($message, $archivalAgreement)
    {
        if (!isset($message->object->archive[0]->archivalProfile) || $message->object->archive[0]->archivalProfile->value == "") {
            $this->sendError("203");

            return;
        }

        $archivalProfileReference = $message->object->archive[0]->archivalProfile->value;

        if ($archivalProfileReference != $archivalAgreement->archivalProfileReference) {
            $this->sendError("203", "Le profil d'archivage identifié par '$archivalProfileReference' ne correspond pas à celui de l'accord de versement");

            return;
        }

        $profilesDirectory = $this->profilesDirectory;

        $errors = array();

        $profileFiles = glob($profilesDirectory.DIRECTORY_SEPARATOR.$archivalProfileReference.'.*');
        if (count($profileFiles) == 0) {
            $this->sendError("203", "Le profil d'archivage identifié par '$archivalProfileReference' n'est pas référencé dans le système.");

            return;
        }

        foreach ($profileFiles as $profileFile) {
            $extension = pathinfo($profileFile, \PATHINFO_EXTENSION);

            switch ($extension) {
                case 'rng':
                    //if ($validationMode == 'jing') {
                    if (true) {
                        $jing = \laabs::newService('dependency/xml/plugins/jing/jing');
                        $xmlFile = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $message->messageId.".xml";
                        $valid = $jing->validate($profileFile, $xmlFile);
                        if (!$valid) {
                            $this->sendError("203");

                            foreach ($jing->getErrors() as $errorMessage) {
                                $this->errors[] = new \core\Error($errorMessage);
                            }
                        }

                    } else {
                        libxml_use_internal_errors(true);
                        $valid = $message->xml->relaxNGValidate($profileFile);
                        if ($valid == false) {
                            $this->sendError("203");
                            foreach (libxml_get_errors() as $libxmlError) {
                                $this->errors[] = new \core\Error($libxmlError->message, null, $libxmlError->code);
                            }
                        }
                        libxml_clear_errors();
                        libxml_use_internal_errors(false);
                    }
                    break;

                case 'xsd':
                    libxml_use_internal_errors(true);
                    $valid = $message->xml->schemaValidate($profileFile);
                    if ($valid == false) {
                        foreach (libxml_get_errors() as $libxmlError) {
                            $this->errors[] = new \core\Error($libxmlError->message, null, $libxmlError->code);
                        }
                    }
                    libxml_clear_errors();
                    libxml_use_internal_errors(false);
                    break;

                default:
                    $this->sendError("203", "Le profil d'archivage identifié par '$archivalProfileReference' est dans un format non géné par le système.");
            }
        }
    }

    protected function validateAttachments($message, $archivalAgreement)
    {
        $serviceLevelController = \laabs::newController("recordsManagement/serviceLevel");
        $serviceLevel = $serviceLevelController->getByReference($archivalAgreement->serviceLevelReference);

        /*if (strpos($serviceLevel->control, 'virusCheck') !== false) {
            $clam = \laabs::newService('dependency/fileSystem/plugins/clam');

            $results = $clam->scan($this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId);

            foreach ($results as $filename => $result) {
                var_dump("$filename => $result");
                if ($result > 0) {
                    $this->sendError("310", "Le document nommé '".$filename."' comporte du code malveillant");

                    return false;
                }
            }
        }*/

        if (!isset($this->dfi)) {
            $this->dfi = \laabs::newService('dependency/fileSystem/plugins/dfi');
        }

        // Retrieve format information
        $dfimodules = 0;
        if (strpos($serviceLevel->control, 'formatDetection') !== false) {
            $dfimodules += DFI_USE_DROID;
        }
        if (strpos($serviceLevel->control, 'formatValidation') !== false) {
            $dfimodules += DFI_USE_JHOVE;
        }
        if (strpos($serviceLevel->control, 'formatCharacterization') !== false) {
            $dfimodules += DFI_USE_MEDIAINFO;
            // other modules ?
        }

        $messageDir = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId;
        $messageFiles = array($messageDir . DIRECTORY_SEPARATOR . (string) $message->messageId . '.xml');

        $receivedFiles = glob($messageDir.DIRECTORY_SEPARATOR."*.*");

        $allowedFormats = \laabs\explode(' ', $archivalAgreement->allowedFormats);

        $attachmentId = 1;
        $attachmentElements = $message->xPath->query(".//seda:Attachment");
        foreach ($attachmentElements as $attachmentElement) {
            $documentElement = $attachmentElement->parentNode;

            if ($attachmentElement->hasAttribute('filename')) {
                $filename = $attachmentElement->getAttribute('filename');
                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.$filename;

                if (!is_file($filepath)) {
                    $this->sendError("211", "Le document nommé '".$filename."' n'a pas été transmis.");

                    continue;
                }

                $messageFiles[] = $filepath;

                $contents = file_get_contents($filepath);
            } elseif ($attachmentElement->hasAttribute('uri')) {
                $uri = $attachmentElement->getAttribute('uri');
                $contents = file_get_contents($uri);

                if (!$contents) {
                    $this->sendError("211", "Le document à l'adresse '$uri' est indisponible.");

                    continue;
                }

                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $attachmentId;
                file_put_contents($filepath, $contents);
                $messageFiles[] = $filepath;
            } else {
                if (strlen($attachmentElement->nodeValue) == 0) {
                    $this->sendError("211", "Le contenu du document n'a pas été transmis.");

                    continue;
                }

                $contents = base64_decode($attachmentElement->nodeValue);

                if (!$contents) {
                    $this->sendError("211", "Le contenu du document n'a pas été décodé.");

                    continue;
                }

                $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $message->messageId.DIRECTORY_SEPARATOR.(string) $attachmentId;
                file_put_contents($filepath, $contents);
                $messageFiles[] = $filepath;
            }

            $dfiInfo = $this->dfi->getInfo($filepath, $dfimodules);
            file_put_contents($filepath . ".info", json_encode($dfiInfo, JSON_PRETTY_PRINT));
            $messageFiles[] = $filepath . ".info";

            if (strpos($serviceLevel->control, 'formatDetection') !== false) {
                if ($attachmentElement->hasAttribute('format')) {
                    $sentPuid = $attachmentElement->getAttribute('format');
                } else {
                    $sentPuid = null;
                }

                if (!$dfiInfo->format->puid) {
                    $this->sendError("205", "Le format du document '".basename($filepath)."' n'a pas pu être détecté");

                } else {
                    $puid = $dfiInfo->format->puid;
                    if ($sentPuid && $puid != $sentPuid) {
                        $this->sendError("205", "Le format du document '".basename($filepath)."' ($puid) ne correspond pas à celui indiqué (".$sentPuid.").");
                    }
                }
            } else {
                if ($attachmentElement->hasAttribute('format')) {
                    $puid = $attachmentElement->getAttribute('format');
                } else {
                    $puid = null;
                }
            }

            if (strpos($serviceLevel->control, 'formatValidation') !== false) {
                if (!isset($dfiInfo->status) || $dfiInfo->status->code != 0) {
                    $this->sendError("204", "Le format du document '".basename($filepath)."' n'est pas conforme au standard $module : ".$dfiInfo->status->info);
                }
            }

            if (count($allowedFormats) && isset($puid) && !in_array($puid, $allowedFormats)) {
                $this->sendError("307", "Le format du document '".basename($filepath)."' (".$dfiInfo->format->puid.") n'est pas autorisé par l'accord de versement.");
            }

            // Validation de l'empreinte
            $integrityElement = $message->xPath->query("seda:Integrity", $documentElement)->item(0);
            if ($integrityElement) {
                $hash = $integrityElement->nodeValue;
                $algorithme = $integrityElement->getAttribute('algorithme');
                $hashAlgorithm = substr($algorithme, strrpos($algorithme, '#')+1);

                if ($hash != hash($hashAlgorithm, $contents)) {
                    $this->sendError("207", "L'empreinte numérique du document '".basename($filepath)."' ne correspond pas à celle transmise.");
                }
            }

            $attachmentId++;
        }

        // Check all files received are part of the message
        foreach ($receivedFiles as $receivedFile) {
            if (!in_array($receivedFile, $messageFiles) && $receivedFile != $message->xml->uri) {
                $this->sendError("101", "Le fichier '".basename($receivedFile)."' ,'est pas référencé dans le bordereau.");

                return;
            }
        }
    }

    /**
     * Extract description from Xml
     * @param seda/message $message The message object with the xml
     *
     * @return recordsManagement/archive
     */
    public function process($message)
    {
        $this->loadMessage($message);

        foreach ($message->object->archive as $archive) {
            $message->archive[] = $this->processArchive($message, $archive);
        }
    }

    /* **********************************************************************************
    ** 
    **  PROCESS XML MESSAGES -> rm objects
    **
    ********************************************************************************** */
    protected function processArchive($message, $sedaArchive)
    {
        $archive = \laabs::newInstance('recordsManagement/archive');
        $archive->archiveId = $sedaArchive->id;

        $archive->archiverOrgRegNumber = $message->recipientOrgRegNumber;
        $archive->depositorOrgRegNumber = $message->senderOrgRegNumber;
        $archive->archivalAgreementReference = $message->archivalAgreementReference;

        $archive->archivalProfileReference = $sedaArchive->archivalProfile->value;

        if ($sedaArchive->serviceLevel) {
            $archive->serviceLevelReference = $sedaArchive->serviceLevel[0]->value;
        } else {
            $archivalAgreementController = \laabs::newController("medona/archivalAgreement");
            $archivalAgreement = $archivalAgreementController->getByReference($message->archivalAgreementReference);
            $archive->serviceLevelReference = $archivalAgreement->serviceLevelReference;
        }

        $archive->archiveName = $sedaArchive->name;

        if (isset($sedaArchive->appraisalRule)) {
            $this->processAppraisalRule($archive, $sedaArchive->appraisalRule);
        }

        if (isset($sedaArchive->accessRestrictionRule)) {
            $this->processAccessRestrictionRule($archive, $sedaArchive->accessRestrictionRule);
        }

        if ($sedaArchive->contentDescription) {
            $contentDescription = $this->processContentDescription($archive, $sedaArchive->contentDescription);

            if (isset($sedaArchive->archivalAgencyArchiveIdentifier)) {
                $contentDescription->archiverArchiveId = (string) $sedaArchive->archivalAgencyArchiveIdentifier->value;
            }
            if (isset($sedaArchive->originatingAgencyArchiveIdentifier)) {
                $contentDescription->originatorArchiveId = (string) $sedaArchive->originatingAgencyArchiveIdentifier->value;
            }
            if (isset($sedaArchive->transferringAgencyArchiveIdentifier)) {
                $contentDescription->depositorArchiveId = (string) $sedaArchive->transferringAgencyArchiveIdentifier->value;
            }
            // Récupératon du producteur
            if (isset($sedaArchive->contentDescription->originatingAgency)) {
                $archive->originatorOrgRegNumber = (string) $sedaArchive->contentDescription->originatingAgency[0]->identification->value;
            }

            $archive->descriptionObject = $contentDescription;
        }

        if (isset($sedaArchive->document)) {
            foreach ($sedaArchive->document as $sedaDocument) {
                $document = $this->processDocument($sedaDocument);

                $document->archiveId = $archive->archiveId;

                $archive->document[] = $document;
            }
        }

        if (isset($sedaArchive->archiveObject)) {
            foreach ($sedaArchive->archiveObject as $sedaArchiveObject) {
                $archiveObject = $this->processArchiveObject($sedaArchiveObject, $archive);

                $archiveObject->parentArchiveId = $archive->archiveId;

                $archive->contents[] = $archiveObject;
            }
        }

        //$this->processRelatedObjectReferences($archive, $sedaArchive);

        return $archive;
    }

    protected function processArchiveObject($sedaArchiveObject, $parentArchive)
    {
        $archive = \laabs::newInstance('recordsManagement/archive');
        $archive->archiveId = $sedaArchiveObject->id;

        $archive->archiveName = $sedaArchiveObject->name;

        if (isset($sedaArchiveObject->appraisalRule)) {
            $this->processAppraisalRule($archive, $sedaArchiveObject->appraisalRule);
        }

        if (isset($sedaArchiveObject->accessRestrictionRule)) {
            $this->processAccessRestrictionRule($archive, $sedaArchiveObject->accessRestrictionRule);
        }

        if ($sedaArchiveObject->contentDescription) {
            $contentDescription = $this->processContentDescription($archive, $sedaArchiveObject->contentDescription);

            if (isset($sedaArchiveObject->archivalAgencyObjectIdentifier)) {
                $contentDescription->archiverArchiveId = (string) $sedaArchiveObject->archivalAgencyObjectIdentifier->value;
            }

            if (isset($sedaArchiveObject->originatingAgencyObjectIdentifier)) {
                $contentDescription->originatorArchiveId = (string) $sedaArchiveObject->originatingAgencyObjectIdentifier->value;
            }

            if (isset($sedaArchiveObject->transferringAgencyObjectIdentifier)) {
                $contentDescription->depositorArchiveId = (string) $sedaArchiveObject->transferringAgencyObjectIdentifier->value;
            }

            // Récupératon du producteur
            if (isset($sedaArchiveObject->contentDescription->originatingAgency)) {
                $archive->originatorOrgRegNumber = (string) $sedaArchiveObject->contentDescription->originatingAgency[0]->identification->value;
            }

            $archive->descriptionObject = $contentDescription;
        }

        if (isset($sedaArchiveObject->document)) {
            foreach ($sedaArchiveObject->document as $sedaDocument) {
                $document = $this->processDocument($sedaDocument);

                $document->archiveId = $archive->archiveId;

                $archive->document[] = $document;
            }
        }

        $this->managementRuleInheritance($archive, $parentArchive);

        if (isset($sedaArchiveObject->archiveObject)) {
            foreach ($sedaArchiveObject->archiveObject as $sedaSubObject) {
                $contentArchive = $this->processArchiveObject($sedaSubObject, $archive);

                $contentArchive->parentArchiveId = $archive->archiveId;

                $archive->contents[] = $contentArchive;
            }
        }

        //$this->processRelatedObjectReferences($archive, $sedaArchive);

        return $archive;
    }


    protected function processContentDescription($archive, $sedaContentDescription)
    {
        $contentDescription = \laabs::newInstance('archivesPubliques/contentDescription');
        $contentDescription->contentDescriptionId = $archive->archiveId;

        $archive->descriptionClass = 'archivesPubliques/contentDescription';
        $archive->descriptionId = $archive->archiveId;
        $archive->descriptionObject = $contentDescription;

        $contentDescription->description = (string) $sedaContentDescription->description;
        $contentDescription->descriptionLevel = (string) $sedaContentDescription->descriptionLevel;
        $contentDescription->latestDate = \laabs::cast((string) $sedaContentDescription->latestDate, 'date');
        $contentDescription->oldestDate = \laabs::cast((string) $sedaContentDescription->oldestDate, 'date');

        if (isset($sedaContentDescription->filePlanPosition)) {
            $contentDescription->filePlanPosition = \laabs::newTokenList();
            foreach ($sedaContentDescription->filePlanPosition as $sedaFilePlanPosition) {
                $contentDescription->filePlanPosition[] = (string) $sedaFilePlanPosition->value;
            }
        }

        if (isset($sedaContentDescription->keyword)) {
            foreach ($sedaContentDescription->keyword as $sedaKeyword) {
                $keyword = $this->processKeyword($sedaKeyword);

                $keyword->contentDescriptionId = $contentDescription->contentDescriptionId;

                $contentDescription->keyword[] = $keyword;
            }
        }

        if (isset($sedaContentDescription->custodialHistory)) {
            foreach ($sedaContentDescription->custodialHistory->custodialHistoryItem as $sedaCustodialHistoryItem) {
                $custodialHistoryItem = $this->processCustodialHistoryItem($sedaCustodialHistoryItem);

                $custodialHistoryItem->contentDescriptionId = $contentDescription->contentDescriptionId;

                $contentDescription->custodialHistory[] = $custodialHistoryItem;
            }
        }

        if (isset($sedaContentDescription->accessRestrictionRule)) {
            $this->processAccessRestrictionRule($contentDescription, $sedaContentDescription->accessRestrictionRule);
        }

        return $contentDescription;
    }

    protected function processKeyword($sedaKeyword)
    {
        $keyword = \laabs::newInstance('archivesPubliques/keyword');

        $keyword->content = (string) $sedaKeyword->keywordContent->value;

        if (isset($sedaKeyword->keywordReference)) {
            $keyword->reference = (string) $sedaKeyword->keywordReference->value;
        }

        $keyword->type = (string) $sedaKeyword->keywordType;

        if (isset($sedaKeyword->accessRestrictionRule)) {
            $this->processAccessRestrictionRule($keyword, $sedaKeyword->accessRestrictionRule);
        }

        return $keyword;
    }

    protected function processCustodialHistoryItem($sedaCustodialHistoryItem)
    {
        $custodialHistory = \laabs::newInstance('archivesPubliques/custodialHistory');

        $custodialHistory->item = trim((string) $sedaCustodialHistoryItem->value);
        if (isset($sedaCustodialHistoryItem->when)) {
            $custodialHistory->when = \laabs::newDate($sedaCustodialHistoryItem->when);
        }

        return $custodialHistory;
    }

    protected function processAccessRestrictionRule($object, $sedaAccessRestrictionRule)
    {
        if (isset($sedaAccessRestrictionRule->code)) {
            $object->accessRuleCode = (string) $sedaAccessRestrictionRule->code;
        }

        if (isset($sedaAccessRestrictionRule->startDate)) {
            $object->accessRuleStartDate = \laabs::cast((string) $sedaAccessRestrictionRule->startDate, 'datetime');
        }
    }

    protected function processAppraisalRule($object, $sedaAppraisalRule)
    {
        if (isset($sedaAppraisalRule->code)) {
            switch ((string) $sedaAppraisalRule->code) {
                case 'conserver':
                    $object->finalDisposition = 'preservation';
                    break;

                case 'detruire':
                    $object->finalDisposition = 'destruction';
                    break;
            }
        }

        if (isset($sedaAppraisalRule->startDate)) {
            $object->retentionStartDate = \laabs::cast((string) $sedaAppraisalRule->startDate, 'datetime');
        }

        if (isset($sedaAppraisalRule->duration)) {
            $object->retentionDuration = \laabs::cast((string) $sedaAppraisalRule->duration, 'duration');
        }
    }

    protected function processDocument($sedaDocument)
    {
        $document = \laabs::newInstance('documentManagement/document');

        $document->docId = $sedaDocument->id;

        $document->type = (string) $sedaDocument->type[0]->value;
        if (isset($sedaDocument->control)) {
            $document->control = (string) $sedaDocument->control[0]->value;
        }

        switch ($sedaDocument->copy) {
            case 'true':
            case '1':
                $document->copy = true;
                break;

            default:
                $document->copy = false;
        }

        if (isset($sedaDocument->status)) {
            $document->status = (string) $sedaDocument->status->value;
        }

        //$apDocument = \laabs::newInstance('archivesPubliques/document');
        //$apDocument->docId = $document->docId;

        //$document->descriptionClass = 'archivesPubliques/document';
        //$document->descriptionId = $document->docId;
        //$document->descriptionObject = $apDocument;

        /*if (isset($sedaDocument->archivalAgencyDocumentIdentifier)) {
            $apDocument->archiverDocId = (string) $sedaDocument->archivalAgencyDocumentIdentifier->value;
        }*/
        if (isset($sedaDocument->originatingAgencyDocumentIdentifier)) {
            $document->originatorDocId = (string) $sedaDocument->originatingAgencyDocumentIdentifier->value;
        }
        if (isset($sedaDocument->transferringAgencyDocumentIdentifier)) {
            $document->depositorDocId = (string) $sedaDocument->transferringAgencyDocumentIdentifier->value;
        }
        $document->creation = $sedaDocument->creation;
        $document->description = (string) $sedaDocument->description;
        $document->issue = $sedaDocument->issue;
        $document->language = (string) $sedaDocument->language[0];
        $document->purpose = (string) $sedaDocument->purpose;
        $document->receipt = $sedaDocument->receipt;
        $document->response = $sedaDocument->response;
        $document->submission = $sedaDocument->submission;

        $resource = \laabs::newInstance('digitalResource/digitalResource');
        $resource->resId = $document->resId = $sedaDocument->attachment->id;


        $document->digitalResource = $resource;

        if (isset($sedaDocument->integrity)) {
            $resource->hash = (string) $sedaDocument->integrity->value;
            $resource->hashAlgorithm = substr($sedaDocument->integrity->algorithme, strrpos($sedaDocument->integrity->algorithme, "#")+1);
        }

        if (isset($sedaDocument->attachment)) {
            $this->processBinaryObject($resource, $sedaDocument->attachment);
        }

        if (isset($sedaDocument->size)) {
            $resource->size = (string) $sedaDocument->size->value;
        } else {
            $resource->size = strlen($resource->getContents());
        }

        return $document;
    }


    protected function processBinaryObject($resource, $sedaBinaryObject)
    {
        $resource->fileName = $sedaBinaryObject->filename;
        $resource->mimetype = $sedaBinaryObject->mimeCode;
        $resource->puid = $sedaBinaryObject->format;

        // Get contents
        if (isset($resource->fileName)) {
            $filepath = $this->messageDirectory.DIRECTORY_SEPARATOR.(string) $this->message->messageId.DIRECTORY_SEPARATOR.$resource->fileName;
            $resource->fileExtension = pathinfo($resource->fileName, \PATHINFO_EXTENSION);

            $contents = file_get_contents($filepath);
        } else {
            $contents = $sedaBinaryObject->value;
        }

        $resource->setContents($contents);

        if (isset($sedaBinaryObject->relatedData)) {
            foreach ($sedaBinaryObject->relatedData as $sedaRelatedData) {
                $relatedResource = $this->processRelatedData($sedaRelatedData);
            }
        }
    }

    protected function processRelatedData($sedaRelatedData)
    {
        $resource = \laabs::newInstance('digitalResource/digitalResource');
        $resource->resId = \laabs::newId();

        $resRelationship = \laabs::newInstance('digitalResource/resRelationship');
        $resRelationship->resId = $parent->resId;
        $resRelationship->relatedResId = $resource->resId;
        $resRelationship->typeCode = (string) $sedaRelatedData->relation->value;

        $this->resRelationships[] = $resRelationship;

        $sedaData = $sedaRelatedData->data;
        $this->processBinaryObject($resource, $sedaData);

        $this->digitalResources[] = $resource;
    }

    protected function managementRuleInheritance($archive, $parentArchive)
    {
        if (!isset($archive->retentionDuration) && !isset($archive->finalDisposition)) {
            $archive->retentionDuration = $parentArchive->retentionDuration;
            $archive->retentionStartDate = $parentArchive->retentionStartDate;
            $archive->finalDisposition = $parentArchive->finalDisposition;
        }

        if (!isset($archive->accessRuleCode)) {
            $archive->accessRuleCode = $parentArchive->accessRuleCode;
            $archive->accessRuleStartDate = $parentArchive->accessRuleStartDate;
            $archive->accessRuleDuration = $parentArchive->accessRuleDuration;
        }

        if (!isset($archive->archivalProfileReference)) {
            $archive->archivalProfileReference = $parentArchive->archivalProfileReference;
        }

        if (!isset($archive->serviceLevelReference)) {
            $archive->serviceLevelReference = $parentArchive->serviceLevelReference;
        }

        if (!isset($archive->originatorOrgRegNumber)) {
            $archive->originatorOrgRegNumber = $parentArchive->originatorOrgRegNumber;
        }

        $archive->depositorOrgRegNumber = $parentArchive->depositorOrgRegNumber;

        $archive->archiverOrgRegNumber = $parentArchive->archiverOrgRegNumber;
    }

    protected function processRelatedObjectReferences($parent, $parentElement)
    {
        $relatedObjectReferenceElements = $this->message->xPath->query('seda:RelatedObjectReference', $parentElement);

        for ($i = 0, $l = $relatedObjectReferenceElements->length; $i < $l; $i++) {
            $relatedObjectReferenceElement = $relatedObjectReferenceElements->item($i);

            $archiveRelationship = \laabs::newInstance('recordsManagement/archiveRelationship');
            $archiveRelationship->archiveId = $parent->archiveId;
            $archiveRelationship->relatedArchiveId = $this->message->xPath->query('seda:RelatedObjectIdentifier', $parentElement)->item(0)->nodeValue;
            $archiveRelationship->typeCode = $this->message->xPath->query('seda:Relation', $parentElement)->item(0)->nodeValue;

            $this->archiveRelationships[] = $archiveRelationship;
        }
    }
}
