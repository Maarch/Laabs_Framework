<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle medona.
 *
 * Bundle medona is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle medona is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle medona.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\seda\Controller;

/**
 * Class for ArchiveTransferReply message handling
 */
class ArchiveTransferReply 
    extends abstractMessage
{

    /**
     * Generate a new archive delivery request reply
     * @param medona/message $message
     */
    public function send($message)
    {

        $archiveTransferReply = abstractMessage::send($message);

        $archiveTransferReply->replyCode = $this->sendReplyCode($message->replyCode);
       
        $archiveTransferReply->transferIdentifier = \laabs::newInstance('seda/ID', $message->requestReference);

        $archiveTransferReply->transferReplyIdentifier = \laabs::newInstance('seda/ID', $message->reference);

        $archiveTransferReply->grantDate = $message->operationDate;

        $archiveTransferReply->archivalAgency = $this->sendOrganization($message->senderOrg);

        $archiveTransferReply->transferringAgency = $this->sendOrganization($message->recipientOrg);

    }

}
