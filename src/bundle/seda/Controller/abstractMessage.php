<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle seda.
 *
 * Bundle seda is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle seda is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle seda.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\seda\Controller;
/**
 * Class abstractMessage
 * 
 * @package Seda
 * 
 */
abstract class abstractMessage
{
    protected $archivalAgreements = array();

    protected $currentArchivalAgreement;

    protected $archives;
    
    protected $digitalResources;
    
    protected $archiveRelationships;
    
    protected $resRelationships;
    
    protected $message;
    
    protected $messageDirectory;
    
    protected $profilesDirectory;
    
    protected $orgController;
    
    protected $sdoFactory;

    protected $xml;
    
    protected $xPath;
    
    protected $jhove;

    /**
     * Constructor
     * @param string $messageDirectory
     * @param string $profilesDirectory
     */
    public function __construct($messageDirectory, $profilesDirectory, \dependency\sdo\Factory $sdoFactory)
    {
        $this->messageDirectory = $messageDirectory;
        $this->profilesDirectory = $profilesDirectory;
        $this->sdoFactory = $sdoFactory;

        $this->orgController = \laabs::newController('organization/organization');
        $this->archiveController = \laabs::newController('recordsManagement/archive');
        $this->lifeCyclejournalController = \laabs::newController('lifeCycle/journal');
    }

    /**
     * Load a message
     * @param seda/message $message 
     */
    
    public function loadMessage($message)
    {
        $this->message = $message;

        if (isset($message->xPath)) {
            $message->xPath->registerNamespace('seda', $message->xml->documentElement->namespaceURI);
        }
    }

    protected function useArchivalAgreement($archivalAgreementReference)
    {
        $archivalAgreementController = \laabs::newController('medona/archivalAgreement');

        if (!isset($this->archivalAgreements[$archivalAgreementReference])) {
            $this->currentArchivalAgreement = $archivalAgreementController->getByReference($archivalAgreementReference);

            $this->archivalAgreements[$archivalAgreementReference] = $this->currentArchivalAgreement;
        } else {
            $this->currentArchivalAgreement = $this->archivalAgreements[$archivalAgreementReference];
        }

        return $this->currentArchivalAgreement;
    }

    
    /**
     * Get an attacheent resource from a message
     * @param medona/message $message      The message
     * @param string         $attachmentId The attachment identifier
     *
     * @return The resource
     */
    public function getAttachment($message, $attachmentId)
    {
        foreach ($message->object->archive as $archive) {
            if ($attachment = $this->findAttachment($attachmentId, $archive)) {
                break;
            }
        }

        if (!$attachment) {
            return false;
        }

        $resource = \laabs::newInstance('digitalResource/digitalResource');

        switch(true) {
            case isset($attachment->filename) :
                $filepath = $this->messageDirectory . DIRECTORY_SEPARATOR . (string) $message->messageId . DIRECTORY_SEPARATOR . $attachment->filename;
                $contents = file_get_contents($filepath);

                $resource->fileExtension = pathinfo($attachment->filename, \PATHINFO_EXTENSION);
                $resource->fileName = basename($attachment->filename);
                break;

            case isset($attachment->uri) :
                $contents = file_get_contents($attachment->uri);
                break;

            case isset($attachment->value):
                $contents = base64_decode($attachment->value);
                break;

            default:
                return false;
        }

        $finfo = new \finfo(\FILEINFO_MIME_TYPE);
        $resource->mimetype = $finfo->buffer($contents);

        $resource->setContents($contents);

        return $resource;
    }

    protected function findAttachment($attachmentId, $archive)
    {
        if (isset($archive->document)) {
            foreach ($archive->document as $document) {
                if (isset($document->attachment) && $document->attachment->id == $attachmentId) {
                    return $document->attachment;
                }
            }
        }

        if (isset($archive->archiveObject)) {
            foreach ($archive->archiveObject as $archiveObject) {
                if ($attachment = $this->findAttachment($attachmentId, $archiveObject)) {
                    return $attachment;
                }
            }
        }
    }


    /* **********************************************************************************
    ** 
    **  SEND XML MESSAGES
    **
    ********************************************************************************** */
    protected function send($message)
    {
        $sedaMessage = \laabs::newInstance('seda/' . $message->type);

        $message->object = $sedaMessage;

        $sedaMessage->id = (string) $message->messageId;

        $sedaMessage->comment = $message->comment;

        $sedaMessage->date = $message->date;

        return $sedaMessage;
    }

    protected function sendReplyCode($code)
    {
        $replyCode = \laabs::newInstance('seda/Code', $code);

        $replyCode->name = $this->getReplyMessage($code);

        return $replyCode;
    }

    protected function sendError($code, $message=false)
    {
        if ($message) {
            array_push($this->errors, new \core\Error($message, null, $code));
        } else {
            array_push($this->errors, new \core\Error($this->getReplyMessage($code), null, $code));
        }

        if ($this->replyCode == null) {
            $this->replyCode = $code;
        }
    }

    protected function getReplyMessage($code)
    {
        switch ((string) $code) {
            case "000":
                $name = "OK";
                break;
            case "001":
                $name = "OK (consulter les commentaires pour information)";
                break;
            case "002":
                $name = "OK (Demande aux autorités de contrôle effectuée)";         
                break;
                
            case "101":
                $name = "Message mal formé.";
                break;
            case "102":
                $name = "Système momentanément indisponible.";
                break;
                
            case "200":
                $name = "Service producteur non reconnu.";
                break;
            case "201":
                $name = "Service d'archive non reconnu.";
                break;
            case "202":
                $name = "Service versant non reconnu.";
                break;
            case "203":
                $name = "Dépôt non conforme au profil de données.";
                break;
            case "204":
                $name = "Format de document non géré.";
                break;
            case "205":
                $name = "Format de document non conforme au format déclaré.";
                break;
            case "206":
                $name = "Signature du message invalide.";
                break;
            case "207":
                $name = "Empreinte(s) invalide(s).";
                break;
            case "208":
                $name = "Archive indisponible. Délai de communication non écoulé.";
                break;
            case "209":
                $name = "Archive absente (élimination, restitution, transfert)";
                break;
            case "210":
                $name = "Archive inconnue";
                break;
            case "211":
                $name = "Pièce attachée absente.";
                break;
            case "212":
                $name = "Dérogation refusée.";
                break;
                
            case "300":
                $name = "Convention invalide.";
                break;
            case "301":
                $name = "Dépôt non conforme à la convention. Quota des versements dépassé.";
                break;
            case "302":
                $name = "Dépôt non conforme à la convention. Identifiant du producteur non conforme.";
                break;
            case "303":
                $name = "Dépôt non conforme à la convention. Identifiant du service versant non conforme.";
                break;
            case "304":
                $name = "Dépôt non conforme à la convention. Identifiant du service d'archives non conforme.";
                break;
            case "305":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) absente(s).";
                break;
            case "306":
                $name = "Dépôt non conforme à la convention. Volume non conforme.";
                break;
            case "307":
                $name = "Dépôt non conforme à la convention. Format non conforme.";
                break;
            case "308":
                $name = "Dépôt non conforme à la convention. Empreinte(s) non transmise(s).";
                break;
            case "309":
                $name = "Dépôt non conforme à la convention. Absence de signature du message.";
                break;
            case "310":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) non valide(s).";
                break;
            case "311":
                $name = "Dépôt non conforme à la convention. Signature(s) de document(s) non vérifiée(s).";
                break;
            case "312":
                $name = "Dépôt non conforme à la convention. Dates de début ou de fin non respectées."; 
                break;

            default:
                $name = null;
        }

        return $name;
    }


    protected function sendOrganization($orgOrganization)
    {
        $organization = \laabs::newInstance('seda/Organization');
        $organization->id = (string) $orgOrganization->orgId;
        $organization->identification = \laabs::newInstance('seda/ID', $orgOrganization->registrationNumber);
        $organization->name = $orgOrganization->orgName;

        if (isset($orgOrganization->address)) {
            foreach ($orgOrganization->address as $address) {
                $organization->address[] = $this->sendAddress($address);
            }
        }

        if (isset($orgOrganization->communication)) {
            foreach ($orgOrganization->communication as $communication) {
                $organization->communication[] = $this->sendCommunication($communication);
            }
        }

        if (isset($orgOrganization->contact)) {
            foreach ($orgOrganization->contact as $contact) {    
                $organization->contact[] = $this->sendContact($contact);
            }
        }

        return $organization;
    }

    protected function sendAddress($orgAddress)
    {
        $address = \laabs::newInstance('seda/Address');
        $address->id = (string) $orgAddress->addressId;
        $address->blockName = $orgAddress->block;
        $address->buildingName= $orgAddress->building;
        $address->buildingNumber = $orgAddress->number;
        $address->cityName = $orgAddress->city;
        $address->citySubDivisionName = $orgAddress->citySubDivision;
        $address->country = $orgAddress->country;
        $address->floorIdentification = $orgAddress->floor;
        $address->postCode = $orgAddress->postCode;
        $address->postOfficeBox = $orgAddress->postBox;
        $address->roomIdentification = $orgAddress->room;
        $address->streetName = $orgAddress->street;
        
        return $address;
    }
    
    protected function sendCommunication($orgCommunication)
    {
        $communication = \laabs::newInstance('seda/Communication');
        $communication->id = (string) $orgCommunication->communicationId;
        $communication->channel = $orgCommunication->comMeanCode;

        switch ($orgCommunication->comMeanCode) {
            case 'EM':
            case 'FTP':
                $communication->URIID = $orgCommunication->value;
                break;

            default:
                $communication->completeNumber = $orgCommunication->value;
        }
                
        return $communication;
    }
    
    protected function sendContact($orgContact)
    {
        $contact = \laabs::newInstance('seda/Contact');
        $contact->id = (string) $orgContact->contactId;
        $contact->departmentName = $orgContact->service;
        $contact->identification= $orgContact->contactId;
        $contact->personName = $orgContact->displayName;
        $contact->responsibility = $orgContact->function;
        
        if (isset($orgContact->address)) {
            foreach ($orgContact->address as $address) {
                $contact->address[] = $this->sendAddress($address);
            }
        }
        
        if (isset($orgContact->communication)) {
            foreach ($orgContact->communication as $communication) {
                $contact->communication[] = $this->sendCommunication($communication);
            }
        }
        
        return $contact;
    }   
    
    protected function sendArchive($rmArchive, $withAttachment = true)
    {
        $archive = \laabs::newInstance('seda/Archive');
        $archive->id = (string) $rmArchive->archiveId;

        $archive->archivalAgencyArchiveIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->archiveId);
        $archive->archivalAgreement = \laabs::newInstance('seda/ID', (string) $rmArchive->archivalAgreementReference);
        $archive->archivalProfile = \laabs::newInstance('seda/ID', (string) $rmArchive->archivalProfileReference);

        if (isset($rmArchive->archiveName)) {
            $archive->name = (string) $rmArchive->archiveName;
        }

        if (isset($rmArchive->descriptionLanguage)) {
            foreach ((array) $rmArchive->descriptionLanguage as $descriptionLanguage) {
                $archive->descriptionLanguage[] = (string) $descriptionLanguage;
            }
        }
        
        if (isset($rmArchive->serviceLevelReference)) {
            $archive->serviceLevel = array();
            $archive->serviceLevel[] = \laabs::newInstance('seda/ID', (string) $rmArchive->serviceLevelReference);
        }
        
        // contentDescription object
        if (isset($rmArchive->descriptionObject)) {
            $archive->contentDescription = $this->sendContentDescription($rmArchive->descriptionObject);

            if (isset($rmArchive->descriptionObject->originatorArchiveId)) {
                $archive->originatingAgencyArchiveIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->descriptionObject->originatorArchiveId);
            }
            if (isset($rmArchive->descriptionObject->depositorArchiveId)) {
                $archive->transferringAgencyArchiveIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->descriptionObject->depositorArchiveId);
            }
        }      

        $archive->accessRestrictionRule = $this->sendAccessRestrictionRule($rmArchive);
        
        //Appraisal Rule
        if (isset($rmArchive->appraisalRule)) {
            $archive->appraisalRule = $this->sendAppraisalRule($rmArchive);
        }
        
        // Documents
        if (isset($rmArchive->document)) {
            foreach ($rmArchive->document as $document) {
                $archive->document[] = $this->sendDocument($document, $withAttachment);
            }
        }

        // Archive objects 
        if (isset($rmArchive->contents)) {
            foreach ($rmArchive->contents as $content) {
                $archive->archiveObject[] = $this->sendArchiveObject($content, $withAttachment);
            }
        }

        return $archive;
    }

    protected function sendAccessRestrictionRule($rmArchive)
    {
        $accessRestrictionRule = \laabs::newInstance('seda/AccessRestrictionRule');
        $accessRestrictionRule->code = $rmArchive->accessRuleCode;
        $accessRestrictionRule->startDate = $rmArchive->accessRuleStartDate;
        
        return $accessRestrictionRule;
    }
    
    protected function sendAppraisalRule($rmArchive)
    {
        $appraisalRule = \laabs::newInstance('seda/AppraisalRule');
        $appraisalRule->code = $rmArchive->retentionRuleCode;
        $appraisalRule->duration = $rmArchive->retentionDuration;
        $appraisalRule->startDate = $rmArchive->retentionStartDate;
        
        return $appraisalRule;
    }
    
    protected function sendArchiveObject($rmArchive, $withAttachment)
    {
        $archiveObject = \laabs::newInstance('seda/ArchiveObject');
        $archiveObject->id = (string) $rmArchive->archiveId;
        $archiveObject->archivalAgencyObjectIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->archiveId);
        
        if (isset($rmArchive->archiveName)) {
            $archiveObject->name = (string) $rmArchive->archiveName;
        }

        if (isset($rmArchive->descriptionLanguage)) {
            foreach ($descriptionObject->language as $language) {
                $contentDescription->language[] = (string) $language;
            }
        }
        
        if (isset($rmArchive->descriptionObject)) {
            $archiveObject->contentDescription = $this->sendContentDescription($rmArchive->descriptionObject);

            if (isset($rmArchive->descriptionObject->originatorArchiveId)) {
                $archiveObject->orginatingAgencyObjectIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->descriptionObject->originatorArchiveId);
            }
            if (isset($rmArchive->descriptionObject->depositorArchiveId)) {
                $archiveObject->transferringAgencyObjectIdentifier = \laabs::newInstance('seda/ID', (string) $rmArchive->descriptionObject->depositorArchiveId);
            }
        }
        
        $archiveObject->accessRestrictionRule = $this->sendAccessRestrictionRule($rmArchive);
        
        $archiveObject->appraisalRule = $this->sendAppraisalRule($rmArchive);

        
        if (isset($rmArchive->contents)) {
            foreach ($rmArchive->contents as $content) {
                $archiveObject->archiveObject[] = $this->sendArchiveObject($content, $withAttachment);
            }
        }
        
        if (isset($rmArchive->document)) {
            foreach ($rmArchive->document as $dmDocument) {
                $archiveObject->document[] = $this->sendDocument($dmDocument, $withAttachment);
            }
        }

        return $archiveObject;
    }

    protected function sendContentDescription($descriptionObject)
    {
        $contentDescription = \laabs::newInstance('seda/ContentDescription');

        if (isset($descriptionObject->description)) {
            $contentDescription->description = (string) $descriptionObject->description;
        }

        $contentDescription->descriptionLevel = $descriptionObject->descriptionLevel;
        
        if (isset($descriptionObject->filePlanPosition)) {
            $contentDescription->filePlanPosition = array();
            foreach ($descriptionObject->filePlanPosition as $filePlanPosition) {
                $contentDescription->filePlanPosition[] = \laabs::newInstance('seda/ID', (string) $filePlanPosition);
            }
        }
               
        $contentDescription->latestDate = $descriptionObject->latestDate;
        $contentDescription->oldestDate = $descriptionObject->oldestDate;
        //$contentDescription->otherDescriptiveData = $descriptionObject->otherDescriptiveData;
        $contentDescription->accessRestrictionRule = $this->sendAccessRestrictionRule($descriptionObject);
        $contentDescription->custodialHistory = $this->sendCustodialHistory($descriptionObject->custodialHistory);
        
        if (isset($descriptionObject->keyword)) {
            $contentDescription->keyword = array();
            foreach ($descriptionObject->keyword as $apKeyword) {
                $keyword = \laabs::newInstance('seda/Keyword');
                $contentDescription->keyword[] = $keyword;

                $keyword->keywordContent = \laabs::newInstance('seda/KeywordContent', (string) $apKeyword->content);
                $keyword->keywordType = (string) $apKeyword->type;

                if (isset($apKeyword->reference)) {
                    $keyword->keywordReference = \laabs::newInstance('seda/ID', (string) $apKeyword->reference);
                }
            }
        }
        
        if (isset($descriptionObject->originatingAgency)) {
            $contentDescription->originatingAgency = array();
            foreach ($descriptionObject->originatingAgency as $originatingAgency) {
                $contentDescription->originatingAgency[] = $this->sendOrganization($originatingAgency);
            }
        }
        
        //TODO OtherMetadata
        if (isset($descriptionObject->relatedObjectReference)) {
            $contentDescription->relatedObjectReference = array();
            foreach ($descriptionObject->relatedObjectReference as $relatedObjectReference) {
                $contentDescription->relatedObjectReference[] = $this->sendRelatedObjectReference($relatedObjectReference);
            }
        }
        
        if (isset($descriptionObject->repository)) {
            $contentDescription->repository = $this->sendOrganization($descriptionObject->repository);
        }
    
        return $contentDescription;
    }
    
    protected function sendCustodialHistory($apCustodialHistory) 
    {
        $custodialHistory = \laabs::newInstance('seda/CustodialHistory');
        $custodialHistory->custodialHistoryItem = array();
        
        foreach ($apCustodialHistory as $apCustodialHistoryItem) {
            $custodialHistoryItem = \laabs::newInstance('seda/CustodialHistoryItem', (string) $apCustodialHistoryItem->item, (string) $apCustodialHistoryItem->when);
            $custodialHistory->custodialHistoryItem[] = $custodialHistoryItem;
        }
        
        return $custodialHistory;
    }
    
    protected function sendRelatedObjectReference($relatedObjectReference)
    {
        $relatedObject = \laabs::newInstance('seda/RelatedObjectReference');
        $relatedObject->relatedObjectIdentifier = \laabs::newInstance('seda/ID', (string) $relatedObjectReference->identifier);
        
        return $relatedObject;
    }
    
    protected function sendDocument($dmDocument, $withAttachment)
    {
        $document = \laabs::newInstance('seda/Document');
        $document->docId = $dmDocument->docId;
        
        $document->archivalAgencyDocumentIdentifier = \laabs::newInstance('seda/ID', (string) $dmDocument->docId);

        if ($withAttachment && isset($dmDocument->digitalResource)) {
            $document->attachment = $this->sendAttachment($dmDocument->digitalResource);
        }

        $drResource = $dmDocument->digitalResource;
        
        if (isset($dmDocument->control)) {
            $document->control = array();
            foreach ($dmDocument->control as $control) {
                $document->control = \laabs::newInstance('seda/Code', (string) $control);
            }
        }
        
        $document->copy = $dmDocument->copy;
        
        $document->integrity = \laabs::newInstance('seda/Integrity', $drResource->hash, $drResource->hashAlgorithm);

        $document->type = \laabs::newInstance('seda/Code', (string) $dmDocument->type);
        $document->size = \laabs::newInstance('seda/Measure', (string) $drResource->size, 'A99');
        
        if (isset($dmDocument->status)) {
            $document->status = \laabs::newInstance('seda/Code', (string) $dmDocument->status);
        }

        if (isset($dmDocument->descriptionObject)) {
            $document->description = (string) $dmDocument->descriptionObject->description;
            $document->language = (string) $dmDocument->descriptionObject->language;
            $document->originatingAgencyDocumentIdentifier = \laabs::newInstance('seda/ID', (string) $dmDocument->descriptionObject->originatorDocId);
            $document->purpose = (string) $dmDocument->descriptionObject->purpose;
            
            $document->issue = \laabs::cast($dmDocument->descriptionObject->issue, 'date');
            $document->receipt = \laabs::cast($dmDocument->descriptionObject->receipt, 'date');
            $document->response = \laabs::cast($dmDocument->descriptionObject->response, 'date');            
            $document->submission = \laabs::cast($dmDocument->descriptionObject->submission, 'date');
            $document->creation = \laabs::cast($dmDocument->descriptionObject->creation, 'date');

            if (isset($dmDocument->descriptionObject->depositorDocId)) {
                $document->transferringAgencyDocumentIdentifier = \laabs::newInstance('seda/ID', (string) $dmDocument->descriptionObject->depositorDocId);
            }
            if (isset($dmDocument->descriptionObject->originatorDocId)) {
                $document->originatingAgencyDocumentIdentifier = \laabs::newInstance('seda/ID', (string) $dmDocument->descriptionObject->originatorDocId);
            }
            
            
        
            if (isset($dmDocument->relatedData)) {
                $document->relatedData = array();
                foreach ($dmDocument->relatedData as $relatedData) {
                    $document->relatedData[] = $relatedData;
                }
            }
        }

        return $document;
    }

    protected function sendAttachment($drResource)
    {
        $attachment = \laabs::newInstance('seda/ArchiveBinaryObject'); 

        $attachment->filename = (string) $drResource->resId;
        $attachment->format = $drResource->puid;
        $attachment->id = (string) $drResource->resId;
        // format, size, mimeCode, mimeEncoding

        return $attachment;
    }

    protected function sendXml($message, $withData)
    {
        $message->xml = \laabs::newService('dependency/xml/Document');
        $message->xml->loadResource('seda/'.$message->type.'.xml');
        $message->xml->setSource($message->type, $message->object);
        $message->xml->merge();
        
        // Documents
        if ($withData && isset($message->archive)) {
            foreach ($message->archive as $rmArchive) {
                $this->sendArchiveData($rmArchive, $message);
            }
        }

        if (!is_dir($this->messageDirectory . DIRECTORY_SEPARATOR . $message->messageId)) {
            mkdir($this->messageDirectory . DIRECTORY_SEPARATOR . $message->messageId, 0777, true);
        }

        file_put_contents($this->messageDirectory . DIRECTORY_SEPARATOR . $message->messageId . DIRECTORY_SEPARATOR . $message->messageId . '.xml', $message->xml->saveXml());
    }

    protected function sendArchiveData($rmArchive, $message)
    {
        $outdir = $this->messageDirectory . DIRECTORY_SEPARATOR . $message->messageId;

        if (isset($rmArchive->document)) {

            foreach ($rmArchive->document as $dmDocument) {
                if (isset($dmDocument->digitalResource)) {
                    $drResource = $dmDocument->digitalResource;
                    if ($contents = $drResource->getContents()) {
                        $filename = $drResource->resId;

                        file_put_contents($outdir . DIRECTORY_SEPARATOR . $filename, $contents);
                    }
                }
            }
        }

        // Archive objects 
        if (isset($rmArchive->contents)) {
            foreach ($rmArchive->contents as $content) {
                $this->sendArchiveData($content, $message);
            }
        }
    }

    protected function sendRequest($authorizationRequestContent)
    {
        $request = \laabs::newInstance('seda/Request');

        $request->authorisationReason = $authorizationRequestContent->authorizationReason;
        $request->requestDate = $authorizationRequestContent->requestDate;

        foreach ($authorizationRequestContent->unitIdentifier as $unitIdentifier) {
            $id = \laabs::newInstance('seda/ID');
            $id->schemeID = $unitIdentifier->objectId;
            $id->schemeName = $unitIdentifier->objectClass;
            $request->unitIdentifier[] = $id;
        }

        $request->requester = $this->sendOrganization($authorizationRequestContent->requester);

        return $request;
    }
}
