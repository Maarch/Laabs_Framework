<?php
namespace bundle\seda\Model;
/**
 * The abstract for all business message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AbstractBusinessMessage
{
    /**
     * The message id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * The comments
     *
     * @var string[]
     * @xpath seda:Comment
     */
    public $comment;

    /**
     * The date
     *
     * @var datetime
     * @xpath seda:Date
     */
    public $date;
}
