<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AccessRestrictionRule
{
    /**
     * @var string
     * @xpath seda:Code
     */
    public $code;

    /**
     * @var date
     * @xpath seda:StartDate
     */
    public $startDate;
}