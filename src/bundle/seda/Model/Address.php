<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Address
{
    /**
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var string
     * @xpath seda:BlockName
     */
    public $blockName;

    /**
     * @var string
     * @xpath seda:BuildingName
     */
    public $buildingName;

    /**
     * @var string
     * @xpath seda:BuildingNumber
     */
    public $buildingNumber;

    /**
     * @var string
     * @xpath seda:CityName
     */
    public $cityName;

    /**
     * @var string
     * @xpath seda:CitySub-DivisionName
     */
    public $citySubDivisionName;

    /**
     * @var string
     * @xpath seda:Country
     */
    public $country;

    /**
     * @var string
     * @xpath seda:FloorIdentification
     */
    public $floorIdentification;

    /**
     * @var string
     * @xpath seda:Postcode
     */
    public $postCode;

    /**
     * @var string
     * @xpath seda:PostOfficeBox
     */
    public $postOfficeBox;

    /**
     * @var string
     * @xpath seda:RoomIdentification
     */
    public $roomIdentification;

    /**
     * @var string
     * @xpath seda:StreetName
     */
    public $streetName;

}
