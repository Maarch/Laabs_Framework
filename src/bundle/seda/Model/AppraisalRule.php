<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AppraisalRule
{
    
    /**
     * @var string
     * @xpath seda:Code
     */
    public $code;

    /**
     * @var duration
     * @xpath seda:Duration
     */
    public $duration;
    
    /**
     * @var date
     * @xpath seda:StartDate
     */
    public $startDate;
}