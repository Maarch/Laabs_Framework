<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Archive
{
    /**
     * The archive id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;
    
    /**
     * @var string
     * @xpath seda:Name
     */
    public $name;

    /**
     * @var seda/ID
     * @xpath seda:ArchivalAgencyArchiveIdentifier
     */
    public $archivalAgencyArchiveIdentifier;
    
    /**
     * @var seda/ID
     * @xpath seda:ArchivalAgreement
     */
    public $archivalAgreement;
    
    /**
     * @var seda/ID
     * @xpath seda:ArchivalProfile
     */
    public $archivalProfile;
    
    /**
     * @var string[]
     * @xpath seda:DescriptionLanguage
     */
    public $descriptionLanguage;
    
    /**
     * @var seda/ID
     * @xpath seda:OriginatingAgencyArchiveIdentifier
     */
    public $originatingAgencyArchiveIdentifier;
    
    /**
     * @var seda/ID[]
     * @xpath seda:ServiceLevel
     */
    public $serviceLevel;
    
    /**
     * @var seda/ID
     * @xpath seda:TransferringAgencyArchiveIdentifier
     */
    public $transferringAgencyArchiveIdentifier;
    
    /**
     * @var seda/ContentDescription
     * @xpath seda:ContentDescription
     */
    public $contentDescription;
    
    /**
     * @var seda/AccessRestrictionRule
     * @xpath seda:AccessRestrictionRule
     */
    public $accessRestrictionRule;
    
    /**
     * @var seda/AppraisalRule
     * @xpath seda:AppraisalRule
     */
    public $appraisalRule;
    
    /**
     * @var seda/ArchiveObject[]
     * @xpath seda:ArchiveObject
     */
    public $archiveObject;
    
    /**
     * @var seda/Document[]
     * @xpath seda:Document
     */
    public $document;
}
