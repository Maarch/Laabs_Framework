<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveBinaryObject
{
    /**
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var string
     * @xpath text()
     */
    public $value;

    /**
     * @var string
     * @xpath @format
     */
    public $format;

    /**
     * @var string
     * @xpath @mimeCode
     */
    public $mimeCode;

    /**
     * @var string
     * @xpath @encodingCode
     */
    public $encodingCode;

    /**
     * @var string
     * @xpath @characterSetCode
     */
    public $characterSetCode;

    /**
     * @var string
     * @xpath @listAgencyName
     */
    public $listAgencyName;

    /**
     * @var string
     * @xpath @uri
     */
    public $uri;

    /**
     * @var string
     * @xpath @filename
     */
    public $filename;

    /**
     * Contructor
     * @param string $value
     */
    public function __construct($value=null)
    {
        $this->value = $value;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
