<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveDeliveryRequest
    extends AbstractBusinessMessage
{
    /**
     * @var seda/ID
     * @xpath seda:DeliveryRequestIdentifier
     */
    public $deliveryRequestIdentifier;

    /**
     * @var boolean
     * @xpath seda:Derogation
     */
    public $derogation;

    /**
     * @var seda/ID[]
     * @xpath seda:UnitIdentifier
     */
    public $unitIdentifier;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:Requester
     */
    public $requester;
}
