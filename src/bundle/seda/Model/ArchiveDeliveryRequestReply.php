<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveDeliveryRequestReply
    extends AbstractBusinessMessage
{

    /**
     * @var seda/ID
     * @xpath seda:AuthorizationRequestReplyIdentifier
     */
    public $authorizationRequestReplyIdentifier;

    /**
     * @var seda/ID
     * @xpath seda:DeliveryRequestIdentifier
     */
    public $deliveryRequestIdentifier;

    /**
     * @var seda/ID
     * @xpath seda:DeliveryRequestReplyIdentifier
     */
    public $deliveryRequestReplyIdentifier;

    /**
     * @var seda/Code
     * @xpath seda:ReplyCode
     */
    public $replyCode;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:Requester
     */
    public $requester;

    /**
     * @var seda/Archive[]
     * @xpath seda:Archive
     */
    public $archive;
}
