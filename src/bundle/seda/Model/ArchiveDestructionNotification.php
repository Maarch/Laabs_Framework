<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveDestructionNotification
    extends AbstractBusinessMessage
{
    /**
     * @var seda/ID
     * @xpath seda:AuthorizationRequestReplyIdentifier
     */
    public $authorizationRequestReplyIdentifier;

    /**
     * @var seda/ID
     * @xpath seda:DestructionNotificationIdentifier
     */
    public $destructionNotificationIdentifier;

    /**
     * @var seda/ID[]
     * @xpath seda:UnitIdentifier
     */
    public $unitIdentifier;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:OriginatingAgency
     */
    public $originatingAgency;
}
