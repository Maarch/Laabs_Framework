<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveObject
{
    /**
     * The archive id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;
    
    /**
     * @var seda/ID
     * @xpath seda:ArchivalAgencyObjectIdentifier
     */
    public $archivalAgencyObjectIdentifier;

    /**
     * @var string
     * @xpath seda:Name
     */
    public $name;
    
    /**
     * @var seda/ID
     * @xpath seda:OriginatingAgencyObjectIdentifier
     */
    public $originatingAgencyObjectIdentifier;
    
    /**
     * @var seda/ID
     * @xpath seda:TransferringAgencyObjectIdentifier
     */
    public $transferringAgencyObjectIdentifier;
    
    /**
     * @var seda/ContentDescription
     * @xpath seda:ContentDescription
     */
    public $contentDescription;
    
    /**
     * @var seda/AccessRestrictionRule
     * @xpath seda:AccessRestrictionRule
     */
    public $accessRestrictionRule;
    
    /**
     * @var seda/AppraisalRule
     * @xpath seda:AppraisalRule
     */
    public $appraisalRule;
    
    /**
     * @var seda/ArchiveObject[]
     * @xpath seda:ArchiveObject
     */
    public $archiveObject;
    
    /**
     * @var seda/Document[]
     * @xpath seda:Document
     */
    public $document;
}