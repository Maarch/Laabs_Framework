<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveRestitution
    extends AbstractBusinessMessage
{
    /**
     * @var seda/ID
     * @xpath seda:RestitutionIdentifier
     */
    public $restitutionIdentifier;
    
    /**
     * @var seda/ID
     * @xpath seda:RestitutionRequestReplyIdentifier
     */
    public $restitutionRequestReplyIdentifier;

    /**
     * @var seda/ID[]
     * @xpath seda:UnitIdentifier
     */
    public $unitIdentifier;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:OriginatingAgency
     */
    public $originatingAgency;
    
    /**
     * @var seda/Archive[]
     * @xpath seda:Archive
     */
    public $archive;
}
