<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ArchiveTransferReply
    extends AbstractBusinessMessage
{

    /**
     * @var seda/ID
     * @xpath seda:TransferIdentifier
     */
    public $transferIdentifier;

    /**
     * @var seda/ID
     * @xpath seda:TransferReplyIdentifier
     */
    public $transferReplyIdentifier;

    /**
     * @var seda/Code
     * @xpath seda:ReplyCode
     */
    public $replyCode;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:TransferringAgency
     */
    public $transferringAgency;

    /**
     * @var seda/Archive[]
     * @xpath seda:Archive
     */
    public $archive;
}
