<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AuthorizationControlAuthorityRequest
    extends AbstractBusinessMessage
{

    /**
     * @var seda/ID
     * @xpath seda:AuthorizationControlAuthorityRequestIdentifier
     */
    public $authorizationControlAuthorityRequestIdentifier;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:ControlAuthority
     */
    public $controlAuthority;

    /**
     * @var seda/OriginatingAgencyAuthorization
     * @xpath seda:OriginatingAgencyAuthorization
     */
    public $originatingAgencyAuthorization;
    
    /**
     * @var seda/Request
     * @xpath seda:Request
     */
    public $request;
    
    /**
     * @var seda/Archive[]
     * @xpath seda:Archive
     */
    public $archive;
}
