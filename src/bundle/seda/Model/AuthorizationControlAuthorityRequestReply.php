<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AuthorizationControlAuthorityRequestReply
    extends AbstractBusinessMessage
{

    /**
     * @var seda/ID
     * @xpath seda:AuthorizationControlAuthorityRequestIdentifier
     */
    public $authorizationControlAuthorityRequestIdentifier;
    
    /**
     * @var seda/ID
     * @xpath seda:AuthorizationControlAuthorityRequestReplyIdentifier
     */
    public $authorizationControlAuthorityRequestReplyIdentifier;
    
    /**
     * @var seda/Code
     * @xpath seda:ReplyCode
     */
    public $replyCode;

    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:ControlAuthority
     */
    public $controlAuthority;
}
