<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class AuthorizationOriginatingAgencyRequestReply
    extends AbstractBusinessMessage
{

    /**
     * @var seda/ID
     * @xpath seda:AuthorizationOriginatingAgencyRequestIdentifier
     */
    public $authorizationOriginatingAgencyRequestIdentifier;
    
    /**
     * @var seda/ID
     * @xpath seda:AuthorizationOriginatingAgencyRequestReplyIdentifier
     */
    public $authorizationOriginatingAgencyRequestReplyIdentifier;
    
    /**
     * @var seda/Code
     * @xpath seda:ReplyCode
     */
    public $replyCode;
    
    /**
     * @var seda/Organization
     * @xpath seda:ArchivalAgency
     */
    public $archivalAgency;

    /**
     * @var seda/Organization
     * @xpath seda:OriginatingAgency
     */
    public $originatingAgency;
    
}
