<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Code
{
    /**
     * @var string
     * @xpath text()
     */
    public $value;

    /**
     * @var string
     * @xpath @name
     */
    public $name;

    /**
     * @var string
     * @xpath @listID
     */
    public $listID;

    /**
     * @var string
     * @xpath @listName
     */
    public $listName;

    /**
     * @var string
     * @xpath @listAgencyID
     */
    public $listAgencyID;

    /**
     * @var string
     * @xpath @listAgencyName
     */
    public $listAgencyName;

    /**
     * @var string
     * @xpath @listVersionID
     */
    public $listVersionID;

    /**
     * @var string
     * @xpath @listDataURI
     */
    public $listSchemaURI;

    /**
     * @var string
     * @xpath @listURI
     */
    public $listURI;

    /**
     * Contructor
     * @param string $value
     * @param string $name
     */
    public function __construct($value, $name=null)
    {
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
