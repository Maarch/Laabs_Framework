<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Communication
{
    /**
     * The communication id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var string
     * @xpath seda:Channel
     */
    public $channel;

    /**
     * @var string
     * @xpath seda:CompleteNumber
     */
    public $completeNumber;

    /**
     * @var string
     * @xpath seda:URIID
     */
    public $URIID;

}
