<?php
namespace bundle\seda\Model;
/**
 * Class model
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Contact
{
    /**
     * The contact id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var string
     * @xpath seda:DepartmentName
     */
    public $departmentName;

    /**
     * @var seda/ID
     * @xpath seda:Identification
     */
    public $identification;

    /**
     * @var string
     * @xpath seda:PersonName
     */
    public $personName;

    /**
     * @var string
     * @xpath seda:Responsibility
     */
    public $responsibility;

    /**
     * @var seda/Address[]
     * @xpath seda:Address
     */
    public $address;

    /**
     * @var seda/Communication[]
     * @xpath seda:Communication
     */
    public $communication;

}
