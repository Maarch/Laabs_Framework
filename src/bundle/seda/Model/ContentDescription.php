<?php
namespace bundle\seda\Model;
/**
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ContentDescription
{
    /**
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var string
     * @xpath seda:Description
     */
    public $description;

    /**
     *
     * @var string
     * @xpath seda:DescriptionLevel
     */
    public $descriptionLevel;

    /**
     * @var seda/ID[]
     * @xpath seda:FilePlanPosition
     */
    public $filePlanPosition;

    /**
     * @var string[]
     * @xpath seda:Language
     */
    public $language;

    /**
     * @var date
     * @xpath seda:LatestDate
     */
    public $latestDate;

    /**
     * @var date
     * @xpath seda:OldestDate
     */
    public $oldestDate;
    
    /**
     * @var string
     * @xpath seda:OtherDescriptiveData
     */
    public $otherDescriptiveData;
    
    /**
     * @var seda/AccessRestrictionRule
     * @xpath seda:AccessRestrictionRule
     */
    public $accessRestrictionRule;
    
    /**
     * @var seda/CustodialHistory
     * @xpath seda:CustodialHistory
     */
    public $custodialHistory;
    
    /**
     * @var seda/Keyword[]
     * @xpath seda:Keyword
     */
    public $keyword;
    
    /**
     * @var seda/Organization[]
     * @xpath seda:OriginatingAgency
     */
    public $originatingAgency;
    
    /**
     * @var seda/OtherMetadata[]
     * @xpath seda:OtherMetadata
     */
    public $otherMetadata;
    
    /**
     * @var seda/RelatedObjectReference[]
     * @xpath seda:RelatedObjectReference
     */
    public $relatedObjectReference;
    
    /**
     * @var seda/Organization
     * @xpath seda:Repository
     */
    public $repository;
}
