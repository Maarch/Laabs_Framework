<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class CustodialHistoryItem
{
    /**
     * @var string
     * @xpath text()
     */
    public $value;
    
    /**
     * @var string
     * @xpath @when
     */
    public $when;

    /**
     * Constructor
     * @param string $value
     * @param string $when
     */
    public function __construct($value, $when=null)
    {
        $this->value = $value;
        $this->when = $when;
    }

    /**
     * String value
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}