<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Document
{
    /**
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * @var seda/ID
     * @xpath seda:ArchivalAgencyDocumentIdentifier
     */
    public $archivalAgencyDocumentIdentifier;

    /**
     * @var seda/ArchiveBinaryObject
     * @xpath seda:Attachment
     */
    public $attachment;
    
    /**
     * @var seda/Code[]
     * @xpath seda:Control
     */
    public $control;
    
    /**
     * @var boolean
     * @xpath seda:Copy
     */
    public $copy;
    
    /**
     * @var date
     * @xpath seda:Creation
     */
    public $creation;
    
    /**
     * @var string
     * @xpath seda:Description
     */
    public $description;
    
    /**
     * @var seda/Integrity
     * @xpath seda:Integrity
     */
    public $integrity;
    
    /**
     * @var date
     * @xpath seda:Issue
     */
    public $issue;
    
    /**
     * @var string
     * @xpath seda:Language
     */
    public $language;
    
    /**
     * @var seda/ID
     * @xpath seda:OriginatingAgencyDocumentIdentifier
     */
    public $originatingAgencyDocumentIdentifier;
    
    /**
     * @var string
     * @xpath seda:Purpose
     */
    public $purpose;
    
    /**
     * @var date
     * @xpath seda:Receipt
     */
    public $receipt;
    
    /**
     * @var date
     * @xpath seda:Response
     */
    public $response;
    
    /**
     * @var seda/Measure
     * @xpath seda:Size
     */
    public $size;
    
    /**
     * @var seda/Code
     * @xpath seda:Status
     */
    public $status;
    
    /**
     * @var date
     * @xpath seda:Submission
     */
    public $submission;
    
    /**
     * @var seda/ID
     * @xpath seda:TransferringAgencyDocumentIdentifier
     */
    public $transferringAgencyDocumentIdentifier;
    
    /**
     * @var seda/Code[]
     * @xpath seda:Type
     */
    public $type;
    
    /**
     * @var string
     * @xpath seda:OtherMetadata
     */
    public $otherMetadata;
    
    /**
     * @var seda/RelatedData
     * @xpath seda:RelatedData
     */
    public $relatedData;
}