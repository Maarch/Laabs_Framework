<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class ID
{
    /**
     * @var string
     * @xpath text()
     */
    public $value;

    /**
     * @var string
     * @xpath @schemeID
     */
    public $schemeID;

    /**
     * @var string
     * @xpath @schemeName
     */
    public $schemeName;

    /**
     * @var string
     * @xpath @schemeAgencyID
     */
    public $schemeAgencyID;

    /**
     * @var string
     * @xpath @schemeAgencyName
     */
    public $schemeAgencyName;

    /**
     * @var string
     * @xpath @schemeVersionID
     */
    public $schemeVersionID;

    /**
     * @var string
     * @xpath @schemeDataURI
     */
    public $schemeDataURI;

    /**
     * @var string
     * @xpath @schemeURI
     */
    public $schemeURI;

    /**
     * Contructor
     * @param string $value
     */
    public function __construct($value = null)
    {
        if ($value != null) {
            $value = \laabs::newId();
        }
        
        $this->value = $value;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
