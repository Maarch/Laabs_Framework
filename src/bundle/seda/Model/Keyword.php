<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Keyword
{
    /**
     * @var string
     * @xpath @id
     */
    public $id;

    /**
     * @var seda/KeywordContent
     * @xpath seda:KeywordContent
     */
    public $keywordContent;

    /**
     * @var seda/ID
     * @xpath seda:keywordReference
     */
    public $keywordReference;
    
    /**
     * @var string
     * @xpath seda:KeywordType
     */
    public $keywordType;
    
    /**
     * @var seda/AccessRestrictionRule
     * @xpath seda:AccessRestrictionRule
     */
    public $accessRestrictionRule;
}