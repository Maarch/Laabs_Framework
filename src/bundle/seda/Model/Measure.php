<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Measure
{
    /**
     * @var string
     * @xpath text()
     */
    public $value;

    /**
     * @var string
     * @xpath @unitCode
     */
    public $unitCode;

    /**
     * Contructor
     * @param string $value
     * @param string $unitCode
     */
    public function __construct($value, $unitCode)
    {
        $this->value = $value;
        $this->unitCode = $unitCode;
    }

    /**
     * Get string
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
