<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Organization
{
    /**
     * The org id
     *
     * @var id
     * @xvalue generate-id
     */
    public $id;

    /**
     * The org role
     *
     * @var id
     * @xvalue local-name
     */
    public $orgRoleCode;

    /**
     * @var seda/Code
     * @xpath seda:BusinessType
     */
    public $businessType;

    /**
     * @var string
     * @xpath seda:Description
     */
    public $description;

    /**
     * @var seda/ID
     * @xpath seda:Identification
     */
    public $identification;
    
    /**
     * @var seda/Code
     * @xpath seda:LegalClassification
     */
    public $legalClassification;

    /**
     * @var string
     * @xpath seda:Name
     */
    public $name;

    /**
     * @var seda/Address[]
     * @xpath seda:Address
     */
    public $address;

    /**
     * @var seda/Communication[]
     * @xpath seda:Communication
     */
    public $communication;

    /**
     * @var seda/Contact[]
     * @xpath seda:Contact
     */
    public $contact;

}
