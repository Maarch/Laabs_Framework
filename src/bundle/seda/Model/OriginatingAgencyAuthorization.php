<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class OriginatingAgencyAuthorization
{

    /**
     * @var boolean
     * @xpath seda:Answer
     */
    public $answer;

    /**
     * @var seda/ID
     * @xpath seda:AuthorizationOriginatingAgencyRequestReplyIdentifier
     */
    public $authorizationOriginatingAgencyRequestReplyIdentifier;

    /**
     * @var string
     * @xpath seda:Comment
     */
    public $comment;
    
    /**
     * @var seda/Organization
     * @xpath seda:OrignatingAgency
     */
    public $orignatingAgency;
}
