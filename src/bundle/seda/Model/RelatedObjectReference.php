<?php
namespace bundle\seda\Model;
/**
 * Class model that represents a digital resource format 
 *
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class RelatedObjectReference
{
    /**
     * @var string
     * @xpath seda:Id
     */
    public $id;

    /**
     * @var seda/ID
     * @xpath seda:RelatedObjectIdentifier
     */
    public $relatedObjectIdentifier;
    
    /**
     * @var seda/Code
     * @xpath seda:Relation
     */
    public $relation;
}