<?php
namespace bundle\seda\Model;
/**
 * The archive transfer message
 * 
 * @package Seda
 * @author  Cyril VAZQUEZ (Maarch) <cyril.vazquez@maarch.org>
 * 
 * @xmlns seda fr:gouv:culture:archivesdefrance:seda:v1.0
 * 
 */
class Request
    extends AbstractBusinessMessage
{

    /**
     * @var string
     * @xpath seda:AuthorisationReason
     */
    public $authorisationReason;
    
    /**
     * @var date
     * @xpath seda:RequestDate
     */
    public $requestDate;

    /**
     * @var seda/ID[]
     * @xpath seda:UnitIdentifier
     */
    public $unitIdentifier;   
    
    /**
     * @var seda/Organization
     * @xpath seda:Requester
     */
    public $requester;
}
