﻿-- Schema: seda
DROP SCHEMA IF EXISTS "seda" CASCADE;
CREATE SCHEMA "seda";

-- Table: "seda"."bordereau"
DROP TABLE IF EXISTS "seda"."bordereau" CASCADE;
CREATE TABLE "seda"."bordereau"
(
  "bordereauId" text PRIMARY KEY,
  "messageId" text,
  "depositDate" timestamp,
  "validationDate" timestamp,
  "type" text,
  "reference" text,
  "description" text,
  "archiver" text,
  "depositor" text,
  "fileName" text NOT NULL,
  "files" text
)
WITH (
  OIDS=FALSE
);
